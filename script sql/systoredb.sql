-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-08-2020 a las 01:23:42
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `systoredb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `idArticulo` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `precioUnitario` float DEFAULT NULL,
  `costo` float DEFAULT NULL,
  `stock` int(10) DEFAULT NULL,
  `stockMin` int(10) DEFAULT NULL,
  `Rubro_idRubro` int(11) NOT NULL,
  `Marca_idMarca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`idArticulo`, `descripcion`, `precioUnitario`, `costo`, `stock`, `stockMin`, `Rubro_idRubro`, `Marca_idMarca`) VALUES
(11, 'Galletitas sabor chocolate 250 grs', 30, 25, 1, 5, 8, 30),
(12, 'Aceite de girasol 1.5 lts', 79, 65, 3, 10, 20, 158),
(14, 'Arroz largo fino 500 grs', 16, 12, 5, 5, 2, 160),
(15, 'Azucar tipo A 1 kgr', 20, 14, 29, 5, 2, 161),
(16, 'Fideos cortos/largos 500 grs', 27.99, 22, 13, 5, 2, 162),
(17, 'Harina de trigo 1 kg', 27.99, 22, 26, 5, 2, 159),
(18, 'Caballa 380 grs', 64.99, 55, 13, 5, 21, 164),
(19, 'Pure de tomate ', 19.99, 15, 25, 10, 2, 7),
(20, 'Mayonesa 475 grs', 39.99, 32, 15, 4, 2, 165),
(22, 'Yerba compuesta 1 kg', 79.99, 70, 24, 10, 2, 166),
(23, 'Yerba compuesta 1 kg', 79.99, 70, 20, 10, 2, 167),
(24, 'Yerba compuesta 1 kg', 79.99, 70, 20, 10, 2, 168),
(25, 'Galletas ', 23.99, 18, 10, 4, 8, 169),
(26, 'Jabon liquido 3 lts', 169, 155, 15, 3, 18, 170),
(27, 'Shampoo 400 ml', 109.99, 100, 12, 2, 18, 171),
(28, 'Acondicionador 400 ml', 109.99, 100, 12, 2, 18, 171),
(29, 'Detergente cristal 750 ml', 84.99, 77.99, 10, 3, 18, 172),
(30, 'Pasta dental total clean 90 grs', 49, 40, 25, 4, 18, 33),
(31, 'Papel higienico ', 9, 6, 45, 10, 18, 47),
(32, 'Jabon de tocador', 29.99, 22, 20, 5, 18, 45),
(33, 'Gaseosa 3 lts', 64.99, 58, 23, 6, 19, 104),
(35, 'Gaseosa 3 lts', 64.99, 58.5, 20, 6, 19, 176),
(36, 'Gaseosa 3L', 70, 60, 30, 6, 19, 31),
(37, 'Café modilo 250 gr', 40, 34, 12, 4, 2, 178);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE `banco` (
  `idBanco` int(11) NOT NULL,
  `razonSocial` varchar(80) DEFAULT NULL,
  `cuentaBancaria` varchar(30) DEFAULT NULL,
  `Empleado_idEmpleado` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banco`
--

INSERT INTO `banco` (`idBanco`, `razonSocial`, `cuentaBancaria`, `Empleado_idEmpleado`) VALUES
(3, 'BANCO EMPLEADO MESSI', 'CUENTA EMPLEADO MESSI', 1),
(4, 'Banco 1', 'Cuenta 1', 2),
(5, 'BANCO', 'CB', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nroCUIT` varchar(25) DEFAULT NULL,
  `observacion` varchar(140) DEFAULT NULL,
  `Persona_idPersona` int(11) NOT NULL,
  `TipoDeCUIT_idTipoDeCUIT` int(11) NOT NULL,
  `domicilio` varchar(100) DEFAULT NULL,
  `pais` varchar(35) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `codigoPostal` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nroCUIT`, `observacion`, `Persona_idPersona`, `TipoDeCUIT_idTipoDeCUIT`, `domicilio`, `pais`, `provincia`, `localidad`, `codigoPostal`) VALUES
(1, '56-48745123-1', '\nNinguna', 2, 2, 'Santa Fe 350', 'Argentina', 'Tucumán', 'San Miguel de Tucumán', 4000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `idCompra` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `total` double DEFAULT NULL,
  `Proveedor_idProveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`idCompra`, `fecha`, `total`, `Proveedor_idProveedor`) VALUES
(1, '2019-03-07', 1265, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concepto`
--

CREATE TABLE `concepto` (
  `idConcepto` int(10) NOT NULL,
  `abreviatura` varchar(20) DEFAULT NULL,
  `descripcion` varchar(40) DEFAULT NULL,
  `estadoConcepto` int(1) DEFAULT NULL,
  `esRemunerativo` int(1) DEFAULT NULL,
  `tipoDeConcepto` varchar(15) DEFAULT NULL,
  `cantidadPorcentual` double DEFAULT NULL,
  `cantidadFija` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `concepto`
--

INSERT INTO `concepto` (`idConcepto`, `abreviatura`, `descripcion`, `estadoConcepto`, `esRemunerativo`, `tipoDeConcepto`, `cantidadPorcentual`, `cantidadFija`) VALUES
(0, 'SAC', 'Sueldo anual complementario', 1, 1, 'Haber', 0, 0),
(1, 'Bas', 'Básico', 1, 1, 'Haber', 0, 0),
(2, 'Ant', 'Antiguedad', 1, 1, 'Haber', 0, 0),
(3, 'Pres', 'Presentismo', 1, 1, 'Haber', 8.33, 0),
(4, 'Jub', 'Jubilación', 1, 1, 'Retención', 11, 0),
(5, 'INSSJP', 'INSSJP Ley 19032', 1, 1, 'Retención', 3, 0),
(6, 'OS', 'Obra Social', 1, 1, 'Retención', 3, 0),
(7, 'HE50', 'Horas extra 50%', 1, 1, 'Haber', 0, 0),
(8, 'HE100', 'Horas extra 100%', 1, 1, 'Haber', 0, 0),
(9, 'Sind', 'Sindicato', 1, 1, 'Retención', 2, 0),
(10, 'ART', 'ART', 1, 0, 'Retención', 1.5, 0),
(11, 'Vac', 'Vacaciones ', 1, 0, 'Haber', 0, 0),
(12, 'ANRM', 'Adicional No Remunerativo Mensualizado', 1, 0, 'Haber', 0, 800),
(13, 'Abrev 1 ', 'Descripcion 1', 1, 1, 'Haber', 0, 250),
(14, 'Abrev 2', 'Descripcion2', 1, 0, 'Haber', 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datospersona`
--

CREATE TABLE `datospersona` (
  `idDatosPersona` int(11) NOT NULL,
  `dni` int(9) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `Persona_idPersona` int(11) NOT NULL,
  `EstadoCivil_idEstadoCivil` int(11) NOT NULL,
  `sexo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datospersona`
--

INSERT INTO `datospersona` (`idDatosPersona`, `dni`, `fechaNacimiento`, `Persona_idPersona`, `EstadoCivil_idEstadoCivil`, `sexo`) VALUES
(1, 54567542, '1990-02-02', 1, 1, 'Masculino'),
(2, 28115494, '1985-03-06', 3, 1, 'Femenino'),
(3, 21212122, '2019-03-08', 4, 5, 'Masculino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalledesueldo`
--

CREATE TABLE `detalledesueldo` (
  `idDetalleDeSueldo` int(11) NOT NULL,
  `Sueldo_idSueldo` int(11) NOT NULL,
  `Concepto_idConcepto` int(10) NOT NULL,
  `subtotal` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalledesueldo`
--

INSERT INTO `detalledesueldo` (`idDetalleDeSueldo`, `Sueldo_idSueldo`, `Concepto_idConcepto`, `subtotal`) VALUES
(42, 36, 1, 23878),
(43, 36, 2, 955.12),
(44, 36, 3, 1989.04),
(45, 36, 4, 2626.58),
(46, 36, 5, 716.34),
(47, 36, 6, 716.34),
(48, 36, 7, 358.17),
(49, 36, 8, 477.56),
(50, 36, 9, 477.56),
(51, 36, 10, 358.17),
(52, 37, 1, 23990),
(53, 37, 2, 0),
(54, 37, 3, 1998.37),
(55, 37, 4, 2638.9),
(56, 37, 5, 719.7),
(57, 37, 6, 719.7),
(58, 37, 7, 359.85),
(59, 37, 8, 479.8),
(60, 37, 9, 479.8),
(61, 37, 12, 800);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idEmpleado` int(8) NOT NULL,
  `legajo` int(5) DEFAULT NULL,
  `cuil` varchar(40) DEFAULT NULL,
  `fechaDeIngreso` date DEFAULT NULL,
  `estadoDelEmpleado` int(1) DEFAULT NULL,
  `Persona_idPersona` int(11) NOT NULL,
  `Puesto_idPuesto` int(11) NOT NULL,
  `ObraSocial_idObraSocial` int(11) NOT NULL,
  `domicilio` varchar(100) DEFAULT NULL,
  `pais` varchar(35) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `codigoPostal` int(5) DEFAULT NULL,
  `nacionalidad` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `legajo`, `cuil`, `fechaDeIngreso`, `estadoDelEmpleado`, `Persona_idPersona`, `Puesto_idPuesto`, `ObraSocial_idObraSocial`, `domicilio`, `pais`, `provincia`, `localidad`, `codigoPostal`, `nacionalidad`) VALUES
(1, 10, '20-54567542-3', '2015-02-26', 1, 1, 9, 1, 'Rivadavia 1050', 'Argentina', 'Tucumán', 'San Miguel de Tucumán', 4000, 'Argentina'),
(2, 11, '20-28115494-5', '2017-03-04', 1, 3, 23, 2, 'Domicilio Mascherano', 'Argentina', 'Tucumán', 'San Miguel de Tucumán', 4000, 'Argentino'),
(3, 12, '21-21212122-2', '2019-03-10', 1, 4, 9, 2, 'DOMICILIO', 'ARGENTINA', 'TUCUMAN', 'LOCALIDAD', 4000, 'ARGENTINO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleadoconcepto`
--

CREATE TABLE `empleadoconcepto` (
  `idEmpleadoConcepto` int(11) NOT NULL,
  `Empleado_idEmpleado` int(8) NOT NULL,
  `Concepto_idConcepto` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleadoconcepto`
--

INSERT INTO `empleadoconcepto` (`idEmpleadoConcepto`, `Empleado_idEmpleado`, `Concepto_idConcepto`) VALUES
(80, 1, 2),
(81, 1, 3),
(82, 1, 4),
(83, 1, 5),
(84, 1, 6),
(85, 1, 7),
(86, 1, 8),
(87, 1, 9),
(88, 1, 10),
(113, 2, 2),
(114, 2, 3),
(115, 2, 4),
(116, 2, 5),
(117, 2, 6),
(118, 2, 7),
(119, 2, 8),
(120, 2, 9),
(121, 2, 12),
(122, 3, 0),
(123, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocivil`
--

CREATE TABLE `estadocivil` (
  `idEstadoCivil` int(11) NOT NULL,
  `descripcion` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estadocivil`
--

INSERT INTO `estadocivil` (`idEstadoCivil`, `descripcion`) VALUES
(1, 'Casado/a'),
(2, 'Viudo/a'),
(3, 'Divorciado/a'),
(5, 'Soltero/a');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupofamiliar`
--

CREATE TABLE `grupofamiliar` (
  `idGrupoFamiliar` int(11) NOT NULL,
  `apellido` varchar(40) DEFAULT NULL,
  `nombre` varchar(40) DEFAULT NULL,
  `fechaDeNacimiento` date DEFAULT NULL,
  `dni` int(9) DEFAULT NULL,
  `Empleado_idEmpleado` int(11) NOT NULL,
  `Parentesco_idParentesco` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineadecompra`
--

CREATE TABLE `lineadecompra` (
  `idLineaDeCompra` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `Articulo_idArticulo` int(11) NOT NULL,
  `Compra_idCompra` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lineadecompra`
--

INSERT INTO `lineadecompra` (`idLineaDeCompra`, `cantidad`, `Articulo_idArticulo`, `Compra_idCompra`) VALUES
(1, 5, 26, 1),
(2, 10, 30, 1),
(3, 15, 31, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineadeperdida`
--

CREATE TABLE `lineadeperdida` (
  `idLineaDePerdida` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `Articulo_idArticulo` int(11) NOT NULL,
  `Perdida_idPerdida` int(11) NOT NULL,
  `MotivoPerdida_idMotivoPerdida` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lineadeperdida`
--

INSERT INTO `lineadeperdida` (`idLineaDePerdida`, `cantidad`, `Articulo_idArticulo`, `Perdida_idPerdida`, `MotivoPerdida_idMotivoPerdida`) VALUES
(1, 2, 16, 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineadeventa`
--

CREATE TABLE `lineadeventa` (
  `idLineaDeVenta` int(11) NOT NULL,
  `cantidad` int(10) DEFAULT NULL,
  `Articulo_idArticulo` int(11) NOT NULL,
  `Venta_idVenta` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lineadeventa`
--

INSERT INTO `lineadeventa` (`idLineaDeVenta`, `cantidad`, `Articulo_idArticulo`, `Venta_idVenta`) VALUES
(1, 1, 17, 1),
(2, 2, 11, 2),
(3, 1, 33, 2),
(4, 1, 22, 2),
(5, 1, 15, 3),
(6, 2, 17, 3),
(7, 1, 18, 3),
(8, 1, 11, 3),
(9, 1, 17, 4),
(10, 1, 18, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idMarca` int(11) NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idMarca`, `descripcion`) VALUES
(1, '7 UP'),
(2, '9 de Oro'),
(3, 'Ades'),
(4, 'Aguila'),
(5, 'Alca'),
(6, 'Alikal'),
(7, 'Arcor'),
(8, 'Aspirina'),
(9, 'Axe'),
(10, 'Baggio'),
(11, 'Bagley'),
(12, 'Balbo'),
(13, 'Bayer'),
(14, 'Bazooka'),
(15, 'Beldent'),
(16, 'Bic'),
(17, 'Billiken'),
(18, 'Bimbo'),
(19, 'Bon o Bon'),
(20, 'Bonafide'),
(21, 'Branca'),
(22, 'Buscapina'),
(23, 'Calipso'),
(24, 'Camaleon'),
(25, 'Camel'),
(26, 'Ceral'),
(27, 'Cereal Fort'),
(28, 'Cerealitas'),
(29, 'Chocolinas'),
(30, 'Cindor'),
(31, 'Coca Cola'),
(32, 'Cofler'),
(33, 'Colgate'),
(34, 'Criollitas'),
(35, 'Curitas'),
(36, 'Danononi'),
(37, 'Daqui'),
(38, 'Delicias'),
(39, 'Derby'),
(40, 'Diversion'),
(41, 'Dolca'),
(42, 'Don Satur'),
(43, 'Donnuts'),
(44, 'Dos Corazones'),
(45, 'Dove'),
(46, 'Duracell'),
(47, 'Elite'),
(48, 'Energizer'),
(49, 'Espadol'),
(50, 'Estrella'),
(51, 'Eveready'),
(52, 'Fanta'),
(53, 'Fantoche'),
(54, 'Felfort'),
(55, 'Flynn Paff'),
(56, 'Fort'),
(57, 'Fragata'),
(58, 'Fresita'),
(59, 'Fulbito'),
(60, 'Full Power'),
(61, 'Geniol'),
(62, 'Gillete'),
(63, 'Habana'),
(64, 'Halls'),
(65, 'Hamlet'),
(66, 'Higienol'),
(67, 'Ibuevanol'),
(68, 'Ibupirac'),
(69, 'Ilolay'),
(70, 'Issue'),
(71, 'Jack'),
(72, 'Jorgito'),
(73, 'kesitas'),
(74, 'Kinder'),
(75, 'Kolinos'),
(76, 'La Morenita'),
(77, 'La Serenisima'),
(78, 'La Virginia'),
(79, 'Le Mans'),
(80, 'Lenguetazo'),
(81, 'Lux'),
(82, 'Malbo'),
(83, 'Mana'),
(84, 'Mantecol'),
(85, 'Malboro'),
(86, 'Marroc'),
(87, 'Menthoplus'),
(88, 'Merengadas'),
(89, 'Milk'),
(90, 'Milka'),
(92, 'Mogul'),
(93, 'Nestle'),
(94, 'Nivea'),
(95, 'Nugaton'),
(96, 'Opera'),
(97, 'Oreo'),
(98, 'Palitos de la selva'),
(99, 'Palmolive'),
(100, 'Paresien'),
(101, 'Paso de los toros'),
(102, 'Pepas'),
(103, 'Pepitos'),
(104, 'Pepsi'),
(105, 'Phillips Morris'),
(106, 'Pico Dulce'),
(107, 'Polyana'),
(108, 'Push Pop'),
(109, 'Rayovac'),
(110, 'Red Bull'),
(111, 'Rex'),
(112, 'Rexona'),
(113, 'Rocklets'),
(114, 'Rosamonte'),
(115, 'Rumba'),
(116, 'Saladix'),
(117, 'Sedal'),
(118, 'Ser'),
(119, 'Sertal'),
(120, 'Shot'),
(121, 'Sonrisas'),
(122, 'Speed'),
(123, 'Sprite'),
(124, 'Suave'),
(125, 'Sugus'),
(126, 'Swift'),
(127, 'Tafirol'),
(128, 'Tang'),
(129, 'Taragui'),
(130, 'Tatin'),
(131, 'Terrabusi'),
(132, 'Tia Maruca'),
(133, 'Tic Tac'),
(134, 'Tita'),
(135, 'Toffi'),
(136, 'Topline'),
(137, 'Traviata'),
(138, 'Tres Patitos'),
(139, 'Tulipan'),
(140, 'Turron'),
(141, 'Uvasal'),
(142, 'Uvita'),
(143, 'Viceroy'),
(144, 'Villa del sur'),
(145, 'Villavicencio'),
(146, 'Viña de Alvear'),
(147, 'Viñas de Balbo'),
(148, 'Yahoo'),
(149, 'Manaos'),
(151, 'DC'),
(153, 'dfgdhg'),
(154, 'Blem'),
(155, 'Poett'),
(156, 'Torasso'),
(157, 'Chocolinas'),
(158, 'Natura'),
(159, 'Pureza'),
(160, 'Barbara'),
(161, 'Independencia'),
(162, 'La Santeña'),
(163, 'Pureza'),
(164, 'Marbella'),
(165, 'Hellmanns'),
(166, 'CBsé'),
(167, 'Cachamate'),
(168, 'Verdeflor'),
(169, 'Media tarde'),
(170, 'Zorro'),
(171, 'Pantene'),
(172, 'Magistral'),
(173, 'Elite'),
(174, 'Dove'),
(176, 'Mirinda'),
(177, 'Coca Cola'),
(178, 'Cabrales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `motivoperdida`
--

CREATE TABLE `motivoperdida` (
  `idMotivoPerdida` int(11) NOT NULL,
  `descripcionMotivo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `motivoperdida`
--

INSERT INTO `motivoperdida` (`idMotivoPerdida`, `descripcionMotivo`) VALUES
(1, 'Robo'),
(2, 'Extravio'),
(4, 'Rotura'),
(5, 'Vencimiento');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obrasocial`
--

CREATE TABLE `obrasocial` (
  `idObraSocial` int(11) NOT NULL,
  `razonSocialOb` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `obrasocial`
--

INSERT INTO `obrasocial` (`idObraSocial`, `razonSocialOb`) VALUES
(1, 'Boreal'),
(2, 'Red'),
(3, 'Otra 2'),
(4, 'Otra 1'),
(6, 'NUEVA OBRA SOCIAL ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parentesco`
--

CREATE TABLE `parentesco` (
  `idParentesco` int(11) NOT NULL,
  `descripcionParentesco` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `parentesco`
--

INSERT INTO `parentesco` (`idParentesco`, `descripcionParentesco`) VALUES
(1, 'Hijo/a'),
(2, 'Nieto/a'),
(3, 'Sobrino/a'),
(4, 'Hermano/a'),
(5, 'Cu?ado/a'),
(6, 'Conyuge'),
(7, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perdida`
--

CREATE TABLE `perdida` (
  `idPerdida` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `observacion` varchar(200) DEFAULT NULL,
  `Empleado_idEmpleado` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perdida`
--

INSERT INTO `perdida` (`idPerdida`, `fecha`, `observacion`, `Empleado_idEmpleado`) VALUES
(1, '2019-03-07', 'Rotura del embase', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idPersona`, `nombre`, `apellido`, `email`, `telefono`) VALUES
(1, 'Lionel', 'Messi', 'lioMessi10@gmail.com', '456789'),
(2, 'Luis', 'Suarez', 'luis-suarez@gmail.com', '463347'),
(3, 'Javier', 'Mascherano', 'javier_mascherano@gmail.com', '4548456'),
(4, 'NOMBRE', 'APELLIDO', 'MAIL', '5456464');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL,
  `razonSocial` varchar(40) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `nroCUIT` varchar(30) DEFAULT NULL,
  `web` varchar(60) DEFAULT NULL,
  `observacion` varchar(140) DEFAULT NULL,
  `TipoDeCUIT_idTipoDeCUIT` int(11) NOT NULL,
  `Rubro_idRubro` int(11) NOT NULL,
  `domicilio` varchar(100) DEFAULT NULL,
  `pais` varchar(35) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `localidad` varchar(50) DEFAULT NULL,
  `codigoPostal` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `razonSocial`, `telefono`, `celular`, `email`, `nroCUIT`, `web`, `observacion`, `TipoDeCUIT_idTipoDeCUIT`, `Rubro_idRubro`, `domicilio`, `pais`, `provincia`, `localidad`, `codigoPostal`) VALUES
(2, 'Distribuidora razon social', '445697', '3817878456', 'distri_razonsocial@gmail.com', '45-32154512-7', 'www.pagina.com', 'Ninguna', 2, 18, 'Av. Roca', 'Argentina', 'Tucumán', 'San Miguel de Tucumán', 4000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puesto`
--

CREATE TABLE `puesto` (
  `idPuesto` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `salario` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puesto`
--

INSERT INTO `puesto` (`idPuesto`, `descripcion`, `salario`) VALUES
(6, 'Maestranza A', 23544.49),
(7, 'Maestranza B', 23633.57),
(8, 'Maestranza C', 23945.77),
(9, 'Administración A', 23878.91),
(10, 'Administración B', 24012.8),
(11, 'Administración C', 24146.59),
(12, 'Administración D', 24548.03),
(13, 'Administración E', 24882.49),
(14, 'Administración F', 24373.14),
(15, 'Cajeros A', 23990.39),
(16, 'Cajeros B', 24146.58),
(17, 'Cajeros C', 24347.3),
(18, 'Personal Auxiliar A', 23990.39),
(19, 'Personal Auxiliar B', 24213.41),
(20, 'Personal Auxiliar C', 24949.41),
(21, 'Auxiliar Especializado A', 24258.14),
(22, 'Auxiliar Especializado B', 24659.5),
(23, 'Vendedor A', 23990.39),
(24, 'Vendedor B', 24659.59),
(25, 'Vendedor C', 24882.49),
(26, 'Vendedor D', 25373.14),
(27, 'NUEVO PUESTO', 23000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rubro`
--

CREATE TABLE `rubro` (
  `idRubro` int(11) NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rubro`
--

INSERT INTO `rubro` (`idRubro`, `descripcion`) VALUES
(1, 'Alfajores'),
(2, 'Almacen'),
(3, 'Bebidas con alcohol'),
(4, 'Cereal'),
(5, 'Chocolates'),
(6, 'Cigarrillos'),
(7, 'Farmacia'),
(8, 'Galletitas'),
(9, 'Gaseosas'),
(10, 'Golosinas'),
(11, 'Helados'),
(12, 'Jugos'),
(13, 'Lacteos'),
(14, 'Libreria'),
(15, 'Perfumeria'),
(16, 'Snacks'),
(17, 'Varios'),
(18, 'Limpieza'),
(19, 'Bebidas sin alcohol'),
(20, 'Aceites'),
(21, 'Enlatado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesionusuario`
--

CREATE TABLE `sesionusuario` (
  `idSesionUsuario` int(11) NOT NULL,
  `fechaHoraEntrada` datetime DEFAULT NULL,
  `fechaHoraSalida` datetime DEFAULT NULL,
  `Usuario_idusuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sesionusuario`
--

INSERT INTO `sesionusuario` (`idSesionUsuario`, `fechaHoraEntrada`, `fechaHoraSalida`, `Usuario_idusuario`) VALUES
(1609, '2019-03-05 13:29:00', '2019-03-05 13:32:16', 2),
(1610, '2019-03-05 13:32:26', '2019-03-05 13:32:34', 2),
(1611, '2019-03-05 16:20:48', '2019-03-05 16:22:27', 2),
(1612, '2019-03-05 16:26:32', '2019-03-05 16:29:00', 2),
(1613, '2019-03-07 00:11:52', '2019-03-07 00:12:22', 2),
(1614, '2019-03-07 00:13:09', '2019-03-07 00:13:16', 2),
(1615, '2019-03-07 00:13:25', '2019-03-07 00:13:38', 2),
(1616, '2019-03-07 00:16:34', '2019-03-07 00:16:42', 2),
(1617, '2019-03-07 00:17:15', '2019-03-07 00:17:23', 2),
(1618, '2019-03-07 00:17:32', '2019-03-07 00:17:57', 2),
(1619, '2019-03-07 00:18:27', '2019-03-07 00:18:53', 2),
(1620, '2019-03-07 00:23:33', '2019-03-07 00:36:12', 2),
(1621, '2019-03-07 00:36:53', '2019-03-07 00:37:02', 2),
(1622, '2019-03-07 00:37:30', '2019-03-07 00:37:43', 2),
(1623, '2019-03-07 00:37:56', '2019-03-07 00:40:15', 2),
(1624, '2019-03-07 00:40:39', '2019-03-07 00:40:43', 2),
(1625, '2019-03-07 00:42:31', '2019-03-07 01:03:56', 2),
(1626, '2019-03-07 01:04:26', '2019-03-07 01:05:15', 2),
(1627, '2019-03-07 01:07:54', '2019-03-07 01:26:50', 2),
(1628, '2019-03-07 01:28:03', '2019-03-07 01:33:44', 2),
(1629, '2019-03-07 01:34:12', '2019-03-07 01:43:36', 2),
(1630, '2019-03-07 02:02:23', '2019-03-07 02:03:15', 2),
(1631, '2019-03-07 02:05:03', '2019-03-07 02:07:42', 2),
(1632, '2019-03-07 02:08:45', '2019-03-07 02:14:03', 2),
(1633, '2019-03-07 02:14:38', '2019-03-07 02:31:54', 2),
(1634, '2019-03-07 02:32:28', '2019-03-07 02:33:24', 2),
(1635, '2019-03-07 02:53:24', '2019-03-07 02:53:33', 2),
(1636, '2019-03-07 02:59:44', '2019-03-07 02:59:59', 2),
(1637, '2019-03-07 03:04:24', '2019-03-07 04:00:02', 2),
(1638, '2019-03-07 22:14:41', '2019-03-07 22:15:04', 2),
(1639, '2019-03-07 22:59:08', '2019-03-07 22:59:37', 2),
(1640, '2019-03-07 23:12:45', '2019-03-07 23:12:59', 2),
(1641, '2019-03-07 23:31:15', '2019-03-07 23:32:47', 2),
(1642, '2019-03-07 23:33:01', '2019-03-07 23:33:42', 2),
(1643, '2019-03-07 23:34:18', '2019-03-07 23:34:45', 2),
(1644, '2019-03-07 23:35:17', '2019-03-07 23:35:27', 2),
(1645, '2019-03-07 23:35:46', '2019-03-07 23:35:53', 2),
(1646, '2019-03-07 23:36:04', NULL, 2),
(1647, '2019-03-07 23:39:15', '2019-03-07 23:40:16', 2),
(1648, '2019-03-07 23:42:46', '2019-03-07 23:42:52', 2),
(1649, '2019-03-07 23:43:14', '2019-03-07 23:43:19', 2),
(1650, '2019-03-07 23:43:42', '2019-03-07 23:43:52', 2),
(1651, '2019-03-07 23:45:07', '2019-03-07 23:45:21', 2),
(1652, '2019-03-07 23:45:49', '2019-03-07 23:45:53', 2),
(1653, '2019-03-07 23:47:19', '2019-03-07 23:47:43', 2),
(1654, '2019-03-07 23:49:52', '2019-03-07 23:49:58', 2),
(1655, '2019-03-07 23:50:16', '2019-03-07 23:50:47', 2),
(1656, '2019-03-08 01:30:45', '2019-03-08 01:31:09', 2),
(1657, '2019-03-08 01:48:07', '2019-03-08 01:48:48', 2),
(1658, '2019-03-08 01:48:51', '2019-03-08 01:49:00', 2),
(1659, '2019-03-08 01:50:04', '2019-03-08 01:50:14', 2),
(1660, '2019-03-08 01:51:15', '2019-03-08 01:51:29', 2),
(1661, '2019-03-08 01:52:06', '2019-03-08 01:52:20', 2),
(1662, '2019-03-08 01:55:12', '2019-03-08 01:55:36', 2),
(1663, '2019-03-08 01:55:48', '2019-03-08 01:56:09', 2),
(1664, '2019-03-08 01:57:53', '2019-03-08 01:58:07', 2),
(1665, '2019-03-08 01:58:39', '2019-03-08 01:58:55', 2),
(1666, '2019-03-08 02:00:46', '2019-03-08 02:01:10', 2),
(1667, '2019-03-08 02:04:48', '2019-03-08 02:04:55', 2),
(1668, '2019-03-08 02:25:21', '2019-03-08 02:25:51', 2),
(1669, '2019-03-08 02:26:52', '2019-03-08 02:26:53', 2),
(1670, '2019-03-08 02:27:00', '2019-03-08 02:27:16', 2),
(1671, '2019-03-08 02:31:45', '2019-03-08 02:32:02', 2),
(1672, '2019-03-08 02:35:58', '2019-03-08 02:36:10', 2),
(1673, '2019-03-08 02:37:52', '2019-03-08 02:42:37', 2),
(1674, '2019-03-08 03:08:36', '2019-03-08 03:08:38', 2),
(1675, '2019-03-08 03:36:29', '2019-03-08 03:36:42', 2),
(1676, '2019-03-08 03:44:51', '2019-03-08 03:45:10', 2),
(1677, '2019-03-08 03:57:40', '2019-03-08 03:57:46', 2),
(1678, '2019-03-08 03:59:43', '2019-03-08 04:00:03', 2),
(1679, '2019-03-08 04:00:19', '2019-03-08 04:00:35', 2),
(1680, '2019-03-08 04:02:38', '2019-03-08 04:03:44', 2),
(1681, '2019-03-08 04:07:17', '2019-03-08 04:11:44', 2),
(1682, '2019-03-08 04:14:12', '2019-03-08 04:14:45', 2),
(1683, '2019-03-08 04:16:05', '2019-03-08 04:17:06', 2),
(1684, '2019-03-08 04:17:29', '2019-03-08 04:17:47', 2),
(1685, '2019-03-08 04:19:02', '2019-03-08 04:21:23', 2),
(1686, '2019-03-08 04:22:18', '2019-03-08 04:22:58', 2),
(1687, '2019-03-08 04:25:58', '2019-03-08 04:27:55', 2),
(1688, '2019-03-08 04:30:22', NULL, 2),
(1689, '2019-03-08 04:34:22', '2019-03-08 04:36:09', 2),
(1690, '2019-03-08 04:37:50', '2019-03-08 04:38:02', 2),
(1691, '2019-03-08 04:41:05', '2019-03-08 04:42:02', 2),
(1692, '2019-03-08 04:42:45', '2019-03-08 04:46:28', 2),
(1693, '2019-03-08 04:48:02', '2019-03-08 04:50:22', 2),
(1694, '2019-03-08 04:50:28', '2019-03-08 04:51:11', 2),
(1695, '2019-03-08 04:52:03', '2019-03-08 04:53:46', 2),
(1696, '2019-03-08 04:54:30', '2019-03-08 04:54:37', 2),
(1697, '2019-03-08 04:56:36', '2019-03-08 04:57:43', 2),
(1698, '2019-03-08 04:58:25', '2019-03-08 04:59:12', 2),
(1699, '2019-03-08 04:59:20', '2019-03-08 04:59:45', 2),
(1700, '2019-03-08 05:01:22', '2019-03-08 05:01:27', 2),
(1701, '2019-03-08 05:06:12', '2019-03-08 05:06:17', 2),
(1702, '2019-03-08 05:09:12', '2019-03-08 05:09:23', 2),
(1703, '2019-03-08 05:09:44', '2019-03-08 05:09:51', 2),
(1704, '2019-03-08 05:10:56', '2019-03-08 05:11:03', 2),
(1705, '2019-03-08 05:12:05', '2019-03-08 05:12:23', 2),
(1706, '2019-03-08 05:12:48', '2019-03-08 05:12:54', 2),
(1707, '2019-03-08 05:13:27', '2019-03-08 05:13:45', 2),
(1708, '2019-03-08 05:14:10', '2019-03-08 05:14:29', 2),
(1709, '2019-03-08 05:14:39', '2019-03-08 05:14:58', 2),
(1710, '2019-03-08 05:19:42', '2019-03-08 05:22:05', 2),
(1711, '2019-03-08 05:25:12', '2019-03-08 05:27:09', 2),
(1712, '2019-03-08 05:28:55', '2019-03-08 05:29:12', 2),
(1713, '2019-03-08 05:31:39', '2019-03-08 05:32:04', 2),
(1714, '2019-03-08 05:33:59', '2019-03-08 05:34:41', 2),
(1715, '2019-04-23 03:40:47', '2019-04-23 03:45:24', 2),
(1716, '2019-04-27 09:45:48', '2019-04-27 09:47:28', 2),
(1717, '2019-04-27 09:47:35', '2019-04-27 09:47:57', 2),
(1718, '2019-06-04 00:39:15', '2019-06-04 00:39:17', 2),
(1719, '2019-06-04 18:59:27', '2019-06-04 18:59:32', 2),
(1720, '2019-06-05 04:41:39', '2019-06-05 04:41:49', 2),
(1721, '2019-06-05 04:53:29', '2019-06-05 04:53:31', 2),
(1722, '2019-06-05 05:49:09', '2019-06-05 05:50:34', 2),
(1723, '2019-06-06 02:46:12', '2019-06-06 02:47:08', 2),
(1724, '2019-06-06 02:51:33', '2019-06-06 02:54:29', 2),
(1725, '2019-06-06 05:01:54', '2019-06-06 05:02:05', 2),
(1726, '2019-06-06 05:07:20', '2019-06-06 05:08:06', 2),
(1727, '2019-06-06 05:26:15', '2019-06-06 05:26:31', 2),
(1728, '2019-06-07 03:35:44', '2019-06-07 03:36:57', 2),
(1729, '2019-06-11 02:04:19', '2019-06-11 02:04:25', 2),
(1730, '2019-06-11 02:10:04', '2019-06-11 02:10:43', 2),
(1731, '2019-06-11 03:51:47', '2019-06-11 03:52:21', 2),
(1732, '2019-06-11 05:02:57', '2019-06-11 05:03:25', 2),
(1733, '2019-06-11 05:37:22', '2019-06-11 05:37:32', 2),
(1734, '2019-06-12 04:24:18', NULL, 2),
(1735, '2019-06-12 04:27:00', '2019-06-12 04:27:04', 2),
(1736, '2019-06-12 04:54:24', '2019-06-12 04:54:26', 2),
(1737, '2019-06-12 05:14:54', '2019-06-12 05:15:35', 2),
(1738, '2019-06-12 05:15:41', '2019-06-12 05:16:28', 2),
(1739, '2019-08-02 00:43:39', '2019-08-02 00:43:56', 2),
(1740, '2020-07-10 17:34:54', '2020-07-10 17:37:58', 2),
(1741, '2020-07-17 00:40:01', '2020-07-17 00:40:44', 2),
(1742, '2020-07-17 00:49:54', '2020-07-17 00:49:57', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sueldo`
--

CREATE TABLE `sueldo` (
  `idSueldo` int(11) NOT NULL,
  `Empleado_idEmpleado` int(8) NOT NULL,
  `fechaDePago` date DEFAULT NULL,
  `antiguedad` int(2) DEFAULT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `sueldoBruto` float DEFAULT NULL,
  `sueldoNeto` float DEFAULT NULL,
  `tipo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sueldo`
--

INSERT INTO `sueldo` (`idSueldo`, `Empleado_idEmpleado`, `fechaDePago`, `antiguedad`, `categoria`, `sueldoBruto`, `sueldoNeto`, `tipo`) VALUES
(34, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 1, '2019-03-08', 4, 'Administración A', 27657.9, 22762.9, 'Especial'),
(37, 2, '2019-03-08', 0, 'Vendedor A', 26828, 23069.9, 'Especial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodecuit`
--

CREATE TABLE `tipodecuit` (
  `idTipoDeCUIT` int(11) NOT NULL,
  `descripcion` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipodecuit`
--

INSERT INTO `tipodecuit` (`idTipoDeCUIT`, `descripcion`) VALUES
(1, 'Consumidor Final'),
(2, 'Resp. Inscripto'),
(3, 'Resp. No Inscripto'),
(4, 'No Responsable'),
(5, 'Exento'),
(6, 'Monotributista'),
(7, 'Resp. No Insc. N.I.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodeusuario`
--

CREATE TABLE `tipodeusuario` (
  `idTipoDeUsuario` int(11) NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipodeusuario`
--

INSERT INTO `tipodeusuario` (`idTipoDeUsuario`, `descripcion`) VALUES
(1, 'Administrador'),
(2, 'Vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `user` varchar(20) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `estadoDelUsuario` int(1) DEFAULT NULL,
  `Empleado_idEmpleado` int(11) NOT NULL,
  `TipoDeUsuario_idTipoDeUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `user`, `pass`, `estadoDelUsuario`, `Empleado_idEmpleado`, `TipoDeUsuario_idTipoDeUsuario`) VALUES
(2, 'lionel', 'p+0giICy5aSPDxwubhiucg==', 1, 1, 1),
(3, 'javier', 'Kvmbufg3hgwEElg7iR0ogg==', 0, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idVenta` int(10) NOT NULL,
  `fecha` date DEFAULT NULL,
  `total` float DEFAULT NULL,
  `Empleado_idEmpleado` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idVenta`, `fecha`, `total`, `Empleado_idEmpleado`) VALUES
(1, '2019-03-07', 27, 1),
(2, '2019-03-07', 203, 1),
(3, '2019-04-23', 168, 1),
(4, '2019-04-27', 91, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`idArticulo`,`Rubro_idRubro`,`Marca_idMarca`),
  ADD KEY `fk_Articulo_Marca1_idx` (`Marca_idMarca`),
  ADD KEY `fk_Articulo_Rubro1_idx` (`Rubro_idRubro`);

--
-- Indices de la tabla `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`idBanco`,`Empleado_idEmpleado`),
  ADD KEY `fk_Banco_Empleado1_idx` (`Empleado_idEmpleado`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`,`Persona_idPersona`,`TipoDeCUIT_idTipoDeCUIT`),
  ADD KEY `fk_Clientes_Persona1_idx` (`Persona_idPersona`),
  ADD KEY `fk_Clientes_TipoDeCUIT1_idx` (`TipoDeCUIT_idTipoDeCUIT`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`idCompra`,`Proveedor_idProveedor`),
  ADD KEY `fk_Compra_Proveedor1_idx` (`Proveedor_idProveedor`);

--
-- Indices de la tabla `concepto`
--
ALTER TABLE `concepto`
  ADD PRIMARY KEY (`idConcepto`);

--
-- Indices de la tabla `datospersona`
--
ALTER TABLE `datospersona`
  ADD PRIMARY KEY (`idDatosPersona`,`Persona_idPersona`,`EstadoCivil_idEstadoCivil`),
  ADD UNIQUE KEY `Persona_idPersona_UNIQUE` (`Persona_idPersona`),
  ADD KEY `fk_DatosPersona_Persona1_idx` (`Persona_idPersona`),
  ADD KEY `fk_DatosPersona_EstadoCivil1_idx` (`EstadoCivil_idEstadoCivil`);

--
-- Indices de la tabla `detalledesueldo`
--
ALTER TABLE `detalledesueldo`
  ADD PRIMARY KEY (`idDetalleDeSueldo`,`Sueldo_idSueldo`,`Concepto_idConcepto`),
  ADD KEY `fk_DetalleDeSueldo_Sueldo1_idx` (`Sueldo_idSueldo`),
  ADD KEY `fk_DetalleDeSueldo_Concepto1_idx` (`Concepto_idConcepto`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idEmpleado`,`Persona_idPersona`,`Puesto_idPuesto`,`ObraSocial_idObraSocial`),
  ADD KEY `fk_Empleado_Persona1_idx` (`Persona_idPersona`),
  ADD KEY `fk_Empleado_Puesto1_idx` (`Puesto_idPuesto`),
  ADD KEY `fk_Empleado_ObraSocial1_idx` (`ObraSocial_idObraSocial`) USING BTREE;

--
-- Indices de la tabla `empleadoconcepto`
--
ALTER TABLE `empleadoconcepto`
  ADD PRIMARY KEY (`idEmpleadoConcepto`,`Empleado_idEmpleado`,`Concepto_idConcepto`),
  ADD KEY `fk_PuestoEmpleadoConcepto_Empleado1_idx` (`Empleado_idEmpleado`),
  ADD KEY `fk_PuestoEmpleadoConcepto_Concepto1_idx` (`Concepto_idConcepto`);

--
-- Indices de la tabla `estadocivil`
--
ALTER TABLE `estadocivil`
  ADD PRIMARY KEY (`idEstadoCivil`);

--
-- Indices de la tabla `grupofamiliar`
--
ALTER TABLE `grupofamiliar`
  ADD PRIMARY KEY (`idGrupoFamiliar`,`Empleado_idEmpleado`,`Parentesco_idParentesco`),
  ADD KEY `fk_GrupoFamiliar_Empleado1_idx` (`Empleado_idEmpleado`),
  ADD KEY `fk_GrupoFamiliar_Parentesco1_idx` (`Parentesco_idParentesco`);

--
-- Indices de la tabla `lineadecompra`
--
ALTER TABLE `lineadecompra`
  ADD PRIMARY KEY (`idLineaDeCompra`,`Articulo_idArticulo`,`Compra_idCompra`),
  ADD KEY `fk_LineaDeCompra_Articulo1_idx` (`Articulo_idArticulo`),
  ADD KEY `fk_LineaDeCompra_Compra1_idx` (`Compra_idCompra`);

--
-- Indices de la tabla `lineadeperdida`
--
ALTER TABLE `lineadeperdida`
  ADD PRIMARY KEY (`idLineaDePerdida`,`Articulo_idArticulo`,`Perdida_idPerdida`,`MotivoPerdida_idMotivoPerdida`),
  ADD KEY `fk_LineaSalida_Articulo1_idx` (`Articulo_idArticulo`),
  ADD KEY `fk_LineaDePedida_Perdida1_idx` (`Perdida_idPerdida`),
  ADD KEY `fk_LineaDePedida_MotivoPerdida1_idx` (`MotivoPerdida_idMotivoPerdida`);

--
-- Indices de la tabla `lineadeventa`
--
ALTER TABLE `lineadeventa`
  ADD PRIMARY KEY (`idLineaDeVenta`,`Articulo_idArticulo`,`Venta_idVenta`),
  ADD KEY `fk_LineaDeVenta_Articulo1_idx` (`Articulo_idArticulo`),
  ADD KEY `fk_LineaDeVenta_Venta1_idx` (`Venta_idVenta`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idMarca`);

--
-- Indices de la tabla `motivoperdida`
--
ALTER TABLE `motivoperdida`
  ADD PRIMARY KEY (`idMotivoPerdida`);

--
-- Indices de la tabla `obrasocial`
--
ALTER TABLE `obrasocial`
  ADD PRIMARY KEY (`idObraSocial`);

--
-- Indices de la tabla `parentesco`
--
ALTER TABLE `parentesco`
  ADD PRIMARY KEY (`idParentesco`);

--
-- Indices de la tabla `perdida`
--
ALTER TABLE `perdida`
  ADD PRIMARY KEY (`idPerdida`,`Empleado_idEmpleado`),
  ADD KEY `fk_Salida_Empleado1_idx` (`Empleado_idEmpleado`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`,`TipoDeCUIT_idTipoDeCUIT`,`Rubro_idRubro`),
  ADD KEY `fk_Proveedor_TipoDeCUIT1_idx` (`TipoDeCUIT_idTipoDeCUIT`),
  ADD KEY `fk_Proveedor_Rubro1_idx` (`Rubro_idRubro`);

--
-- Indices de la tabla `puesto`
--
ALTER TABLE `puesto`
  ADD PRIMARY KEY (`idPuesto`);

--
-- Indices de la tabla `rubro`
--
ALTER TABLE `rubro`
  ADD PRIMARY KEY (`idRubro`);

--
-- Indices de la tabla `sesionusuario`
--
ALTER TABLE `sesionusuario`
  ADD PRIMARY KEY (`idSesionUsuario`,`Usuario_idusuario`),
  ADD KEY `fk_SesionUsuario_Usuario1_idx` (`Usuario_idusuario`);

--
-- Indices de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  ADD PRIMARY KEY (`idSueldo`,`Empleado_idEmpleado`),
  ADD KEY `fk_Sueldo_Empleado1_idx` (`Empleado_idEmpleado`);

--
-- Indices de la tabla `tipodecuit`
--
ALTER TABLE `tipodecuit`
  ADD PRIMARY KEY (`idTipoDeCUIT`);

--
-- Indices de la tabla `tipodeusuario`
--
ALTER TABLE `tipodeusuario`
  ADD PRIMARY KEY (`idTipoDeUsuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`,`Empleado_idEmpleado`,`TipoDeUsuario_idTipoDeUsuario`),
  ADD KEY `fk_Usuario_TipoDeUsuario1_idx` (`TipoDeUsuario_idTipoDeUsuario`),
  ADD KEY `fk_Usuario_Empleado1_idx` (`Empleado_idEmpleado`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idVenta`,`Empleado_idEmpleado`),
  ADD KEY `fk_Venta_Empleado1_idx` (`Empleado_idEmpleado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `idArticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `banco`
--
ALTER TABLE `banco`
  MODIFY `idBanco` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `idCompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `concepto`
--
ALTER TABLE `concepto`
  MODIFY `idConcepto` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `datospersona`
--
ALTER TABLE `datospersona`
  MODIFY `idDatosPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `detalledesueldo`
--
ALTER TABLE `detalledesueldo`
  MODIFY `idDetalleDeSueldo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idEmpleado` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `empleadoconcepto`
--
ALTER TABLE `empleadoconcepto`
  MODIFY `idEmpleadoConcepto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT de la tabla `estadocivil`
--
ALTER TABLE `estadocivil`
  MODIFY `idEstadoCivil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `grupofamiliar`
--
ALTER TABLE `grupofamiliar`
  MODIFY `idGrupoFamiliar` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lineadecompra`
--
ALTER TABLE `lineadecompra`
  MODIFY `idLineaDeCompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `lineadeperdida`
--
ALTER TABLE `lineadeperdida`
  MODIFY `idLineaDePerdida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `lineadeventa`
--
ALTER TABLE `lineadeventa`
  MODIFY `idLineaDeVenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idMarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT de la tabla `motivoperdida`
--
ALTER TABLE `motivoperdida`
  MODIFY `idMotivoPerdida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `obrasocial`
--
ALTER TABLE `obrasocial`
  MODIFY `idObraSocial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `parentesco`
--
ALTER TABLE `parentesco`
  MODIFY `idParentesco` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `perdida`
--
ALTER TABLE `perdida`
  MODIFY `idPerdida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `puesto`
--
ALTER TABLE `puesto`
  MODIFY `idPuesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de la tabla `rubro`
--
ALTER TABLE `rubro`
  MODIFY `idRubro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `sesionusuario`
--
ALTER TABLE `sesionusuario`
  MODIFY `idSesionUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1743;

--
-- AUTO_INCREMENT de la tabla `sueldo`
--
ALTER TABLE `sueldo`
  MODIFY `idSueldo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `tipodecuit`
--
ALTER TABLE `tipodecuit`
  MODIFY `idTipoDeCUIT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tipodeusuario`
--
ALTER TABLE `tipodeusuario`
  MODIFY `idTipoDeUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idVenta` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `fk_Articulo_Marca1` FOREIGN KEY (`Marca_idMarca`) REFERENCES `marca` (`idMarca`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Articulo_Rubro1` FOREIGN KEY (`Rubro_idRubro`) REFERENCES `rubro` (`idRubro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `banco`
--
ALTER TABLE `banco`
  ADD CONSTRAINT `fk_Banco_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_Clientes_Persona1` FOREIGN KEY (`Persona_idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Clientes_TipoDeCUIT1` FOREIGN KEY (`TipoDeCUIT_idTipoDeCUIT`) REFERENCES `tipodecuit` (`idTipoDeCUIT`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_Compra_Proveedor1` FOREIGN KEY (`Proveedor_idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `datospersona`
--
ALTER TABLE `datospersona`
  ADD CONSTRAINT `fk_DatosPersona_EstadoCivil1` FOREIGN KEY (`EstadoCivil_idEstadoCivil`) REFERENCES `estadocivil` (`idEstadoCivil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_DatosPersona_Persona1` FOREIGN KEY (`Persona_idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalledesueldo`
--
ALTER TABLE `detalledesueldo`
  ADD CONSTRAINT `fk_DetalleDeSueldo_Concepto1` FOREIGN KEY (`Concepto_idConcepto`) REFERENCES `concepto` (`idConcepto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_DetalleDeSueldo_Sueldo1` FOREIGN KEY (`Sueldo_idSueldo`) REFERENCES `sueldo` (`idSueldo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `fk_Empleado_ObraSocial1` FOREIGN KEY (`ObraSocial_idObraSocial`) REFERENCES `obrasocial` (`idObraSocial`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empleado_Persona1` FOREIGN KEY (`Persona_idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empleado_Puesto1` FOREIGN KEY (`Puesto_idPuesto`) REFERENCES `puesto` (`idPuesto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleadoconcepto`
--
ALTER TABLE `empleadoconcepto`
  ADD CONSTRAINT `fk_PuestoEmpleadoConcepto_Concepto1` FOREIGN KEY (`Concepto_idConcepto`) REFERENCES `concepto` (`idConcepto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PuestoEmpleadoConcepto_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grupofamiliar`
--
ALTER TABLE `grupofamiliar`
  ADD CONSTRAINT `fk_GrupoFamiliar_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_GrupoFamiliar_Parentesco1` FOREIGN KEY (`Parentesco_idParentesco`) REFERENCES `parentesco` (`idParentesco`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `lineadecompra`
--
ALTER TABLE `lineadecompra`
  ADD CONSTRAINT `fk_LineaDeCompra_Articulo1` FOREIGN KEY (`Articulo_idArticulo`) REFERENCES `articulo` (`idArticulo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_LineaDeCompra_Compra1` FOREIGN KEY (`Compra_idCompra`) REFERENCES `compra` (`idCompra`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `lineadeperdida`
--
ALTER TABLE `lineadeperdida`
  ADD CONSTRAINT `fk_LineaDePedida_MotivoPerdida1` FOREIGN KEY (`MotivoPerdida_idMotivoPerdida`) REFERENCES `motivoperdida` (`idMotivoPerdida`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_LineaDePedida_Perdida1` FOREIGN KEY (`Perdida_idPerdida`) REFERENCES `perdida` (`idPerdida`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_LineaSalida_Articulo1` FOREIGN KEY (`Articulo_idArticulo`) REFERENCES `articulo` (`idArticulo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `lineadeventa`
--
ALTER TABLE `lineadeventa`
  ADD CONSTRAINT `fk_LineaDeVenta_Articulo1` FOREIGN KEY (`Articulo_idArticulo`) REFERENCES `articulo` (`idArticulo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_LineaDeVenta_Venta1` FOREIGN KEY (`Venta_idVenta`) REFERENCES `venta` (`idVenta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perdida`
--
ALTER TABLE `perdida`
  ADD CONSTRAINT `fk_Salida_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `fk_Proveedor_Rubro1` FOREIGN KEY (`Rubro_idRubro`) REFERENCES `rubro` (`idRubro`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Proveedor_TipoDeCUIT1` FOREIGN KEY (`TipoDeCUIT_idTipoDeCUIT`) REFERENCES `tipodecuit` (`idTipoDeCUIT`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sesionusuario`
--
ALTER TABLE `sesionusuario`
  ADD CONSTRAINT `fk_SesionUsuario_Usuario1` FOREIGN KEY (`Usuario_idusuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sueldo`
--
ALTER TABLE `sueldo`
  ADD CONSTRAINT `fk_Sueldo_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_Usuario_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Usuario_TipoDeUsuario1` FOREIGN KEY (`TipoDeUsuario_idTipoDeUsuario`) REFERENCES `tipodeusuario` (`idTipoDeUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `fk_Venta_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
