
package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBPuestos;

public class UtilFrameMenuPuestos {
    
    private static DBPuestos dbPuestos;
    
    public static void mostrarPuestosEnTabla(JTable tablaPuestos) throws SQLException, ClassNotFoundException {

        dbPuestos = new DBPuestos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cod.", "Descripción", "Salario"};
        modelo.setColumnIdentifiers(titulos);
        tablaPuestos.setModel(modelo);
        String registro[] = new String[8];

        TableColumnModel columnModel = tablaPuestos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        columnModel.getColumn(1).setPreferredWidth(80);
        columnModel.getColumn(2).setPreferredWidth(50);

        ResultSet puestos = dbPuestos.resultSetPuestos();

        while (puestos.next()) {
            registro[0] = String.valueOf(puestos.getInt("idPuesto"));
            registro[1] = puestos.getString("descripcion");
            registro[2] = String.valueOf(puestos.getInt("salario"));
            modelo.addRow(registro);
        }
    }
}
