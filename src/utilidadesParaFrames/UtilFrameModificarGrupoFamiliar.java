package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import objetosDelDominio.Empleado;
import objetosDelDominio.Pariente;
import persistencia.DBAuxiliares;
import persistencia.DBEmpleados;
import vistas.FrameModificarGrupoFamiliar;

public class UtilFrameModificarGrupoFamiliar {

    private static DefaultTableModel modelo;
    private static DBEmpleados dbEmpleados;
    private static DBAuxiliares dbAuxiliares;

    public static void mostrarElGrupoFamiliar(JTable tablaParientes, Empleado empleado) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();
        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"D.N.I.", "Apellido y Nombre", "F. Nac.", "Parentesco"};
        modelo.setColumnIdentifiers(titulos);
        tablaParientes.setModel(modelo);
        String registro[] = new String[4];

        TableColumnModel columnModel = tablaParientes.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(30);
        columnModel.getColumn(1).setPreferredWidth(200);
        columnModel.getColumn(2).setPreferredWidth(50);
        columnModel.getColumn(2).setPreferredWidth(100);

        for (int i = 0; i < empleado.getGrupoFamiliar().size(); i++) {
            registro[0] = "" + String.valueOf(empleado.getGrupoFamiliar().get(i).getDni());
            registro[1] = empleado.getGrupoFamiliar().get(i).getApellido() + ", " + empleado.getGrupoFamiliar().get(i).getNombre();
            registro[2] = empleado.getGrupoFamiliar().get(i).getFechaNacString();
            registro[3] = empleado.getGrupoFamiliar().get(i).getParentesco();
            modelo.addRow(registro);
        }
    }

    public static void mostrarElPariente(FrameModificarGrupoFamiliar frameModificarGrupoFamiliar, int dni, int fila) throws SQLException, ClassNotFoundException {
        dbEmpleados = new DBEmpleados();

        int cantidadDeFilas = 0;
        int posicionDeLaComa = 0;
        int indexParentesco = 0;
        int j = 0;

        ResultSet pariente = dbEmpleados.obtenerPariente(dni);
        ResultSet parentescos = dbAuxiliares.resultSetParentescos();

        String parentesco = "";
        DefaultComboBoxModel modeloComboParentesco = new DefaultComboBoxModel();
        frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);

        modeloComboParentesco.addElement("Hijo/a");
        frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);
        modeloComboParentesco.addElement("Nieto/a");
        frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);
        modeloComboParentesco.addElement("Sobrino/a");
        frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);
        modeloComboParentesco.addElement("Hermano/a");
        frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);
        modeloComboParentesco.addElement("Cuñado/a");
        frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);
        modeloComboParentesco.addElement("Conyuge");
        frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);
        modeloComboParentesco.addElement("Otro");
        frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);

        while (pariente.next()) {
            cantidadDeFilas++;
        }

//        if (cantidadDeFilas == 0) {
        String apellidoN = "" + frameModificarGrupoFamiliar.getTablaParientes().getModel().getValueAt(fila, 1);
        char[] ape = apellidoN.toCharArray();
        for (int i = 0; i < ape.length; i++) {
            String sub = "" + ape[i];
            if (sub.equals(",")) {
                posicionDeLaComa = i;
            }
        }

        String apellido = apellidoN.substring(0, posicionDeLaComa);
        String nombre = apellidoN.substring(posicionDeLaComa + 2, apellidoN.length());

        frameModificarGrupoFamiliar.setTxtApellido(apellido);
        frameModificarGrupoFamiliar.setTxtNombre(nombre);

        parentesco = "" + frameModificarGrupoFamiliar.getTablaParientes().getModel().getValueAt(fila, 3);
//            frameModificarGrupoFamiliar.getJTextFieldDni().setEnabled(true);
//        } else {
//            pariente.beforeFirst();
//            while (pariente.next()) {
//                frameModificarGrupoFamiliar.getJTextFieldDni().setEnabled(false);
//                frameModificarGrupoFamiliar.setTxtApellido(pariente.getString("apellido"));
//                frameModificarGrupoFamiliar.setTxtNombre(pariente.getString("nombre"));
//                parentesco = pariente.getString("parentesco");
//            }
//        }

        while (parentescos.next()) {
            modeloComboParentesco.addElement(parentescos.getObject("descripcionParentesco"));
            frameModificarGrupoFamiliar.getComboParentesco().setModel(modeloComboParentesco);
            if (parentescos.getObject("descripcionParentesco").equals(parentesco)) {
                indexParentesco = j;
                j++;
            }
            j++;
        }

        frameModificarGrupoFamiliar.getComboParentesco().setSelectedIndex(indexParentesco);

        pariente.beforeFirst();
        while (pariente.next()) {
            frameModificarGrupoFamiliar.setJdcFechaDeNac(pariente.getString("fechaDeNacimiento"));

        }

    }

    public static void agregarPariente(ArrayList<Pariente> gf, JTable tablaParientes) throws SQLException, ClassNotFoundException {

        String registro[] = new String[4];

        cargarElModeloDeLaTabla(tablaParientes);

        for (int i = 0; i < gf.size(); i++) {
            registro[0] = "" + String.valueOf(gf.get(i).getDni());
            registro[1] = gf.get(i).getApellido() + ", " + gf.get(i).getNombre();
            registro[2] = gf.get(i).getFechaNacString();
            registro[3] = gf.get(i).getParentesco();

            UtilFrameModificarGrupoFamiliar.modelo.addRow(registro); //Esto es hacer un this al modelo
        }
    }

    public static void cargarElModeloDeLaTabla(JTable tabla) {

        modelo = new DefaultTableModel();

        String titulos[] = {"D.N.I.", "Apellido y Nombre", "F. Nac.", "Parentesco"};
        modelo.setColumnIdentifiers(titulos);
        tabla.setModel(modelo);

        TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(30);
        columnModel.getColumn(1).setPreferredWidth(200);
        columnModel.getColumn(2).setPreferredWidth(50);
        columnModel.getColumn(2).setPreferredWidth(100);
    }

    public static int getFilaSeleccionada(FrameModificarGrupoFamiliar frameModificarGrupoFamiliar) {
        int i = -1;
        i = frameModificarGrupoFamiliar.getTablaParientes().getSelectedRow();
        if (i > -1) {
            return i;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un pariente.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public static void eliminarUnItem(int fila, JTable tablaGrupoFamiliar) {
        DefaultTableModel modelo = (DefaultTableModel) tablaGrupoFamiliar.getModel();
        modelo.removeRow(fila);
    }

    public static int obtenerLaCantidadDeFilasDeLaTabla(JTable tablaGrupoFamiliar) {
        DefaultTableModel modelo = (DefaultTableModel) tablaGrupoFamiliar.getModel();
        return modelo.getRowCount();
    }
}
