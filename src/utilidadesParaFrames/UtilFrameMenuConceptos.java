package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBConceptos;

public class UtilFrameMenuConceptos {

    private static DBConceptos dbConceptos;

    public static void buscar(JTable tablaConceptos, String palabra) throws SQLException, ClassNotFoundException {
        dbConceptos = new DBConceptos();
        ResultSet conceptos = dbConceptos.busquedaPredictivaDeAbreviatura(palabra);
        mostrarConceptosEnTablaDesdeUnResultSet(tablaConceptos, conceptos);
    }

    public static void mostrarConceptosEnTabla(JTable tablaConceptos) throws SQLException, ClassNotFoundException {

        dbConceptos = new DBConceptos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Codigo", "Abreviatura", "Descripcion", "Tipo de concepto", "Estado", "H. Rem."};
        modelo.setColumnIdentifiers(titulos);
        tablaConceptos.setModel(modelo);
        String registro[] = new String[6];
        int estado = 0;
        int esRem = 1;

        TableColumnModel columnModel = tablaConceptos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(10);
        columnModel.getColumn(1).setPreferredWidth(30);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(80);
        columnModel.getColumn(4).setPreferredWidth(20);
        columnModel.getColumn(5).setPreferredWidth(20);

        ResultSet conceptos = dbConceptos.resultSetConceptosActivos();

        while (conceptos.next()) {
            registro[0] = String.valueOf(conceptos.getInt("idConcepto"));
            registro[1] = conceptos.getString("abreviatura");
            registro[2] = conceptos.getString("descripcion");
            registro[3] = conceptos.getString("tipoDeConcepto");

            estado = conceptos.getInt("estadoConcepto");
            if (estado == 1) {
                registro[4] = "Activo";
            }

            esRem = conceptos.getInt("esRemunerativo");
            if (esRem == 1) {
                registro[5] = "Si";
            } else {
                registro[5] = "No";
            }
            modelo.addRow(registro);
        }//fin del while
    }

    public static void mostrarConceptosEnTablaDesdeUnResultSet(JTable tablaConceptos, ResultSet conceptos) throws SQLException, ClassNotFoundException {

        dbConceptos = new DBConceptos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Codigo", "Abreviatura", "Descripcion", "Tipo de concepto", "Estado", "H. Rem."};
        modelo.setColumnIdentifiers(titulos);
        tablaConceptos.setModel(modelo);
        String registro[] = new String[6];
        int estado = 0;
        int esRem = 1;

        TableColumnModel columnModel = tablaConceptos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(10);
        columnModel.getColumn(1).setPreferredWidth(30);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(80);
        columnModel.getColumn(4).setPreferredWidth(20);
        columnModel.getColumn(5).setPreferredWidth(20);

        while (conceptos.next()) {
            registro[0] = String.valueOf(conceptos.getInt("idConcepto"));
            registro[1] = conceptos.getString("abreviatura");
            registro[2] = conceptos.getString("descripcion");
            registro[3] = conceptos.getString("tipoDeConcepto");

            estado = conceptos.getInt("estadoConcepto");
            if (estado == 1) {
                registro[4] = "Activo";
            }

            esRem = conceptos.getInt("esRemunerativo");
            if (esRem == 1) {
                registro[5] = "Si";
            } else {
                registro[5] = "No";
            }
            modelo.addRow(registro);
        }//fin del while

    }
}
