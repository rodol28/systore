package utilidadesParaFrames;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.DBUsuarios;

public class UtilFrameModificarUsuario {

    private static DBUsuarios dbUsuarios;

    public static boolean actualizarElUsuario(String usuario, String clave, String repetirClave) throws SQLException, ClassNotFoundException, Exception {
        dbUsuarios = new DBUsuarios();
        if (clave.equals(repetirClave)) {
            dbUsuarios.modificarUsuario(usuario, clave);
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Las claves no coinciden.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
