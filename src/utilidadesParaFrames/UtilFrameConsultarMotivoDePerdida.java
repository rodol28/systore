package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBArticulos;
import persistencia.DBPerdidas;
import sun.swing.table.DefaultTableCellHeaderRenderer;

public class UtilFrameConsultarMotivoDePerdida {

    private static DBPerdidas dbPerdidas;
    private static DBArticulos dbArticulos;

    public static float calcularTotalDePerdida(int idPerdida) throws SQLException, ClassNotFoundException {
        float total = 0;

        dbPerdidas = new DBPerdidas();
        ResultSet lineasdePerdidas = dbPerdidas.obtenerCantidadPrecioDelArticuloPerdido(idPerdida);
        while (lineasdePerdidas.next()) {
            total = total + (lineasdePerdidas.getInt("precioUnitario") * lineasdePerdidas.getInt("cantidad"));
        }
        return total;
    }

    public static void mostrarPerdidasEnTabla(JTable tablaPerdidas, JTable lineasDePerdida, JTextField txtFecha, JTextField txtNroPerdida, int idPerdida) throws SQLException, ClassNotFoundException {
        dbPerdidas = new DBPerdidas();

        if (idPerdida == -1) {
            limpiarTablaLineasDePerdida(lineasDePerdida);
            txtNroPerdida.setText("");
            txtFecha.setText("");
        }

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Nro. Perdida", "Fecha", "Usuario", "Costo total"};
        modelo.setColumnIdentifiers(titulos);
        tablaPerdidas.setModel(modelo);
        String registro[] = new String[4];

        TableColumnModel columnModel = tablaPerdidas.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(2);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(80);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaPerdidas.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaPerdidas.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaPerdidas.getColumnModel().getColumn(2).setCellRenderer(tcr);
        tablaPerdidas.getColumnModel().getColumn(3).setCellRenderer(tcr);

        ResultSet perdidas = dbPerdidas.resultSetPerdidas();

        while (perdidas.next()) {
            registro[0] = String.valueOf(perdidas.getInt("idPerdida"));
            registro[1] = String.valueOf(perdidas.getDate("fecha"));
            registro[2] = perdidas.getString("Empleado_idEmpleado");
            registro[3] = "$ " + calcularTotalDePerdida(perdidas.getInt("idPerdida"));
            modelo.addRow(registro);
        }
    }

    public static void mostrarLineasDePerdida(JTable tablaLineasDePerdida, JTextField txtFecha, int idPerdida) throws SQLException, ClassNotFoundException {
        dbPerdidas = new DBPerdidas();
        dbArticulos = new DBArticulos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Motivo", "Cant.", "Codigo", "Descripcion", "Precio"};
        modelo.setColumnIdentifiers(titulos);
        tablaLineasDePerdida.setModel(modelo);
        String registro[] = new String[5];

        TableColumnModel columnModel = tablaLineasDePerdida.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(10);
        columnModel.getColumn(1).setPreferredWidth(5);
        columnModel.getColumn(2).setPreferredWidth(5);
        columnModel.getColumn(3).setPreferredWidth(100);
        columnModel.getColumn(4).setPreferredWidth(50);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaLineasDePerdida.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaLineasDePerdida.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaLineasDePerdida.getColumnModel().getColumn(2).setCellRenderer(tcr);
        tablaLineasDePerdida.getColumnModel().getColumn(3).setCellRenderer(tcr);
        tablaLineasDePerdida.getColumnModel().getColumn(4).setCellRenderer(tcr);

        ResultSet lineasDePerdida = dbPerdidas.obtenerDatosParaTablaLineasDePerdida(idPerdida);

        while (lineasDePerdida.next()) {
            registro[0] = lineasDePerdida.getString("descripcionMotivo");
            registro[1] = String.valueOf(lineasDePerdida.getInt("cantidad"));
            registro[2] = String.valueOf(lineasDePerdida.getInt("idArticulo"));
            registro[3] = lineasDePerdida.getString("descripcion");
            registro[4] = String.valueOf(lineasDePerdida.getInt("precioUnitario"));
            modelo.addRow(registro);
        }

        mostrarFecha(idPerdida, txtFecha);
    }

    public static void mostrarFecha(int idPerdida, JTextField txtFecha) throws SQLException, ClassNotFoundException {
        dbPerdidas = new DBPerdidas();
        txtFecha.setText(dbPerdidas.obtenerLaFechaDePerdida(idPerdida));
    }

    public static int getCodigoIngresado() {
        int codigo = -1;
        String txtCodigo = "";
        txtCodigo = "" + JOptionPane.showInputDialog("Ingrese el nro de la perdida: ");
        if (txtCodigo.equals("") == false) {
            codigo = Integer.parseInt(txtCodigo);
            return codigo;
        }
        return codigo;
    }

    public static void mostrarPerdidaNroXEnTabla(JTable tablaPerdidas, JTable tablaLineasDePerdida, JTextField txtFecha, int idPerdida) throws SQLException, ClassNotFoundException {
        dbPerdidas = new DBPerdidas();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Nro. Perdida", "Fecha", "Usuario", "Costo total"};
        modelo.setColumnIdentifiers(titulos);
        tablaPerdidas.setModel(modelo);
        String registro[] = new String[4];

        TableColumnModel columnModel = tablaPerdidas.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(2);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(80);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaPerdidas.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaPerdidas.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaPerdidas.getColumnModel().getColumn(2).setCellRenderer(tcr);
        tablaPerdidas.getColumnModel().getColumn(3).setCellRenderer(tcr);

        ResultSet perdidas = dbPerdidas.obtenerPerdidaNroX(idPerdida);

        while (perdidas.next()) {
            registro[0] = String.valueOf(perdidas.getInt("idPerdida"));
            registro[1] = String.valueOf(perdidas.getDate("fecha"));
            registro[2] = perdidas.getString("Empleado_idEmpleado");
            registro[3] = "$ " + calcularTotalDePerdida(perdidas.getInt("idPerdida"));
            modelo.addRow(registro);
        }

        mostrarLineasDePerdida(tablaLineasDePerdida, txtFecha, idPerdida);
    }

    public static void limpiarTablaLineasDePerdida(JTable tablaLineasDePerdida) {
        DefaultTableModel modelo = (DefaultTableModel) tablaLineasDePerdida.getModel();
        for (int fila = tablaLineasDePerdida.getRowCount() - 1; fila >= 0; fila--) {
            modelo.removeRow(fila);
        }
    }
}
