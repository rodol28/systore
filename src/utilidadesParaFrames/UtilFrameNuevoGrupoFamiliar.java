package utilidadesParaFrames;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import objetosDelDominio.Pariente;
import vistas.FrameNuevoGrupoFamiliar;

public class UtilFrameNuevoGrupoFamiliar {

    private static DefaultTableModel modelo;
    private static int contadorDeParientes = 0;

    public static void agregarPariente(Pariente pariente, JTable tablaParientes) {

        String registro[] = new String[4];

        //Si es primer pariente que es agragado se carga el modelo
        //de la tabla sino no debe ser cargado.
        if (contadorDeParientes == 0) {
            cargarElModeloDeLaTabla(tablaParientes);
            contadorDeParientes++;
        }

        registro[0] = "" + String.valueOf(pariente.getDni());
        registro[1] = pariente.getApellido() + ", " + pariente.getNombre();
        registro[2] = pariente.getFechaNacString();
        registro[3] = pariente.getParentesco();

        UtilFrameNuevoGrupoFamiliar.modelo.addRow(registro); //Esto es hacer un this al modelo
    }

    public static void cargarElModeloDeLaTabla(JTable tabla) {

        modelo = new DefaultTableModel();

        String titulos[] = {"D.N.I.", "Apellido y Nombre", "F. Nac.", "Parentesco"};
        modelo.setColumnIdentifiers(titulos);
        tabla.setModel(modelo);

        TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(30);
        columnModel.getColumn(1).setPreferredWidth(200);
        columnModel.getColumn(2).setPreferredWidth(50);
        columnModel.getColumn(2).setPreferredWidth(100);
    }

    public static void setContadorDeParientes(int contadorDeParientes) {
        UtilFrameNuevoGrupoFamiliar.contadorDeParientes = contadorDeParientes;
    }

    public static void mostrarLosParientesCargados(ArrayList<Pariente> grupoFamiliar, JTable tablaParientes) {

        String registro[] = new String[4];

        //Si es primer pariente que es agragado se carga el modelo
        //de la tabla sino no debe ser cargado.
        if (contadorDeParientes == 0) {
            cargarElModeloDeLaTabla(tablaParientes);
            contadorDeParientes++;
        }
        for (int i = 0; i < grupoFamiliar.size(); i++) {
            registro[0] = "" + String.valueOf(grupoFamiliar.get(i).getDni());
            registro[1] = grupoFamiliar.get(i).getApellido() + ", " + grupoFamiliar.get(i).getNombre();
            registro[2] = grupoFamiliar.get(i).getFechaNacString();
            registro[3] = grupoFamiliar.get(i).getParentesco();
            UtilFrameNuevoGrupoFamiliar.modelo.addRow(registro); //Esto es hacer un this al modelo
        }
    }

    public static int getFilaSeleccionada(FrameNuevoGrupoFamiliar frameNuevoGrupoFamiliar) {
        int i = -1;
        i = frameNuevoGrupoFamiliar.getTablaParientes().getSelectedRow();
        if (i > -1) {
            return i;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un pariente.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public static void eliminarUnItem(int fila, JTable tablaGrupoFamiliar) {
        DefaultTableModel modelo = (DefaultTableModel) tablaGrupoFamiliar.getModel();
        modelo.removeRow(fila);
    }
}
