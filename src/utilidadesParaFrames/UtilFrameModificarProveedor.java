package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import objetosDelDominio.Domicilio;
import objetosDelDominio.Proveedor;
import persistencia.DBArticulos;
import persistencia.DBAuxiliares;
import persistencia.DBProveedores;
import vistas.FrameModificarProveedor;

public class UtilFrameModificarProveedor {

    private static DBArticulos dbArticulos;
    private static DBAuxiliares dbAuxiliares;
    private static DBProveedores dbProveedores;
    private static Proveedor proveedor;

    public static void cargarLosCampos(FrameModificarProveedor frameModificarProveedor, String rubroPro, String provinciaPro, int idProveedor) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();
        dbArticulos = new DBArticulos();
        dbProveedores = new DBProveedores();

        proveedor = dbProveedores.obtenerElProveedor(idProveedor);

        int indexRubro = 0;
        int j = 0;
        int indexTipoDeCuit = 0;
        int l = 0;

        String nombreTipoDeCuit = "";

        DefaultComboBoxModel modeloComboRubro = new DefaultComboBoxModel();
        DefaultComboBoxModel modeloComboTipoDeCuit = new DefaultComboBoxModel();

        frameModificarProveedor.getComboRubros().setModel(modeloComboRubro);
        frameModificarProveedor.getComboCuit().setModel(modeloComboTipoDeCuit);

        ResultSet rubros = dbAuxiliares.resultSetRubros();
        ResultSet tiposDeCuit = dbAuxiliares.resultSetCompletoTipoDeCuit();

        while (rubros.next()) {
            modeloComboRubro.addElement(rubros.getObject("descripcion"));
            frameModificarProveedor.getComboRubros().setModel(modeloComboRubro);
            if (rubros.getObject("descripcion").equals(rubroPro)) {
                indexRubro = j;
                j++;
            }
            j++;
        }

        nombreTipoDeCuit = dbAuxiliares.buscarTipoCuitPorID(proveedor.getTipoDeCuit());

        while (tiposDeCuit.next()) {
            modeloComboTipoDeCuit.addElement(tiposDeCuit.getObject("descripcion"));
            frameModificarProveedor.getComboCuit().setModel(modeloComboTipoDeCuit);
            if (tiposDeCuit.getObject("descripcion").equals(nombreTipoDeCuit)) {
                indexTipoDeCuit = l;
                l++;
            }
            l++;
        }

        frameModificarProveedor.setTxtCodigoPro("" + idProveedor);
        frameModificarProveedor.setTxtRazonSocial(proveedor.getRazonSocial());
        frameModificarProveedor.setTxtDomicilio(proveedor.getDomicilio());
        frameModificarProveedor.setTxtPais(proveedor.getPais());
        frameModificarProveedor.setTxtProvincia(proveedor.getProvincia());
        frameModificarProveedor.setTxtLocalidad(proveedor.getLocalidad());
        frameModificarProveedor.setTxtCodPostal("" + proveedor.getCodigoPostal());
        frameModificarProveedor.setTxtCuit(proveedor.getNroCUIT());
        frameModificarProveedor.setTxtCelular(proveedor.getCelular());
        frameModificarProveedor.setTxtTelefono(proveedor.getTelefono());
        frameModificarProveedor.setTxtMail(proveedor.getEmail());
        frameModificarProveedor.setTxtWeb(proveedor.getWeb());
        frameModificarProveedor.setTxtObserv(proveedor.getObservacion());

        frameModificarProveedor.getComboRubros().setSelectedIndex(indexRubro);
        frameModificarProveedor.getComboCuit().setSelectedIndex(indexTipoDeCuit);
    }

    public static void actualizarElProveedor(FrameModificarProveedor frameModificarProveedor) throws SQLException, ClassNotFoundException {
        dbProveedores = new DBProveedores();
        dbAuxiliares = new DBAuxiliares();

        Proveedor proveedor = new Proveedor(frameModificarProveedor.getTxtCodigoPro(),
                frameModificarProveedor.getTxtRazonSocial(),
                frameModificarProveedor.getTxtTelefono(),
                frameModificarProveedor.getTxtCelular(),
                frameModificarProveedor.getTxtMail(),
                frameModificarProveedor.getTxtCuit(),
                frameModificarProveedor.getTxtWeb(),
                frameModificarProveedor.getTxtObserv(),
                dbAuxiliares.buscarCuitPorDescripcion(frameModificarProveedor.getCuitSeleccionado()),
                dbAuxiliares.buscarRubroPorDescripcion(frameModificarProveedor.getRubroSeleccionado()),
                frameModificarProveedor.getTxtDomicilio(),
                frameModificarProveedor.getTxtPais(),
                frameModificarProveedor.getTxtProvincia(),
                frameModificarProveedor.getTxtLocalidad(),
                frameModificarProveedor.getTxtCodPostal());

        dbProveedores.modificarProveedor(proveedor);
    }

}
