package utilidadesParaFrames;

import objetosDelDominio.Puesto;
import persistencia.DBPuestos;
import vistas.FrameNuevoPuesto;

public class UtilFrameNuevoPuesto {

    private static DBPuestos dbPuestos;
    private static Puesto puesto;

    public static void registrarPuesto(FrameNuevoPuesto frameNuevoPuesto) throws ClassNotFoundException {
        
        dbPuestos = new DBPuestos();
                
        puesto = new Puesto(frameNuevoPuesto.getTxtDescripcion(),
                frameNuevoPuesto.getTxtSueldoBasico());
        
        dbPuestos.altaDePuesto(puesto);
    }
}
