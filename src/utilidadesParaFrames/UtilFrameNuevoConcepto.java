package utilidadesParaFrames;

import java.sql.SQLException;
import javax.swing.JTextField;
import objetosDelDominio.Concepto;
import persistencia.DBConceptos;

public class UtilFrameNuevoConcepto {

    private static DBConceptos dbConceptos;

    public static void mostraElIdNuevo(JTextField txtCodigo) throws SQLException, ClassNotFoundException {
        dbConceptos = new DBConceptos();
        int ultimoId = dbConceptos.obtenerElUltimoIdConcepto();
        int idNuevo = ultimoId + 1;
        txtCodigo.setText(idNuevo + "");
    }

    public static void registrarConcepto(Concepto concepto) throws ClassNotFoundException {
        dbConceptos = new DBConceptos();
        dbConceptos.altaDeConcepto(concepto);
    }
}
