package utilidadesParaFrames;

import java.sql.SQLException;
import javax.swing.JTextField;
import objetosDelDominio.Domicilio;
import objetosDelDominio.Empleado;
import persistencia.DBAuxiliares;
import persistencia.DBEmpleados;
import persistencia.DBPuestos;
import vistas.FrameNuevoEmpleado;

public class UtilFrameNuevoEmpleado {

    private static DBEmpleados dbEmpleados;
    private static DBAuxiliares dbAuxiliares;
    private static DBPuestos dbPuestos;

    public static void mostraElLegajoNuevo(JTextField txtLegajo) throws SQLException, ClassNotFoundException {
        dbEmpleados = new DBEmpleados();
        int ultimoLegajo = dbEmpleados.obtenerElUltimoLegajo();
        int legajoNuevo = ultimoLegajo + 1;
        txtLegajo.setText(legajoNuevo + "");
    }

    public static void registrarEmpleado(Empleado empleado, FrameNuevoEmpleado frameNuevoEmpleado) throws SQLException, ClassNotFoundException {

        dbAuxiliares = new DBAuxiliares();
        dbEmpleados = new DBEmpleados();
        dbPuestos = new DBPuestos();

        empleado.setApellido(frameNuevoEmpleado.getTxtApellido());
        empleado.setNombre(frameNuevoEmpleado.getTxtNombre());
        empleado.setEmail(frameNuevoEmpleado.getTxtMail());
        empleado.setTelefono(frameNuevoEmpleado.getTxtTelefono());

        empleado.setDni(frameNuevoEmpleado.getTxtDni());
        empleado.setFechaNacString("" + frameNuevoEmpleado.getJdcFechaDeNac());
        empleado.setSexo(frameNuevoEmpleado.getSexoSeleccionado());

        empleado.setNroCuil(frameNuevoEmpleado.getTxtCuil());
        empleado.setFechaIngString("" + frameNuevoEmpleado.getJdcFechaDeIng());
        empleado.setIdPuesto(dbPuestos.buscarPuestoPorDescripcion(frameNuevoEmpleado.getPuestoSeleccionado()));
        empleado.setIdObraSocial(dbAuxiliares.buscarObraSocialPorRazonSocial(frameNuevoEmpleado.getObraSocialSeleccionada()));

        empleado.setDomicilio(frameNuevoEmpleado.getTxtDomicilio());
        empleado.setPais(frameNuevoEmpleado.getTxtPais());
        empleado.setProvincia(frameNuevoEmpleado.getTxtProvincia());
        empleado.setLocalidad(frameNuevoEmpleado.getTxtLocalidad());
        empleado.setCodigoPostal(frameNuevoEmpleado.getTxtCodPostal());
        empleado.setNacionalidad(frameNuevoEmpleado.getTxtNacionalidad());
        empleado.setEstadoCivil(dbAuxiliares.buscarEstadoCivilPorDescripicion(frameNuevoEmpleado.getEstadoCivilSeleccionado()));

        empleado.setRazonSocialBanco(frameNuevoEmpleado.getTxtBanco());
        empleado.setCuentaBancaria(frameNuevoEmpleado.getTxtCuentaBancaria());

        dbEmpleados.altaDeEmpleado(empleado);
    }
}
