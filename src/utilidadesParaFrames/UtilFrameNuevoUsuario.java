package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import objetosDelDominio.Usuario;
import persistencia.DBEmpleados;
import persistencia.DBUsuarios;
import vistas.FrameNuevoUsuario;

public class UtilFrameNuevoUsuario {

    private static DBUsuarios dbUsuarios;
    private static DBEmpleados dbEmpleados;

    public static void llenarComboTipoDeUsuario(JComboBox<String> comboTipoDeUsuario) throws SQLException, ClassNotFoundException {
        dbUsuarios = new DBUsuarios();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un tipo de usuario");//es el primer registro q mostrara el combo
        comboTipoDeUsuario.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet usuarios = dbUsuarios.resultSetUsuarios();

        while (usuarios.next()) {
            modeloCombo.addElement(usuarios.getObject("descripcion"));
            comboTipoDeUsuario.setModel(modeloCombo);
        }
    }

    public static void llenarElComboEmpleados(JComboBox<String> comboEmpleados) throws SQLException, ClassNotFoundException {
        dbEmpleados = new DBEmpleados();

        String nombre = "";
        String apellido = "";
        String legajo = "";

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un empleado");//es el primer registro q mostrara el combo
        comboEmpleados.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet empleados = dbEmpleados.obtenerLegajoNombreApellidoDeLosEmpleados();

        while (empleados.next()) {
            legajo = String.valueOf(empleados.getInt("legajo"));
            apellido = empleados.getString("apellido");
            nombre = empleados.getString("nombre");
            modeloCombo.addElement(legajo + " - " + apellido + ", " + nombre);
            comboEmpleados.setModel(modeloCombo);
        }
    }

    public static boolean registrarUsuario(FrameNuevoUsuario frameNuevoUsuario) throws ClassNotFoundException, SQLException, Exception {
        dbUsuarios = new DBUsuarios();
        dbEmpleados = new DBEmpleados();

        int legajo = Integer.parseInt(frameNuevoUsuario.getEmpleadoSeleccionado().substring(0, 2));
        int idEmpleado = dbEmpleados.obtenerIdEmpleadoPorLegajo(legajo);

        String user = frameNuevoUsuario.getTxtNombre();
        String pass = frameNuevoUsuario.getTxtClave();
        String pass1 = frameNuevoUsuario.getTxtRepetirClave();

        if (dbUsuarios.verificarLaExisteciaDeUnUsuarioParaElEmpleado(idEmpleado)) {
            JOptionPane.showMessageDialog(null, "El empleado ya tiene un usuario registrado.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        } else {
            if (dbUsuarios.buscarUsuarioPorNombreDeUsuario(user)) {
                JOptionPane.showMessageDialog(null, "El nombre de usuario ya esta registrado.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
                return false;
            } else {
                if (pass.equals(pass1) == true) {
                    Usuario usuario = new Usuario(user,
                            pass,
                            idEmpleado,
                            dbUsuarios.buscarTipoDeUsuarioPorDescripcion(frameNuevoUsuario.getTipoDeUsuarioSeleccionado()));

                    dbUsuarios.altaDeUsuario(usuario);
                    return true;
                } else {
                    JOptionPane.showMessageDialog(null, "Las claves no coinciden.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            }
        }
        return false;
    }
}
