package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBUsuarios;
import sun.swing.table.DefaultTableCellHeaderRenderer;

public class UtilFrameMenuUsuarios {

    private static DBUsuarios dbUsuarios;

    public static void mostrarUsuarriosEnTabla(JTable tablaListaDeUsuarios) throws SQLException, ClassNotFoundException {
        dbUsuarios = new DBUsuarios();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Legajo", "Empleado", "Nombre de usuario", "Tipo de usuario"};
        modelo.setColumnIdentifiers(titulos);
        tablaListaDeUsuarios.setModel(modelo);
        String registro[] = new String[4];

        TableColumnModel columnModel = tablaListaDeUsuarios.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(15);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(80);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaListaDeUsuarios.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaListaDeUsuarios.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaListaDeUsuarios.getColumnModel().getColumn(2).setCellRenderer(tcr);
        tablaListaDeUsuarios.getColumnModel().getColumn(3).setCellRenderer(tcr);

        ResultSet usuarios = dbUsuarios.resultSetDatosDeUsuarios();

        while (usuarios.next()) {
            registro[0] = String.valueOf(usuarios.getInt("legajo"));
            registro[1] = usuarios.getString("apellido") + ", " + usuarios.getString("nombre");
            registro[2] = usuarios.getString("user");
            registro[3] = usuarios.getString("descripcion");
            modelo.addRow(registro);
        }
    }
}
