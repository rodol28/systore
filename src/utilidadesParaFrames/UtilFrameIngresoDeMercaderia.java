package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import objetosDelDominio.Articulo;
import objetosDelDominio.Compra;
import objetosDelDominio.LineaDeCompra;
import persistencia.DBArticulos;
import persistencia.DBAuxiliares;
import persistencia.DBCompras;
import persistencia.DBProveedores;
import vistas.FrameIngresoDeMercaderia;

public class UtilFrameIngresoDeMercaderia {

    private static Compra compra;
    private static LineaDeCompra lineaDeCompra;
    private static Articulo articulo;
    private static DBAuxiliares dbAuxiliares;
    private static DBCompras dbCompras;
    private static DBArticulos dbArticulos;
    private static DBProveedores dbProveedores;
    private static DefaultTableModel modelo;
    //Variable bandera que indica que un articulo ya fue agregado a la tabla
    //y que lo tanto el modelo de la tabla ya se cargo y no es necesario volver a cargarlo.
    private static int contadorDeArticulo = 0;

    public static void mostraElCodigoDelNuevoArticulo(FrameIngresoDeMercaderia frameIngresoDeMercaderia) throws SQLException, ClassNotFoundException {
        dbCompras = new DBCompras();
        int intCodigo = dbCompras.obtenerElUltimoIdCompra() + 1;
        frameIngresoDeMercaderia.setTxtNroCompra(intCodigo + "");
    }

    public static void llenarComboProveedores(JComboBox<String> comboProveedores) throws SQLException, ClassNotFoundException {
        dbProveedores = new DBProveedores();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un proveedor");//es el primer registro q mostrara el combo
        comboProveedores.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet proveedores = dbProveedores.resultSetProveedores();

        while (proveedores.next()) {
            modeloCombo.addElement(proveedores.getObject("razonSocial"));
            comboProveedores.setModel(modeloCombo);
        }
    }

    public static int getFilaSeleccionada(FrameIngresoDeMercaderia frameIngresoDeMercaderia) {
        int i = -1;
        i = frameIngresoDeMercaderia.getTablaArticulos().getSelectedRow();
        if (i > -1) {
            return i;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public static int getCantidadIngresada(int i) {
        int cantidad = 1;
        String c = "1";
        if (i > -1) {
            c = JOptionPane.showInputDialog("Ingrese cantidad:");
            if (c != null) {
                cantidad = Integer.parseInt(c);
                return cantidad;
            }
        }
        return cantidad;
    }

    public static void agregarArticuloEnTabla(int idArticulo, JTable tablaArticulos, boolean metodoDeIngreso) throws SQLException, ClassNotFoundException {
        /*El parametro "metodoDeIngreso" recibido en es metodo indica la forma en que
        se esta ingresando en articulo.
        - true ---> ingresando el codigo y presionando enter.
        - false ---> ingresando atraves de la frame auxiliar.*/
        dbArticulos = new DBArticulos();

        String registro[] = new String[8];

        articulo = new Articulo();
        articulo = dbArticulos.obtenerElArticulo(idArticulo);

        if (articulo != null) {//Si es primer producto que es agragado se carga el modelo
            //de la tabla sino no debe ser cargado.
            if (contadorDeArticulo == 0) {
                cargarElModeloDeLaTabla(tablaArticulos);
                contadorDeArticulo++;
            }

            int stockNuevo = articulo.getStock() + 1;
            //int cantidad = getCantidadIngresada(1);

            registro[0] = "1";
            registro[1] = articulo.getCodigo() + "";
            registro[2] = articulo.getDescripcion();
            registro[3] = dbArticulos.obtenerMarcaDelArticulo(articulo.getMarca());
            registro[4] = articulo.getCosto() + "";
            registro[5] = articulo.getPrecioUnitario() + "";
            registro[6] = articulo.getStock() + "";
            registro[7] = stockNuevo + "";
            UtilFrameIngresoDeMercaderia.modelo.addRow(registro); //Esto es hacer un this al modelo
        } else {
            JOptionPane.showMessageDialog(null, "No se encontro un articulo con el codigo ingresado.");
        }
    }

    public static void cargarElModeloDeLaTabla(JTable tabla) {

        modelo = new DefaultTableModel();

        String titulos[] = {"Cant.", "Cod.", "Descripción", "Marca", "Costo", "Precio de venta", "Stock Ant.", "Actual"};
        modelo.setColumnIdentifiers(titulos);
        tabla.setModel(modelo);

        TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(30);
        columnModel.getColumn(1).setPreferredWidth(5);
        columnModel.getColumn(2).setPreferredWidth(200);
    }

    public static void setContadorDeArticulo(int contadorDeArticulo) {
        UtilFrameIngresoDeMercaderia.contadorDeArticulo = contadorDeArticulo;
    }

    /*Actuliza la tabla de articulos con la cantidad adquira.
    Modifica las columnas cantidad y stock actual*/
    public static void actualizarCantidadStockActual(int cantidad, int fila, JTable tablaArticulos) {
        int stockAnt = Integer.parseInt((String) tablaArticulos.getModel().getValueAt(fila, 6));
        int stockNuevo = stockAnt + cantidad;
        tablaArticulos.getModel().setValueAt(cantidad, fila, 0);
        tablaArticulos.getModel().setValueAt(stockNuevo, fila, 7);
    }

    public static void actualizarPrecioCosto(float costo, float precio, int fila, JTable tablaArticulos) {
        tablaArticulos.getModel().setValueAt(costo, fila, 4);
        tablaArticulos.getModel().setValueAt(precio, fila, 5);
    }

    public static void eliminarUnItem(int fila, JTable tablaArticulos) {
        DefaultTableModel modelo = (DefaultTableModel) tablaArticulos.getModel();
        modelo.removeRow(fila);
    }

    public static void registrarCompra(FrameIngresoDeMercaderia frameIngresoDeMercaderia) throws ClassNotFoundException, SQLException {
        int codigo = 0;
        float precio = 0;
        float costo = 0;
        int stockNuevo = 0;
        int cantidad = 0;
        int idProveedor = 0;

        dbCompras = new DBCompras();
        dbProveedores = new DBProveedores();

        idProveedor = dbProveedores.buscarProveedorPorRazonSocial("" + frameIngresoDeMercaderia.getComboProvedores().getSelectedItem());

        /*Se crea la compra*/
        compra = new Compra(frameIngresoDeMercaderia.getTxtNroCompra(), "" + frameIngresoDeMercaderia.getJdcFechaDeCompra(), idProveedor);

        /*Se crean las lineas de compra*/
        for (int i = 0; i < frameIngresoDeMercaderia.getTablaArticulos().getRowCount(); i++) {
            codigo = Integer.parseInt("" + frameIngresoDeMercaderia.getTablaArticulos().getModel().getValueAt(i, 1));
            precio = Float.parseFloat("" + frameIngresoDeMercaderia.getTablaArticulos().getModel().getValueAt(i, 5));
            costo = Float.parseFloat("" + frameIngresoDeMercaderia.getTablaArticulos().getModel().getValueAt(i, 4));
            stockNuevo = Integer.parseInt("" + frameIngresoDeMercaderia.getTablaArticulos().getModel().getValueAt(i, 7));
            cantidad = Integer.parseInt("" + frameIngresoDeMercaderia.getTablaArticulos().getModel().getValueAt(i, 0));

            articulo = new Articulo(codigo, precio, costo, stockNuevo);
            lineaDeCompra = new LineaDeCompra(articulo, cantidad);
            compra.agregarLineaDeCompra(lineaDeCompra);
        }
        compra.calcularTotal();
        dbCompras.altaDeCompra(compra);
    }
    
    public static int obtenerLaCantidadDeFilasDeLaTabla(JTable tablaArticulos) {
        DefaultTableModel modelo = (DefaultTableModel) tablaArticulos.getModel();
        return modelo.getRowCount();
    }
}
