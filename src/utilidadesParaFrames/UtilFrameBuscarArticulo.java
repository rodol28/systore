package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import persistencia.DBArticulos;

public class UtilFrameBuscarArticulo {

    private static DBArticulos dbArticulos;

    public static void mostrarArticulosEnTabla(JTable tablaArticulos) throws SQLException, ClassNotFoundException {
        dbArticulos = new DBArticulos();
        ResultSet articulos = dbArticulos.resultSetArticulos();
        cargarModeloEnTabla(articulos, tablaArticulos);
    }

    public static void buscar(JTable tablaArticulos, String palabra) throws SQLException, ClassNotFoundException {
        dbArticulos = new DBArticulos();
        ResultSet articulos = dbArticulos.busquedaPredictivaDeDescripcion(palabra);
        cargarModeloEnTabla(articulos, tablaArticulos);
    }

    public static void cargarModeloEnTabla(ResultSet articulos, JTable tablaArticulos) throws SQLException, ClassNotFoundException {

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Codigo", "Descripción", "Marca", "Stock"};
        modelo.setColumnIdentifiers(titulos);
        tablaArticulos.setModel(modelo);
        String registro[] = new String[4];

        while (articulos.next()) {
            registro[0] = String.valueOf(articulos.getInt("idArticulo"));
            registro[1] = articulos.getString("descripcion");
            registro[2] = dbArticulos.obtenerMarcaDelArticulo(articulos.getInt("Marca_idMarca"));
            registro[3] = String.valueOf(articulos.getInt("stock"));
            modelo.addRow(registro);
        }
    }
}
