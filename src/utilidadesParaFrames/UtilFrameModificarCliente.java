package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import objetosDelDominio.Cliente;
import objetosDelDominio.Domicilio;
import persistencia.DBAuxiliares;
import persistencia.DBClientes;
import vistas.FrameModificarCliente;

public class UtilFrameModificarCliente {

    private static Cliente cliente;
    private static DBClientes dbClientes;
    private static DBAuxiliares dbAuxiliares;

    public static void cargarLosCampos(FrameModificarCliente frameModificarCliente, String tipoDeCuit, int idCliente) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();
        dbClientes = new DBClientes();
        cliente = dbClientes.obtenerElCliente(idCliente);
        
        int indexTipoDeCuit = 0;
        int l = 0;
        
        DefaultComboBoxModel modeloComboTipoDeCuit = new DefaultComboBoxModel();
        frameModificarCliente.getComboCuit().setModel(modeloComboTipoDeCuit);
        ResultSet tiposDeCuit = dbAuxiliares.resultSetCompletoTipoDeCuit();

        String nombreTipoDeCuit = "";
        
        nombreTipoDeCuit = dbAuxiliares.buscarTipoCuitPorID(cliente.getTipoDeCuit());
        
        while (tiposDeCuit.next()) {
            modeloComboTipoDeCuit.addElement(tiposDeCuit.getObject("descripcion"));
            frameModificarCliente.getComboCuit().setModel(modeloComboTipoDeCuit);
            if (tiposDeCuit.getObject("descripcion").equals(nombreTipoDeCuit)) {
                indexTipoDeCuit = l;
                l++;
            }
            l++;
        }

        frameModificarCliente.setTxtCodigoCli("" + idCliente);
        frameModificarCliente.setTxtNombre(cliente.getNombre());
        frameModificarCliente.setTxtApellido(cliente.getApellido());
        frameModificarCliente.setTxtDomilicio(cliente.getDomicilio());
        frameModificarCliente.setTxtPais(cliente.getPais());
        frameModificarCliente.setTxtProvincia(cliente.getProvincia());
        frameModificarCliente.setTxtLocalidad(cliente.getLocalidad());
        frameModificarCliente.setTxtCodPostal("" + cliente.getCodigoPostal());
        frameModificarCliente.setTxtNroCuit(cliente.getNroCuit());
        frameModificarCliente.setTxtTelefono(cliente.getTelefono());
        frameModificarCliente.setTxtMail(cliente.getEmail());
        frameModificarCliente.setTxtObser(cliente.getObservacion());
        
        frameModificarCliente.getComboCuit().setSelectedIndex(indexTipoDeCuit);

    }

    public static void actualizarElCliente(FrameModificarCliente frameModificarCliente) throws SQLException, ClassNotFoundException {
        dbClientes = new DBClientes();
        dbAuxiliares = new DBAuxiliares();
        
        Cliente cliente = new Cliente(frameModificarCliente.getTxtCodigoCli(),
                frameModificarCliente.getTxtNroCuit(),
                frameModificarCliente.getTxtObser(),
                dbAuxiliares.buscarCuitPorDescripcion(frameModificarCliente.getCuitSeleccionado()),
                frameModificarCliente.getTxtDomicilio(),
                frameModificarCliente.getTxtPais(),
                frameModificarCliente.getTxtProvincia(),
                frameModificarCliente.getTxtLocalidad(),
                frameModificarCliente.getTxtCodPostal(),
                frameModificarCliente.getTxtNombre(),
                frameModificarCliente.getTxtApellido(),
                frameModificarCliente.getTxtMail(),
                frameModificarCliente.getTxtTelefono());

        dbClientes.modificarCliente(cliente);
    }

}
