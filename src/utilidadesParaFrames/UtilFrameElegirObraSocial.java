package utilidadesParaFrames;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import objetosDelDominio.Empleado;
import vistasAuxiliares.FrameElegirObraSocial;

public class UtilFrameElegirObraSocial {

    private static DefaultTableModel modelo;
    private static int contadorDeObrasSociales = 0;

    public static void mostrarObrasSocialesDelEmpleado(JTable tablaParientes, Empleado empleado) throws SQLException, ClassNotFoundException {
//        dbAuxiliares = new DBAuxiliares();
        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Razon social"};
        modelo.setColumnIdentifiers(titulos);
        tablaParientes.setModel(modelo);
        String registro[] = new String[1];

        TableColumnModel columnModel = tablaParientes.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(100);
//
//        for (int i = 0; i < empleado.getObrasSociales().size(); i++) {
//            registro[0] = "" + String.valueOf(empleado.getObrasSociales().get(i));
//            modelo.addRow(registro);
//        }
    }

    public static void agregarObraSocial(ArrayList<String> os, JTable tablaObrasSociales) throws SQLException, ClassNotFoundException {

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Razon social"};
        modelo.setColumnIdentifiers(titulos);
        tablaObrasSociales.setModel(modelo);
        String registro[] = new String[1];

        TableColumnModel columnModel = tablaObrasSociales.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(100);

        for (int i = 0; i < os.size(); i++) {
            registro[0] = "" + os.get(i);
            modelo.addRow(registro);
        }
    }

    public static void cargarElModeloDeLaTabla(JTable tabla) {

        modelo = new DefaultTableModel();

        String titulos[] = {"Razon social"};
        modelo.setColumnIdentifiers(titulos);
        tabla.setModel(modelo);

        TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(100);
    }

    public static void setContadorDeObrasSociales(int contadorDeObrasSociales) {
        UtilFrameElegirObraSocial.contadorDeObrasSociales = contadorDeObrasSociales;
    }

    public static int getFilaSeleccionada(FrameElegirObraSocial frameElegirObraSocial) {
        int i = -1;
        i = frameElegirObraSocial.getTablaObrasSociales().getSelectedRow();
        if (i > -1) {
            return i;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar una obra social.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public static void eliminarUnItem(int fila, JTable tablaObrasSociales) {
        DefaultTableModel modelo = (DefaultTableModel) tablaObrasSociales.getModel();
        modelo.removeRow(fila);
    }

    public static int obtenerLaCantidadDeFilasDeLaTabla(JTable tablaObrasSociales) {
        DefaultTableModel modelo = (DefaultTableModel) tablaObrasSociales.getModel();
        return modelo.getRowCount();
    }
}
