package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import objetosDelDominio.ConceptoLiquidado;
import objetosDelDominio.Sueldo;
import objetosDelDominio.DetalleSueldo;
import otros.HorasExtra;
import otros.ModeloDeTablaLegajos;
import persistencia.DBConceptos;
import persistencia.DBEmpleados;
import persistencia.DBLiquidacion;
import persistencia.DBPuestos;
import persistencia.DBReciboSueldo;
import sun.swing.table.DefaultTableCellHeaderRenderer;
import vistas.FrameLiquidacion;

public class UtilFrameLiquidacion {

    private static DBConceptos dbConceptos;
    private static DBEmpleados dbEmpleados;
    private static DBPuestos dbPuestos;
    private static DBLiquidacion dbLiquidacion;
    private static DBReciboSueldo dbReciboSueldo;

    public static void mostraElCodigoDeLaNuevaLiquidacion(FrameLiquidacion frameLiquidacion) throws SQLException, ClassNotFoundException {
        dbLiquidacion = new DBLiquidacion();
        int codigo = dbLiquidacion.obtenerElUltimoIdLiquidacion() + 1;
        frameLiquidacion.setTxtNroLiq(codigo + "");
    }

    public static void mostrarEmpleadosEnTabla(JTable tablalegajos) throws SQLException, ClassNotFoundException {
        dbEmpleados = new DBEmpleados();
        DefaultTableModel modelo = cargarModeloDeTablaLegajos(tablalegajos);

        Object registro[] = new Object[4];

        ResultSet empleados = dbEmpleados.obtenerLosDatosDeLosEmplados();

        while (empleados.next()) {
            registro[0] = false;
            registro[1] = empleados.getInt("empleado_legajo");
            registro[2] = empleados.getString("persona_apellido") + ", " + empleados.getString("persona_nombre");
            registro[3] = empleados.getString("empleado_cuil");
            modelo.addRow(registro);
        }
    }

    public static void mostrarLosEmpleadosDeUnPuestoEnTabla(JTable tablalegajos, String puesto, JTable tablaConceptos) throws SQLException, ClassNotFoundException {
        dbEmpleados = new DBEmpleados();

        int idPuesto = UtilFrameLiquidacion.buscarElPuestoPorDescripcion(puesto);
        DefaultTableModel modelo = cargarModeloDeTablaLegajos(tablalegajos);

        Object registro[] = new Object[4];

        UtilFrameLiquidacion.cargarModeloDeLaTablaConceptos(tablaConceptos);
        if (puesto.equals("Mostrar todos")) {
            UtilFrameLiquidacion.mostrarEmpleadosEnTabla(tablalegajos);
        } else {
            ResultSet empleados = dbEmpleados.obtenerLosDatosDeLosEmpladosDeUnPuestoParticular(idPuesto);
            while (empleados.next()) {
                registro[0] = false;
                registro[1] = empleados.getInt("empleado_legajo");
                registro[2] = empleados.getString("persona_apellido") + ", " + empleados.getString("persona_nombre");
                registro[3] = empleados.getString("empleado_cuil");
                modelo.addRow(registro);
            }
        }

    }

    public static DefaultTableModel cargarModeloDeTablaLegajos(JTable tablaLegajos) {

        DefaultTableModel modelo = new ModeloDeTablaLegajos();

        modelo.addColumn(modelo.getColumnClass(0));
        modelo.addColumn(modelo.getColumnClass(1));
        modelo.addColumn(modelo.getColumnClass(2));
        modelo.addColumn(modelo.getColumnClass(3));

        String titulos[] = {"", "Legajo", "Ape.y Nombre", "C.U.I.L."};
        modelo.setColumnIdentifiers(titulos);
        tablaLegajos.setModel(modelo);

        TableColumnModel columnModel = tablaLegajos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        columnModel.getColumn(1).setPreferredWidth(10);
        columnModel.getColumn(2).setPreferredWidth(120);
        columnModel.getColumn(3).setPreferredWidth(30);

        return modelo;
    }

    public static int buscarElPuestoPorDescripcion(String descripcion) throws ClassNotFoundException, SQLException {
        dbPuestos = new DBPuestos();
        int idPuesto = dbPuestos.buscarPuestoPorDescripcion(descripcion);
        return idPuesto;
    }

    public static float obtenerElBasicoDelPuesto(int idPuesto) throws ClassNotFoundException, SQLException {
        dbPuestos = new DBPuestos();
        float basico = dbPuestos.obtenerElBasicoDelPuestoX(idPuesto);
        return basico;
    }

    public static DefaultTableModel cargarModeloDeLaTablaConceptos(JTable tablaConceptos) {
        DefaultTableModel modelo = cargarModeloDeTablaConceptos(tablaConceptos);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaConceptos.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaConceptos.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaConceptos.getColumnModel().getColumn(2).setCellRenderer(tcr);
        tablaConceptos.getColumnModel().getColumn(3).setCellRenderer(tcr);
        tablaConceptos.getColumnModel().getColumn(4).setCellRenderer(tcr);
        tablaConceptos.getColumnModel().getColumn(5).setCellRenderer(tcr);
        return modelo;
    }

    public static void mostrarLosConceptosDelEmpleadoEnTabla(JTable tablaConceptos, int legajo) throws SQLException, ClassNotFoundException, ParseException {
        dbEmpleados = new DBEmpleados();
        dbConceptos = new DBConceptos();
        dbLiquidacion = new DBLiquidacion();

        float sueldoBrutoTotal = 0;
        float basicoMasHaberes = 0;

        DefaultTableModel modelo = cargarModeloDeLaTablaConceptos(tablaConceptos);

        int idPuesto = dbEmpleados.obtenerElIdPuestoDelEmpleado(legajo);
        ResultSet conceptos = dbConceptos.resultSetConceptosDelEmpleado(legajo);

        float porcentAnt = calcularElPorcentajeDeAntiguedad(legajo);

        Object registro[] = new Object[6];

        registro[0] = "1";
        registro[1] = "Basico";
        float basico = dbEmpleados.obtenerBasicoDelEmpleado(legajo);
        registro[2] = "$" + basico;
        modelo.addRow(registro);

        while (conceptos.next()) {
            int idConcepto = conceptos.getInt("concepto_idConcepto");
            if (idConcepto != 0 && idConcepto != 1) {
                registro[0] = conceptos.getInt("concepto_idConcepto");
                registro[1] = conceptos.getString("concepto_descripcion");
                if (conceptos.getString("concepto_tipoDeConcepto").equals("Haber")) {
                    if (conceptos.getDouble("concepto_cantidadPorcentual") == 0) {
                        if (conceptos.getString("concepto_descripcion").equals("Horas extra 50%")
                                || conceptos.getString("concepto_descripcion").equals("Horas extra 100%")) {
                            registro[2] = 0;
                        } else if (conceptos.getString("concepto_descripcion").equals("Antiguedad")) {
                            registro[2] = porcentAnt + "%";
                            if (porcentAnt != 0) {
                                registro[3] = formatearNumero((basico * porcentAnt) / 100);
                            } else {
                                registro[3] = (basico * porcentAnt) / 100;
                            }
//                        registro[3] = String.format("%.2f",(basico * porcentAnt) / 100);
                        } else {
                            if (conceptos.getInt("concepto_esRemunerativo") == 1) {
                                registro[2] = "" + conceptos.getDouble("concepto_cantidadFija");
                                registro[3] = "" + conceptos.getDouble("concepto_cantidadFija");
                            } else {
                                registro[2] = "" + conceptos.getDouble("concepto_cantidadFija");
                                registro[5] = "" + conceptos.getDouble("concepto_cantidadFija");
                            }
                        }

                    } else {
                        registro[2] = conceptos.getDouble("concepto_cantidadPorcentual") + "%";
                        registro[3] = formatearNumero((float) ((basico * conceptos.getDouble("concepto_cantidadPorcentual")) / 100));
//                    registro[3] = "" + String.format("%.2f", (basico * conceptos.getDouble("concepto_cantidadPorcentual")) / 100);
                    }
                } else {
                    if (conceptos.getDouble("concepto_cantidadPorcentual") == 0) {
                        registro[2] = "" + conceptos.getDouble("concepto_cantidadFija");
                        registro[4] = formatearNumero((float) conceptos.getDouble("concepto_cantidadFija"));
//                    registro[4] = String.format("%.2f", conceptos.getDouble("concepto_cantidadFija"));
                    } else {
                        registro[2] = conceptos.getDouble("concepto_cantidadPorcentual") + "%";
                        registro[4] = formatearNumero((float) ((basico * conceptos.getDouble("concepto_cantidadPorcentual")) / 100));
//                    registro[4] = "" + String.format("%.2f", (basico * conceptos.getDouble("concepto_cantidadPorcentual")) / 100);
                    }
                }
                modelo.addRow(registro);
                for (int i = 0; i < registro.length; i++) {
                    registro[i] = "";
                }
            }//fin del if principal
        }//fin del while de conceptos
        tablaConceptos.getModel().setValueAt(basico, 0, 3);
    }

    public static double formatearNumero(float numero) throws ParseException {
        DecimalFormat df = new DecimalFormat("0.00");
        String formate = df.format(numero);
        double finalValue = (double) df.parse(formate);
        return finalValue;
    }

    public static DefaultTableModel cargarModeloDeTablaConceptos(JTable tablaConceptos) {

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Codigo", "Descripcion", "Ts. Hs. Ds.", "Haberes", "Deducciones", "Haberes S/Descuento"};
        modelo.setColumnIdentifiers(titulos);
        tablaConceptos.setModel(modelo);

        TableColumnModel columnModel = tablaConceptos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(30);
        columnModel.getColumn(3).setPreferredWidth(30);
        columnModel.getColumn(4).setPreferredWidth(30);
        columnModel.getColumn(5).setPreferredWidth(30);

        return modelo;
    }

    public static float calcularElPorcentajeDeAntiguedad(int legajo) throws SQLException, ClassNotFoundException {
        int ant = dbEmpleados.obtenerAntiguedadDelEmpleado(legajo);
        float porcentAnt = 0;

        if (ant > 0 && ant <= 5) {
            porcentAnt = ant * 1;
        }
        if (ant > 5 && ant <= 10) {
            float aux0 = (float) ((ant - 5) * 2.5);
            float aux1 = 5 * 1;
            porcentAnt = aux0 + aux1;
        }

        if (ant > 10 && ant <= 20) {
            float aux0 = ((ant - 5) - 5) * 4;
            float aux1 = (float) (5 * 2.5);
            float aux2 = 5 * 1;
            porcentAnt = aux0 + aux1 + aux2;
        }
        if (ant > 20 && ant <= 30) {
            float aux0 = (float) ((((ant - 10) - 5) - 5) * 5.5);
            float aux1 = 10 * 4;
            float aux2 = (float) (5 * 2.5);
            float aux3 = 5 * 1;
            porcentAnt = aux0 + aux1 + aux2 + aux3;
        }
        if (ant > 30) {
            float aux0 = (ant - 30) * 7;
            float aux1 = (float) (10 * 5.5);
            float aux2 = 10 * 4;
            float aux3 = (float) (5 * 2.5);
            float aux4 = 5 * 1;
            porcentAnt = aux0 + aux1 + aux2 + aux3 + aux4;
        }
        return porcentAnt;
    }

    public static int getFilaSeleccionada(FrameLiquidacion frameLiquidacion) {
        int i = -1;
        i = frameLiquidacion.getTablaLegajos().getSelectedRow();
        if (i > -1) {
            return i;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un legajo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public static void mostrarHorasExtraCargadas(FrameLiquidacion frameLiquidacion, ArrayList<HorasExtra> horasExt) {
    }

    public static void cargarCantidadDeHoras(JTable tablaConceptos, HorasExtra horasExtra) throws ParseException {
        int cantidadDeFilas = obtenerLaCantidadDeFilasDeLaTabla(tablaConceptos);
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            int idConcepto = Integer.parseInt("" + tablaConceptos.getModel().getValueAt(fila, 0));
            if (7 == idConcepto && horasExtra.getHoras50() != 0) {
                tablaConceptos.getModel().setValueAt(horasExtra.getHoras50(), fila, 2);
                String bas = "" + tablaConceptos.getModel().getValueAt(0, 2);
                float basico = Float.parseFloat(bas.substring(1, bas.length()));
                float haberHrs50 = (float) ((basico / 200) * horasExtra.getHoras50() * 1.5);
                tablaConceptos.getModel().setValueAt("" + formatearNumero(haberHrs50), fila, 3);
            }
            if (8 == idConcepto && horasExtra.getHoras100() != 0) {
                tablaConceptos.getModel().setValueAt(horasExtra.getHoras100(), fila, 2);
                String bas = "" + tablaConceptos.getModel().getValueAt(0, 2);
                float basico = Float.parseFloat(bas.substring(1, bas.length()));
                float haberHrs100 = (float) ((basico / 200) * horasExtra.getHoras100() * 2);
                float hora = (float) formatearNumero(haberHrs100);
                tablaConceptos.getModel().setValueAt("" + hora, fila, 3);

            }
        }
    }

    public static int obtenerLaCantidadDeFilasDeLaTabla(JTable tablaConceptos) {
        DefaultTableModel modelo = (DefaultTableModel) tablaConceptos.getModel();
        return modelo.getRowCount();
    }

    public static void mostrarLosConceptosDelSACDelEmpleadoEnTabla(JTable tablaConceptos, int legajo, int año, String mes) throws SQLException, ClassNotFoundException, ParseException {
        dbEmpleados = new DBEmpleados();
        dbConceptos = new DBConceptos();
        dbLiquidacion = new DBLiquidacion();

        float sueldoBrutoTotal = 0;
        float basicoMasHaberes = 0;

        DefaultTableModel modelo = cargarModeloDeLaTablaConceptos(tablaConceptos);

        int idPuesto = dbEmpleados.obtenerElIdPuestoDelEmpleado(legajo);
        ResultSet conceptos = dbConceptos.resultSetConceptosDelEmpleado(legajo);

        float porcentAnt = calcularElPorcentajeDeAntiguedad(legajo);

        Object registro[] = new Object[6];

        registro[0] = "0";
        registro[1] = "SAC Semestral";
        float mayorSueldo = dbEmpleados.obtenerElMayorSueldoDelEmpleado(legajo, año, mes) / 2;
        registro[2] = "" + mayorSueldo;
        modelo.addRow(registro);

        while (conceptos.next()) {
            int idConcepto = conceptos.getInt("concepto_idConcepto");
            if (idConcepto != 0 && idConcepto != 1) {
                registro[0] = conceptos.getInt("concepto_idConcepto");
                registro[1] = conceptos.getString("concepto_descripcion");
                if (conceptos.getString("concepto_tipoDeConcepto").equals("Haber")) {
                    if (conceptos.getDouble("concepto_cantidadPorcentual") == 0) {
                        if (conceptos.getString("concepto_descripcion").equals("Horas extra 50%")
                                || conceptos.getString("concepto_descripcion").equals("Horas extra 100%")) {
                            registro[2] = 0;
                        } else if (conceptos.getString("concepto_descripcion").equals("Antiguedad")) {
                            registro[2] = porcentAnt + "%";
                            registro[3] = formatearNumero((mayorSueldo * porcentAnt) / 100);
//                        registro[3] = (mayorSueldo * porcentAnt) / 100;
                        } else {
                            if (conceptos.getInt("concepto_esRemunerativo") == 1) {
                                registro[2] = "" + conceptos.getDouble("concepto_cantidadFija");
                                registro[3] = "" + conceptos.getDouble("concepto_cantidadFija");
                            } else {
                                registro[2] = "" + conceptos.getDouble("concepto_cantidadFija");
                                registro[5] = "" + conceptos.getDouble("concepto_cantidadFija");
                            }
                        }

                    } else {
                        registro[2] = conceptos.getDouble("concepto_cantidadPorcentual") + "%";
                        registro[3] = formatearNumero((float) ((mayorSueldo * conceptos.getDouble("concepto_cantidadPorcentual")) / 100));
//                    registro[3] = "" + String.format("%.2f", (mayorSueldo * conceptos.getDouble("concepto_cantidadPorcentual")) / 100);
                    }
                } else {
                    if (conceptos.getDouble("concepto_cantidadPorcentual") == 0) {
                        registro[2] = "" + conceptos.getDouble("concepto_cantidadFija");
                        registro[4] = formatearNumero((float) conceptos.getDouble("concepto_cantidadFija"));
//                    registro[4] = String.format("%.2f", conceptos.getDouble("concepto_cantidadFija"));
                    } else {
                        registro[2] = conceptos.getDouble("concepto_cantidadPorcentual") + "%";
                        registro[4] = formatearNumero((float) ((mayorSueldo * conceptos.getDouble("concepto_cantidadPorcentual")) / 100));
//                    registro[4] = "" + String.format("%.2f", (mayorSueldo * conceptos.getDouble("concepto_cantidadPorcentual")) / 100);
                    }
                }
                modelo.addRow(registro);
                for (int i = 0; i < registro.length; i++) {
                    registro[i] = "";
                }
            }
        }//fin del while de conceptos
        tablaConceptos.getModel().setValueAt(mayorSueldo, 0, 3);
    }

    public static void registrarLiquidacion(int legajo, FrameLiquidacion frameLiquidacion, JTable tablaConceptos, String tipoDeLiquidacion) throws SQLException, ClassNotFoundException, ParseException {
        dbReciboSueldo = new DBReciboSueldo();
        dbEmpleados = new DBEmpleados();

        ResultSet datosRecibo = dbReciboSueldo.resultSetDatosRecibo(legajo);

        float totalHaberes = 0;
        float totalDeducciones = 0;
        float totalHaberesSinDeduc = 0;

        int cantidadDeFilas = UtilFrameLiquidacion.obtenerLaCantidadDeFilasDeLaTabla(tablaConceptos);
        int antiguedad = dbEmpleados.obtenerAntiguedadDelEmpleado(legajo);
        int idEmpleado = dbEmpleados.obtenerElIdEmpleado(legajo);

        while (datosRecibo.next()) {

//            totalHaberes = (float) datosRecibo.getDouble("salario");
            Sueldo sueldo = new Sueldo(idEmpleado,
                    "" + frameLiquidacion.getJdcFechaDePago(),
                    antiguedad,
                    datosRecibo.getString("puesto"),
                    0,
                    0,
                    tipoDeLiquidacion);

            for (int j = 0; j < cantidadDeFilas; j++) {
                String haber = "";
                String deduc = "";
                String haberSinDes = "";
                /**
                 * ****************Haberes******************
                 */
                if (tablaConceptos.getModel().getValueAt(j, 3) == null) {
                    haber = "";
                } else {
                    String aux1 = "" + tablaConceptos.getModel().getValueAt(j, 3);
                    if (!aux1.equals("")) {
                        float haberFloat = Float.valueOf(aux1);
                        DetalleSueldo detalleSueldo = new DetalleSueldo(Integer.parseInt("" + tablaConceptos.getModel().getValueAt(j, 0)), haberFloat);
                        sueldo.addDetalleSueldo(detalleSueldo);
                        totalHaberes = totalHaberes + haberFloat;
                    }
                    haber = "" + tablaConceptos.getModel().getValueAt(j, 3);
                }
                /**
                 * ****************Deducciones******************
                 */
                if (tablaConceptos.getModel().getValueAt(j, 4) == null) {
                    deduc = "";
                } else {
                    String aux2 = "" + tablaConceptos.getModel().getValueAt(j, 4);
                    if (!aux2.equals("")) {
                        float valor = Float.valueOf(aux2);
                        DetalleSueldo detalleSueldo = new DetalleSueldo(Integer.parseInt("" + tablaConceptos.getModel().getValueAt(j, 0)), valor);
                        sueldo.addDetalleSueldo(detalleSueldo);
                        totalDeducciones = totalDeducciones + valor;
                    }
                    deduc = "" + tablaConceptos.getModel().getValueAt(j, 4);
                }
                /**
                 * ****************Haberes sin descuento******************
                 */
                if (tablaConceptos.getModel().getValueAt(j, 5) == null) {
                    haberSinDes = "";
                } else {
                    String aux3 = "" + tablaConceptos.getModel().getValueAt(j, 5);
                    if (!aux3.equals("")) {
                        float valor = Float.valueOf(aux3);
                        DetalleSueldo detalleSueldo = new DetalleSueldo(Integer.parseInt("" + tablaConceptos.getModel().getValueAt(j, 0)), valor);
                        sueldo.addDetalleSueldo(detalleSueldo);
                        totalHaberesSinDeduc = totalHaberesSinDeduc + valor;
                    }
                    haberSinDes = "" + tablaConceptos.getModel().getValueAt(j, 5);
                }

            }//fin del for externo

            sueldo.setSueldoBruto(formatearNumero(totalHaberes));
            float sueldoNeto = (float) (totalHaberes - totalDeducciones) + totalHaberesSinDeduc;
            sueldo.setSueldoNeto(formatearNumero(sueldoNeto));

            dbLiquidacion.altaDeLiquidacion(sueldo);
        }//fin del while
    }
}
