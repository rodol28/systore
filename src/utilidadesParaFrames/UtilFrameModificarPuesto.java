package utilidadesParaFrames;

import objetosDelDominio.Puesto;
import persistencia.DBPuestos;
import vistas.FrameModificarPuesto;

public class UtilFrameModificarPuesto {

    private static DBPuestos dbPuestos;
    private static Puesto puesto;

    public static void cargarCamposDelPuesto(FrameModificarPuesto frameModificarPuesto, int idPuesto, String descripcion, float sueldoBasico) throws ClassNotFoundException {
        frameModificarPuesto.setTxtDescripcion(descripcion);
        frameModificarPuesto.setTxtSueldoBasico(sueldoBasico + "");
    }

    public static void modificarElPuesto(int idPuesto, float sueldoBasico) throws ClassNotFoundException {
        dbPuestos = new DBPuestos();

        puesto = new Puesto(idPuesto, sueldoBasico);

        dbPuestos.modificarPuesto(puesto);
    }

}
