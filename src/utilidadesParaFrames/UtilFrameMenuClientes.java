package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBAuxiliares;
import persistencia.DBClientes;

public class UtilFrameMenuClientes {

    private static DBClientes dbClientes;
    private static DBAuxiliares dbAuxiliares;

    public static void mostrarClientesEnTabla(JTable tablaClientes) throws SQLException, ClassNotFoundException {

        dbClientes = new DBClientes();
        dbAuxiliares = new DBAuxiliares();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cod.", "Nombre", "CUIT", "Telefono",
            "Mail", "Direccion", "CP", "Localidad", "Provincia", "Tipo de CUIT"};
        modelo.setColumnIdentifiers(titulos);
        tablaClientes.setModel(modelo);
        String registro[] = new String[10];

        TableColumnModel columnModel = tablaClientes.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(10);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(5).setPreferredWidth(100);
        columnModel.getColumn(6).setPreferredWidth(15);
        columnModel.getColumn(7).setPreferredWidth(50);

        ResultSet clientes = dbClientes.resultSetClientes();
        ResultSet persona;

        while (clientes.next()) {
            registro[0] = String.valueOf(clientes.getInt("idCliente"));
            registro[2] = clientes.getString("nroCUIT");
            registro[5] = clientes.getString("domicilio");
            registro[6] = clientes.getString("codigoPostal");
            registro[7] = clientes.getString("localidad");
            registro[8] = clientes.getString("provincia");
            registro[9] = dbAuxiliares.buscarTipoCuitPorID(clientes.getInt("TipoDeCUIT_idTipoDeCUIT"));

            persona = dbAuxiliares.resultSetPersona(clientes.getInt("Persona_idPersona"));

            while (persona.next()) {
                registro[1] = persona.getString("nombre") + " " + persona.getString("apellido");
                registro[3] = persona.getString("telefono");
                registro[4] = persona.getString("email");
            }

            modelo.addRow(registro);
        }

    }

    public static int getCodigoIngresado() {
        int codigo = -1;
        String txtCodigo = "";
        txtCodigo = "" + JOptionPane.showInputDialog("Ingrese el codigo de cliente: ");
        if (txtCodigo.equals("") == false) {
            codigo = Integer.parseInt(txtCodigo);
            return codigo;
        }
        return codigo;
    }

    public static void mostrarClienteXEnTabla(JTable tablaClientes, int nroCliente) throws SQLException, ClassNotFoundException {

        dbClientes = new DBClientes();
        dbAuxiliares = new DBAuxiliares();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cod.", "Nombre", "CUIT", "Telefono",
            "Mail", "Direccion", "CP", "Localidad", "Provincia", "Tipo de CUIT"};
        modelo.setColumnIdentifiers(titulos);
        tablaClientes.setModel(modelo);
        String registro[] = new String[10];

        TableColumnModel columnModel = tablaClientes.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(10);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(5).setPreferredWidth(100);
        columnModel.getColumn(6).setPreferredWidth(15);
        columnModel.getColumn(7).setPreferredWidth(50);

        ResultSet cliente = dbClientes.obtenerElClienteNroX(nroCliente);
        ResultSet persona;

        while (cliente.next()) {
            registro[0] = String.valueOf(cliente.getInt("idCliente"));
            registro[2] = cliente.getString("nroCUIT");
            registro[5] = cliente.getString("domicilio");
            registro[6] = cliente.getString("codigoPostal");
            registro[7] = cliente.getString("localidad");
            registro[8] = cliente.getString("provincia");
            registro[9] = dbAuxiliares.buscarTipoCuitPorID(cliente.getInt("TipoDeCUIT_idTipoDeCUIT"));

            persona = dbAuxiliares.resultSetPersona(cliente.getInt("Persona_idPersona"));

            while (persona.next()) {
                registro[1] = persona.getString("nombre") + " " + persona.getString("apellido");
                registro[3] = persona.getString("telefono");
                registro[4] = persona.getString("email");
            }

            modelo.addRow(registro);
        }
    }
}
