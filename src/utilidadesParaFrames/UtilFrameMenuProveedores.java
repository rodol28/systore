package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBAuxiliares;
import persistencia.DBProveedores;

public class UtilFrameMenuProveedores {

    private static DBProveedores dbProveedores;
    private static DBAuxiliares dbAuxiliares;

    public static void mostrarProveedoresEnTabla(JTable tablaProveedores) throws SQLException, ClassNotFoundException {

        dbProveedores = new DBProveedores();
        dbAuxiliares = new DBAuxiliares();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cod.", "Razon soc.", "Telefono",
            "Direccion", "Mail", "Celular", "Rubro", "CP", "Localidad", "Provincia", "Web"};
        modelo.setColumnIdentifiers(titulos);
        tablaProveedores.setModel(modelo);
        String registro[] = new String[11];

        TableColumnModel columnModel = tablaProveedores.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        columnModel.getColumn(1).setPreferredWidth(110);
        columnModel.getColumn(2).setPreferredWidth(35);
        columnModel.getColumn(3).setPreferredWidth(125);
        columnModel.getColumn(4).setPreferredWidth(100);
        columnModel.getColumn(5).setPreferredWidth(44);
        columnModel.getColumn(6).setPreferredWidth(42);
        columnModel.getColumn(7).setPreferredWidth(6);
        columnModel.getColumn(8).setPreferredWidth(110);
        columnModel.getColumn(9).setPreferredWidth(50);
        columnModel.getColumn(10).setPreferredWidth(80);

        ResultSet proveedores = dbProveedores.resultSetCompletoProveedores();

        while (proveedores.next()) {
            registro[0] = String.valueOf(proveedores.getInt("idProveedor"));
            registro[1] = proveedores.getString("razonSocial");
            registro[2] = proveedores.getString("telefono");
            registro[3] = proveedores.getString("domicilio");
            registro[4] = proveedores.getString("email");
            registro[5] = proveedores.getString("celular");
            registro[6] = dbAuxiliares.buscarRubroPorID(proveedores.getInt("Rubro_idRubro"));
            registro[7] = String.valueOf(proveedores.getInt("codigoPostal"));
            registro[8] = proveedores.getString("localidad");
            registro[9] = proveedores.getString("provincia");
            registro[10] = proveedores.getString("web");

            modelo.addRow(registro);
        }
    }

    public static void buscar(JTable tablaProveedores, String palabra) throws SQLException, ClassNotFoundException {
        dbProveedores = new DBProveedores();
        dbAuxiliares = new DBAuxiliares();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cod.", "Razon soc.", "Telefono",
            "Direccion", "Mail", "Celular", "Rubro", "CP", "Localidad", "Provincia", "Web"};
        modelo.setColumnIdentifiers(titulos);
        tablaProveedores.setModel(modelo);
        String registro[] = new String[11];

        ResultSet proveedores = dbProveedores.busquedaPredictivaDeRazonSocial(palabra);

        while (proveedores.next()) {
            registro[0] = String.valueOf(proveedores.getInt("idProveedor"));
            registro[1] = proveedores.getString("razonSocial");
            registro[2] = proveedores.getString("telefono");
            registro[3] = proveedores.getString("domicilio");
            registro[4] = proveedores.getString("email");
            registro[5] = proveedores.getString("celular");
            registro[6] = dbAuxiliares.buscarRubroPorID(proveedores.getInt("Rubro_idRubro"));
            registro[7] = String.valueOf(proveedores.getInt("codigoPostal"));
            registro[8] = proveedores.getString("localidad");
            registro[9] = proveedores.getString("provincia");
            registro[10] = proveedores.getString("web");

            modelo.addRow(registro);
        }
    }

    public static int getCodigoIngresado() {
        int codigo = -1;
        String c = "";
        c = "" + JOptionPane.showInputDialog("Ingrese el codigo del proveedor: ");

        if (c.equals("") == false) {
            if (c.equals("null")) {
                return codigo;
            }
        }
        if (c.equals("") == false) {
            codigo = Integer.parseInt(c);
            return codigo;
        }
        return codigo;
    }

    public static void mostrarElProveedorX(JTable tablaProveedores, int nroProveedor) throws SQLException, ClassNotFoundException {

        dbProveedores = new DBProveedores();
        dbAuxiliares = new DBAuxiliares();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cod.", "Razon soc.", "Telefono",
            "Direccion", "Mail", "Celular", "Rubro", "CP", "Localidad", "Provincia", "Web"};
        modelo.setColumnIdentifiers(titulos);
        tablaProveedores.setModel(modelo);
        String registro[] = new String[11];

        TableColumnModel columnModel = tablaProveedores.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        columnModel.getColumn(1).setPreferredWidth(110);
        columnModel.getColumn(2).setPreferredWidth(35);
        columnModel.getColumn(3).setPreferredWidth(125);
        columnModel.getColumn(4).setPreferredWidth(100);
        columnModel.getColumn(5).setPreferredWidth(44);
        columnModel.getColumn(6).setPreferredWidth(42);
        columnModel.getColumn(7).setPreferredWidth(6);
        columnModel.getColumn(8).setPreferredWidth(110);
        columnModel.getColumn(9).setPreferredWidth(50);
        columnModel.getColumn(10).setPreferredWidth(80);

        ResultSet proveedor = dbProveedores.obtenerElProveedorNroX(nroProveedor);

        while (proveedor.next()) {
            registro[0] = String.valueOf(proveedor.getInt("idProveedor"));
            registro[1] = proveedor.getString("razonSocial");
            registro[2] = proveedor.getString("telefono");
            registro[3] = proveedor.getString("domicilio");
            registro[4] = proveedor.getString("email");
            registro[5] = proveedor.getString("celular");
            registro[6] = dbAuxiliares.buscarRubroPorID(proveedor.getInt("Rubro_idRubro"));
            registro[7] = String.valueOf(proveedor.getInt("codigoPostal"));
            registro[8] = proveedor.getString("localidad");
            registro[9] = proveedor.getString("provincia");
            registro[10] = proveedor.getString("web");

            modelo.addRow(registro);
        }
    }
}
