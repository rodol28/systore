package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import otros.ModeloTablaConceptos;
import persistencia.DBConceptos;
import persistencia.DBEmpleados;
import vistas.FrameSeleccionDeConceptos;

public class UtilFrameSeleccionDeConceptos {

    private static DBConceptos dbConceptos;
    private static DBEmpleados dbEmpleados;
    private static int filaDelCon = 0;
    private static ArrayList<Object> obj = new ArrayList<Object>();

    public static void mostrarConceptosEnTabla(JTable tablaConceptos) throws SQLException, ClassNotFoundException {

        dbConceptos = new DBConceptos();

        DefaultTableModel modelo = cargarModeloDeTabla(tablaConceptos);

        Object registro[] = new Object[8];

        int estado = 0;
        int esRem = 1;
        int i = 0;

        ResultSet conceptos = dbConceptos.resultSetConceptosActivos();

        while (conceptos.next()) {

            registro[0] = false;

            int idConcepto = conceptos.getInt("idConcepto");
            if ((idConcepto != 0) && (idConcepto != 1)) {
                int b = 0; //bandera para indicar que ya se agrego una Q

                registro[1] = String.valueOf(conceptos.getInt("idConcepto"));
                registro[2] = conceptos.getString("abreviatura");
                registro[3] = conceptos.getString("descripcion");
                registro[4] = conceptos.getString("tipoDeConcepto");

                estado = conceptos.getInt("estadoConcepto");
                if (estado == 1) {
                    registro[5] = "Activo";
                }

                esRem = conceptos.getInt("esRemunerativo");
                if (esRem == 1) {
                    registro[6] = "Si";
                } else {
                    registro[6] = "No";
                }
                modelo.addRow(registro);
////            System.out.println(idConcepto + ": " + lineaDeDescripcionesLiq + " - " + lineaDeDescripcionesConcep);
            }
        }//fin del while
    }

    public static int mostrarApeNombreYConceptos(FrameSeleccionDeConceptos frameSeleccionDeConceptos, int legajo, JTable tablaConceptos) throws SQLException, ClassNotFoundException {
        dbConceptos = new DBConceptos();
        dbEmpleados = new DBEmpleados();

        ResultSet datos = dbConceptos.buscarEmpleadoYPuesto(legajo);
        int idPuesto = 0;

        while (datos.next()) {
            frameSeleccionDeConceptos.setTxtApeNombre(datos.getString("persona_apellido") + ", " + datos.getString("persona_nombre"));
            frameSeleccionDeConceptos.setTxtPuesto(datos.getString("puesto_descripcion"));
            idPuesto = datos.getInt("puesto_idPuesto");
        }

        int cantidadDeFilas = obtenerLaCantidadDeFilasDeLaTabla(tablaConceptos);
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            tablaConceptos.getModel().setValueAt(false, fila, 0);
        }

        int idEmpleado = dbEmpleados.obtenerElIdEmpleado(legajo);
        ResultSet rs = dbConceptos.resultSetEmpleadoConcepto(idEmpleado);

        while (rs.next()) {
            int idConcepto = rs.getInt("Concepto_idConcepto");
            boolean existe = verificarSiExiste(tablaConceptos, idConcepto);
            if (existe) {
                tablaConceptos.getModel().setValueAt(true, filaDelCon, 0);
                filaDelCon = 0;
            }
        }
        return idPuesto;
    }

    public static boolean verificarSiExiste(JTable tablaConceptos, int id) {
        int cantidadDeFilas = obtenerLaCantidadDeFilasDeLaTabla(tablaConceptos);
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            int idConcepto = Integer.parseInt("" + tablaConceptos.getModel().getValueAt(fila, 1));
            if (idConcepto == id) {
                filaDelCon = fila;
                return true;
            }
        }
        return false;
    }

    public static int obtenerLaCantidadDeFilasDeLaTabla(JTable tabla) {
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
        return modelo.getRowCount();
    }

    public static void registrarConceptosSeleccionados(ArrayList<Integer> concepSelec, int idPuesto, int legajo) throws SQLException, ClassNotFoundException {
        dbConceptos = new DBConceptos();
        dbEmpleados = new DBEmpleados();
        int idEmpleado = dbEmpleados.obtenerElIdEmpleado(legajo);
        dbConceptos.registrarPuestoEmpleadoConcepto(concepSelec, idPuesto, idEmpleado);
    }

    public static DefaultTableModel cargarModeloDeTabla(JTable tablaConceptos) {

        DefaultTableModel modelo = new ModeloTablaConceptos();

        modelo.addColumn(modelo.getColumnClass(0));
        modelo.addColumn(modelo.getColumnClass(1));
        modelo.addColumn(modelo.getColumnClass(2));
        modelo.addColumn(modelo.getColumnClass(3));
        modelo.addColumn(modelo.getColumnClass(4));
        modelo.addColumn(modelo.getColumnClass(5));
        modelo.addColumn(modelo.getColumnClass(6));

        String titulos[] = {"", "Codigo", "Abreviatura", "Descripcion", "Tipo de concepto", "Estado", "H. Rem."};
        modelo.setColumnIdentifiers(titulos);
        tablaConceptos.setModel(modelo);

        TableColumnModel columnModel = tablaConceptos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        columnModel.getColumn(1).setPreferredWidth(10);
        columnModel.getColumn(2).setPreferredWidth(30);
        columnModel.getColumn(3).setPreferredWidth(100);
        columnModel.getColumn(4).setPreferredWidth(80);
        columnModel.getColumn(5).setPreferredWidth(20);
        columnModel.getColumn(6).setPreferredWidth(20);

        return modelo;
    }
}
