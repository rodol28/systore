package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBArticulos;
import persistencia.DBCompras;
import sun.swing.table.DefaultTableCellHeaderRenderer;

public class UtilFrameConsultarCompras {

    private static DBCompras dbCompras;
    private static DBArticulos dbArticulos;

    public static void mostrarComprasEnTabla(JTable tablaCompras, JTable lineasDeCompra, JTextField txtNroFactura, JTextField txtFecha, int nroFactura) throws SQLException, ClassNotFoundException {
        dbCompras = new DBCompras();

        if (nroFactura == -1) {
            limpiarTablaLineasDeCompra(lineasDeCompra);
            txtNroFactura.setText("");
            txtFecha.setText("");
        }

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Nro. Comp.", "Fecha", "Total"};
        modelo.setColumnIdentifiers(titulos);
        tablaCompras.setModel(modelo);
        String registro[] = new String[3];

        TableColumnModel columnModel = tablaCompras.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(2);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaCompras.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaCompras.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaCompras.getColumnModel().getColumn(2).setCellRenderer(tcr);

        ResultSet compras = dbCompras.resultSetCompras();

        while (compras.next()) {
            registro[0] = String.valueOf(compras.getInt("idCompra"));
            registro[1] = String.valueOf(compras.getDate("fecha"));
            registro[2] = "$ " + String.valueOf(compras.getInt("total"));
            modelo.addRow(registro);
        }
    }

    public static void mostrarLineasDeCompra(JTable tablaLineasDeCompra, int idCompra) throws SQLException, ClassNotFoundException {
        dbCompras = new DBCompras();
        dbArticulos = new DBArticulos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cantidad", "Descripcion", "Subtotal"};
        modelo.setColumnIdentifiers(titulos);
        tablaLineasDeCompra.setModel(modelo);
        String registro[] = new String[3];

        TableColumnModel columnModel = tablaLineasDeCompra.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(2);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaLineasDeCompra.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaLineasDeCompra.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaLineasDeCompra.getColumnModel().getColumn(2).setCellRenderer(tcr);

        ResultSet lineasDeCompra = dbCompras.resultSetLineasDeCompra(idCompra);

        while (lineasDeCompra.next()) {
            registro[0] = String.valueOf(lineasDeCompra.getInt("cantidad"));
            registro[1] = dbArticulos.obtenerDescripcionDelArticulo(lineasDeCompra.getInt("Articulo_idArticulo"));
            registro[2] = "$ " + calcularSubtotal(lineasDeCompra.getInt("cantidad"), dbArticulos.obtenerPrecioDelArticulo(lineasDeCompra.getInt("Articulo_idArticulo")));
            modelo.addRow(registro);
        }
    }

    public static float calcularSubtotal(int cantidad, float precio) {
        float subtotal = cantidad * precio;
        return subtotal;
    }

    public static void mostrarFecha(int idCompra, JTextField txtFecha) throws SQLException, ClassNotFoundException {
        dbCompras = new DBCompras();
        txtFecha.setText(dbCompras.obtenerLaFechaDeCompra(idCompra));
    }

    public static int getCodigoIngresado() {
        int codigo = -1;
        String txtCodigo = "";
        txtCodigo = "" + JOptionPane.showInputDialog("Ingrese el nro de factura: ");
        if (txtCodigo.equals("") == false) {
            codigo = Integer.parseInt(txtCodigo);
            return codigo;
        }
        return codigo;
    }

    public static void mostrarCompraNroXEnTabla(JTable tablaCompras, JTable tablaLineasDeCompra, JTextField txtFecha, int nroFactura) throws SQLException, ClassNotFoundException {
        dbCompras = new DBCompras();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Nro. Comp.", "Fecha", "Total"};
        modelo.setColumnIdentifiers(titulos);
        tablaCompras.setModel(modelo);
        String registro[] = new String[3];

        TableColumnModel columnModel = tablaCompras.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(2);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaCompras.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaCompras.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaCompras.getColumnModel().getColumn(2).setCellRenderer(tcr);

        ResultSet compra = dbCompras.obtenerLaCompraNroX(nroFactura);

        while (compra.next()) {
            registro[0] = String.valueOf(compra.getInt("idCompra"));
            registro[1] = String.valueOf(compra.getDate("fecha"));
            registro[2] = "$ " + String.valueOf(compra.getInt("total"));
            modelo.addRow(registro);
        }

        mostrarFecha(nroFactura, txtFecha);
        mostrarLineasDeCompra(tablaLineasDeCompra, nroFactura);
    }

    public static void limpiarTablaLineasDeCompra(JTable tablaLineasDeCompra) {
        DefaultTableModel modelo = (DefaultTableModel) tablaLineasDeCompra.getModel();
        for (int fila = tablaLineasDeCompra.getRowCount() - 1; fila >= 0; fila--) {
            modelo.removeRow(fila);
        }
    }
}
