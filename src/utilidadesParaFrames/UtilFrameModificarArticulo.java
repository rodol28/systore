package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import objetosDelDominio.Articulo;
import persistencia.DBArticulos;
import persistencia.DBAuxiliares;
import vistas.FrameModificarArticulo;

public class UtilFrameModificarArticulo {

    private static DBArticulos dbArticulos;
    private static DBAuxiliares dbAuxiliares;
    private static Articulo articulo;

    public static void cargarLosCampos(FrameModificarArticulo frameModificarArticulo, String marcaArt, String rubroArt, int idArticulo) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();
        dbArticulos = new DBArticulos();

        int indexMarca = 0;
        int i = 0;
        int indexRubro = 0;
        int j = 0;

        frameModificarArticulo.setTxtCodigo(idArticulo + "");

        DefaultComboBoxModel modeloComboMarca = new DefaultComboBoxModel();
        DefaultComboBoxModel modeloComboRubro = new DefaultComboBoxModel();
        frameModificarArticulo.getComboMarca().setModel(modeloComboMarca);
        frameModificarArticulo.getComboRubro().setModel(modeloComboRubro);

        ResultSet marcas = dbAuxiliares.resultSetMarcas();
        ResultSet rubros = dbAuxiliares.resultSetRubros();

        while (marcas.next()) {
            modeloComboMarca.addElement(marcas.getObject("descripcion"));
            frameModificarArticulo.getComboMarca().setModel(modeloComboMarca);
            if (marcas.getObject("descripcion").equals(marcaArt)) {
                indexMarca = i;
                i++;
            }
            i++;
        }

        while (rubros.next()) {
            modeloComboRubro.addElement(rubros.getObject("descripcion"));
            frameModificarArticulo.getComboRubro().setModel(modeloComboRubro);
            if (rubros.getObject("descripcion").equals(rubroArt)) {
                indexRubro = j;
                j++;
            }
            j++;
        }

        articulo = dbArticulos.obtenerElArticulo(idArticulo);

        frameModificarArticulo.setTxtDescripcion(articulo.getDescripcion());
        frameModificarArticulo.setTxtPrecio(articulo.getPrecioUnitario() + "");
        frameModificarArticulo.setTxtCosto(articulo.getCosto() + "");
        frameModificarArticulo.setTxtStock(articulo.getStock() + "");
        frameModificarArticulo.setTxtStockMin(articulo.getStockMin() + "");

        frameModificarArticulo.getComboMarca().setSelectedIndex(indexMarca);
        frameModificarArticulo.getComboRubro().setSelectedIndex(indexRubro);

    }

    public static void actualizarElArticulo(FrameModificarArticulo frameModificarArticulo) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        Articulo articulo = new Articulo(frameModificarArticulo.getTxtCodigo(),
                frameModificarArticulo.getTxtDescripcion(),
                frameModificarArticulo.getTxtPrecio(),
                frameModificarArticulo.getTxtCosto(),
                frameModificarArticulo.getTxtStock(),
                frameModificarArticulo.getTxtStockMin(),
                dbAuxiliares.buscarRubroPorDescripcion(frameModificarArticulo.getRubroSeleccionado()),
                dbArticulos.buscarMarcaPorDescripcion(frameModificarArticulo.getMarcaSeleccionada()));

        dbArticulos.modificarArticulo(articulo);
    }

}
