package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBEmpleados;

public class UtilFrameMenuEmpleados {

    private static DBEmpleados dbEmpleados;

    public static void mostrarEmpleadosEnTabla(JTable tablaEmpleados, boolean mostrarTodos) throws SQLException, ClassNotFoundException {

        dbEmpleados = new DBEmpleados();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Legajo", "Nombre", "C.U.I.L.", "Telefono", "Estado"};
        modelo.setColumnIdentifiers(titulos);
        tablaEmpleados.setModel(modelo);
        String registro[] = new String[5];

        TableColumnModel columnModel = tablaEmpleados.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(10);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(50);
        columnModel.getColumn(3).setPreferredWidth(50);
        columnModel.getColumn(3).setPreferredWidth(50);

        ResultSet emplados = dbEmpleados.resultSetEmpleados(mostrarTodos);

        while (emplados.next()) {
            registro[0] = String.valueOf(emplados.getInt("legajo"));
            registro[1] = emplados.getString("apellido") + ", " + emplados.getString("nombre");
            registro[2] = emplados.getString("cuil");
            registro[3] = emplados.getString("telefono");
            if (emplados.getInt("estadoDelEmpleado") == 1) {
                registro[4] = "Activo";
            } else {
                registro[4] = "Inactivo";
            }
            modelo.addRow(registro);
        }
    }

    public static void mostrarEmpleadoX(JTable tablaEmpleados, int legajo) throws SQLException, ClassNotFoundException {

        dbEmpleados = new DBEmpleados();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Legajo", "Nombre", "C.U.I.L.", "Telefono", "Estado"};
        modelo.setColumnIdentifiers(titulos);
        tablaEmpleados.setModel(modelo);
        String registro[] = new String[5];

        TableColumnModel columnModel = tablaEmpleados.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(10);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(50);
        columnModel.getColumn(3).setPreferredWidth(50);
        columnModel.getColumn(3).setPreferredWidth(50);

        ResultSet emplados = dbEmpleados.resultSetDatosDelEmpleadoX(legajo);

        while (emplados.next()) {
            registro[0] = String.valueOf(emplados.getInt("legajo"));
            registro[1] = emplados.getString("apellido") + ", " + emplados.getString("nombre");
            registro[2] = String.valueOf(emplados.getInt("cuil"));
            registro[3] = emplados.getString("telefono");
            if (emplados.getInt("estadoDelEmpleado") == 1) {
                registro[4] = "Activo";
            } else {
                registro[4] = "Inactivo";
            }
            modelo.addRow(registro);
        }
    }

    public static int getLegajoIngresado() {
        int codigo = -1;
        String txtCodigo = "";
        txtCodigo = "" + JOptionPane.showInputDialog("Ingrese el nro de legajo: ");
        if (txtCodigo.equals("") == false) {
            codigo = Integer.parseInt(txtCodigo);
            return codigo;
        }
        return codigo;
    }
}
