package utilidadesParaFrames;

import java.sql.SQLException;
import objetosDelDominio.Proveedor;
import persistencia.DBAuxiliares;
import persistencia.DBProveedores;
import vistas.FrameNuevoProveedor;

public class UtilFrameNuevoProveedor {

    private static DBProveedores dbProveedores;
    private static DBAuxiliares dbAuxiliares;

    public static void registarProveedor(FrameNuevoProveedor frameNuevoProveedor) throws SQLException, ClassNotFoundException {

        dbProveedores = new DBProveedores();
        dbAuxiliares = new DBAuxiliares();

        int idTipoDeCuit = dbAuxiliares.buscarCuitPorDescripcion(frameNuevoProveedor.getCuitSeleccionado());
        int idRubro = dbAuxiliares.buscarRubroPorDescripcion(frameNuevoProveedor.getRubroSeleccionado());

        Proveedor proveedor = new Proveedor(frameNuevoProveedor.getTxtRazonSocial(),
                frameNuevoProveedor.getTxtTelefono(),
                frameNuevoProveedor.getTxtCelular(),
                frameNuevoProveedor.getTxtMail(),
                frameNuevoProveedor.getTxtCuit(),
                frameNuevoProveedor.getTxtWeb(),
                frameNuevoProveedor.getTxtObserv(),
                idTipoDeCuit,
                idRubro,
                frameNuevoProveedor.getTxtDomicilio(),
                frameNuevoProveedor.getTxtPais(),
                frameNuevoProveedor.getTxtProvincia(),
                frameNuevoProveedor.getTxtLocalidad(),
                frameNuevoProveedor.getTxtCodPostal());

        dbProveedores.altaDeProveedor(proveedor);
    }

}
