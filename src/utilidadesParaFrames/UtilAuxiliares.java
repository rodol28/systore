package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import objetosDelDominio.Fecha;
import persistencia.DBAuxiliares;
import persistencia.DBUsuarios;

public class UtilAuxiliares {

    private static DBAuxiliares dbAuxiliares;
    private static DBUsuarios dbUsuarios;

    /*Para para obtener del datePicker una fecha (Wed Aug 15 00:00:00 ART 2018)*/
    public static Fecha obtenerFecha(String f) {

        int dia = Integer.parseInt(f.substring(8, 10));
        int mes = 0;
        int año = Integer.parseInt(f.substring(24, 28));

        String m = f.substring(4, 7);

        if (m.equals("Jan")) {
            mes = 1;
        }
        if (m.equals("Feb")) {
            mes = 2;
        }
        if (m.equals("Mar")) {
            mes = 3;
        }
        if (m.equals("Apr")) {
            mes = 4;
        }
        if (m.equals("May")) {
            mes = 5;
        }
        if (m.equals("Jun")) {
            mes = 6;
        }
        if (m.equals("Jul")) {
            mes = 7;
        }
        if (m.equals("Aug")) {
            mes = 8;
        }
        if (m.equals("Sep")) {
            mes = 9;
        }
        if (m.equals("Oct")) {
            mes = 10;
        }
        if (m.equals("Nov")) {
            mes = 11;
        }
        if (m.equals("Dec")) {
            mes = 12;
        }

        Fecha fecha = new Fecha(dia, mes, año);
        return fecha;
    }

    /*Metodo para obtener la fecha actual tomada del sistema
    en el formato sql (DATE) listo para insertar en la base de datos*/
    public static String obtenerFechaHoy() {
        Calendar hoy = Calendar.getInstance();

        int dia = hoy.get(Calendar.DAY_OF_MONTH);
        int mes = hoy.get(Calendar.MONTH) + 1;
        int año = hoy.get(Calendar.YEAR);

        String fechaHoy = "('" + año + "-"
                + mes + "-"
                + dia + "')";

        return fechaHoy;
    }

    /*Metodo para obtener la fecha y hora actual tomadas del sistema
    en el formato sql (DATETIME) listo para insertar en la base de datos*/
    public static String obtenerFechaHoraHoy() {
        String fecha = "";
        Calendar calendario = Calendar.getInstance();

        int dia = calendario.get(Calendar.DAY_OF_MONTH);
        int mes = calendario.get(Calendar.MONTH) + 1;
        int año = calendario.get(Calendar.YEAR);

        String date = "'" + año + "-"
                + mes + "-"
                + dia + " ";

        int hora = calendario.get(Calendar.HOUR_OF_DAY);
        int minutos = calendario.get(Calendar.MINUTE);
        int segundos = calendario.get(Calendar.SECOND);

        String time = hora + ":" + minutos + ":" + segundos + "'";

        fecha = date + time;

        return fecha;
    }

    /*Metodo para para convertir un objeto Fecha
    en el formato sql (DATE) listo para insertar en la base de datos*/
    public static String obtenerDateDeUnObjFecha(Fecha fecha) {
        int dia = fecha.getDia();
        int mes = fecha.getMes();
        int año = fecha.getAño();

        String fechaDate = "('" + año + "-"
                + mes + "-"
                + dia + "')";

        return fechaDate;
    }

    /*Metodo para para obtener un objeto Fecha
    con la fecha actual tomada del sistema*/
    public static Fecha obtenerObjFechaHoy() {
        Fecha fecha;
        Calendar calendario = Calendar.getInstance();

        int dia = calendario.get(Calendar.DAY_OF_MONTH);
        int mes = calendario.get(Calendar.MONTH) + 1;
        int año = calendario.get(Calendar.YEAR);

        fecha = new Fecha(dia, mes, año);

        return fecha;
    }

    /*Metodo para para obtener un objeto Date
    a partir del objeto fecha recibido*/
//    public static Date obtenerObjDateDeUnObjFecha(Fecha fecha) {
//        Date date = new Date();
//
//        System.out.println("DIA RECIBIDO: " + fecha.getDia());
//        System.out.println("MES RECIBIDO: " + fecha.getMes());
//        System.out.println("AÑO RECIBIDO: " + fecha.getAño());
//
//        date.setDate(fecha.getDia());
//        date.setMonth(fecha.getMes());
//        date.setYear(fecha.getAño());
//
//        System.out.println("==========");
//        System.out.println(date.getDate());
//        System.out.println(date.getMonth());
//        System.out.println(date.getYear());
//        System.out.println("==========");
//        return date;
//    }

    /*Metodo para obtener un objeto Fecha
    con la fecha actual tomada del sistema*/
    public static Fecha obtenerObjFechaDeUnString(String fechaDate) {
        Fecha fecha;

        int año = Integer.parseInt(fechaDate.substring(0, 4));
        int mes = Integer.parseInt(fechaDate.substring(5, 7));
        int dia = Integer.parseInt(fechaDate.substring(8, 10));
        
        fecha = new Fecha(dia, mes, año);

        return fecha;
    }

    public static void llenarComboRubro(JComboBox<String> comboRubro) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un rubro");//es el primer registro q mostrara el combo
        comboRubro.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet rubros = dbAuxiliares.resultSetRubros();

        while (rubros.next()) {
            modeloCombo.addElement(rubros.getObject("descripcion"));
            comboRubro.setModel(modeloCombo);
        }
    }

    public static void llenarComboMarca(JComboBox<String> comboMarcas) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione una marca");//es el primer registro q mostrara el combo
        comboMarcas.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet marcas = dbAuxiliares.resultSetMarcas();

        while (marcas.next()) {
            modeloCombo.addElement(marcas.getObject("descripcion"));
            comboMarcas.setModel(modeloCombo);
        }
    }

    public static void llenarComboProvincias(JComboBox<String> comboProvincias) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione una provincia");//es el primer registro q mostrara el combo
        comboProvincias.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet provincias = dbAuxiliares.resultSetProvincias();

        while (provincias.next()) {
            modeloCombo.addElement(provincias.getObject("nombre"));
            comboProvincias.setModel(modeloCombo);
        }
    }

    public static void llenarComboPaises(JComboBox<String> comboPaises) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un pais");//es el primer registro q mostrara el combo
        comboPaises.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet paises = dbAuxiliares.resultSetPaises();

        while (paises.next()) {
            modeloCombo.addElement(paises.getObject("nombre"));
            comboPaises.setModel(modeloCombo);
        }
    }
    
     public static void llenarComboParentescos(JComboBox<String> comboParentescos) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un parentesco");//es el primer registro q mostrara el combo
        comboParentescos.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet parentescos = dbAuxiliares.resultSetParentescos();

        while (parentescos.next()) {
            modeloCombo.addElement(parentescos.getObject("descripcionParentesco"));
            comboParentescos.setModel(modeloCombo);
        }
       
    }
 
    public static void llenarComboPuestos(JComboBox<String> comboPuestos) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Mostrar todos");//es el primer registro q mostrara el combo
        comboPuestos.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet puestos = dbAuxiliares.resultSetPuestos();

        while (puestos.next()) {
            modeloCombo.addElement(puestos.getObject("descripcion"));
            comboPuestos.setModel(modeloCombo);
        }
    }
    
    public static void llenarComboPuestosNuevoEmpleado(JComboBox<String> comboPuestos) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un puesto");//es el primer registro q mostrara el combo
        comboPuestos.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet puestos = dbAuxiliares.resultSetPuestos();

        while (puestos.next()) {
            modeloCombo.addElement(puestos.getObject("descripcion"));
            comboPuestos.setModel(modeloCombo);
        }
    }

    public static void llenarComboEstadosCivil(JComboBox<String> comboEstadosCivil) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un estado civil");//es el primer registro q mostrara el combo
        comboEstadosCivil.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet estadosCivil = dbAuxiliares.resultSetEstadosCivil();

        while (estadosCivil.next()) {
            modeloCombo.addElement(estadosCivil.getObject("descripcion"));
            comboEstadosCivil.setModel(modeloCombo);
        }
    }
    
    public static void llenarComboSexo(JComboBox<String> comboSexo) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un sexo");//es el primer registro q mostrara el combo
        comboSexo.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet sexos = dbAuxiliares.resultSetSexos();

        while (sexos.next()) {
            modeloCombo.addElement(sexos.getObject("sexoDescripcion"));
            comboSexo.setModel(modeloCombo);
        }
    }

    public static void llenarComboTipoDeCuit(JComboBox<String> comboTipoDeCuit) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione un tipo de cuit");//es el primer registro q mostrara el combo
        comboTipoDeCuit.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet tiposDeCuit = dbAuxiliares.resultSetTipoDeCuit();

        while (tiposDeCuit.next()) {
            modeloCombo.addElement(tiposDeCuit.getObject("descripcion"));
            comboTipoDeCuit.setModel(modeloCombo);
        }
    } 
    
    public static void llenarComboObrasSociales(JComboBox<String> comboObrasSociales) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        modeloCombo.addElement("Seleccione una obra social");//es el primer registro q mostrara el combo
        comboObrasSociales.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet obrasSociales = dbAuxiliares.resultSetObrasSociales();

        while (obrasSociales.next()) {
            modeloCombo.addElement(obrasSociales.getObject("razonSocialOb"));
            comboObrasSociales.setModel(modeloCombo);
        }
    }
    
    public static void mostraElCodigoDelUsuarioActivo(JTextField txtUsuario) throws SQLException, ClassNotFoundException {
        dbUsuarios = new DBUsuarios();
        int codigoEmpleado = dbUsuarios.obtenerElIdEmpleadoDeLaSesionActiva();
        txtUsuario.setText(codigoEmpleado + "");
    }
}
