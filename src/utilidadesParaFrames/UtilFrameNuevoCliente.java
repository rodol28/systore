package utilidadesParaFrames;

import java.sql.SQLException;
import objetosDelDominio.Cliente;
import objetosDelDominio.Domicilio;
import persistencia.DBAuxiliares;
import persistencia.DBClientes;
import vistas.FrameNuevoCliente;

public class UtilFrameNuevoCliente {

    private static DBAuxiliares dbAuxiliares;
    private static DBClientes dbClientes;

    public static void registraCliente(FrameNuevoCliente frameNuevoCliente) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();
        dbClientes = new DBClientes();

        int idTipoDeCuit = dbAuxiliares.buscarCuitPorDescripcion(frameNuevoCliente.getCuitSeleccionado());

        Cliente cliente = new Cliente(frameNuevoCliente.getTxtNroCuit(),
                frameNuevoCliente.getTxtObser(),
                idTipoDeCuit,
                frameNuevoCliente.getTxtDomicilio(),
                frameNuevoCliente.getTxtPais(),
                frameNuevoCliente.getTxtProvincia(),
                frameNuevoCliente.getTxtLocalidad(),
                frameNuevoCliente.getTxtCodPostal(),
                frameNuevoCliente.getTxtNombre(),
                frameNuevoCliente.getTxtApellido(),
                frameNuevoCliente.getTxtMail(),
                frameNuevoCliente.getTxtTelefono());

        dbClientes.altaDeCliente(cliente);
    }
}
