package utilidadesParaFrames;

import java.sql.SQLException;
import objetosDelDominio.Concepto;
import persistencia.DBConceptos;
import vistas.FrameModificarConcepto;

public class UtilFrameModificarConcepto {

    private static DBConceptos dbConceptos;

    public static Concepto obtenerElConcepto(int idConcepto) throws SQLException, ClassNotFoundException {
        Concepto concepto;
        dbConceptos = new DBConceptos();
        concepto = dbConceptos.obtenerElConcepto(idConcepto);
        return concepto;
    }

    public static void cargarLosCampos(FrameModificarConcepto frameModificarConceptos, int idConcepto) throws SQLException, ClassNotFoundException {
        Concepto concepto = obtenerElConcepto(idConcepto);
        frameModificarConceptos.setTxtCodigo(concepto.getIdConcepto() + "");
        frameModificarConceptos.setTxtAbrev(concepto.getAbreviatura());
        frameModificarConceptos.setTxtDescripcion(concepto.getDescripcion());
        frameModificarConceptos.setCheckHaberRemunerativo(concepto.isEsRemunerativo());

        if (concepto.getTipoDeConcepto().equals("Haber")) {
            frameModificarConceptos.setCheckHaber(true);
            frameModificarConceptos.setTxtFijohaber("" + concepto.getCantidadFija());
            frameModificarConceptos.setTxtPorcHaber("" + concepto.getCantidadPorcentual());
        }
        if (concepto.getTipoDeConcepto().equals("Retención")) {
            frameModificarConceptos.setCheckRetencion(true);
            frameModificarConceptos.setTxtFijoRetencion("" + concepto.getCantidadFija());
            frameModificarConceptos.setTxtPorcRetencion("" + concepto.getCantidadPorcentual());
        }
    }

    public static void actualizarConcepto(Concepto concepto) throws ClassNotFoundException {
        dbConceptos = new DBConceptos();
        dbConceptos.modificarElConcepto(concepto);
    }

}
