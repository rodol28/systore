package utilidadesParaFrames;

import java.sql.SQLException;
import objetosDelDominio.Articulo;
import persistencia.DBArticulos;
import persistencia.DBAuxiliares;
import vistas.FrameNuevoArticulo;

public class UtilFrameNuevoArticulo {
    
    private static DBAuxiliares dbAuxiliares;
    private static DBArticulos dbArticulos;
    
    public static void mostraElCodigoDelNuevoArticulo(FrameNuevoArticulo frameNuevoArticulo) throws SQLException, ClassNotFoundException {
        dbArticulos = new DBArticulos();
        int intCodigo = dbArticulos.obtenerElUltimoIdArticulo() + 1;
        frameNuevoArticulo.setTxtCodigo(intCodigo + "");
    }

    public static void registarElArticulo(FrameNuevoArticulo frameNuevoArticulo) throws SQLException, ClassNotFoundException {
        dbArticulos = new DBArticulos();
        dbAuxiliares = new DBAuxiliares();
                
        Articulo articulo = new Articulo(frameNuevoArticulo.getTxtCodigo(),
                frameNuevoArticulo.getTxtDescripcion(),
                frameNuevoArticulo.getTxtPrecio(),
                frameNuevoArticulo.getTxtCosto(),
                frameNuevoArticulo.getTxtStock1(),
                frameNuevoArticulo.getTxtStockMin1(),
                dbAuxiliares.buscarRubroPorDescripcion(frameNuevoArticulo.getRubroSeleccionado()),
                dbArticulos.buscarMarcaPorDescripcion(frameNuevoArticulo.getMarcaSeleccionada()));

        dbArticulos.altaDeArticulo(articulo);
    }
}
