package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import objetosDelDominio.Empleado;
import persistencia.DBAuxiliares;
import persistencia.DBEmpleados;
import persistencia.DBPuestos;
import vistas.FrameModificarEmpleado;

public class UtilFrameModificarEmpleado {

    private static DBEmpleados dbEmpleados;
    private static DBAuxiliares dbAuxiliares;
    private static DBPuestos dbPuestos;

    public static Empleado obtenerElEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        Empleado empleado;
        dbEmpleados = new DBEmpleados();
        dbAuxiliares = new DBAuxiliares();

        empleado = dbEmpleados.obtenerElEmpleado(legajo);
        return empleado;
    }

    public static Empleado cargarLosCampos(FrameModificarEmpleado frameModificarEmpleado, int legajo) throws SQLException, ClassNotFoundException {
        Empleado empleado;

        ArrayList<String> sexos = new ArrayList<String>();
        sexos.add("Masculino");
        sexos.add("Femenino");
        dbEmpleados = new DBEmpleados();
        dbAuxiliares = new DBAuxiliares();

        empleado = dbEmpleados.obtenerElEmpleado(legajo);

        String puestoDescripcion = dbAuxiliares.buscarPuestoPorID(empleado.getIdPuesto());
        String estadoCivilDescripcion = dbAuxiliares.buscarEstadoCivilPorID(empleado.getIdEstadoCivil());
        String sexoDescripcion = empleado.getSexo();
        String obraSocialDescripcion = empleado.getObraSocial();

        int indexPuesto = 0;
        int l = 0;
        int indexEstadoCivil = 0;
        int m = 0;
        int indexSexo = 0;
        int n = 0;
        int indexObraSocial = 0;
        int o = 0;

        DefaultComboBoxModel modeloComboPuesto = new DefaultComboBoxModel();
        DefaultComboBoxModel modeloComboEstadoCivil = new DefaultComboBoxModel();
        DefaultComboBoxModel modeloComboSexo = new DefaultComboBoxModel();
        DefaultComboBoxModel modeloComboObraSocial = new DefaultComboBoxModel();

        frameModificarEmpleado.getComboPuestos().setModel(modeloComboPuesto);
        frameModificarEmpleado.getComboEstadoCivil().setModel(modeloComboEstadoCivil);
        frameModificarEmpleado.getComboSexo().setModel(modeloComboSexo);
        frameModificarEmpleado.getComboObraSocial().setModel(modeloComboObraSocial);

        ResultSet puestos = dbAuxiliares.resultSetPuestos();
        ResultSet estadosCivil = dbAuxiliares.resultSetEstadosCivil();
        ResultSet obrasSociales = dbAuxiliares.resultSetObrasSociales();

        while (puestos.next()) {
            modeloComboPuesto.addElement(puestos.getObject("descripcion"));
            frameModificarEmpleado.getComboPuestos().setModel(modeloComboPuesto);
            if (puestos.getObject("descripcion").equals(puestoDescripcion)) {
                indexPuesto = l;
                l++;
            }
            l++;
        }

        while (estadosCivil.next()) {
            modeloComboEstadoCivil.addElement(estadosCivil.getObject("descripcion"));
            frameModificarEmpleado.getComboEstadoCivil().setModel(modeloComboEstadoCivil);
            if (estadosCivil.getObject("descripcion").equals(estadoCivilDescripcion)) {
                indexEstadoCivil = m;
                m++;
            }
            m++;
        }

        while (obrasSociales.next()) {
            modeloComboObraSocial.addElement(obrasSociales.getObject("razonSocialOb"));
            frameModificarEmpleado.getComboObraSocial().setModel(modeloComboObraSocial);
            if (obrasSociales.getObject("razonSocialOb").equals(obraSocialDescripcion)) {
                indexObraSocial = o;
                o++;
            }
            o++;
        }
        
        for(int idx = 0; idx < sexos.size(); idx++) {
            modeloComboSexo.addElement(sexos.get(idx));
            frameModificarEmpleado.getComboSexo().setModel(modeloComboSexo);
            if (sexos.get(idx).equals(sexoDescripcion)) {
                indexSexo = n;
                n++;
            }
            n++;
        }

        frameModificarEmpleado.setTxtLegajo("" + legajo);
        frameModificarEmpleado.setTxtApellido(empleado.getApellido());
        frameModificarEmpleado.setTxtNombre(empleado.getNombre());
        frameModificarEmpleado.setTxtCuil("" + empleado.getNroCuil());
        frameModificarEmpleado.setTxtDni("" + empleado.getDni());
        frameModificarEmpleado.setTxtDomicilio(empleado.getDomicilio());
        frameModificarEmpleado.setTxtPais(empleado.getPais());
        frameModificarEmpleado.setTxtProvincia(empleado.getProvincia());
        frameModificarEmpleado.setTxtLocalidad(empleado.getLocalidad());
        frameModificarEmpleado.setTxtCodPostal("" + empleado.getCodigoPostal());
        frameModificarEmpleado.setTxtMail(empleado.getEmail());
        frameModificarEmpleado.setTxtTelefono(empleado.getTelefono());
        frameModificarEmpleado.setTxtNacionalidad(empleado.getNacionalidad());
   
        frameModificarEmpleado.getComboPuestos().setSelectedIndex(indexPuesto);
        frameModificarEmpleado.getComboEstadoCivil().setSelectedIndex(indexEstadoCivil);
        frameModificarEmpleado.getComboSexo().setSelectedIndex(indexSexo);
        frameModificarEmpleado.getComboObraSocial().setSelectedIndex(indexObraSocial);

        ResultSet banco = dbAuxiliares.resultSetBanco(dbEmpleados.obtenerElIdEmpleado(legajo));

        while (banco.next()) {
            frameModificarEmpleado.setTxtBanco(banco.getString("razonSocial"));
            empleado.setRazonSocialBanco(banco.getString("razonSocial"));
            frameModificarEmpleado.setTxtCuentaBancaria(banco.getString("cuentaBancaria"));
            empleado.setCuentaBancaria(banco.getString("cuentaBancaria"));
        }

        frameModificarEmpleado.setJdcFechaDeNac(empleado.getFechaNacString());
        frameModificarEmpleado.setJdcFechaDeIng(empleado.getFechaIngString());
        return empleado;
    }

    public static void actualizarElEmpleado(Empleado empleado, FrameModificarEmpleado frameModificarEmpleado) throws SQLException, ClassNotFoundException {

        dbAuxiliares = new DBAuxiliares();
        dbEmpleados = new DBEmpleados();
        dbPuestos = new DBPuestos();
        
        empleado.setApellido(frameModificarEmpleado.getTxtApellido());
        empleado.setNombre(frameModificarEmpleado.getTxtNombre());
        empleado.setNroCuil(frameModificarEmpleado.getTxtCuil());
        empleado.setDni(frameModificarEmpleado.getTxtDni());
        empleado.setFechaNacString("" + frameModificarEmpleado.getJdcFechaDeNac());
        empleado.setSexo(frameModificarEmpleado.getSexoSeleccionado());
        empleado.setEstadoCivil(dbAuxiliares.buscarEstadoCivilPorDescripicion(frameModificarEmpleado.getEstadoCivilSeleccionado()));
        empleado.setNacionalidad(frameModificarEmpleado.getTxtNacionalidad());
        
        empleado.setDomicilio(frameModificarEmpleado.getTxtDomicilio());
        empleado.setPais(frameModificarEmpleado.getTxtPais());
        empleado.setProvincia(frameModificarEmpleado.getTxtProvincia());
        empleado.setLocalidad(frameModificarEmpleado.getTxtLocalidad());
        empleado.setCodigoPostal(frameModificarEmpleado.getTxtCodPostal());
        empleado.setEmail(frameModificarEmpleado.getTxtMail());
        empleado.setTelefono(frameModificarEmpleado.getTxtTelefono());
        
        empleado.setFechaIngString("" + frameModificarEmpleado.getJdcFechaDeIng());
        empleado.setIdPuesto(dbPuestos.buscarPuestoPorDescripcion(frameModificarEmpleado.getPuestoSeleccionado()));
        empleado.setIdObraSocial(dbAuxiliares.buscarObraSocialPorRazonSocial(frameModificarEmpleado.getObraSocialSeleccionada()));
        empleado.setRazonSocialBanco(frameModificarEmpleado.getTxtBanco());
        empleado.setCuentaBancaria(frameModificarEmpleado.getTxtCuentaBancaria());
        dbEmpleados.modificarElEmpleado(empleado);
    }
}
