package utilidadesParaFrames;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import objetosDelDominio.Articulo;
import objetosDelDominio.LineaDeVenta;
import objetosDelDominio.Venta;
import persistencia.DBArticulos;
import persistencia.DBVentas;
import vistas.FrameFactura;

public class UtilFrameFactura {

    private static DBVentas dbVentas;
    private static DBArticulos dbArticulos;
    private static Articulo articulo;
    private static DefaultTableModel modelo;
    private static int contadorDeArticulo = 0;
    private static Venta venta;
    private static LineaDeVenta lineaDeVenta;

    public static void mostraElCodigoDeLaNuevaVenta(FrameFactura frameFactura) throws SQLException, ClassNotFoundException {
        dbVentas = new DBVentas();
        int codigo = dbVentas.obtenerElUltimoIdVenta() + 1;
        frameFactura.setTxtNroFactura(codigo + "");
    }
    
    public static void agregarArticuloEnTabla(int idArticulo, JTable tablaLineasDeCompra, FrameFactura frameFactura) throws SQLException, ClassNotFoundException {

        dbArticulos = new DBArticulos();

        String registro[] = new String[5];

        articulo = new Articulo();
        articulo = dbArticulos.obtenerElArticulo(idArticulo);

        //Si es primer producto que es agragado se carga el modelo
        //de la tabla sino no debe ser cargado.
        if (contadorDeArticulo == 0) {
            cargarElModeloDeLaTabla(tablaLineasDeCompra);
            contadorDeArticulo++;
        }

        registro[0] = "1";
        registro[1] = articulo.getCodigo() + "";
        registro[2] = articulo.getDescripcion();
        registro[3] = articulo.getPrecioUnitario() + "";
        registro[4] = "" + articulo.getPrecioUnitario() * 1;
        UtilFrameFactura.modelo.addRow(registro); //Esto es hacer un this al modelo
        calcularTotal(frameFactura);
    }

    public static void cargarElModeloDeLaTabla(JTable tabla) {

        modelo = new DefaultTableModel();

        String titulos[] = {"Cantidad", "Codigo", "Descripción", "$ Unit.", "$ Subtotal"};
        modelo.setColumnIdentifiers(titulos);
        tabla.setModel(modelo);

        TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(30);
        columnModel.getColumn(1).setPreferredWidth(5);
        columnModel.getColumn(2).setPreferredWidth(200);
    }

    public static void setContadorDeArticulo(int contadorDeArticulo) {
        UtilFrameFactura.contadorDeArticulo = contadorDeArticulo;
    }

    public static int getFilaSeleccionada(FrameFactura frameFactura) {
        int i = -1;
        i = frameFactura.getTablaLineasDeVenta().getSelectedRow();
        if (i > -1) {
            return i;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public static int getCantidadIngresada(int i) {
        int cantidad = 1;
        String c = "1";
        if (i > -1) {
            c = JOptionPane.showInputDialog("Ingrese cantidad:");
            if (c != null) {
                cantidad = Integer.parseInt(c);
                return cantidad;
            }
        }
        return cantidad;
    }

    /*Actuliza la tabla de articulos con la cantidad adquira.
    Modifica las columnas cantidad y stock actual*/
    public static void actualizarCantidad(int cantidad, int fila, FrameFactura frameFactura) {
        float precio = Float.parseFloat((String) frameFactura.getTablaLineasDeVenta().getModel().getValueAt(fila, 3));
        frameFactura.getTablaLineasDeVenta().getModel().setValueAt(calcularSubtotal(cantidad, precio), fila, 4);
        frameFactura.getTablaLineasDeVenta().getModel().setValueAt(cantidad, fila, 0);
        calcularTotal(frameFactura);
    }

    public static float calcularSubtotal(int cantidad, float precio) {
        return (cantidad * precio);
    }

    public static void calcularTotal(FrameFactura frameFactura) {
        float total = 0, subtotal = 0;
        DefaultTableModel modelo = (DefaultTableModel) frameFactura.getTablaLineasDeVenta().getModel();
        for (int fila = 0; fila < modelo.getRowCount(); fila++) {
            subtotal = Float.parseFloat("" + modelo.getValueAt(fila, 4));
            total = total + subtotal;
        }
        frameFactura.setTxtTotal(total + "");
    }

    public static void eliminarUnItem(int fila, FrameFactura frameFactura) {
        recalcularTotal(fila, frameFactura);
        DefaultTableModel modelo = (DefaultTableModel) frameFactura.getTablaLineasDeVenta().getModel();
        modelo.removeRow(fila);
    }

    public static void recalcularTotal(int fila, FrameFactura frameFactura) {
        float total = 0, subtotal = 0;
        subtotal = Float.parseFloat("" + modelo.getValueAt(fila, 4));
        total = frameFactura.getTxtTotal();
        total = total - subtotal;
        frameFactura.setTxtTotal(total + "");
    }

    public static boolean registrarVenta(FrameFactura frameFactura) throws ClassNotFoundException, SQLException {

        dbArticulos = new DBArticulos();

        int idArticulo = 0;
        float precio = 0;
        int cantidad = 0;
        int stockActualDelArticulo = 0;
        int idEmpleado = 0;
        boolean stockMayorACantidad = true;

        for (int j = 0; j < frameFactura.getTablaLineasDeVenta().getRowCount(); j++) {
            idArticulo = Integer.parseInt("" + frameFactura.getTablaLineasDeVenta().getModel().getValueAt(j, 1));
            cantidad = Integer.parseInt("" + frameFactura.getTablaLineasDeVenta().getModel().getValueAt(j, 0));

            stockActualDelArticulo = dbArticulos.obtenerStockActualDelArticulo(idArticulo);
            if (stockActualDelArticulo < cantidad) {
                stockMayorACantidad = false;
            }
        }

        if (stockMayorACantidad) {
            dbVentas = new DBVentas();

            idEmpleado = frameFactura.getTxtVendedor();
            venta = new Venta(frameFactura.getTxtNroFactura(), UtilAuxiliares.obtenerObjFechaHoy(), frameFactura.getTxtTotal(), idEmpleado);

            for (int i = 0; i < frameFactura.getTablaLineasDeVenta().getRowCount(); i++) {
                idArticulo = Integer.parseInt("" + frameFactura.getTablaLineasDeVenta().getModel().getValueAt(i, 1));
                precio = Float.parseFloat("" + frameFactura.getTablaLineasDeVenta().getModel().getValueAt(i, 3));
                cantidad = Integer.parseInt("" + frameFactura.getTablaLineasDeVenta().getModel().getValueAt(i, 0));

                articulo = new Articulo(idArticulo, precio);
                lineaDeVenta = new LineaDeVenta(articulo, cantidad);
                venta.agregarLineaDeVenta(lineaDeVenta);
            }
            dbVentas.altaDeVenta(venta);
            stockMayorACantidad = true;
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Un articulo no dispone de stock sufiente para realizar la venta.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    
    public static int obtenerLaCantidadDeFilasDeLaTabla(JTable tablaArticulos) {
        DefaultTableModel modelo = (DefaultTableModel) tablaArticulos.getModel();
        return modelo.getRowCount();
    }
}
