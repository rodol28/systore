package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBArticulos;

public class UtilFrameMenuArticulos {

    private static DBArticulos dbArticulos;

    public static void mostrarArticulosEnTabla(JTable tablaArticulos) throws SQLException, ClassNotFoundException {

        dbArticulos = new DBArticulos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cod.", "Descripción", "Marca", "Rubro",
            "Precio de venta", "Precio de compra", "Stock", "Stock minimo"};
        modelo.setColumnIdentifiers(titulos);
        tablaArticulos.setModel(modelo);
        String registro[] = new String[8];

        TableColumnModel columnModel = tablaArticulos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        columnModel.getColumn(1).setPreferredWidth(200);
//        columnModel.getColumn(2).setPreferredWidth(200);
//        columnModel.getColumn(3).setPreferredWidth(250);

        ResultSet articulos = dbArticulos.resultSetArticulos();

        while (articulos.next()) {
            registro[0] = String.valueOf(articulos.getInt("idArticulo"));
            registro[1] = articulos.getString("descripcion");
            registro[2] = dbArticulos.obtenerMarcaDelArticulo(articulos.getInt("Marca_idMarca"));
            registro[3] = dbArticulos.obtenerRubroDelArticulo(articulos.getInt("Rubro_idRubro"));
            registro[4] = String.valueOf(articulos.getFloat("precioUnitario"));
            registro[5] = String.valueOf(articulos.getFloat("costo"));
            registro[6] = String.valueOf(articulos.getInt("stock"));
            registro[7] = String.valueOf(articulos.getInt("stockMin"));
            modelo.addRow(registro);
        }
    }

    public static void buscar(JTable tablaArticulos, String palabra) throws SQLException, ClassNotFoundException {
        dbArticulos = new DBArticulos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Codigo", "Descripción", "Marca", "Rubro",
            "Precio de venta", "Precio de compra", "Stock", "Stock minimo"};
        modelo.setColumnIdentifiers(titulos);
        tablaArticulos.setModel(modelo);
        String registro[] = new String[8];

        ResultSet articulos = dbArticulos.busquedaPredictivaDeDescripcion(palabra);

        while (articulos.next()) {
            registro[0] = String.valueOf(articulos.getInt("idArticulo"));
            registro[1] = articulos.getString("descripcion");
            registro[2] = dbArticulos.obtenerMarcaDelArticulo(articulos.getInt("Marca_idMarca"));
            registro[3] = dbArticulos.obtenerRubroDelArticulo(articulos.getInt("Rubro_idRubro"));
            registro[4] = String.valueOf(articulos.getFloat("precioUnitario"));
            registro[5] = String.valueOf(articulos.getFloat("costo"));
            registro[6] = String.valueOf(articulos.getInt("stock"));
            registro[7] = String.valueOf(articulos.getInt("stockMin"));
            modelo.addRow(registro);
        }
    }

//    public static int getCodigoIngresado() {
//        int codigo = -1;
//        String txtCodigo = "";
//        txtCodigo = "" + JOptionPane.showInputDialog("Ingrese el codigo de articulo: ");
//        if (txtCodigo.equals("") == false) {
//            codigo = Integer.parseInt(txtCodigo);
//            return codigo;
//        }
//        return codigo;
//    }
    public static int getCodigoIngresado() {
        int codigo = -1;
        String c = "";
        c = "" + JOptionPane.showInputDialog("Ingrese el codigo de articulo: ");

        if (c.equals("") == false) {
            if (c.equals("null")) {
                return codigo;
            }
        } 
        if (c.equals("") == false) {
            codigo = Integer.parseInt(c);
            return codigo;
        }
        return codigo;
    }

    public static void mostrarArticuloXEnTabla(JTable tablaArticulos, int nroArticulo) throws SQLException, ClassNotFoundException {
        dbArticulos = new DBArticulos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cod.", "Descripción", "Marca", "Rubro",
            "Precio de venta", "Precio de compra", "Stock", "Stock minimo"};
        modelo.setColumnIdentifiers(titulos);
        tablaArticulos.setModel(modelo);
        String registro[] = new String[8];

        TableColumnModel columnModel = tablaArticulos.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(5);
        columnModel.getColumn(1).setPreferredWidth(200);
//        columnModel.getColumn(2).setPreferredWidth(200);
//        columnModel.getColumn(3).setPreferredWidth(250);

        ResultSet articulo = dbArticulos.obtenerElArticuloNroX(nroArticulo);

        if (articulo == null) {
            mostrarArticulosEnTabla(tablaArticulos);
        } else {
            while (articulo.next()) {
                registro[0] = String.valueOf(articulo.getInt("idArticulo"));
                registro[1] = articulo.getString("descripcion");
                registro[2] = dbArticulos.obtenerMarcaDelArticulo(articulo.getInt("Marca_idMarca"));
                registro[3] = dbArticulos.obtenerRubroDelArticulo(articulo.getInt("Rubro_idRubro"));
                registro[4] = String.valueOf(articulo.getFloat("precioUnitario"));
                registro[5] = String.valueOf(articulo.getFloat("costo"));
                registro[6] = String.valueOf(articulo.getInt("stock"));
                registro[7] = String.valueOf(articulo.getInt("stockMin"));
                modelo.addRow(registro);
            }
        }

    }
}
