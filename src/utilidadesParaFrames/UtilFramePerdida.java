package utilidadesParaFrames;

import controladoresAuxiliares.ControlFrameElegirMotivoDeSalida;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import objetosDelDominio.Articulo;
import objetosDelDominio.LineaDePerdida;
import objetosDelDominio.Perdida;
import persistencia.DBArticulos;
import persistencia.DBAuxiliares;
import persistencia.DBPerdidas;
import vistas.FramePerdida;
import vistasAuxiliares.FrameBuscarArticulo;

public class UtilFramePerdida {

    private static DBPerdidas dbPerdidas;
    private static DBArticulos dbArticulos;
    private static Articulo articulo;
    private static DefaultTableModel modelo;
    //Variable bandera que indica que un articulo ya fue agregado a la tabla
    //y que lo tanto el modelo de la tabla ya se cargo y no es necesario volver a cargarlo.
    private static int contadorDeArticulo = 0;
    private static String motivoDeSalida = "";
    private static Perdida perdida;
    private static LineaDePerdida lineaDePerdida;
    private static DBAuxiliares dbAuxiliares;

    public static void agregarArticuloEnTabla(int idArticulo, JTable tablaLineasDePerdida, boolean metodoDeIngreso, FrameBuscarArticulo frameBuscarArticulo) throws SQLException, ClassNotFoundException {
        /*El parametro "metodoDeIngreso" recibido en es metodo indica la forma en que
        se esta ingresando en articulo.
        - true ---> ingresando el codigo y presionando enter.
        - false ---> ingresando atraves de la frame auxiliar.*/
        dbArticulos = new DBArticulos();

        String registro[] = new String[7];

        articulo = new Articulo();
        articulo = dbArticulos.obtenerElArticulo(idArticulo);

        if (articulo != null) {//Si es primer producto que es agragado se carga el modelo
            //de la tabla sino no debe ser cargado.
            if (contadorDeArticulo == 0) {
                cargarElModeloDeLaTabla(tablaLineasDePerdida);
                contadorDeArticulo++;
            }
            System.out.println("HOLA 2");
            ControlFrameElegirMotivoDeSalida controlFrameElegirMotivoDeSalida = new ControlFrameElegirMotivoDeSalida(frameBuscarArticulo);

            registro[0] = motivoDeSalida;

            int cantidad = getCantidadIngresada();
            int stockNuevo = articulo.getStock() - cantidad;

            registro[1] = "" + cantidad;
            registro[2] = "" + articulo.getCodigo();
            registro[3] = articulo.getDescripcion();
            registro[4] = "$ " + articulo.getPrecioUnitario();
            registro[5] = "" + articulo.getStock();
            registro[6] = "" + stockNuevo;
            UtilFramePerdida.modelo.addRow(registro); //Esto es hacer un this al modelo
        } else {
            JOptionPane.showMessageDialog(null, "No se encontro un articulo con el codigo ingresado.");
        }
    }

    public static void cargarElModeloDeLaTabla(JTable tabla) {
        modelo = new DefaultTableModel();

        String titulos[] = {"Motivo", "Cant.", "Cod.", "Descripción", "Precio", "Stock ant.", "Actual"};
        modelo.setColumnIdentifiers(titulos);
        tabla.setModel(modelo);

        TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(55);
        columnModel.getColumn(1).setPreferredWidth(5);
        columnModel.getColumn(2).setPreferredWidth(5);
        columnModel.getColumn(3).setPreferredWidth(200);
        columnModel.getColumn(4).setPreferredWidth(10);
        columnModel.getColumn(5).setPreferredWidth(35);
        columnModel.getColumn(6).setPreferredWidth(25);
    }

    public static void setContadorDeArticulo(int contadorDeArticulo) {
        UtilFramePerdida.contadorDeArticulo = contadorDeArticulo;
    }

    public static void getMotivoDeSalida(String motivo) {
        motivoDeSalida = motivo;
    }

    public static int getCantidadIngresada() {
        int cantidad = 1;
        String txtCantidad = "";
        txtCantidad = "" + JOptionPane.showInputDialog("Ingrese el cantidad:");
        if (txtCantidad.equals("") == false) {
            cantidad = Integer.parseInt(txtCantidad);
            return cantidad;
        }
        return cantidad;
    }

    public static void llenarComboMotivos(JComboBox<String> comboMotivos) throws SQLException, ClassNotFoundException {
        dbPerdidas = new DBPerdidas();

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        comboMotivos.setModel(modeloCombo);//con esto lo agregamos el modelo al jcombobox

        ResultSet motivos = dbPerdidas.resultSetMotivos();

        while (motivos.next()) {
            modeloCombo.addElement(motivos.getObject("descripcionMotivo"));
            comboMotivos.setModel(modeloCombo);
        }
    }

    public static void registrarPerdida(FramePerdida framePerdida) throws ClassNotFoundException, SQLException {
        int codigoArticulo = 0;
        int cantidad = 0;
        int stockNuevo = 0;
        int idMotivo = 0;
        String motivo = "";

        dbPerdidas = new DBPerdidas();
        dbAuxiliares = new DBAuxiliares();

        /*Se crea la perdida*/
        perdida = new Perdida(framePerdida.getTxtObservacion(), framePerdida.getTxtVendedor());

        /*Se crean las lineas de perdida*/
        for (int i = 0; i < framePerdida.getTablaLineasDePerdida().getRowCount(); i++) {
            motivo = "" + framePerdida.getTablaLineasDePerdida().getModel().getValueAt(i, 0);
            cantidad = Integer.parseInt("" + framePerdida.getTablaLineasDePerdida().getModel().getValueAt(i, 1));
            codigoArticulo = Integer.parseInt("" + framePerdida.getTablaLineasDePerdida().getModel().getValueAt(i, 2));
            stockNuevo = Integer.parseInt("" + framePerdida.getTablaLineasDePerdida().getModel().getValueAt(i, 6));

            articulo = new Articulo(codigoArticulo, stockNuevo);
            idMotivo = dbAuxiliares.buscarMotivoDePerdidaPorDescripcion(motivo);
            lineaDePerdida = new LineaDePerdida(articulo, cantidad, idMotivo);
            perdida.agregarLineaDePerdida(lineaDePerdida);
        }
        dbPerdidas.altaDePerdida(perdida);
    }

    public static int getFilaSeleccionada(FramePerdida framePerdida) {
        int i = -1;
        i = framePerdida.getTablaLineasDePerdida().getSelectedRow();
        if (i > -1) {
            return i;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return i;
    }

    public static int getCantidadIngresada(int i) {
        int cantidad = 1;
        String c = "1";
        if (i > -1) {
            c = JOptionPane.showInputDialog("Ingrese cantidad:");
            if (c.equals("") == false) {
                if (c.equals("null")) {
                    return cantidad;
                }
            }
            if (c.equals("") == false) {
                cantidad = Integer.parseInt(c);
                return cantidad;
            }
        }
        return cantidad;
    }

    /*Actuliza la tabla de lineas de perdida con la cantidad perdida.
    Modifica las columnas cantidad y stock actual*/
    public static void actualizarCantidadStockActual(int cantidad, int fila, JTable tablaLineasDePerdida) {
        int stockAnt = Integer.parseInt((String) tablaLineasDePerdida.getModel().getValueAt(fila, 5));
        int stockNuevo = stockAnt - cantidad;
        tablaLineasDePerdida.getModel().setValueAt(cantidad, fila, 1);
        tablaLineasDePerdida.getModel().setValueAt(stockNuevo, fila, 6);
    }

    public static void eliminarUnItem(int fila, JTable tablaLineasDePerdida) {
        DefaultTableModel modelo = (DefaultTableModel) tablaLineasDePerdida.getModel();
        modelo.removeRow(fila);
    }
}
