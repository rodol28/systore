package utilidadesParaFrames;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import persistencia.DBArticulos;
import persistencia.DBEmpleados;
import persistencia.DBPersonas;
import persistencia.DBVentas;
import sun.swing.table.DefaultTableCellHeaderRenderer;

public class UtilFrameConsultarVentas {

    private static DBVentas dbVentas;
    private static DBArticulos dbArticulos;
    private static DBEmpleados dbEmpleados;
    private static DBPersonas dbPersonas;

    public static void mostrarVentasEnTabla(JTable tablaVenta, JTable tablaDeLineaDeVenta,JTextField txtNroFactura,JTextField txtFecha, JTextField txtVendedor,int nroFactura) throws SQLException, ClassNotFoundException {
        dbVentas = new DBVentas();

        if (nroFactura == -1) {
            limpiarTablaLineasDeVenta(tablaDeLineaDeVenta);
            txtFecha.setText("");
            txtNroFactura.setText("");
            txtVendedor.setText("");
        }
        
        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Nro. Factura", "Fecha", "Total"};
        modelo.setColumnIdentifiers(titulos);
        tablaVenta.setModel(modelo);
        String registro[] = new String[3];

        TableColumnModel columnModel = tablaVenta.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(2);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaVenta.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaVenta.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaVenta.getColumnModel().getColumn(2).setCellRenderer(tcr);

        ResultSet ventas = dbVentas.resultSetVentas();

        while (ventas.next()) {
            registro[0] = String.valueOf(ventas.getInt("idVenta"));
            registro[1] = String.valueOf(ventas.getDate("fecha"));
            registro[2] = "$ " + String.valueOf(ventas.getInt("total"));
            modelo.addRow(registro);
        }
    }

    public static void mostrarLineasDeVenta(JTextField txtFecha, JTable tablaLineasDeVenta, int idVenta) throws SQLException, ClassNotFoundException {
        dbVentas = new DBVentas();
        dbArticulos = new DBArticulos();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Cantidad", "Descripcion", "Precio", "Subtotal"};
        modelo.setColumnIdentifiers(titulos);
        tablaLineasDeVenta.setModel(modelo);
        String registro[] = new String[4];

        TableColumnModel columnModel = tablaLineasDeVenta.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(2);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaLineasDeVenta.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaLineasDeVenta.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaLineasDeVenta.getColumnModel().getColumn(2).setCellRenderer(tcr);
        tablaLineasDeVenta.getColumnModel().getColumn(3).setCellRenderer(tcr);

        ResultSet lineasDeVenta = dbVentas.resultSetLineasDeVenta(idVenta);

        while (lineasDeVenta.next()) {
            registro[0] = String.valueOf(lineasDeVenta.getInt("cantidad"));
            registro[1] = dbArticulos.obtenerDescripcionDelArticulo(lineasDeVenta.getInt("Articulo_idArticulo"));
            registro[2] = String.valueOf(dbArticulos.obtenerPrecioDelArticulo(lineasDeVenta.getInt("Articulo_idArticulo")));
            registro[3] = "$ " + calcularSubtotal(lineasDeVenta.getInt("cantidad"), dbArticulos.obtenerPrecioDelArticulo(lineasDeVenta.getInt("Articulo_idArticulo")));
            modelo.addRow(registro);
        }
    }

    public static float calcularSubtotal(int cantidad, float precio) {
        float subtotal = cantidad * precio;
        return subtotal;
    }

    public static void mostrarFecha(int idVenta, JTextField txtFecha) throws SQLException, ClassNotFoundException {
        dbVentas = new DBVentas();
        txtFecha.setText(dbVentas.obtenerLaFechaDeVenta(idVenta));
    }

    public static void mostrarVendedor(int idVenta, JTextField txtVendedor) throws SQLException, ClassNotFoundException {
        dbVentas = new DBVentas();
        dbEmpleados = new DBEmpleados();
        dbPersonas = new DBPersonas();
        int idEmpleado = dbVentas.obtenerElIdEmpleadoEnLaVenta(idVenta);
        int idPersona = dbEmpleados.obtenerElIdPersona(idEmpleado);
        txtVendedor.setText(dbPersonas.obtenerElNombreDeLaPersona(idPersona));
    }

    public static int getCodigoIngresado() {
        int codigo = -1;
        String txtCodigo = "";
        txtCodigo = "" + JOptionPane.showInputDialog("Ingrese el nro de factura: ");
        if (txtCodigo.equals("") == false) {
            codigo = Integer.parseInt(txtCodigo);
            return codigo;
        }
        return codigo;
    }

    public static void mostrarVentaXEnTabla(JTable tablaCompras, JTable tablaLineasDeVenta, JTextField txtFecha,int nroFactura) throws SQLException, ClassNotFoundException {
        dbVentas = new DBVentas();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Nro. Factura", "Fecha", "Total"};
        modelo.setColumnIdentifiers(titulos);
        tablaCompras.setModel(modelo);
        String registro[] = new String[3];

        TableColumnModel columnModel = tablaCompras.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(2);
        columnModel.getColumn(1).setPreferredWidth(100);
        columnModel.getColumn(2).setPreferredWidth(100);

        DefaultTableCellRenderer tcr = new DefaultTableCellHeaderRenderer();
        tcr.setHorizontalAlignment(SwingConstants.CENTER);
        tablaCompras.getColumnModel().getColumn(0).setCellRenderer(tcr);
        tablaCompras.getColumnModel().getColumn(1).setCellRenderer(tcr);
        tablaCompras.getColumnModel().getColumn(2).setCellRenderer(tcr);

        ResultSet venta = dbVentas.obtenerLaVentaNroX(nroFactura);

        while (venta.next()) {
            registro[0] = String.valueOf(venta.getInt("idVenta"));
            registro[1] = String.valueOf(venta.getDate("fecha"));
            registro[2] = "$ " + String.valueOf(venta.getInt("total"));
            modelo.addRow(registro);
        }
        
        mostrarLineasDeVenta(txtFecha,tablaLineasDeVenta, nroFactura);
    }
    
    public static void limpiarTablaLineasDeVenta(JTable tablaLineasDeVenta) {
        DefaultTableModel modelo = (DefaultTableModel) tablaLineasDeVenta.getModel();
        for (int fila = tablaLineasDeVenta.getRowCount() - 1; fila >= 0 ; fila--) {
            modelo.removeRow(fila);
        }
    }
}
