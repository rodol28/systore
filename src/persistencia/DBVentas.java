package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import objetosDelDominio.LineaDeVenta;
import objetosDelDominio.Venta;

public class DBVentas {

    private DBArticulos dbArticulos;

    public ResultSet resultSetVentas() throws SQLException, ClassNotFoundException {
        ResultSet ventas = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM venta";
            Statement consulta = con.getConexion().createStatement();
            ventas = consulta.executeQuery(url);
            return ventas;
        } catch (SQLException ex) {
        }
        return ventas;
    }

    public ResultSet obtenerLaVentaNroX(int nroFactura) throws SQLException, ClassNotFoundException {
        ResultSet venta = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM venta WHERE `idVenta`=" + nroFactura;
            Statement consulta = con.getConexion().createStatement();
            venta = consulta.executeQuery(url);
            return venta;
        } catch (SQLException ex) {
        }
        return venta;
    }

    public ResultSet resultSetLineasDeVenta(int idVenta) throws SQLException, ClassNotFoundException {
        ResultSet lineasDeVenta = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM lineadeventa WHERE Venta_idVenta=" + idVenta;
            Statement consulta = con.getConexion().createStatement();
            lineasDeVenta = consulta.executeQuery(url);
            return lineasDeVenta;
        } catch (SQLException ex) {
        }
        return lineasDeVenta;
    }

    public String obtenerLaFechaDeVenta(int idVenta) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        String fecha = "";
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM venta WHERE idVenta=" + idVenta);
            rs = st.executeQuery();

            while (rs.next()) {
                fecha = rs.getString("fecha");
            }

            return fecha;
        } catch (SQLException ex) {
        }
        return fecha;
    }

    public static int obtenerElUltimoIdVenta() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idVenta = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idVenta) AS id FROM venta");
        while (rs.next()) {
            idVenta = rs.getInt("id");
        }
        return idVenta;
    }

    public int obtenerElIdEmpleadoEnLaVenta(int idVenta) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idEmpleado = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT Empleado_idEmpleado AS id FROM venta WHERE idVenta=" + idVenta);
        while (rs.next()) {
            idEmpleado = rs.getInt("id");
        }
        return idEmpleado;
    }

    public void altaDeVenta(Venta venta) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String fecha = "('" + venta.getFecha().getAño() + "-"
                    + venta.getFecha().getMes() + "-"
                    + venta.getFecha().getDia() + "')";

            String table = "INSERT INTO `venta` "
                    + "(`idVenta`, "
                    + "`fecha`, "
                    + "`total`, "
                    + "`Empleado_idEmpleado`) ";

            String datos = "VALUES (NULL, "
                    + fecha + ","
                    + " '" + venta.getTotal() + "',"
                    + " '" + venta.getIdEmpleado() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            altaLineasDeCompra(venta.getLineasDeVenta(), venta.getIdventa());

            JOptionPane.showMessageDialog(null, "Venta registrada.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void altaLineasDeCompra(ArrayList<LineaDeVenta> lineas, int idVenta) throws SQLException, ClassNotFoundException {

        dbArticulos = new DBArticulos();
        Conector con = new MySqlConexion();
        con.conectar();

        int cantidad = 0;
        int idArticulo = 0;

        String table = "INSERT INTO `lineadeventa` "
                + "(`idLineaDeVenta`, "
                + "`cantidad`, "
                + "`Articulo_idArticulo`, "
                + "`Venta_idVenta`) ";

        String datos = "";

        for (int i = 0; i < lineas.size(); i++) {
            cantidad = lineas.get(i).getCantidad();
            idArticulo = lineas.get(i).getArticulo().getCodigo();

            datos = "VALUES (NULL, "
                    + " '" + cantidad + "',"
                    + " '" + idArticulo + "',"
                    + " '" + idVenta + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            dbArticulos.actualizarStockDelArticulo(idArticulo, cantidad);
        }
    }
}
