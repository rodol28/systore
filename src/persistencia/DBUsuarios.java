package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetosDelDominio.Usuario;
import servicios.StringEncrypt;
import utilidadesParaFrames.UtilAuxiliares;

public class DBUsuarios {

    private Conector con;
    private Usuario usuario;

    public ResultSet resultSetDatosDeUsuarios() throws SQLException, ClassNotFoundException {
        ResultSet usuarios = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT `legajo`, `apellido`, `nombre`, `user`, `descripcion` "
                    + "FROM `usuario`, `empleado`, `persona`, `tipodeusuario` "
                    + "WHERE Empleado_idEmpleado=idEmpleado "
                    + "AND TipoDeUsuario_idTipoDeUsuario=idTipoDeUsuario "
                    + "AND Persona_idPersona=idPersona "
                    + "AND estadoDelUsuario=1";

            Statement consulta = con.getConexion().createStatement();
            usuarios = consulta.executeQuery(url);
            return usuarios;
        } catch (SQLException ex) {
        }
        return usuarios;
    }

    public void altaDeUsuario(Usuario usuario) throws ClassNotFoundException, Exception {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String passEncriptado = StringEncrypt.encrypt(StringEncrypt.getKey(), StringEncrypt.getiv(), usuario.getPass());

            String table = "INSERT INTO `usuario`(`idUsuario`, `user`, `pass`, `estadoDelUsuario`, `Empleado_idEmpleado`, `TipoDeUsuario_idTipoDeUsuario`) ";
            String datos = "VALUES (NULL,'"
                    + usuario.getUser() + "','"
                    + passEncriptado + "','"
                    + 1 + "','"
                    + usuario.getIdEmpleado() + "','"
                    + usuario.getIdTipo() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
            JOptionPane.showMessageDialog(null, "Usuario registrado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void modificarUsuario(String usuario, String clave) throws ClassNotFoundException, Exception {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String passEncriptado = StringEncrypt.encrypt(StringEncrypt.getKey(), StringEncrypt.getiv(), clave);

            String sql = "UPDATE `usuario` SET "
                    + "`pass`='" + passEncriptado + "'"
                    + "	WHERE `user`='" + usuario + "'";

            con.getConexion().createStatement().execute(sql);

            JOptionPane.showMessageDialog(null, "Usuario modificado.");
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public void bajaDelusuario(String nombreDeUsuario) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `usuario` SET "
                    + "`estadoDelUsuario`='" + 0 + "'"
                    + "	WHERE `user`='" + nombreDeUsuario + "'";

            con.getConexion().createStatement().execute(sql);

            JOptionPane.showMessageDialog(null, "Usuario eliminado.");
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }
    
    public void bajaDelusuarioDelEmpleadoX(int idEmpleado) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `usuario` SET "
                    + "`estadoDelUsuario`='" + 0 + "'"
                    + "	WHERE `Empleado_idEmpleado`='" + idEmpleado + "'";

            con.getConexion().createStatement().execute(sql);
    
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public boolean buscarUsuarioPorNombreDeUsuario(String nombreDeUsuario) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT user FROM usuario WHERE user='" + nombreDeUsuario + "'");
        while (rs.next()) {
            if (rs.getString("user").equals(nombreDeUsuario)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public boolean verificarLaExisteciaDeUnUsuarioParaElEmpleado(int idEmpleado) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int cantidadDeRegistros = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT * FROM `usuario` WHERE `Empleado_idEmpleado`='" + idEmpleado + "'"
                + "AND `estadoDelUsuario`='1'");
        
        while (rs.next()) {
            cantidadDeRegistros++;
        }
        
        if (cantidadDeRegistros > 0) {
            return true;
        } else {
            return false;
        }  
    }

    public int buscarTipoDeUsuarioPorDescripcion(String tipoDeUsuarioDescripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idTipoDeUsuario = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idTipoDeUsuario FROM tipodeusuario WHERE descripcion='" + tipoDeUsuarioDescripcion + "'");
        while (rs.next()) {
            idTipoDeUsuario = rs.getInt("idTipoDeUsuario");
            return idTipoDeUsuario;
        }
        return idTipoDeUsuario;
    }

    public int obtenerElTipoDeUsuario(String nombreUsuario) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idTipoDeUsuario = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT TipoDeUsuario_idTipoDeUsuario FROM usuario WHERE user='" + nombreUsuario + "'");
        while (rs.next()) {
            idTipoDeUsuario = rs.getInt("TipoDeUsuario_idTipoDeUsuario");
            return idTipoDeUsuario;
        }
        return idTipoDeUsuario;
    }

    public String buscarDecripcionPorIdTipoDeUsuario(int idTipoDeUsuario) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String descripcionDeTipoDeUsuario = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT descripcion FROM tipodeusuario WHERE idTipoDeUsuario='" + idTipoDeUsuario + "'");
        while (rs.next()) {
            descripcionDeTipoDeUsuario = rs.getString("descripcion");
            return descripcionDeTipoDeUsuario;
        }
        return descripcionDeTipoDeUsuario;
    }

    public void registrarInicioDeLaSesion(String user) throws SQLException, ClassNotFoundException, Exception {
        Usuario usuario = obtenerElUsuario(user);
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `sesionusuario` "
                    + "(`idSesionUsuario`, "
                    + "`fechaHoraEntrada`, "
                    + "`fechaHoraSalida`, "
                    + "`Usuario_idUsuario`) ";

            String datos = "VALUES (NULL, "
                    + UtilAuxiliares.obtenerFechaHoraHoy() + ","
                    + " NULL,"
                    + " '" + obtenerElIdDelUsuario(user) + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void registrarFinDeLaSesion(int idSesionUsuario) throws SQLException, ClassNotFoundException, Exception {

        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `sesionusuario` SET "
                    + "`fechaHoraSalida`= " + UtilAuxiliares.obtenerFechaHoraHoy()
                    + " WHERE `idSesionUsuario`='" + idSesionUsuario + "'";
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            st.execute();

        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public Usuario obtenerElUsuario(String user) throws SQLException, ClassNotFoundException, Exception {
        Conector con = new MySqlConexion();
        con.conectar();

        String url = "SELECT * FROM usuario WHERE user='" + user + "'";
        Statement consulta = con.getConexion().createStatement();
        ResultSet registro = consulta.executeQuery(url);
        usuario = new Usuario();

        if (registro.next() == true) {
            usuario.setIdUsuario(registro.getInt("idUsuario"));
            usuario.setUser(user);
            usuario.setPass(StringEncrypt.decrypt(StringEncrypt.getKey(), StringEncrypt.getiv(), registro.getString("pass")));
            usuario.setTipo(registro.getInt("TipoDeUsuario_idTIpoDeUsuario"));
        }
        return usuario;
    }

    public int obtenerElIdDelUsuario(String user) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idUsuario = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT idUsuario AS id FROM usuario WHERE user='" + user + "'");
        while (rs.next()) {
            idUsuario = rs.getInt("id");
        }
        return idUsuario;
    }

    public int obtenerElIdUsuarioDeLaSesionActiva() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idSesionUsuario = 0;
        int idUsuario = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idSesionUsuario) AS id FROM sesionusuario");
        while (rs.next()) {
            idSesionUsuario = rs.getInt("id");
        }

        Statement sql = con.getConexion().createStatement();
        ResultSet rsu = sql.executeQuery("SELECT Usuario_idUsuario AS id FROM sesionusuario WHERE idSesionUsuario='" + idSesionUsuario + "'");
        while (rsu.next()) {
            idUsuario = rsu.getInt("id");
        }

        return idUsuario;
    }
    
    public int obtenerElIdUsuarioLogueado(String usuario) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idUsuario = 0;
        
        Statement sql = con.getConexion().createStatement();
        ResultSet rsu = sql.executeQuery("SELECT idUsuario AS id FROM usuario WHERE user='" + usuario + "'");
        while (rsu.next()) {
            idUsuario = rsu.getInt("id");
        }

        return idUsuario;
    }
    

    public int obtenerElIdEmpleadoDeLaSesionActiva() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idSesionUsuario = 0;
        int idUsuario = 0;
        int idEmpleado = 0;

        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idSesionUsuario) AS id FROM sesionusuario");
        while (rs.next()) {
            idSesionUsuario = rs.getInt("id");
        }

        Statement sql1 = con.getConexion().createStatement();
        ResultSet rsu = sql1.executeQuery("SELECT Usuario_idUsuario AS id FROM sesionusuario WHERE idSesionUsuario='" + idSesionUsuario + "'");
        while (rsu.next()) {
            idUsuario = rsu.getInt("id");
        }

        Statement sql2 = con.getConexion().createStatement();
        ResultSet rse = sql2.executeQuery("SELECT `Empleado_idEmpleado` AS id FROM `usuario` WHERE idUsuario='" + idUsuario + "'");

        while (rse.next()) {
            idEmpleado = rse.getInt("id");
        }

        return idEmpleado;
    }

    public int obtenerElIdDeLaSesionActiva() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idSesionUsuario = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idSesionUsuario) AS id FROM sesionusuario");
        while (rs.next()) {
            idSesionUsuario = rs.getInt("id");
        }
        return idSesionUsuario;
    }

    public ResultSet resultSetUsuarios() throws ClassNotFoundException {
        ResultSet usuarios = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `tipodeusuario` ORDER BY `descripcion`";
            Statement consulta = con.getConexion().createStatement();
            usuarios = consulta.executeQuery(url);
            return usuarios;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuarios;
    }
}
