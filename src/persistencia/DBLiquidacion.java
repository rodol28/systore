package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import objetosDelDominio.DetalleSueldo;
import objetosDelDominio.Sueldo;
import utilidadesParaFrames.UtilAuxiliares;

public class DBLiquidacion {

    public static int obtenerElUltimoIdLiquidacion() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idLiquidacion = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idSueldo) AS id FROM sueldo");
        while (rs.next()) {
            idLiquidacion = rs.getInt("id");
        }
        return idLiquidacion;
    }

    public ResultSet obtenerTablaDeAntiguedades() throws SQLException, ClassNotFoundException {
        ResultSet antiguedades = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT `desde`, `hasta`, `porcentaje` FROM `tabladeantiguedades`";
            Statement consulta = con.getConexion().createStatement();
            antiguedades = consulta.executeQuery(url);
            return antiguedades;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return antiguedades;
    }

    public static void altaDeLiquidacion(Sueldo sueldo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String table = "INSERT INTO `sueldo` "
                + "(`idSueldo`, "
                + "`Empleado_idEmpleado`, "
                + "`fechaDePago`, "
                + "`antiguedad`, "
                + "`categoria`, "
                + "`sueldoBruto`, "
                + "`sueldoNeto`, "
                + "`tipo`) ";

        String datos = "VALUES (NULL,'" + sueldo.getIdEmpleado() + "',"
                + "'" + sueldo.getFechaDePago() + "',"
                + "'" + sueldo.getAntiguedad() + "',"
                + "'" + sueldo.getCategoria() + "',"
                + "'" + sueldo.getSueldoBruto() + "',"
                + "'" + sueldo.getSueldoNeto() + "',"
                + "'" + sueldo.getTipo() + "')";

//        System.out.println(table + datos);
        PreparedStatement st = con.getConexion().prepareStatement(table + datos);
        st.execute();

        int idSueldo = obtenerElUltimoIdLiquidacion();
//        System.out.println(idSueldo);
        altaDetalleDeSueldo(sueldo, idSueldo);
    }

    public static void altaDetalleDeSueldo(Sueldo sueldo, int idSueldo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String table = "INSERT INTO `detalledesueldo` "
                + "(`idDetalleDeSueldo`, "
                + "`Sueldo_idSueldo`, "
                + "`Concepto_idConcepto`, "
                + "`subtotal`)";

        for (int i = 0; i < sueldo.getDetalleSueldo().size(); i++) {
            String datos = "VALUES (NULL,'" + idSueldo + "',"
                    + "'" + sueldo.getDetalleSueldo().get(i).getIdConcepto() + "',"
                    + "'" + sueldo.getDetalleSueldo().get(i).getSubTotal() + "')";

//            System.out.println(table + datos);
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        }
    }
}
