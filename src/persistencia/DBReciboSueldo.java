package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBReciboSueldo {

    private Conector con;

    public DBReciboSueldo() {
    }

    public ResultSet resultSetDatosRecibo(int legajo) throws SQLException, ClassNotFoundException {
        ResultSet datosRecibo = null;
       
        try {
            Conector con = new MySqlConexion();
            con.conectar();
            String sql = "SELECT\n"
                    + " empleado.`legajo` AS empleado_legajo,\n"
                    + " empleado.`cuil` AS cuil,\n"
                    + " persona.`nombre` AS nombre,\n"
                    + " persona.`apellido` AS apellido,\n"
                    + " obrasocial.`razonSocialOb` AS obrasocial,\n"
                    + " puesto.`salario` AS salario,\n"
                    + " empleado.`idEmpleado` AS empleado_idEmpleado,\n"
                    + " empleado.`fechaDeIngreso` AS fechaDeIngreso,\n"
                    + " puesto.`descripcion` AS puesto\n"
                    + " FROM\n"
                    + " `persona` persona INNER JOIN `empleado` empleado ON persona.`idPersona` = empleado.`Persona_idPersona`\n"
                    + " INNER JOIN `obrasocial` obrasocial ON empleado.`ObraSocial_idObraSocial` = obrasocial.`idObraSocial`\n"
                    + " INNER JOIN `puesto` puesto ON empleado.`Puesto_idPuesto` = puesto.`idPuesto`"
                    + " AND `legajo`='" + legajo + "'";
            
//            System.out.println(sql);
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            datosRecibo = st.executeQuery();
            return datosRecibo;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datosRecibo;
    }

}
