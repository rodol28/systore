package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import objetosDelDominio.Persona;
import utilidadesParaFrames.UtilAuxiliares;

public class DBPersonas {

    public String obtenerElNombreDeLaPersona(int idPersona) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String nombre = "";
        String apellido = "";
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT `apellido`,`nombre` FROM `persona` WHERE idPersona=" + idPersona);
        while (rs.next()) {
            nombre = rs.getString("nombre");
            apellido = rs.getString("apellido");
        }
        return apellido + ", " + nombre;
    }

    public Persona obtenerLaPersonaCompleta(int idPersona) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        Persona persona = new Persona();

        Statement consulta = con.getConexion().createStatement();
        String query = "SELECT nombre, apellido, email, telefono, "
                + "dni, sexo, fechaNacimiento, "
                + "idEstadoCivil "
                + "FROM persona, datospersona, estadocivil "
                + "WHERE Persona_idPersona=idPersona AND "
                + "EstadoCivil_idEstadoCivil=idEstadoCivil AND "
                + "idPersona='" + idPersona + "'";

        rs = consulta.executeQuery(query);

        while (rs.next()) {
            persona.setNombre(rs.getString("nombre"));
            persona.setApellido(rs.getString("apellido"));
            persona.setEmail(rs.getString("email"));
            persona.setTelefono(rs.getString("telefono"));
            persona.setDni(rs.getInt("dni"));
            persona.setSexo(rs.getString("sexo"));
            persona.setFechaNacString(rs.getString("fechaNacimiento"));
            persona.setEstadoCivil(rs.getInt("idEstadoCivil"));
        }

        return persona;
    }

    public int obtenerElIdPaisDeLaNacionalidad(int idPersona) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        int idPais = 0;

        PreparedStatement st = con.getConexion().prepareStatement("SELECT Pais_idPais AS idPais FROM nacionalidad WHERE Persona_idPersona=" + idPersona);
        rs = st.executeQuery();

        while (rs.next()) {
            idPais = rs.getInt("idPais");
        }
        return idPais;
    }

    public int obtenerElidDomicilioDelProveedor(int idProveedor) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        int idDomicilio = 0;

        while (rs.next()) {
            idDomicilio = rs.getInt("Domicilio_idDomicilio");
        }
        return idDomicilio;
    }

}
