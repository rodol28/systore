package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetosDelDominio.Empleado;
import objetosDelDominio.Pariente;
import objetosDelDominio.Persona;

public class DBEmpleados {

    private DBAuxiliares dbAuxiliares;
    private DBPersonas dbPersonas;
    private DBUsuarios dbUsuarios;

    public ResultSet resultSetEmpleados(boolean mostrarTodos) throws SQLException, ClassNotFoundException {
        ResultSet empleados = null;

        try {
            Conector con = new MySqlConexion();
            con.conectar();

            if (mostrarTodos) {
                PreparedStatement st = con.getConexion().prepareStatement("SELECT legajo, apellido, nombre, cuil, telefono, estadoDelEmpleado "
                        + "FROM persona, empleado "
                        + "WHERE idPersona=Persona_idPersona "
                        + "ORDER BY legajo ASC");
                empleados = st.executeQuery();
            } else {
                PreparedStatement st = con.getConexion().prepareStatement("SELECT legajo, apellido, nombre, cuil, telefono, estadoDelEmpleado "
                        + "FROM persona, empleado "
                        + "WHERE idPersona=Persona_idPersona AND "
                        + "estadoDelEmpleado=1 "
                        + "ORDER BY legajo ASC");
                empleados = st.executeQuery();
            }

            return empleados;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empleados;
    }

    public ResultSet resultSetDatosDelEmpleadoX(int legajo) throws ClassNotFoundException {
        ResultSet empleados = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT legajo, apellido, nombre, cuil, telefono, estadoDelEmpleado "
                    + "FROM persona, empleado "
                    + "WHERE idPersona=Persona_idPersona AND "
                    + "estadoDelEmpleado=1 AND "
                    + "legajo='" + legajo + "' "
                    + "ORDER BY legajo ASC");
            empleados = st.executeQuery();
            return empleados;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empleados;
    }

    public int obtenerElIdPersona(int idEmpleado) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idPersona = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT Persona_idPersona AS id FROM empleado WHERE idEmpleado=" + idEmpleado);
        while (rs.next()) {
            idPersona = rs.getInt("id");
        }
        return idPersona;
    }

    public ResultSet obtenerLegajoNombreApellidoDeLosEmpleados() throws SQLException, ClassNotFoundException {
        ResultSet empleados = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT legajo, nombre, apellido FROM empleado,persona WHERE Persona_idPersona=idPersona AND `estadoDelEmpleado`='1'");
            empleados = st.executeQuery();
            return empleados;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empleados;
    }

    public int obtenerIdEmpleadoPorLegajo(int legajo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idEmpleado = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idEmpleado FROM empleado WHERE legajo='" + legajo + "'");
        while (rs.next()) {
            idEmpleado = rs.getInt("idEmpleado");
            return idEmpleado;
        }
        return idEmpleado;
    }

    public int obtenerElUltimoLegajo() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int legajo = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(legajo) AS legajo FROM empleado");
        while (rs.next()) {
            legajo = rs.getInt("legajo");
        }
        return legajo;
    }

    public void altaDeEmpleado(Empleado emp) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();
            dbAuxiliares = new DBAuxiliares();

            dbAuxiliares.altaDePersonaCompleta(emp);
            int idPersona = dbAuxiliares.obtenerElUltimoIdPersona();

//            String fechaIngreso = "('" + emp.getFechaDeIngreso().getAño() + "-"
//                    + emp.getFechaDeIngreso().getMes() + "-"
//                    + emp.getFechaDeIngreso().getDia() + "')";
            String fechaIngreso = "('" + emp.getFechaIngString() + "')";

            String table = "INSERT INTO `empleado` "
                    + "(`idEmpleado`, "
                    + "`legajo`, "
                    + "`cuil`, "
                    + "`fechaDeIngreso`, "
                    + "`estadoDelEmpleado`, "
                    + "`Persona_idPersona`, "
                    + "`Puesto_idPuesto`, "
                    + "`ObraSocial_idObraSocial`, "
                    + "`domicilio`, "
                    + "`pais`, "
                    + "`provincia`, "
                    + "`localidad`, "
                    + "`codigoPostal`, "
                    + "`nacionalidad`) ";

            String datos = "VALUES (NULL, "
                    + " '" + emp.getLegajo() + "',"
                    + " '" + emp.getNroCuil() + "',"
                    + " " + fechaIngreso + ","
                    + " '" + 1 + "',"
                    + " '" + idPersona + "',"
                    + " '" + emp.getIdPuesto() + "',"
                    + " '" + emp.getIdObraSocial() + "',"
                    + " '" + emp.getDomicilio() + "',"
                    + " '" + emp.getPais() + "',"
                    + " '" + emp.getProvincia() + "',"
                    + " '" + emp.getLocalidad() + "',"
                    + " '" + emp.getCodigoPostal() + "',"
                    + " '" + emp.getNacionalidad() + "')";

//            System.out.println(table + datos);
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            int idEmpleado = obtenerElUltimoIdEmpleado();

            if (emp.getGrupoFamiliar().size() > 0) {
                altaDeGrupoFamiliar(emp.getGrupoFamiliar(), idEmpleado);
            }

            dbAuxiliares.altaDeBanco(idEmpleado, emp.getRazonSocialBanco(), emp.getCuentaBancaria());

            registrarConceptosIniciales(idEmpleado);
            JOptionPane.showMessageDialog(null, "Empleado registrado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    private void registrarConceptosIniciales(int idEmpleado) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String table = "INSERT INTO `empleadoconcepto` "
                + "(`idEmpleadoConcepto`, "
                + "`Empleado_idEmpleado`, "
                + "`Concepto_idConcepto`) ";

        String datos1 = "VALUES (NULL, "
                + " '" + idEmpleado + "',"
                + " '" + 0 + "')";
        
        String datos2 = "VALUES (NULL, "
                + " '" + idEmpleado + "',"
                + " '" + 1 + "')";

        PreparedStatement st1 = con.getConexion().prepareStatement(table + datos1);
        st1.execute();
        
        PreparedStatement st2 = con.getConexion().prepareStatement(table + datos2);
        st2.execute();

    }

    private void altaDeGrupoFamiliar(ArrayList<Pariente> grupoFamiliar, int idEmpleado) throws SQLException, ClassNotFoundException {

        Conector con = new MySqlConexion();
        con.conectar();

        String table = "INSERT INTO `grupofamiliar` "
                + "(`idGrupoFamiliar`, "
                + "`apellido`, "
                + "`nombre`, "
                + "`fechaDeNacimiento`, "
                + "`dni`, "
                + "`Empleado_idEmpleado`, "
                + "`Parentesco_idParentesco`) ";

        for (int i = 0; i < grupoFamiliar.size(); i++) {

//            String fechaNac = "('" + grupoFamiliar.get(i).getFechaNac().getAño() + "-"
//                    + grupoFamiliar.get(i).getFechaNac().getMes() + "-"
//                    + grupoFamiliar.get(i).getFechaNac().getDia() + "')";
            String fechaNac = "('" + grupoFamiliar.get(i).getFechaNacString() + "')";

            String datos = "VALUES (NULL, "
                    + " '" + grupoFamiliar.get(i).getApellido() + "',"
                    + " '" + grupoFamiliar.get(i).getNombre() + "',"
                    + " " + fechaNac + ","
                    + " '" + grupoFamiliar.get(i).getDni() + "',"
                    + " '" + idEmpleado + "',"
                    + " '" + grupoFamiliar.get(i).getIdParentesco() + "')";

//            System.out.println(table + datos);
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        }
    }

    public void modificarElEmpleado(Empleado emp) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();
            dbAuxiliares = new DBAuxiliares();
            int idEmpleado = obtenerElIdEmpleado(emp.getLegajo());

            int idPersona = obtenerElIdPersonaDelEmpleado(emp.getLegajo());
            dbAuxiliares.actualizarPersonaCompleta(emp, idPersona);

//            String fechaIngreso = "('" + emp.getFechaDeIngreso().getAño() + "-"
//                    + emp.getFechaDeIngreso().getMes() + "-"
//                    + emp.getFechaDeIngreso().getDia() + "')";
            String fechaIngreso = "('" + emp.getFechaIngString() + "')";

            String sql = "UPDATE `empleado` SET "
                    + "`cuil`='" + emp.getNroCuil() + "',"
                    + "`Puesto_idPuesto`='" + emp.getIdPuesto() + "',"
                    + "`ObraSocial_idObraSocial`='" + emp.getIdObraSocial() + "',"
                    + "`domicilio`='" + emp.getDomicilio() + "',"
                    + "`pais`='" + emp.getPais() + "',"
                    + "`provincia`='" + emp.getProvincia() + "',"
                    + "`localidad`='" + emp.getLocalidad() + "',"
                    + "`codigoPostal`='" + emp.getCodigoPostal() + "',"
                    + "`nacionalidad`='" + emp.getNacionalidad() + "'"
                    + "	WHERE `legajo`=" + emp.getLegajo();

            System.out.println(sql);
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            st.execute();

            if (emp.getGrupoFamiliar().size() > 0) {
                actualizarElGrupoFamiliarDelEmpleado(emp.getGrupoFamiliar(), idEmpleado);
            }

            dbAuxiliares.actulizarBanco(emp, idEmpleado);

            JOptionPane.showMessageDialog(null, "Empleado actualizado.");
        } catch (SQLException ex) {
            System.out.println("Error al actualizar el registro empleado.");
        }
    }

//    public void mostrar(Empleado empleado) {
//        for (int i = 0; i < empleado.getGrupoFamiliar().size(); i++) {
//            System.out.println("==========================");
//            System.out.println(empleado.getGrupoFamiliar().get(i).getDni());
//            System.out.println(empleado.getGrupoFamiliar().get(i).getApellido());
//            System.out.println(empleado.getGrupoFamiliar().get(i).getNombre());
//            String dia = "" + empleado.getGrupoFamiliar().get(i).getFechaNac().getDia();
//            String mes = "" + empleado.getGrupoFamiliar().get(i).getFechaNac().getMes();
//            String año = "" + empleado.getGrupoFamiliar().get(i).getFechaNac().getAño();
//            String fechaNac = dia + "/" + mes + "/" + año;
//            System.out.println(fechaNac);
//            System.out.println(empleado.getGrupoFamiliar().get(i).getParentesco());
//        }
//    }
    private void actualizarElGrupoFamiliarDelEmpleado(ArrayList<Pariente> grupoFamiliar, int idEmpleado) throws SQLException, ClassNotFoundException {

        Conector con = new MySqlConexion();
        con.conectar();

        String sql = "DELETE FROM `grupofamiliar` WHERE `Empleado_idEmpleado`='" + idEmpleado + "'";
        PreparedStatement st = con.getConexion().prepareStatement(sql);
        st.execute();

        for (int i = 0; i < grupoFamiliar.size(); i++) {
            altaDePariente(grupoFamiliar.get(i), idEmpleado);
        }
    }

    private void actualizarObrasSocialesDelEmpleado(ArrayList<String> obrasSociales, int idEmpleado) throws SQLException, ClassNotFoundException {

        Conector con = new MySqlConexion();
        con.conectar();

        String sql = "DELETE FROM `obrasocialempleado` WHERE `Empleado_idEmpleado`='" + idEmpleado + "'";
        PreparedStatement st = con.getConexion().prepareStatement(sql);
        st.execute();

        for (int i = 0; i < obrasSociales.size(); i++) {
            altaDeObraSocialDelEmpleado(obrasSociales.get(i), idEmpleado);
        }
    }

    public ResultSet resultSetPariente(int dniPariente) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet pariente = null;

        PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM `grupofamiliar` WHERE dni=" + dniPariente);
        pariente = st.executeQuery();
        return pariente;
    }

    public void altaDePariente(Pariente pariente, int idEmpleado) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            dbAuxiliares = new DBAuxiliares();

            String table = "INSERT INTO `grupofamiliar` "
                    + "(`idGrupoFamiliar`, "
                    + "`apellido`, "
                    + "`nombre`, "
                    + "`fechaDeNacimiento`, "
                    + "`dni`, "
                    + "`Empleado_idEmpleado`, "
                    + "`Parentesco_idParentesco`) ";

//            String fechaNac = "('" + pariente.getFechaNac().getAño() + "-"
//                    + pariente.getFechaNac().getMes() + "-"
//                    + pariente.getFechaNac().getDia() + "')";
            String fechaNac = "('" + pariente.getFechaNacString() + "')";

            String datos = "VALUES (NULL, "
                    + " '" + pariente.getApellido() + "',"
                    + " '" + pariente.getNombre() + "',"
                    + " " + fechaNac + ","
                    + " '" + pariente.getDni() + "',"
                    + " '" + idEmpleado + "',"
                    + " '" + dbAuxiliares.buscarParentescoPorDescripcion(pariente.getParentesco()) + "')";

            System.out.println(table + datos);
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al actualizar el registro pariente.");
        }
    }

    public void altaDeObraSocialDelEmpleado(String obraSocial, int idEmpleado) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            dbAuxiliares = new DBAuxiliares();

            String table = "INSERT INTO `obrasocialempleado` "
                    + "(`idObraSocialEmpleado`, "
                    + "`ObraSocial_idObraSocial`, "
                    + "`Empleado_idEmpleado`) ";

            String datos = "VALUES (NULL, "
                    + " '" + dbAuxiliares.buscarObraSocialPorRazonSocial(obraSocial) + "',"
                    + " '" + idEmpleado + "')";

            System.out.println(table + datos);
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al actualizar el registro pariente.");
        }
    }

    public int obtenerElUltimoIdEmpleado() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idEmpleado = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idEmpleado) AS id FROM empleado");
        while (rs.next()) {
            idEmpleado = rs.getInt("id");
        }
        return idEmpleado;
    }

    public void bajaDelEmpleado(int legajo) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            dbUsuarios = new DBUsuarios();

            String sql = "UPDATE `empleado` SET "
                    + "`estadoDelEmpleado`='" + 0 + "'"
                    + "	WHERE `legajo`='" + legajo + "'";

            con.getConexion().createStatement().execute(sql);

            dbUsuarios.bajaDelusuarioDelEmpleadoX(obtenerElIdEmpleado(legajo));

            JOptionPane.showMessageDialog(null, "Empleado eliminado.");
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public Empleado obtenerElEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();
        dbPersonas = new DBPersonas();

        ResultSet rs = null;
        Persona persona = null;

        Empleado empleado = new Empleado(legajo);
        empleado.setLegajo(legajo);

        Pariente pariente = null;
        int cantidadDeParientes = 0;

        Conector con = new MySqlConexion();
        con.conectar();

        int idEmpleado = obtenerElIdEmpleado(legajo);
        int idPersona = obtenerElIdPersonaDelEmpleado(legajo);

        ResultSet parientes = obtenerLosParientesDelEmpleado(idEmpleado);

        while (parientes.next()) {
            cantidadDeParientes++;
        }

        /*Reinicia el resultSet al principio para volver a recorrerlo*/
        parientes.beforeFirst();

        if (cantidadDeParientes != 0) {
            while (parientes.next()) {

                pariente = new Pariente(parientes.getInt("dni"),
                        parientes.getString("nombre"),
                        parientes.getString("apellido"),
                        "" + parientes.getString("fechaDeNacimiento"),
                        parientes.getInt("Parentesco_idParentesco"));

                pariente.setParentesco(dbAuxiliares.buscarParentescoPorID(parientes.getInt("Parentesco_idParentesco")));

                empleado.agregarPariente(pariente);
            }
        }

        persona = dbPersonas.obtenerLaPersonaCompleta(idPersona);

        empleado.setNombre(persona.getNombre());
        empleado.setApellido(persona.getApellido());
        empleado.setEmail(persona.getEmail());
        empleado.setTelefono(persona.getTelefono());
        empleado.setDni(persona.getDni());
        empleado.setSexo(persona.getSexo());
        empleado.setFechaNacString(persona.getFechaNacString());
        empleado.setEstadoCivil(persona.getIdEstadoCivil());

        PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM empleado WHERE legajo=" + legajo);
        rs = st.executeQuery();

        while (rs.next()) {
            empleado.setNroCuil(rs.getString("cuil"));
            empleado.setFechaIngString(rs.getString("fechaDeIngreso"));
            empleado.setIdPuesto(rs.getInt("Puesto_idPuesto"));
            empleado.setObraSocial(dbAuxiliares.obtenerLaObraSocialDelEmpleado(rs.getInt("ObraSocial_idObraSocial")));
            empleado.setDomicilio(rs.getString("domicilio"));
            empleado.setPais(rs.getString("pais"));
            empleado.setProvincia(rs.getString("provincia"));
            empleado.setLocalidad(rs.getString("localidad"));
            empleado.setCodigoPostal(rs.getInt("codigoPostal"));
            empleado.setNacionalidad(rs.getString("Nacionalidad"));
        }
        return empleado;
    }

    public ResultSet obtenerLosParientesDelEmpleado(int idEmpleado) throws SQLException, ClassNotFoundException {
        ResultSet parientes = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `grupofamiliar` WHERE Empleado_idEmpleado=" + idEmpleado;
            Statement consulta = con.getConexion().createStatement();
            parientes = consulta.executeQuery(url);
            return parientes;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return parientes;
    }

    public int obtenerElIdEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idEmpleado = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT idEmpleado AS id FROM empleado WHERE legajo=" + legajo);
        while (rs.next()) {
            idEmpleado = rs.getInt("id");
        }
        return idEmpleado;
    }

    public int obtenerElIdPersonaDelEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idPersona = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT Persona_idPersona FROM empleado WHERE legajo=" + legajo);
        while (rs.next()) {
            idPersona = rs.getInt("Persona_idPersona");
        }
        return idPersona;
    }

    public ResultSet obtenerPariente(int dni) throws SQLException, ClassNotFoundException {
        ResultSet pariente = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `grupofamiliar` WHERE dni=" + dni;
            Statement consulta = con.getConexion().createStatement();
            pariente = consulta.executeQuery(url);
            return pariente;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pariente;
    }

    public int obtenerIdPersonaDelEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idPersona = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT Persona_idPersona AS id FROM empleado WHERE legajo=" + legajo);
        while (rs.next()) {
            idPersona = rs.getInt("id");
        }
        return idPersona;
    }

    public ResultSet obtenerLosDatosDeLosEmplados() throws SQLException, ClassNotFoundException {
        ResultSet empleados = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT\n"
                    + " empleado.`idEmpleado` AS empleado_idEmpleado,\n"
                    + " empleado.`legajo` AS empleado_legajo,\n"
                    + " persona.`nombre` AS persona_nombre,\n"
                    + " persona.`apellido` AS persona_apellido,\n"
                    + " empleado.`cuil` AS empleado_cuil,\n"
                    + " empleado.`fechaDeIngreso` AS empleado_fechaDeIngreso,\n"
                    + " empleado.`estadoDelEmpleado` AS empleado_estadoDelEmpleado,\n"
                    + " empleado.`Persona_idPersona` AS empleado_Persona_idPersona,\n"
                    + " empleado.`Puesto_idPuesto` AS empleado_Puesto_idPuesto,\n"
                    + " persona.`idPersona` AS persona_idPersona\n"
                    + " FROM\n"
                    + " `persona` persona INNER JOIN `empleado` empleado ON persona.`idPersona` = empleado.`Persona_idPersona`";
            Statement consulta = con.getConexion().createStatement();
//            System.out.println(url);
            empleados = consulta.executeQuery(url);
            return empleados;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empleados;
    }

    public ResultSet obtenerLosDatosDeLosEmpladosDeUnPuestoParticular(int idPuesto) throws SQLException, ClassNotFoundException {
        ResultSet empleados = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT\n"
                    + " empleado.`idEmpleado` AS empleado_idEmpleado,\n"
                    + " empleado.`legajo` AS empleado_legajo,\n"
                    + " persona.`nombre` AS persona_nombre,\n"
                    + " persona.`apellido` AS persona_apellido,\n"
                    + " empleado.`cuil` AS empleado_cuil,\n"
                    + " empleado.`fechaDeIngreso` AS empleado_fechaDeIngreso,\n"
                    + " empleado.`estadoDelEmpleado` AS empleado_estadoDelEmpleado,\n"
                    + " empleado.`Persona_idPersona` AS empleado_Persona_idPersona,\n"
                    + " empleado.`Puesto_idPuesto` AS empleado_Puesto_idPuesto,\n"
                    + " persona.`idPersona` AS persona_idPersona\n"
                    + " FROM\n"
                    + " `persona` persona INNER JOIN `empleado` empleado ON persona.`idPersona` = empleado.`Persona_idPersona`"
                    + " WHERE `Puesto_idPuesto`='" + idPuesto + "'";
            Statement consulta = con.getConexion().createStatement();
            empleados = consulta.executeQuery(url);
            return empleados;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empleados;
    }

    public int obtenerElIdPuestoDelEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idPuesto = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT `Puesto_idPuesto` FROM `empleado` WHERE `legajo`='" + legajo + "'");
        while (rs.next()) {
            idPuesto = rs.getInt("Puesto_idPuesto");
            return idPuesto;
        }
        return 0;
    }

    public float obtenerBasicoDelEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        float basico = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT\n"
                + " puesto.`salario` AS puesto_salario\n"
                + "FROM\n"
                + " `puesto` puesto INNER JOIN `empleado` empleado ON puesto.`idPuesto` = empleado.`Puesto_idPuesto` "
                + "WHERE empleado.`legajo`='" + legajo + "'");
        while (rs.next()) {
            basico = rs.getInt("puesto_salario");
            return basico;
        }
        return 0;
    }

    public float obtenerElMayorSueldoDelEmpleado(int legajo, int año, String mes) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        float sueldoMax = 0;
        ResultSet rs = null;

        int idEmpleado = obtenerElIdEmpleado(legajo);

        Statement consulta = con.getConexion().createStatement();

        if (mes.equals("Junio")) {
            String sql = "SELECT `fechaDePago`, MAX(`sueldoBruto`) AS sueldoMax"
                    + " FROM `sueldo` "
                    + " WHERE `fechaDePago` BETWEEN CAST('" + (año - 1) + "-12-31' AS DATE) AND CAST('" + año + "-07-01' AS DATE) "
                    + " AND `tipo` = 'Mensual'"
                    + " AND `Empleado_idEmpleado`='" + idEmpleado + "'";
//            System.out.println(sql);
            rs = consulta.executeQuery(sql);

        } else if (mes.equals("Diciembre")) {

            String sql = "SELECT `fechaDePago`, MAX(`sueldoBruto`) AS sueldoMax"
                    + " FROM `sueldo` "
                    + " WHERE `fechaDePago` BETWEEN CAST('" + año + "-07-01' AS DATE) AND CAST('" + año + "-12-31' AS DATE) "
                    + " AND `tipo` = 'Mensual'"
                    + " AND `Empleado_idEmpleado`='" + idEmpleado + "'";
//            System.out.println(sql);
            rs = consulta.executeQuery(sql);
        }

        while (rs.next()) {
            sueldoMax = rs.getFloat("sueldoMax");
            return sueldoMax;
        }
        return 0;
    }

    public int obtenerAntiguedadDelEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        Calendar hoy = Calendar.getInstance();

        int dia = hoy.get(Calendar.DAY_OF_MONTH);
        int mes = hoy.get(Calendar.MONTH) + 1;
        int año = hoy.get(Calendar.YEAR);

        String fechaHoy = "'" + año + "-" + mes + "-" + dia + "'";

        int antiguedad = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT TIMESTAMPDIFF(YEAR, (SELECT `fechaDeIngreso` FROM empleado"
                + " WHERE `legajo`=" + legajo + "),"
                + fechaHoy + ")"
                + " AS antiguedad");

        while (rs.next()) {
            antiguedad = rs.getInt("antiguedad");
            return antiguedad;
        }
        return 0;
    }
}
