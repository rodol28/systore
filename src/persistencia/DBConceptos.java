package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetosDelDominio.Concepto;
import objetosDelDominio.TipoDeConcepto;

public class DBConceptos {

    private Conector con;

    public DBConceptos() {
    }

    public int obtenerElUltimoIdConcepto() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idConcepto = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idConcepto) AS id FROM concepto");
        while (rs.next()) {
            idConcepto = rs.getInt("id");
        }
        return idConcepto;
    }

    public ResultSet resultSetConceptosYDescripciones() throws SQLException, ClassNotFoundException {
        ResultSet conceptos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT\n"
                    + " concepto.`abreviatura` AS concepto_abreviatura,\n"
                    + " concepto.`descripcion` AS concepto_descripcion,\n"
                    + " concepto.`estadoConcepto` AS concepto_estadoConcepto,\n"
                    + " tipoconceptodescripcion.`tipoConceptoDescripcion` AS tipoconceptodescripcion_tipoConceptoDescripcion,\n"
                    + " concepto.`idConcepto` AS concepto_idConcepto,\n"
                    + " tipodeliquidaciondescripcion.`descripcionDeTipoDeLiquidacion` AS tipodeliquidaciondescripcion_descripcionDeTipoDeLiquidacion\n"
                    + " FROM\n"
                    + " `concepto` concepto INNER JOIN `conceptotipodeliquidacion` conceptotipodeliquidacion ON concepto.`idConcepto` = conceptotipodeliquidacion.`Concepto_idConcepto`\n"
                    + " INNER JOIN `tipodeconcepto` tipodeconcepto ON concepto.`idConcepto` = tipodeconcepto.`Concepto_idConcepto`\n"
                    + " INNER JOIN `tipoconceptodescripcion` tipoconceptodescripcion ON tipodeconcepto.`TipoConceptoDescripcion_idTipoConceptoDescripcion` = tipoconceptodescripcion.`idTipoConceptoDescripcion`\n"
                    + " INNER JOIN `tipodeliquidaciondescripcion` tipodeliquidaciondescripcion ON conceptotipodeliquidacion.`TipoDeLiquidacionDescripcion_idTipoDeLiquidacionDescripcion` = tipodeliquidaciondescripcion.`idTipoDeLiquidacionDescripcion` "
                    + " AND estadoConcepto = 1";

            Statement consulta = con.getConexion().createStatement();
            conceptos = consulta.executeQuery(url);
//            System.out.println(url);

            return conceptos;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conceptos;
    }

    public ResultSet resultSetConceptosActivos() throws SQLException, ClassNotFoundException {
        ResultSet conceptos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `concepto` WHERE `estadoConcepto` = 1";

            Statement consulta = con.getConexion().createStatement();
            conceptos = consulta.executeQuery(url);
//            System.out.println(url);

            return conceptos;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conceptos;
    }

    public ResultSet resultSetDescripcionDeTipoDeLiquidacionParaIdConcepto(int idConcepto) throws SQLException, ClassNotFoundException {
        ResultSet descripcionesDeTipoDeLiquidacion = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT\n"
                    + " tipodeliquidaciondescripcion.`descripcionDeTipoDeLiquidacion` AS tipodeliquidaciondescripcion_descripcionDeTipoDeLiquidacion\n"
                    + " FROM\n"
                    + " `concepto` concepto INNER JOIN `conceptotipodeliquidacion` conceptotipodeliquidacion ON concepto.`idConcepto` = conceptotipodeliquidacion.`Concepto_idConcepto`\n"
                    + " INNER JOIN `tipodeliquidaciondescripcion` tipodeliquidaciondescripcion ON conceptotipodeliquidacion.`TipoDeLiquidacionDescripcion_idTipoDeLiquidacionDescripcion` = tipodeliquidaciondescripcion.`idTipoDeLiquidacionDescripcion`"
                    + " WHERE Concepto_idConcepto = " + idConcepto;

            Statement consulta = con.getConexion().createStatement();
            descripcionesDeTipoDeLiquidacion = consulta.executeQuery(url);
//            System.out.println(url);

            return descripcionesDeTipoDeLiquidacion;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return descripcionesDeTipoDeLiquidacion;
    }

    public ResultSet resultSetDescripcionDeTipoDeConceptoParaIdConcepto(int idConcepto) throws SQLException, ClassNotFoundException {
        ResultSet descripcionesDeTipoDeConcepto = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT\n"
                    + " tipoconceptodescripcion.`tipoConceptoDescripcion` AS tipoconceptodescripcion_tipoConceptoDescripcion\n"
                    + " FROM\n"
                    + " `concepto` concepto INNER JOIN `tipodeconcepto` tipodeconcepto ON concepto.`idConcepto` = tipodeconcepto.`Concepto_idConcepto`\n"
                    + " INNER JOIN `tipoconceptodescripcion` tipoconceptodescripcion ON tipodeconcepto.`TipoConceptoDescripcion_idTipoConceptoDescripcion` = tipoconceptodescripcion.`idTipoConceptoDescripcion`"
                    + " WHERE `Concepto_idConcepto` = " + idConcepto;

            Statement consulta = con.getConexion().createStatement();
            descripcionesDeTipoDeConcepto = consulta.executeQuery(url);
//            System.out.println(url);

            return descripcionesDeTipoDeConcepto;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return descripcionesDeTipoDeConcepto;
    }

    public int obtenerElIdTipoDeConceptoDescripcion(String descripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idTipoDeConceptoDescripcion = 0;
        Statement consulta = con.getConexion().createStatement();
//        System.out.println("SELECT `idTipoConceptoDescripcion` FROM `tipoconceptodescripcion` WHERE `tipoConceptoDescripcion`='" + descripcion + "'");
        ResultSet rs = consulta.executeQuery("SELECT `idTipoConceptoDescripcion` FROM `tipoconceptodescripcion` WHERE `tipoConceptoDescripcion`='" + descripcion + "'");
        while (rs.next()) {
            idTipoDeConceptoDescripcion = rs.getInt("idTipoConceptoDescripcion");
        }
        return idTipoDeConceptoDescripcion;
    }

    public String obtenerLaDescripcionTipoDeConceptoDescripcion(int idTipoDeConceptoDescripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String tipoDeConceptoDescripcion = "";
        Statement consulta = con.getConexion().createStatement();
//        System.out.println("SELECT `idTipoConceptoDescripcion` FROM `tipoconceptodescripcion` WHERE `tipoConceptoDescripcion`='" + descripcion + "'");
        ResultSet rs = consulta.executeQuery("SELECT `tipoConceptoDescripcion` FROM `tipoconceptodescripcion` WHERE `idTipoConceptoDescripcion`='" + idTipoDeConceptoDescripcion + "'");
        while (rs.next()) {
            tipoDeConceptoDescripcion = rs.getString("tipoConceptoDescripcion");
        }
        return tipoDeConceptoDescripcion;
    }

    public int obtenerElIdTipoDeLiquidacionDescripcion(String descripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idTipoDeLiquidacionDescripcion = 0;
        Statement consulta = con.getConexion().createStatement();
//        System.out.println("SELECT `idTipoDeLiquidacionDescripcion` FROM `tipodeliquidaciondescripcion` WHERE `descripcionDeTipoDeLiquidacion`='" + descripcion + "'");
        ResultSet rs = consulta.executeQuery("SELECT `idTipoDeLiquidacionDescripcion` FROM `tipodeliquidaciondescripcion` WHERE `descripcionDeTipoDeLiquidacion`='" + descripcion + "'");
        while (rs.next()) {
            idTipoDeLiquidacionDescripcion = rs.getInt("idTipoDeLiquidacionDescripcion");
        }
        return idTipoDeLiquidacionDescripcion;
    }

    public int obtenerElUltimoIdTipoConcepto() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idTipoConcepto = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idTipoDeConcepto) AS id FROM `tipodeconcepto`");
        while (rs.next()) {
            idTipoConcepto = rs.getInt("id");
        }
        return idTipoConcepto;
    }

    public void altaDeConcepto(Concepto concep) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            int esRemunerativo = 1;
            if (concep.isEsRemunerativo()) {
                esRemunerativo = 1;
            } else {
                esRemunerativo = 0;
            }

            String table1 = "INSERT INTO `concepto` "
                    + "(`idConcepto`, "
                    + "`abreviatura`, "
                    + "`descripcion`, "
                    + "`estadoConcepto`, "
                    + "`esRemunerativo`, "
                    + "`tipoDeConcepto`, "
                    + "`cantidadPorcentual`, "
                    + "`cantidadFija`) ";

            String datos1 = "VALUES (NULL, "
                    + " '" + concep.getAbreviatura() + "',"
                    + " '" + concep.getDescripcion() + "',"
                    + " '" + 1 + "',"
                    + " '" + esRemunerativo + "',"
                    + " '" + concep.getTipoDeConcepto() + "',"
                    + " '" + concep.getCantidadPorcentual() + "',"
                    + " '" + concep.getCantidadFija() + "')";

            PreparedStatement st1 = con.getConexion().prepareStatement(table1 + datos1);
            st1.execute();

            JOptionPane.showMessageDialog(null, "Concepto agregado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public Concepto obtenerElConcepto(int idConcepto) throws SQLException, ClassNotFoundException {
        Concepto concepto = new Concepto();

        ResultSet concep = null;

        Conector con = new MySqlConexion();
        con.conectar();

        String url1 = "SELECT * FROM `concepto` WHERE `idConcepto` = '" + idConcepto + "'";

        Statement consulta1 = con.getConexion().createStatement();
        concep = consulta1.executeQuery(url1);

        while (concep.next()) {
            concepto.setIdConcepto(idConcepto);
            concepto.setAbreviatura(concep.getString("abreviatura"));
            concepto.setDescripcion(concep.getString("descripcion"));
            if (concep.getInt("esRemunerativo") == 1) {
                concepto.setEsRemunerativo(true);
            } else {
                concepto.setEsRemunerativo(false);
            }
            concepto.setTipoDeConcepto(concep.getString("tipoDeConcepto"));
            concepto.setCantidadFija(concep.getDouble("cantidadFija"));
            concepto.setCantidadPorcentual(concep.getDouble("cantidadPorcentual"));
        }

        return concepto;
    }

    public float obtenerLaCantidadFijaDelConcepto(int idTipoDeConcepto) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        float cantidadFija = 0;
        Statement consulta = con.getConexion().createStatement();
//        System.out.println("SELECT `idTipoConceptoDescripcion` FROM `tipoconceptodescripcion` WHERE `tipoConceptoDescripcion`='" + descripcion + "'");
        ResultSet rs = consulta.executeQuery("SELECT `cantidadFija` FROM `fijoconcepto` WHERE `TipoDeConcepto_idTipoDeConcepto`='" + idTipoDeConcepto + "'");
        while (rs.next()) {
            cantidadFija = rs.getFloat("cantidadFija");
        }
        return cantidadFija;
    }

    public float obtenerLaCantidadPorcentualDelConcepto(int idTipoDeConcepto) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        float cantidadPorcentual = 0;
        Statement consulta = con.getConexion().createStatement();
//        System.out.println("SELECT `idTipoConceptoDescripcion` FROM `tipoconceptodescripcion` WHERE `tipoConceptoDescripcion`='" + descripcion + "'");
        ResultSet rs = consulta.executeQuery("SELECT `cantidadPorcentual` FROM `porcentajeconcepto` WHERE `TipoDeConcepto_idTipoDeConcepto`='" + idTipoDeConcepto + "'");
        while (rs.next()) {
            cantidadPorcentual = rs.getFloat("cantidadPorcentual");
        }
        return cantidadPorcentual;
    }

    public String obtenerLaDescripcionDelTipoDeLiquidacion(int idTipoDeLiquidacionDescripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String descripcionDeTipoDeLiquidacion = "";
        Statement consulta = con.getConexion().createStatement();
//        System.out.println("SELECT `idTipoConceptoDescripcion` FROM `tipoconceptodescripcion` WHERE `tipoConceptoDescripcion`='" + descripcion + "'");
        ResultSet rs = consulta.executeQuery("SELECT `descripcionDeTipoDeLiquidacion` FROM `tipodeliquidaciondescripcion` WHERE `idTipoDeLiquidacionDescripcion`='" + idTipoDeLiquidacionDescripcion + "'");
        while (rs.next()) {
            descripcionDeTipoDeLiquidacion = rs.getString("descripcionDeTipoDeLiquidacion");
        }
        return descripcionDeTipoDeLiquidacion;
    }

    public void modificarElConcepto(Concepto concep) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            int esRemunerativo = 1;
            if (concep.isEsRemunerativo()) {
                esRemunerativo = 1;
            } else {
                esRemunerativo = 0;
            }

            String sql1 = "UPDATE `concepto` SET "
                    + "`abreviatura`='" + concep.getAbreviatura() + "',"
                    + "`descripcion`='" + concep.getDescripcion() + "',"
                    + "`esRemunerativo`='" + esRemunerativo + "',"
                    + "`tipoDeConcepto`='" + concep.getTipoDeConcepto() + "',"
                    + "`cantidadPorcentual`='" + concep.getCantidadPorcentual() + "',"
                    + "`cantidadFija`='" + concep.getCantidadFija() + "'"
                    + "	WHERE `idConcepto`=" + concep.getIdConcepto();

            System.out.println(sql1);
            PreparedStatement st1 = con.getConexion().prepareStatement(sql1);
            st1.execute();
            JOptionPane.showMessageDialog(null, "Concepto actualizado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void bajaDeTipoDeConcepto(int idConcepto) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `tipodeconcepto` WHERE Concepto_idConcepto='" + idConcepto + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public void bajaDeFijoConcepto(int idTipoDeConcepto) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `fijoconcepto` WHERE TipoDeConcepto_idTipoDeConcepto='" + idTipoDeConcepto + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public void bajaDePorcentajeConcepto(int idTipoDeConcepto) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `porcentajeconcepto` WHERE TipoDeConcepto_idTipoDeConcepto='" + idTipoDeConcepto + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public void bajaDeConceptoTipoDeLiquidacion(int idConcepto) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `conceptotipodeliquidacion` WHERE Concepto_idConcepto='" + idConcepto + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public ResultSet obtenerElIdTipoConcepto(int idConcepto) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idTipoConcepto = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT `idTipoDeConcepto` AS id FROM `tipodeconcepto` WHERE `Concepto_idConcepto`='" + idConcepto + "'");

        return rs;
    }

    public void bajaDelConcepto(int idConcepto) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `concepto` SET "
                    + "`estadoConcepto`='" + 0 + "'"
                    + "	WHERE `idConcepto`=" + idConcepto;

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public ResultSet busquedaPredictivaDeAbreviatura(String palabra) throws SQLException, ClassNotFoundException {
        ResultSet conceptos = null;

        String sql = "SELECT * FROM `concepto` WHERE `abreviatura` LIKE '" + palabra + "%'";

        try {
            Conector con = new MySqlConexion();
            con.conectar();

//            System.out.println(sql);
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            conceptos = st.executeQuery();

            return conceptos;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conceptos;
    }

    public ResultSet buscarEmpleadoYPuesto(int legajo) throws SQLException, ClassNotFoundException {
        ResultSet datos = null;

        String sql = "SELECT\n"
                + " persona.`nombre` AS persona_nombre,\n"
                + " persona.`apellido` AS persona_apellido,\n"
                + " puesto.`idPuesto` AS puesto_idPuesto,\n"
                + " puesto.`descripcion` AS puesto_descripcion\n"
                + " FROM\n"
                + " `persona` persona INNER JOIN `empleado` empleado ON persona.`idPersona` = empleado.`Persona_idPersona`\n"
                + " INNER JOIN `puesto` puesto ON empleado.`Puesto_idPuesto` = puesto.`idPuesto`"
                + " WHERE empleado.`legajo`='" + legajo + "'";

        try {
            Conector con = new MySqlConexion();
            con.conectar();

//            System.out.println(sql);
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            datos = st.executeQuery();

            return datos;
        } catch (SQLException ex) {
        }

        return datos;
    }

    public void registrarPuestoEmpleadoConcepto(ArrayList<Integer> concepSelec, int idPuesto, int idEmpleado) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `empleadoconcepto` WHERE `Empleado_idEmpleado`='" + idEmpleado + "'";
            PreparedStatement st1 = con.getConexion().prepareStatement(sql);
//            System.out.println(sql);
            st1.execute();

            String table = "INSERT INTO `empleadoconcepto` "
                    + "(`idEmpleadoConcepto`, "
                    + "`Empleado_idEmpleado`, "
                    + "`Concepto_idConcepto`) ";

            for (int i = 0; i < concepSelec.size(); i++) {
                String datos = "VALUES (NULL, "
                        + " '" + idEmpleado + "',"
                        + " '" + concepSelec.get(i) + "')";

                PreparedStatement st2 = con.getConexion().prepareStatement(table + datos);
//                System.out.println(table + datos);
                st2.execute();
            }

            JOptionPane.showMessageDialog(null, "Conceptos registrardos para el puesto y el empleado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public ResultSet resultSetEmpleadoConcepto(int idEmpleado) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;

        String sql = "SELECT * FROM `empleadoconcepto` WHERE `Empleado_idEmpleado`='" + idEmpleado + "'";

        try {
            Conector con = new MySqlConexion();
            con.conectar();
//            System.out.println(sql);
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            rs = st.executeQuery();
            return rs;
        } catch (SQLException ex) {
        }
        return rs;
    }

    public ResultSet resultSetConceptosDelEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;

        String sql = "SELECT\n"
                + " concepto.`idConcepto` AS concepto_idConcepto,\n"
                + " concepto.`abreviatura` AS concepto_abreviatura,\n"
                + " concepto.`descripcion` AS concepto_descripcion,\n"
                + " concepto.`estadoConcepto` AS concepto_estadoConcepto,\n"
                + " concepto.`esRemunerativo` AS concepto_esRemunerativo,\n"
                + " concepto.`tipoDeConcepto` AS concepto_tipoDeConcepto,\n"
                + " concepto.`cantidadPorcentual` AS concepto_cantidadPorcentual,\n"
                + " concepto.`cantidadFija` AS concepto_cantidadFija\n"
                + " FROM\n"
                + " `empleado` empleado INNER JOIN `empleadoconcepto`"
                + " empleadoconcepto ON empleado.`idEmpleado` = empleadoconcepto.`Empleado_idEmpleado`\n"
                + " INNER JOIN `concepto` concepto ON empleadoconcepto.`Concepto_idConcepto` = concepto.`idConcepto`"
                + " AND empleado.legajo='" + legajo + "'";
        try {
            Conector con = new MySqlConexion();
            con.conectar();
//            System.out.println(sql);
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            rs = st.executeQuery();
            return rs;
        } catch (SQLException ex) {
        }
        return rs;
    }

    public ResultSet resultSetTiposDeConceptos(int idConceptos) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;

        String sql = "SELECT * FROM `tipodeconcepto` WHERE `Concepto_idConcepto`='" + idConceptos + "'";
        try {
            Conector con = new MySqlConexion();
            con.conectar();
//            System.out.println(sql);
            PreparedStatement st = con.getConexion().prepareStatement(sql);
            rs = st.executeQuery();
            return rs;
        } catch (SQLException ex) {
        }
        return rs;
    }

    public String buscarElTipoDeConcepto(int idTipoConceptoDescripcion) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        String tipoDeConcepto = "";
        String sql = "SELECT `tipoConceptoDescripcion` FROM `tipoconceptodescripcion` WHERE `idTipoConceptoDescripcion`='" + idTipoConceptoDescripcion + "'";

        Conector con = new MySqlConexion();
        con.conectar();
//            System.out.println(sql);
        PreparedStatement st = con.getConexion().prepareStatement(sql);
        rs = st.executeQuery();

        while (rs.next()) {
            tipoDeConcepto = rs.getString("tipoConceptoDescripcion");
            return tipoDeConcepto;
        }

        return tipoDeConcepto;
    }

    public float buscarCantidadFijaDelConcepto(int idTipoConcepto) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        float cantidadFija = 0;
        String sql = "SELECT `cantidadFija` FROM `fijoconcepto` WHERE `TipoDeConcepto_idTipoDeConcepto`='" + idTipoConcepto + "'";

        Conector con = new MySqlConexion();
        con.conectar();
//            System.out.println(sql);
        PreparedStatement st = con.getConexion().prepareStatement(sql);
        rs = st.executeQuery();

        while (rs.next()) {
            cantidadFija = rs.getFloat("cantidadFija");
            return cantidadFija;
        }
        return cantidadFija;
    }

    public float buscarCantidadPorcentualDelConcepto(int idTipoConcepto) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        float cantidadPorcentual = 0;
        String sql = "SELECT `cantidadPorcentual` FROM `porcentajeconcepto` WHERE `TipoDeConcepto_idTipoDeConcepto`='" + idTipoConcepto + "'";

        Conector con = new MySqlConexion();
        con.conectar();
//            System.out.println(sql);
        PreparedStatement st = con.getConexion().prepareStatement(sql);
        rs = st.executeQuery();

        while (rs.next()) {
            cantidadPorcentual = rs.getFloat("cantidadPorcentual");
            return cantidadPorcentual;
        }
        return cantidadPorcentual;
    }
}
