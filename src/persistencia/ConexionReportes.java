package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConexionReportes {

    private final String baseDeDatos = "systoredb";
    private final String usuario = "root";
    private final String password = "";
    private final String url = "jdbc:mysql://localhost:3306/" + baseDeDatos;
    private Connection con = null;

    public Connection getConexion() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(this.url, this.usuario, this.password);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConexionReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

}
