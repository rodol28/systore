package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import objetosDelDominio.Domicilio;
import objetosDelDominio.Empleado;
import objetosDelDominio.Persona;

public class DBAuxiliares {

    private Conector con;

    public DBAuxiliares() {
    }

    public ResultSet resultSetMarcas() throws SQLException, ClassNotFoundException {
        ResultSet marcas = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `marca` ORDER BY `descripcion`";
            Statement consulta = con.getConexion().createStatement();
            marcas = consulta.executeQuery(url);
            return marcas;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return marcas;
    }

    public ResultSet resultSetRubros() throws ClassNotFoundException {
        ResultSet rubros = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `rubro` ORDER BY `descripcion`";
            Statement consulta = con.getConexion().createStatement();
            rubros = consulta.executeQuery(url);
            return rubros;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rubros;
    }

    public ResultSet resultSetMotivosDePerdida() throws SQLException, ClassNotFoundException {
        ResultSet motivos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `motivoperdida` ORDER BY `descripcionMotivo`";
            Statement consulta = con.getConexion().createStatement();
            motivos = consulta.executeQuery(url);
            return motivos;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return motivos;
    }

    public ResultSet resultSetObrasSociales() throws SQLException, ClassNotFoundException {
        ResultSet obrasSociales = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `obrasocial` ORDER BY `razonSocialOb`";
            Statement consulta = con.getConexion().createStatement();
            obrasSociales = consulta.executeQuery(url);
            return obrasSociales;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obrasSociales;
    }

    public void altaDeMarca(String descripcionMarca) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `marca`(`idMarca`, `descripcion`) ";
            String datos = "VALUES (NULL,'" + descripcionMarca + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void altaDeRubro(String descripcionRubro) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `rubro`(`idRubro`, `descripcion`) ";
            String datos = "VALUES (NULL,'" + descripcionRubro + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void altaDeMotivoDePerdida(String descripcionMotivo) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `motivoperdida`(`idMotivoPerdida`, `descripcionMotivo`) ";
            String datos = "VALUES (NULL,'" + descripcionMotivo + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void altaDeObraSocial(String razonSocialOb) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `obrasocial`(`idObraSocial`, `razonSocialOb`) ";
            String datos = "VALUES (NULL,'" + razonSocialOb + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void bajaDeMarca(String descripcionMarca) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `marca` WHERE descripcion='" + descripcionMarca + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public void bajaDeRubro(String descripcionRubro) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `rubro` WHERE descripcion='" + descripcionRubro + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public void bajaDeMotivoDePerdida(String descripcionMotivo) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `motivoperdida` WHERE descripcionMotivo='" + descripcionMotivo + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public void bajaDeObraSocial(String razonSocialOb) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `obrasocial` WHERE razonSocialOb='" + razonSocialOb + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public ResultSet resultSetProvincias() throws SQLException, ClassNotFoundException {
        ResultSet provincias = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT nombre FROM provincia";
            Statement consulta = con.getConexion().createStatement();
            provincias = consulta.executeQuery(url);
            return provincias;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return provincias;
    }

    public ResultSet resultSetCompletoProvincias() throws SQLException, ClassNotFoundException {
        ResultSet provincias = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM provincia ORDER BY nombre ASC";
            Statement consulta = con.getConexion().createStatement();
            provincias = consulta.executeQuery(url);
            return provincias;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return provincias;
    }

    public ResultSet resultSetPaises() throws SQLException, ClassNotFoundException {
        ResultSet paises = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT nombre FROM pais";
            Statement consulta = con.getConexion().createStatement();
            paises = consulta.executeQuery(url);
            return paises;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return paises;
    }

    public ResultSet resultSetParentescos() throws SQLException, ClassNotFoundException {
        ResultSet parentescos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT descripcionParentesco FROM parentesco ORDER BY idParentesco ASC";
            Statement consulta = con.getConexion().createStatement();
            parentescos = consulta.executeQuery(url);
            return parentescos;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return parentescos;
    }

    public ResultSet resultSetPuestos() throws SQLException, ClassNotFoundException {
        ResultSet puestos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT descripcion FROM puesto ORDER BY descripcion ASC";
            Statement consulta = con.getConexion().createStatement();
            puestos = consulta.executeQuery(url);
            return puestos;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return puestos;
    }

    public ResultSet resultSetEstadosCivil() throws SQLException, ClassNotFoundException {
        ResultSet estadosCivil = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT descripcion FROM estadocivil";
            Statement consulta = con.getConexion().createStatement();
            estadosCivil = consulta.executeQuery(url);
            return estadosCivil;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return estadosCivil;
    }

    public ResultSet resultSetSexos() throws SQLException, ClassNotFoundException {
        ResultSet sexos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT sexoDescripcion FROM sexo ORDER BY sexoDescripcion ASC";
            Statement consulta = con.getConexion().createStatement();
            sexos = consulta.executeQuery(url);
            return sexos;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sexos;
    }

    public ResultSet resultSetCompletoPaises() throws SQLException, ClassNotFoundException {
        ResultSet paises = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM pais ORDER BY nombre ASC";
            Statement consulta = con.getConexion().createStatement();
            paises = consulta.executeQuery(url);
            return paises;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return paises;
    }

    public ResultSet resultSetTipoDeCuit() throws SQLException, ClassNotFoundException {
        ResultSet tiposDeCuit = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT descripcion FROM tipodecuit";
            Statement consulta = con.getConexion().createStatement();
            tiposDeCuit = consulta.executeQuery(url);
            return tiposDeCuit;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tiposDeCuit;
    }

    public ResultSet resultSetCompletoTipoDeCuit() throws SQLException, ClassNotFoundException {
        ResultSet tiposDeCuit = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM tipodecuit";
            Statement consulta = con.getConexion().createStatement();
            tiposDeCuit = consulta.executeQuery(url);
            return tiposDeCuit;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tiposDeCuit;
    }

    public int obtenerElUltimoIdDomicilio() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idDomicilio = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idDomicilio) AS id FROM domicilio");
        while (rs.next()) {
            idDomicilio = rs.getInt("id");
        }
        return idDomicilio;
    }

    public int obtenerElIdDomicilioDelEmpleado(int legajo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idDomicilio = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT Domicilio_idDomicilio AS id FROM empleado");
        while (rs.next()) {
            idDomicilio = rs.getInt("id");
        }
        return idDomicilio;
    }

    public int buscarRubroPorDescripcion(String rubroDescripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idRubro = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT * FROM rubro WHERE descripcion='" + rubroDescripcion + "'");
        while (rs.next()) {
            idRubro = rs.getInt("idRubro");
            return idRubro;
        }
        return 0;
    }

    public int buscarObraSocialPorRazonSocial(String razonSocialOb) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idObraSocial = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idObraSocial FROM obrasocial WHERE razonSocialOb='" + razonSocialOb + "'");
        while (rs.next()) {
            idObraSocial = rs.getInt("idObraSocial");
            return idObraSocial;
        }
        return 0;
    }

    public int buscarProvinciaPorNombre(String nombre) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idProvincia = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idProvincia FROM provincia WHERE nombre='" + nombre + "'");
        while (rs.next()) {
            idProvincia = rs.getInt("idProvincia");
            return idProvincia;
        }
        return 0;
    }

    public int buscarPaisPorNombre(String nombre) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idPais = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idPais FROM pais WHERE nombre='" + nombre + "'");
        while (rs.next()) {
            idPais = rs.getInt("idPais");
            return idPais;
        }
        return 0;
    }

    public int buscarSexoPorDescripcion(String descripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idSexo = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idSexo FROM sexo WHERE sexoDescripcion='" + descripcion + "'");
        while (rs.next()) {
            idSexo = rs.getInt("idSexo");
            return idSexo;
        }
        return 0;
    }

    public int buscarParentescoPorDescripcion(String descripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idParentesco = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idParentesco FROM parentesco WHERE descripcionParentesco='" + descripcion + "'");
        while (rs.next()) {
            idParentesco = rs.getInt("idParentesco");
            return idParentesco;
        }
        return 0;
    }

    public int buscarCuitPorDescripcion(String descripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idTipoDeCuit = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idTipoDeCUIT FROM tipodecuit WHERE descripcion='" + descripcion + "'");
        while (rs.next()) {
            idTipoDeCuit = rs.getInt("idTipoDeCUIT");
            return idTipoDeCuit;
        }
        return 0;
    }

    public int buscarMotivoDePerdidaPorDescripcion(String descripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idMotivoPerdida = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idMotivoPerdida FROM motivoperdida WHERE descripcionMotivo='" + descripcion + "'");
        while (rs.next()) {
            idMotivoPerdida = rs.getInt("idMotivoPerdida");
            return idMotivoPerdida;
        }
        return 0;
    }

    public void altaDelDomicilio(Domicilio doc) throws SQLException, ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `domicilio` "
                    + "(`idDomicilio`, "
                    + "`calle`, "
                    + "`numero`, "
                    + "`localidad`, "
                    + "`codigoPostal`, "
                    + "`Provincia_idProvincia`, "
                    + "`Pais_idPais`) ";

            String datos = "VALUES (NULL, "
                    + " '" + doc.getCalle() + "',"
                    + " '" + doc.getNumero() + "',"
                    + " '" + doc.getLocalidad() + "',"
                    + " '" + doc.getCodigoPostal() + "',"
                    + " '" + doc.getProvincia() + "',"
                    + " '" + doc.getPais() + "')";

//            System.out.println(table + datos);
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void altaDelDomicilioCompleto(Domicilio doc) throws SQLException, ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `domicilio` "
                    + "(`idDomicilio`, "
                    + "`calle`, "
                    + "`numero`, "
                    + "`localidad`, "
                    + "`codigoPostal`, "
                    + "`Provincia_idProvincia`, "
                    + "`Pais_idPais`) ";

            String datos = "VALUES (NULL, "
                    + " '" + doc.getCalle() + "',"
                    + " '" + doc.getNumero() + "',"
                    + " '" + doc.getLocalidad() + "',"
                    + " '" + doc.getCodigoPostal() + "',"
                    + " '" + doc.getProvincia() + "',"
                    + " '" + doc.getPais() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            int idDomicilio = obtenerElUltimoIdDomicilio();

            String table2 = "INSERT INTO `edificio` "
                    + "(`idEdificio`, "
                    + "`piso`, "
                    + "`departamento`, "
                    + "`Domicilio_idDomicilio`) ";

            String datos2 = "VALUES (NULL, "
                    + " '" + doc.getPiso() + "',"
                    + " '" + doc.getNroDepartamento() + "',"
                    + " '" + idDomicilio + "')";

            PreparedStatement st2 = con.getConexion().prepareStatement(table2 + datos2);
            st2.execute();

//            System.out.println(table + datos);
//            System.out.println(table2 + datos2);
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void actualizarDomicilioCompleto(Domicilio dom, int idDomicilio) throws SQLException, ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql1 = "UPDATE `domicilio` SET "
                    + "`calle`='" + dom.getCalle() + "',"
                    + "`numero`='" + dom.getNumero() + "',"
                    + "`localidad`='" + dom.getLocalidad() + "',"
                    + "`codigoPostal`='" + dom.getCodigoPostal() + "',"
                    + "`Provincia_idProvincia`='" + dom.getProvincia() + "',"
                    + "`Pais_idPais`='" + dom.getPais() + "'"
                    + "	WHERE `idDomicilio`=" + idDomicilio;

            PreparedStatement st = con.getConexion().prepareStatement(sql1);
            st.execute();

            String sql2 = "DELETE FROM `edificio` WHERE `Domicilio_idDomicilio`='" + idDomicilio + "'";

            PreparedStatement st2 = con.getConexion().prepareStatement(sql2);
            st2.execute();

            String table = "INSERT INTO `edificio` "
                    + "(`idEdificio`, "
                    + "`piso`, "
                    + "`departamento`, "
                    + "`Domicilio_idDomicilio`) ";

            String datos = "VALUES (NULL, "
                    + " '" + dom.getPiso() + "',"
                    + " '" + dom.getNroDepartamento() + "',"
                    + " '" + idDomicilio + "')";

            PreparedStatement st3 = con.getConexion().prepareStatement(table + datos);
            st3.execute();

            System.out.println(sql1);
            System.out.println(sql2);
            System.out.println(table + datos);

        } catch (SQLException ex) {
            System.out.println("Error al actualizar el registro domicilio completo.");
        }
    }

    public String buscarPaisPorID(int idPais) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String nombre = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT nombre FROM pais WHERE idPais='" + idPais + "'");
        while (rs.next()) {
            nombre = rs.getString("nombre");
            return nombre;
        }
        return "";
    }

    public String buscarParentescoPorID(int idParentesco) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String descripcion = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT descripcionParentesco FROM parentesco WHERE idParentesco='" + idParentesco + "'");
        while (rs.next()) {
            descripcion = rs.getString("descripcionParentesco");
            return descripcion;
        }
        return "";
    }

    public String buscarProvinciaPorID(int idProvincia) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String nombre = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT nombre FROM provincia WHERE idProvincia='" + idProvincia + "'");
        while (rs.next()) {
            nombre = rs.getString("nombre");
            return nombre;
        }
        return "";
    }

    public String buscarTipoCuitPorID(int idTipoDeCUIT) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String descripcion = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT `descripcion` FROM tipodecuit WHERE idTipoDeCUIT='" + idTipoDeCUIT + "'");
        while (rs.next()) {
            descripcion = rs.getString("descripcion");
        }
        return descripcion;
    }

    public String buscarRubroPorID(int idRubro) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String descripcion = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT descripcion FROM rubro WHERE idRubro='" + idRubro + "'");
        while (rs.next()) {
            descripcion = rs.getString("descripcion");
            return descripcion;
        }
        return "";
    }

    public String buscarPuestoPorID(int idPuesto) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String descripcion = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT descripcion FROM puesto WHERE idPuesto='" + idPuesto + "'");
        while (rs.next()) {
            descripcion = rs.getString("descripcion");
            return descripcion;
        }
        return "";
    }

    public String buscarEstadoCivilPorID(int idEstadoCivil) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String descripcion = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT descripcion FROM estadocivil WHERE idEstadoCivil='" + idEstadoCivil + "'");
        while (rs.next()) {
            descripcion = rs.getString("descripcion");
            return descripcion;
        }
        return "";
    }

    public String buscarSexoPorID(int idSexo) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String descripcion = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT sexoDescripcion FROM sexo WHERE idSexo='" + idSexo + "'");
        while (rs.next()) {
            descripcion = rs.getString("sexoDescripcion");
            return descripcion;
        }
        return "";
    }

    public String buscarObraSocialPorID(int idObraSocial) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String razosSocialOb = "";
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT `razonSocialOb` FROM obrasocial WHERE idObraSocial='" + idObraSocial + "'");
        while (rs.next()) {
            razosSocialOb = rs.getString("razonSocialOb");
            return razosSocialOb;
        }
        return "";
    }

    public ResultSet resultSetDomicilio(int idDomicilio) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet domicilio = null;

        Statement consulta = con.getConexion().createStatement();
        domicilio = consulta.executeQuery("SELECT * FROM domicilio WHERE idDomicilio='" + idDomicilio + "'");

        return domicilio;
    }

    public ResultSet resultSetPersona(int idPersona) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet persona = null;

        Statement consulta = con.getConexion().createStatement();
        persona = consulta.executeQuery("SELECT * FROM persona WHERE idPersona='" + idPersona + "'");

        return persona;
    }

    public Domicilio obtenerElDomicilio(int idDomicilio) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        Domicilio domicilio = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT * FROM domicilio WHERE idDomicilio='" + idDomicilio + "'");

        while (rs.next()) {
            domicilio = new Domicilio(rs.getString("calle"),
                    rs.getString("numero"),
                    rs.getString("localidad"),
                    rs.getInt("codigoPostal"),
                    rs.getInt("Provincia_idProvincia"),
                    rs.getInt("Pais_idPais"));
        }

        return domicilio;
    }

    public Domicilio obtenerElDomicilioConOSinEdificio(int idDomicilio) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int count = 0;
        ResultSet rs1 = null;
        ResultSet rs = null;
        Domicilio domicilio = null;

        /*Primero se consulta si existe un edificio que le corresponda al domicilio.
        Se cuenta la cantidad de filas del resultSet obtenido de edificio,
        si es igual a cero significa que el domicilio no tiene asociado un edificio,
        en caso contrario significa que el domicilio tiene asociado un registro edificio
        en la tabla edificio.*/
        Statement consulta1 = con.getConexion().createStatement();
        rs1 = consulta1.executeQuery("SELECT * FROM edificio WHERE Domicilio_idDomicilio=" + idDomicilio);

        while (rs1.next()) {
            count++;
        }

        if (count == 0) {
            Statement consulta = con.getConexion().createStatement();
            rs = consulta.executeQuery("SELECT calle, numero, localidad, codigoPostal, "
                    + "Provincia_idProvincia, Pais_idPais "
                    + "FROM domicilio "
                    + "WHERE idDomicilio='" + idDomicilio + "'");

            while (rs.next()) {
                domicilio = new Domicilio(rs.getString("calle"),
                        rs.getString("numero"),
                        rs.getString("localidad"),
                        rs.getInt("codigoPostal"),
                        rs.getInt("Provincia_idProvincia"),
                        rs.getInt("Pais_idPais"));
            }
            return domicilio;

        } else {
            Statement consulta = con.getConexion().createStatement();
            rs = consulta.executeQuery("SELECT calle, numero, localidad, codigoPostal, "
                    + "Provincia_idProvincia, Pais_idPais, piso, departamento "
                    + "FROM domicilio, edificio "
                    + "WHERE idDomicilio=Domicilio_idDomicilio "
                    + "AND idDomicilio='" + idDomicilio + "'");

            while (rs.next()) {
                domicilio = new Domicilio(rs.getString("calle"),
                        rs.getString("numero"),
                        rs.getString("localidad"),
                        rs.getInt("codigoPostal"),
                        rs.getInt("Provincia_idProvincia"),
                        rs.getInt("Pais_idPais"),
                        rs.getString("departamento"),
                        rs.getString("piso"));
            }
            return domicilio;
        }
    }

    public int obtenerElidDomicilioDelProveedor(int idProveedor) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        int idDomicilio = 0;

        PreparedStatement st = con.getConexion().prepareStatement("SELECT Domicilio_idDomicilio FROM proveedor WHERE idProveedor=" + idProveedor);
        rs = st.executeQuery();

        while (rs.next()) {
            idDomicilio = rs.getInt("Domicilio_idDomicilio");
        }
        return idDomicilio;
    }

    public int obtenerElidDomicilioDelCliente(int idCliente) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        int idDomicilio = 0;

        PreparedStatement st = con.getConexion().prepareStatement("SELECT Domicilio_idDomicilio FROM cliente WHERE idCliente=" + idCliente);
        rs = st.executeQuery();

        while (rs.next()) {
            idDomicilio = rs.getInt("Domicilio_idDomicilio");
        }
        return idDomicilio;
    }

    public int obtenerElidDomicilioDelEmpleado(int idEmpleado) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        int idDomicilio = 0;

        PreparedStatement st = con.getConexion().prepareStatement("SELECT Domicilio_idDomicilio FROM empleado WHERE idEmpleado=" + idEmpleado);
        rs = st.executeQuery();

        while (rs.next()) {
            idDomicilio = rs.getInt("Domicilio_idDomicilio");
        }
        return idDomicilio;
    }

    public ResultSet resultSetEdificio(int idDomicilio) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet edificio = null;

        Statement consulta = con.getConexion().createStatement();
        edificio = consulta.executeQuery("SELECT * FROM edificio WHERE Domicilio_idDomicilio='" + idDomicilio + "'");

        return edificio;
    }

    public void actulizarDomicilio(Domicilio dom, int idDomicilio) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `domicilio` SET "
                    + "`calle`='" + dom.getCalle() + "',"
                    + "`numero`='" + dom.getNumero() + "',"
                    + "`localidad`='" + dom.getLocalidad() + "',"
                    + "`codigoPostal`='" + dom.getCodigoPostal() + "',"
                    + "`Provincia_idProvincia`='" + dom.getProvincia() + "',"
                    + "`Pais_idPais`='" + dom.getPais() + "'"
                    + "	WHERE `idDomicilio`=" + idDomicilio;

            System.out.println(sql);
            con.getConexion().createStatement().execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al actualizar el registro domicilio.");
        }
    }

    public void actulizarPersona(Persona per, int idPersona) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `persona` SET "
                    + "`nombre`='" + per.getNombre() + "',"
                    + "`apellido`='" + per.getApellido() + "',"
                    + "`email`='" + per.getEmail() + "',"
                    + "`telefono`='" + per.getTelefono() + "'"
                    + "	WHERE `idPersona`=" + idPersona;

            con.getConexion().createStatement().execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public void altaDeDatosDePersona(Persona per, int idPersona) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        String fecha = "('" + per.getFechaNac().getAño() + "-"
                + per.getFechaNac().getMes() + "-"
                + per.getFechaNac().getDia() + "')";

        String table = "INSERT INTO `datospersona` "
                + "(`idDatosPersona`, "
                + "`dni`, "
                + "`sexo`, "
                + "`fechaDeNacimiento`, "
                + "`Persona_idPersona`, "
                + "`EstadoCivil_idEstadoCivil`)";

        String datos = " VALUES (NULL, "
                + " '" + per.getDni() + "',"
                + " '" + per.getSexo() + "',"
                + " '" + fecha + "',"
                + " '" + idPersona + "',"
                + " '" + per.getIdEstadoCivil() + "')";

        System.out.println(table + datos);
        PreparedStatement st = con.getConexion().prepareStatement(table + datos);
        st.execute();
    }

    public void altaDePersona(Persona per) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `persona` "
                    + "(`idPersona`, "
                    + "`nombre`, "
                    + "`apellido`, "
                    + "`email`, "
                    + "`telefono`)";

            String datos = " VALUES (NULL, "
                    + " '" + per.getNombre() + "',"
                    + " '" + per.getApellido() + "',"
                    + " '" + per.getEmail() + "',"
                    + " '" + per.getTelefono() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void altaDePersonaCompleta(Persona per) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `persona` "
                    + "(`idPersona`, "
                    + "`nombre`, "
                    + "`apellido`, "
                    + "`email`, "
                    + "`telefono`)";

            String datos = " VALUES (NULL, "
                    + " '" + per.getNombre() + "',"
                    + " '" + per.getApellido() + "',"
                    + " '" + per.getEmail() + "',"
                    + " '" + per.getTelefono() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            int idPersona = obtenerElUltimoIdPersona();

//            String fecha = "('" + per.getFechaNac().getAño() + "-"
//                    + per.getFechaNac().getMes() + "-"
//                    + per.getFechaNac().getDia() + "')";
            String fecha = "('" + per.getFechaNacString() + "')";

            String table2 = "INSERT INTO `datospersona` "
                    + "(`idDatosPersona`, "
                    + "`dni`, "
                    + "`fechaNacimiento`, "
                    + "`Persona_idPersona`, "
                    + "`EstadoCivil_idEstadoCivil`, "
                    + "`sexo`)";

            String datos2 = " VALUES (NULL, "
                    + " '" + per.getDni() + "',"
                    + " " + fecha + ","
                    + " '" + idPersona + "',"
                    + " '" + per.getIdEstadoCivil() + "',"
                    + " '" + per.getSexo() + "')";

            PreparedStatement st2 = con.getConexion().prepareStatement(table2 + datos2);
            st2.execute();

//            System.out.println(table + datos);
//            System.out.println(table2 + datos2);
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void actualizarPersonaCompleta(Persona per, int idPersona) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql1 = "UPDATE `persona` SET "
                    + "`nombre`='" + per.getNombre() + "',"
                    + "`apellido`='" + per.getApellido() + "',"
                    + "`email`='" + per.getEmail() + "',"
                    + "`telefono`='" + per.getTelefono() + "' "
                    + "WHERE `idPersona`=" + idPersona;

            PreparedStatement st = con.getConexion().prepareStatement(sql1);
            st.execute();

//            String fecha = "('" + per.getFechaNac().getAño() + "-"
//                    + per.getFechaNac().getMes() + "-"
//                    + per.getFechaNac().getDia() + "')";
            String fechaNac = "('" + per.getFechaNacString() + "')";

            String sql2 = "UPDATE `datospersona` SET "
                    + "`dni`='" + per.getDni() + "', "
                    + "`fechaNacimiento`=" + fechaNac + ", "
                    + "`EstadoCivil_idEstadoCivil`='" + per.getIdEstadoCivil() + "', "
                    + "`sexo`='" + per.getSexo() + "' "
                    + "WHERE `Persona_idPersona`=" + idPersona;

            PreparedStatement st2 = con.getConexion().prepareStatement(sql2);
            st2.execute();

            System.out.println(sql1);
            System.out.println(sql2);
        } catch (SQLException ex) {
            System.out.println("Error al actulizar el registro persona.");
        }
    }

    public int obtenerElUltimoIdPersona() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idPersona = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idPersona) AS id FROM persona");
        while (rs.next()) {
            idPersona = rs.getInt("id");
        }
        return idPersona;
    }

    public int obtenerElUltimoIdDatosPersona() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idDatosPersona = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idDatosPersona) AS id FROM datospersona");
        while (rs.next()) {
            idDatosPersona = rs.getInt("id");
        }
        return idDatosPersona;
    }

    public Persona obtenerLaPersona(int idPersona) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        Persona persona = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT * FROM persona WHERE idPersona='" + idPersona + "'");

        while (rs.next()) {
            persona = new Persona(rs.getString("nombre"),
                    rs.getString("apellido"),
                    rs.getString("email"),
                    rs.getString("telefono"));
        }
        return persona;
    }

    public int obtenerElidPersonaDelCliente(int idCliente) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        ResultSet rs = null;
        int idPersona = 0;

        PreparedStatement st = con.getConexion().prepareStatement("SELECT Persona_idPersona FROM cliente WHERE idCliente=" + idCliente);
        rs = st.executeQuery();

        while (rs.next()) {
            idPersona = rs.getInt("Persona_idPersona");
        }
        return idPersona;
    }

    public int buscarEstadoCivilPorDescripicion(String descripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idEstadoCivil = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idEstadoCivil FROM estadocivil WHERE descripcion='" + descripcion + "'");
        while (rs.next()) {
            idEstadoCivil = rs.getInt("idEstadoCivil");
            return idEstadoCivil;
        }
        return 0;
    }

    public void altaDeObraSocialDelEmpleado(ArrayList<String> obrasSociales, int idEmpleado) throws SQLException, ClassNotFoundException {

        Conector con = new MySqlConexion();
        con.conectar();

        String table = "INSERT INTO `obrasocialempleado` "
                + "(`idObraSocialEmpleado`, "
                + "`ObraSocial_idObraSocial`, "
                + "`Empleado_idEmpleado`) ";

        for (int i = 0; i < obrasSociales.size(); i++) {
            int idObraSocial = buscarObraSocialPorRazonSocial(obrasSociales.get(i));

            String datos = "VALUES (NULL, "
                    + " '" + idObraSocial + "',"
                    + " '" + idEmpleado + "')";

//            System.out.println(table + datos);
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        }
    }

    public ResultSet obtenerLasObraSocialDelEmpleado(int idEmpleado) throws SQLException, ClassNotFoundException {
        ResultSet obrasSociales = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `obrasocial` WHERE Empleado_idEmpleado='" + idEmpleado + "'";
            Statement consulta = con.getConexion().createStatement();
            obrasSociales = consulta.executeQuery(url);
            return obrasSociales;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obrasSociales;
    }

    public String obtenerLaObraSocialDelEmpleado(int idObraSocial) throws SQLException, ClassNotFoundException {
        ResultSet obraSocial = null;
        String os = "";
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT razonSocialOb FROM `obrasocial` WHERE idObraSocial='" + idObraSocial + "'";
            Statement consulta = con.getConexion().createStatement();
            obraSocial = consulta.executeQuery(url);
            while (obraSocial.next()) {
                os = obraSocial.getString("razonSocialOb");
            }
            return os;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return os;
    }

    public void altaDeBanco(int idEmpleado, String razonSocial, String cuentaBancaria) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `banco`(`idBanco`, `razonSocial`, `cuentaBancaria`, `Empleado_idEmpleado`) ";
            String datos = "VALUES (NULL,'" + razonSocial + "','" + cuentaBancaria + "','" + idEmpleado + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public ResultSet resultSetBanco(int idEmpleado) throws SQLException, ClassNotFoundException {
        ResultSet banco = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `banco` WHERE `Empleado_idEmpleado`='" + idEmpleado + "'";
            Statement consulta = con.getConexion().createStatement();
            banco = consulta.executeQuery(url);
            return banco;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return banco;
    }

    public void actulizarBanco(Empleado empleado, int idEmpleado) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `banco` SET "
                    + "`razonSocial`='" + empleado.getRazonSocialBanco() + "', "
                    + "`cuentaBancaria`='" + empleado.getCuentaBancaria() + "'"
                    + "	WHERE `Empleado_idEmpleado`=" + idEmpleado;

            System.out.println(sql);
            con.getConexion().createStatement().execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }
}
