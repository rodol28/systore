package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import objetosDelDominio.Compra;
import objetosDelDominio.LineaDeCompra;

public class DBCompras {

    private DBArticulos dbArticulos;

    public ResultSet resultSetCompras() throws SQLException, ClassNotFoundException {
        ResultSet compras = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM compra";
            Statement consulta = con.getConexion().createStatement();
            compras = consulta.executeQuery(url);
            return compras;
        } catch (SQLException ex) {
        }
        return compras;
    }

    public ResultSet resultSetLineasDeCompra(int idCompra) throws SQLException, ClassNotFoundException {
        ResultSet lineasDeCompras = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM lineadecompra WHERE Compra_idCompra=" + idCompra;
            Statement consulta = con.getConexion().createStatement();
            lineasDeCompras = consulta.executeQuery(url);
            return lineasDeCompras;
        } catch (SQLException ex) {
        }
        return lineasDeCompras;
    }

    public static int obtenerElUltimoIdCompra() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idCompra = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idCompra) AS id FROM compra");
        while (rs.next()) {
            idCompra = rs.getInt("id");
        }
        return idCompra;
    }

    public void altaDeCompra(Compra compra) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

//            String fecha = "('" + compra.getFecha().getAño() + "-"
//                    + compra.getFecha().getMes() + "-"
//                    + compra.getFecha().getDia() + "')";
            
            String fechaDeCompra = "('" + compra.getFechaDeCompraString() + "')";

            String table = "INSERT INTO `compra` "
                    + "(`idCompra`, "
                    + "`fecha`, "
                    + "`total`, "
                    + "`Proveedor_idProveedor`) ";

            String datos = "VALUES (NULL, "
                    + fechaDeCompra + ","
                    + " '" + compra.getTotal() + "',"
                    + " '" + compra.getIdProveedor() + "')";

//            System.out.println(table + datos);
            
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            altaLineasDeCompra(compra.getLineasDeCompra(), compra.getCodigo());

            JOptionPane.showMessageDialog(null, "Compra registrada.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void altaLineasDeCompra(ArrayList<LineaDeCompra> lineas, int idCompra) throws SQLException, ClassNotFoundException {

        dbArticulos = new DBArticulos();
        Conector con = new MySqlConexion();
        con.conectar();

        int cantidad = 0;
        int idArticulo = 0;

        String table = "INSERT INTO `lineadecompra` "
                + "(`idLineaDeCompra`, "
                + "`cantidad`, "
                + "`Articulo_idArticulo`, "
                + "`Compra_idCompra`) ";

        String datos = "";

        for (int i = 0; i < lineas.size(); i++) {
            cantidad = lineas.get(i).getCantidad();
            idArticulo = lineas.get(i).getArticulo().getCodigo();

            datos = "VALUES (NULL, "
                    + " '" + cantidad + "',"
                    + " '" + idArticulo + "',"
                    + " '" + idCompra + "')";
            
//            System.out.println(table + datos);

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            dbArticulos.actualizarStockCostoPrecio(lineas.get(i).getArticulo());
        }
    }

    public String obtenerLaFechaDeCompra(int idCompra) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        String fecha = "";
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM compra WHERE idCompra=" + idCompra);
            rs = st.executeQuery();

            while (rs.next()) {
                fecha = rs.getString("fecha");
            }

            return fecha;
        } catch (SQLException ex) {
        }
        return fecha;
    }

    public ResultSet obtenerLaCompraNroX(int nroFactura) throws SQLException, ClassNotFoundException {
        ResultSet compra = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM compra WHERE `idCompra`=" + nroFactura;
            Statement consulta = con.getConexion().createStatement();
            compra = consulta.executeQuery(url);
            return compra;
        } catch (SQLException ex) {
        }
        return compra;
    }
}
