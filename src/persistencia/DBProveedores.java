package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetosDelDominio.Domicilio;
import objetosDelDominio.Proveedor;

public class DBProveedores {

    private DBAuxiliares dbAuxiliares;

    public ResultSet resultSetProveedores() throws ClassNotFoundException {
        ResultSet proveedores = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT razonSocial FROM proveedor";
            Statement consulta = con.getConexion().createStatement();
            proveedores = consulta.executeQuery(url);
            return proveedores;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return proveedores;
    }

    public ResultSet resultSetCompletoProveedores() throws ClassNotFoundException {
        ResultSet proveedores = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM proveedor";
            Statement consulta = con.getConexion().createStatement();
            proveedores = consulta.executeQuery(url);
            return proveedores;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return proveedores;
    }

    public int buscarProveedorPorRazonSocial(String razonSocial) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idProveedor = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idProveedor FROM proveedor WHERE razonSocial='" + razonSocial + "'");
        while (rs.next()) {
            idProveedor = rs.getInt("idProveedor");
            return idProveedor;
        }
        return 0;
    }

    public void altaDeProveedor(Proveedor pro) throws SQLException, ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            dbAuxiliares = new DBAuxiliares();

            String table = "INSERT INTO `proveedor` "
                    + "(`idProveedor`, "
                    + "`razonSocial`, "
                    + "`telefono`, "
                    + "`celular`, "
                    + "`email`, "
                    + "`nroCUIT`, "
                    + "`web`, "
                    + "`observacion`, "
                    + "`TipoDeCUIT_idTipoDeCUIT`, "
                    + "`Rubro_idRubro`, "
                    + "`domicilio`, "
                    + "`pais`, "
                    + "`provincia`, "
                    + "`localidad`, "
                    + "`codigoPostal`) ";

            String datos = "VALUES (NULL, "
                    + " '" + pro.getRazonSocial() + "',"
                    + " '" + pro.getTelefono() + "',"
                    + " '" + pro.getCelular() + "',"
                    + " '" + pro.getEmail() + "',"
                    + " '" + pro.getNroCUIT() + "',"
                    + " '" + pro.getWeb() + "',"
                    + " '" + pro.getObservacion() + "',"
                    + " '" + pro.getTipoDeCuit() + "',"
                    + " '" + pro.getRubro() + "',"
                    + " '" + pro.getDomicilio() + "',"
                    + " '" + pro.getPais() + "',"
                    + " '" + pro.getProvincia() + "',"
                    + " '" + pro.getLocalidad() + "',"
                    + " '" + pro.getCodigoPostal() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            JOptionPane.showMessageDialog(null, "Proveedor agregado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void bajaDeProveedor(int idProveedor) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();
            
            String deletePro = "DELETE FROM `proveedor` WHERE idProveedor=" + idProveedor;
            Statement st2 = con.getConexion().createStatement();
            st2.execute(deletePro);

        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public Proveedor obtenerElProveedor(int idProveedor) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        Proveedor proveedor = null;

        Conector con = new MySqlConexion();
        con.conectar();

        dbAuxiliares = new DBAuxiliares();

        PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM proveedor WHERE idProveedor=" + idProveedor);
        rs = st.executeQuery();

        while (rs.next()) {
            proveedor = new Proveedor(idProveedor,
                    rs.getString("razonSocial"),
                    rs.getString("telefono"),
                    rs.getString("celular"),
                    rs.getString("email"),
                    rs.getString("nroCUIT"),
                    rs.getString("web"),
                    rs.getString("observacion"),
                    rs.getInt("TipoDeCuit_idTipoDeCuit"),
                    rs.getInt("Rubro_idrubro"),
                    rs.getString("domicilio"),
                    rs.getString("pais"),
                    rs.getString("provincia"),
                    rs.getString("localidad"),
                    rs.getInt("codigoPostal"));
        }
        return proveedor;
    }

    public void modificarProveedor(Proveedor pro) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();
            dbAuxiliares = new DBAuxiliares();

            String sql = "UPDATE `proveedor` SET "
                    + "`razonSocial`='" + pro.getRazonSocial() + "',"
                    + "`telefono`='" + pro.getTelefono() + "',"
                    + "`celular`='" + pro.getCelular() + "',"
                    + "`email`='" + pro.getEmail() + "',"
                    + "`nroCUIT`='" + pro.getNroCUIT() + "',"
                    + "`web`='" + pro.getWeb() + "',"
                    + "`observacion`='" + pro.getObservacion() + "',"
                    + "`TipoDeCUIT_idTipoDeCUIT`='" + pro.getTipoDeCuit() + "',"
                    + "`Rubro_idRubro`='" + pro.getRubro() + "',"
                    + "`domicilio`='" + pro.getDomicilio() + "',"
                    + "`pais`='" + pro.getPais() + "',"
                    + "`provincia`='" + pro.getProvincia() + "',"
                    + "`localidad`='" + pro.getLocalidad() + "',"
                    + "`codigoPostal`='" + pro.getCodigoPostal() + "'"
                    + "	WHERE `idProveedor`=" + pro.getCodigo();
            con.getConexion().createStatement().execute(sql);

            JOptionPane.showMessageDialog(null, "Proveedor modificado.");
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public ResultSet busquedaPredictivaDeRazonSocial(String palabra) throws SQLException, ClassNotFoundException {
        ResultSet proveedores = null;

        String sql = "SELECT * FROM `proveedor` WHERE `razonSocial` LIKE '" + palabra + "%'";

        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement(sql);
            proveedores = st.executeQuery();

            return proveedores;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return proveedores;
    }

    public ResultSet obtenerElProveedorNroX(int nroProveedor) throws ClassNotFoundException {
        ResultSet proveedor = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM proveedor WHERE `idProveedor`=" + nroProveedor;
            Statement consulta = con.getConexion().createStatement();
            proveedor = consulta.executeQuery(url);
            return proveedor;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return proveedor;
    }
}
