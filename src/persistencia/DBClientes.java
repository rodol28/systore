package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetosDelDominio.Cliente;
import objetosDelDominio.Domicilio;
import objetosDelDominio.Persona;

public class DBClientes {

    private DBAuxiliares dbAuxiliares;

    public ResultSet resultSetClientes() throws SQLException, ClassNotFoundException {
        ResultSet clientes = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM cliente";
            Statement consulta = con.getConexion().createStatement();
            clientes = consulta.executeQuery(url);
            return clientes;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientes;
    }

    public void altaDeCliente(Cliente cli) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();
            dbAuxiliares = new DBAuxiliares();

            dbAuxiliares.altaDePersona(cli);
            int idPersona = dbAuxiliares.obtenerElUltimoIdPersona();

            String table = "INSERT INTO `cliente` "
                    + "(`idCliente`, "
                    + "`nroCUIT`, "
                    + "`observacion`, "
                    + "`Persona_idPersona`, "
                    + "`TipoDeCUIT_idTipoDeCUIT`, "
                    + "`domicilio`, "
                    + "`pais`, "
                    + "`provincia`, "
                    + "`localidad`, "
                    + "`codigoPostal`) ";

            String datos = "VALUES (NULL, "
                    + " '" + cli.getNroCuit() + "',"
                    + " '" + cli.getObservacion() + "',"
                    + " '" + idPersona + "',"
                    + " '" + cli.getTipoDeCuit() + "',"
                    + " '" + cli.getDomicilio() + "',"
                    + " '" + cli.getPais() + "',"
                    + " '" + cli.getProvincia() + "',"
                    + " '" + cli.getLocalidad() + "',"
                    + " '" + cli.getCodigoPostal() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            JOptionPane.showMessageDialog(null, "Cliente agregado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void bajaDeCliente(int idCliente) throws ClassNotFoundException, ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            ResultSet persona = null;
            int idPersona = 0;

            /*--------------------------------------------------------------------------------*/
            String sql2 = "SELECT `Persona_idPersona` FROM `cliente` WHERE idCliente=" + idCliente;

            Statement consulta2 = con.getConexion().createStatement();
            persona = consulta2.executeQuery(sql2);

            while (persona.next()) {
                idPersona = persona.getInt("Persona_idPersona");
            }

            /*--------------------------------------------------------------------------------*/
            String deletePro = "DELETE FROM `cliente` WHERE idCliente=" + idCliente;
            Statement st1 = con.getConexion().createStatement();
            st1.execute(deletePro);

            /*--------------------------------------------------------------------------------*/
            String deletePer = "DELETE FROM `persona` WHERE `idPersona`=" + idPersona;
            Statement st3 = con.getConexion().createStatement();
            st3.execute(deletePer);

        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public Cliente obtenerElCliente(int idCliente) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;

        Persona persona = null;
        Cliente cliente = null;

        Conector con = new MySqlConexion();
        con.conectar();

        dbAuxiliares = new DBAuxiliares();

        PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM cliente WHERE idCliente=" + idCliente);
        rs = st.executeQuery();

        persona = dbAuxiliares.obtenerLaPersona(dbAuxiliares.obtenerElidPersonaDelCliente(idCliente));

        while (rs.next()) {
            cliente = new Cliente(idCliente,
                    rs.getString("nroCUIT"),
                    rs.getString("observacion"),
                    rs.getInt("TipoDeCUIT_idTipoDeCUIT"),
                    rs.getString("domicilio"),
                    rs.getString("pais"),
                    rs.getString("provincia"),
                    rs.getString("localidad"),
                    rs.getInt("codigoPostal"),
                    persona.getNombre(),
                    persona.getApellido(),
                    persona.getEmail(),
                    persona.getTelefono());
        }
        return cliente;
    }

    public void modificarCliente(Cliente cli) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();
            dbAuxiliares = new DBAuxiliares();

            String sql = "UPDATE `cliente` SET "
                    + "`nroCUIT`='" + cli.getNroCuit() + "',"
                    + "`observacion`='" + cli.getObservacion() + "',"
                    + "`TipoDeCUIT_idTipoDeCUIT`='" + cli.getTipoDeCuit() + "',"
                    + "`domicilio`='" + cli.getDomicilio() + "',"
                    + "`pais`='" + cli.getPais() + "',"
                    + "`provincia`='" + cli.getProvincia() + "',"
                    + "`localidad`='" + cli.getLocalidad() + "',"
                    + "`codigoPostal`='" + cli.getCodigoPostal() + "'"
                    + "	WHERE `idCliente`=" + cli.getCodigo();
            con.getConexion().createStatement().execute(sql);
//            System.out.println(sql);
            int idPersona = dbAuxiliares.obtenerElidPersonaDelCliente(cli.getCodigo());
            dbAuxiliares.actulizarPersona(cli, idPersona);

            JOptionPane.showMessageDialog(null, "Proveedor modificado.");
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public ResultSet obtenerElClienteNroX(int nroCliente) throws SQLException, ClassNotFoundException {
        ResultSet cliente = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM cliente WHERE `idCliente`=" + nroCliente;
            Statement consulta = con.getConexion().createStatement();
            cliente = consulta.executeQuery(url);
            return cliente;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cliente;
    }
}
