package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetosDelDominio.Puesto;

public class DBPuestos {

    public ResultSet resultSetPuestos() throws SQLException, ClassNotFoundException {
        ResultSet puestos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM puesto");
            puestos = st.executeQuery();
            return puestos;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return puestos;
    }

    public void altaDePuesto(Puesto puesto) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `puesto`(`idPuesto`, `descripcion`, "
                    + "`salario`) ";
            String datos = "VALUES (NULL,'" + puesto.getDescripcionPuesto() + "','"
                    + puesto.getSueldoBasico() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
            JOptionPane.showMessageDialog(null, "Puesto registrado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void modificarPuesto(Puesto puesto) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `puesto` SET "
                    + "`salario`=" + puesto.getSueldoBasico()
                    + "	WHERE `idPuesto`=" + puesto.getIdPuesto();
            con.getConexion().createStatement().execute(sql);

            JOptionPane.showMessageDialog(null, "Sueldo basico actualizado.");
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public void bajaDePuesto(int idPuesto) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `puesto` WHERE idPuesto='" + idPuesto + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public int buscarPuestoPorDescripcion(String descripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idPuesto = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT idPuesto FROM puesto WHERE descripcion='" + descripcion + "'");
        while (rs.next()) {
            idPuesto = rs.getInt("idPuesto");
            return idPuesto;
        }
        return 0;
    }

    public float obtenerElBasicoDelPuestoX(int idPuesto) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        float basico = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT salario FROM puesto WHERE idPuesto='" + idPuesto + "'");
        while (rs.next()) {
            basico = rs.getFloat("salario");
            return basico;
        }
        return 0;
    }
}
