package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetosDelDominio.Articulo;

public class DBArticulos {

    private Conector con;
    private Articulo articulo;

    public DBArticulos() {
    }

    public ResultSet resultSetArticulos() throws SQLException, ClassNotFoundException {
        ResultSet articulos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM articulo");
            articulos = st.executeQuery();
            return articulos;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return articulos;
    }

    public String obtenerRubroDelArticulo(int idRubro) throws ClassNotFoundException {
        String rubro = "";
        ResultSet rs = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            Statement consulta = con.getConexion().createStatement();
            rs = consulta.executeQuery("SELECT * FROM rubro WHERE idRubro='" + idRubro + "'");
            while (rs.next()) {
                rubro = rs.getString("descripcion");
            }
            return rubro;
        } catch (SQLException ex) {
            System.out.println("Error al buscar el registro.");
        }
        return rubro;
    }

    public String obtenerMarcaDelArticulo(int idMarca) throws ClassNotFoundException {
        String marca = "";
        ResultSet rs = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            Statement consulta = con.getConexion().createStatement();
            rs = consulta.executeQuery("SELECT * FROM marca WHERE idMarca='" + idMarca + "'");
            while (rs.next()) {
                marca = rs.getString("descripcion");
            }
            return marca;
        } catch (SQLException ex) {
            System.out.println("Error al buscar el registro.");
        }
        return marca;
    }

    public int obtenerElUltimoIdArticulo() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idArticulo = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idArticulo) AS id FROM articulo");
        while (rs.next()) {
            idArticulo = rs.getInt("id");
        }
        return idArticulo;
    }

    public int buscarMarcaPorDescripcion(String marcaDescripcion) throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idMarca = 0;
        ResultSet rs = null;

        Statement consulta = con.getConexion().createStatement();
        rs = consulta.executeQuery("SELECT * FROM marca WHERE descripcion='" + marcaDescripcion + "'");
        while (rs.next()) {
            idMarca = rs.getInt("idMarca");
            return idMarca;
        }
        return 0;
    }

    public void altaDeArticulo(Articulo art) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String table = "INSERT INTO `articulo` "
                    + "(`idArticulo`, "
                    + "`descripcion`, "
                    + "`precioUnitario`, "
                    + "`costo`, "
                    + "`stock`, "
                    + "`stockMin`, "
                    + "`Rubro_idRubro`, "
                    + "`Marca_idMarca`) ";

            String datos = "VALUES (NULL, "
                    + " '" + art.getDescripcion() + "',"
                    + " '" + art.getPrecioUnitario() + "',"
                    + " '" + art.getCosto() + "',"
                    + " '" + art.getStock() + "',"
                    + " '" + art.getStockMin() + "',"
                    + " '" + art.getRubro() + "',"
                    + " '" + art.getMarca() + "')";
            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();
            JOptionPane.showMessageDialog(null, "Articulo agregado.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void bajaDeArticulo(int idArticulo) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "DELETE FROM `articulo` WHERE idArticulo='" + idArticulo + "'";

            Statement st = con.getConexion().createStatement();
            st.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al eliminar el registro.");
        }
    }

    public Articulo obtenerElArticulo(int idArticulo) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;

        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM articulo WHERE idArticulo=" + idArticulo);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    articulo = new Articulo(idArticulo, rs.getString("descripcion"),
                            rs.getInt("precioUnitario"),
                            rs.getInt("costo"),
                            rs.getInt("stock"),
                            rs.getInt("stockMin"),
                            rs.getInt("Rubro_idRubro"),
                            rs.getInt("Marca_idMarca"));
                }
                return articulo;
            } else {
                return null;
            }

        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return articulo;
    }

    public void modificarArticulo(Articulo art) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `articulo` SET "
                    + "`descripcion`='" + art.getDescripcion() + "',"
                    + "`precioUnitario`=" + art.getPrecioUnitario() + ","
                    + "`costo`=" + art.getCosto() + ","
                    + "`stock`=" + art.getStock() + ","
                    + "`stockMin`=" + art.getStockMin() + ","
                    + "`Rubro_idRubro`=" + art.getRubro() + ","
                    + "`Marca_idMarca`=" + art.getMarca()
                    + "	WHERE `idArticulo`=" + art.getCodigo();
            con.getConexion().createStatement().execute(sql);

            JOptionPane.showMessageDialog(null, "Articulo modificado.");
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public ResultSet busquedaPredictivaDeDescripcion(String palabra) throws SQLException, ClassNotFoundException {
        ResultSet articulos = null;

        String sql = "SELECT * FROM `articulo` WHERE `descripcion` LIKE '" + palabra + "%'";

        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement(sql);
            articulos = st.executeQuery();

            return articulos;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return articulos;
    }

    public void actualizarStockCostoPrecio(Articulo art) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String sql = "UPDATE `articulo` SET "
                    + "`precioUnitario`=" + art.getPrecioUnitario() + ","
                    + "`costo`=" + art.getCosto() + ","
                    + "`stock`=" + art.getStock()
                    + "	WHERE `idArticulo`=" + art.getCodigo();

            con.getConexion().createStatement().execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public void actualizarStockDelArticulo(int idArticulo, int cantidad) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            int stockActual = obtenerStockActualDelArticulo(idArticulo);
            int stockNuevo = stockActual - cantidad;

            String sql = "UPDATE `articulo` SET "
                    + "`stock`='" + stockNuevo + "'"
                    + "	WHERE `idArticulo`=" + idArticulo;
            con.getConexion().createStatement().execute(sql);
        } catch (SQLException ex) {
            System.out.println("Error al modificar el registro.");
        }
    }

    public int obtenerStockActualDelArticulo(int idArticulo) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        int stock = 0;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM articulo WHERE idArticulo=" + idArticulo);
            rs = st.executeQuery();

            while (rs.next()) {
                stock = rs.getInt("stock");
            }

            return stock;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stock;
    }

    public String obtenerDescripcionDelArticulo(int idArticulo) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        String descripcion = "";
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM articulo WHERE idArticulo=" + idArticulo);

            rs = st.executeQuery();
            while (rs.next()) {
                descripcion = rs.getString("descripcion");
            }

            return descripcion;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return descripcion;
    }

    public float obtenerPrecioDelArticulo(int idArticulo) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        float precio = 0;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM articulo WHERE idArticulo=" + idArticulo);

            rs = st.executeQuery();
            while (rs.next()) {
                precio = rs.getFloat("costo");
            }

            return precio;
        } catch (SQLException ex) {
            Logger.getLogger(Conector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return precio;
    }

    public ResultSet obtenerElArticuloNroX(int nroArticulo) throws SQLException, ClassNotFoundException {
        ResultSet articulo = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM articulo WHERE `idArticulo`=" + nroArticulo);
            articulo = st.executeQuery();
            if (articulo == null) {
                return null;
            } else {
                return articulo;
            }
        } catch (SQLException ex) {
        }
        return articulo;
    }
}
