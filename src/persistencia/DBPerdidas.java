package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import objetosDelDominio.LineaDePerdida;
import objetosDelDominio.Perdida;
import utilidadesParaFrames.UtilAuxiliares;

public class DBPerdidas {

    private Conector con;
    private DBArticulos dbArticulos;

    public ResultSet resultSetMotivos() throws SQLException, ClassNotFoundException {
        ResultSet motivos = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM `motivoperdida` ORDER BY `descripcionMotivo`";
            Statement consulta = con.getConexion().createStatement();
            motivos = consulta.executeQuery(url);
            return motivos;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return motivos;
    }

    public void altaDePerdida(Perdida perdida) throws ClassNotFoundException {
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String fecha = UtilAuxiliares.obtenerFechaHoy();

            String table = "INSERT INTO `perdida` "
                    + "(`idPerdida`, "
                    + "`fecha`, "
                    + "`observacion`, "
                    + "`Empleado_idEmpleado`) ";

            String datos = "VALUES (NULL, "
                    + fecha + ","
                    + " '" + perdida.getObservacion() + "',"
                    + " '" + perdida.getIdEmpleado() + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            altaLineasDePerdida(perdida.getLineasDePerdida(), obtenerElUltimoIdPerdida());

            JOptionPane.showMessageDialog(null, "Perdida registrada.");
        } catch (SQLException ex) {
            System.out.println("Error al agregar el registro.");
        }
    }

    public void altaLineasDePerdida(ArrayList<LineaDePerdida> lineas, int idPerdida) throws SQLException, ClassNotFoundException {

        dbArticulos = new DBArticulos();
        Conector con = new MySqlConexion();
        con.conectar();

        int cantidad = 0;
        int idArticulo = 0;
        int idMotivo = 0;

        String table = "INSERT INTO `lineadeperdida` "
                + "(`idLineaDePerdida`, "
                + "`cantidad`, "
                + "`Articulo_idArticulo`, "
                + "`Perdida_idPerdida`, "
                + "`MotivoPerdida_idMotivoPerdida`) ";

        String datos = "";

        for (int i = 0; i < lineas.size(); i++) {
            cantidad = lineas.get(i).getCantidad();
            idArticulo = lineas.get(i).getArticulo().getCodigo();
            idMotivo = lineas.get(i).getIdMotivo();
            datos = "VALUES (NULL, "
                    + " '" + cantidad + "',"
                    + " '" + idArticulo + "',"
                    + " '" + idPerdida + "',"
                    + " '" + idMotivo + "')";

            PreparedStatement st = con.getConexion().prepareStatement(table + datos);
            st.execute();

            dbArticulos.actualizarStockDelArticulo(idArticulo, cantidad);
        }
    }

    public static int obtenerElUltimoIdPerdida() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        int idPerdida = 0;
        Statement consulta = con.getConexion().createStatement();
        ResultSet rs = consulta.executeQuery("SELECT MAX(idPerdida) AS id FROM perdida");
        while (rs.next()) {
            idPerdida = rs.getInt("id");
        }
        return idPerdida;
    }

    public ResultSet resultSetPerdidas() throws SQLException, ClassNotFoundException {
        ResultSet perdidas = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM perdida";
            Statement consulta = con.getConexion().createStatement();
            perdidas = consulta.executeQuery(url);
            return perdidas;
        } catch (SQLException ex) {
        }
        return perdidas;
    }

    public ResultSet obtenerPerdidaNroX(int idPerdida) throws SQLException, ClassNotFoundException {
        ResultSet perdida = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT * FROM perdida WHERE idPerdida=" + idPerdida;
            Statement consulta = con.getConexion().createStatement();
            perdida = consulta.executeQuery(url);
            return perdida;
        } catch (SQLException ex) {
        }
        return perdida;
    }

    public ResultSet obtenerCantidadPrecioDelArticuloPerdido(int idPerdida) throws SQLException, ClassNotFoundException {
        ResultSet lineasPerdida = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT idArticulo, cantidad, precioUnitario FROM lineadeperdida, articulo WHERE idArticulo=Articulo_idArticulo AND Perdida_idPerdida=" + idPerdida;
            Statement consulta = con.getConexion().createStatement();
            lineasPerdida = consulta.executeQuery(url);
            return lineasPerdida;
        } catch (SQLException ex) {
        }
        return lineasPerdida;
    }

    public String obtenerLaFechaDePerdida(int idPerdida) throws SQLException, ClassNotFoundException {
        ResultSet rs = null;
        String fecha = "";
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            PreparedStatement st = con.getConexion().prepareStatement("SELECT * FROM perdida WHERE idPerdida=" + idPerdida);
            rs = st.executeQuery();

            while (rs.next()) {
                fecha = rs.getString("fecha");
            }

            return fecha;
        } catch (SQLException ex) {
        }
        return fecha;
    }

    public ResultSet obtenerDatosParaTablaLineasDePerdida(int idPerdida) throws ClassNotFoundException {
        ResultSet linea = null;
        try {
            Conector con = new MySqlConexion();
            con.conectar();

            String url = "SELECT descripcionMotivo, cantidad, idArticulo, descripcion, precioUnitario "
                    + "FROM lineadeperdida, motivoperdida, articulo "
                    + "WHERE MotivoPerdida_idMotivoPerdida=idMotivoPerdida "
                    + "AND Articulo_idArticulo=idArticulo "
                    + "AND Perdida_idPerdida=" + idPerdida;
            Statement consulta = con.getConexion().createStatement();
            linea = consulta.executeQuery(url);
            return linea;
        } catch (SQLException ex) {
            Logger.getLogger(DBAuxiliares.class.getName()).log(Level.SEVERE, null, ex);
        }
        return linea;
    }
}
