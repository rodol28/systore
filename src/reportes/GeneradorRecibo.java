package reportes;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.JTable;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import objetosDelDominio.ConceptoLiquidado;
import objetosDelDominio.ReciboSueldo;
import persistencia.DBEmpleados;
import persistencia.DBLiquidacion;
import persistencia.DBReciboSueldo;
import servicios.NumberToLetterConverter;
import utilidadesParaFrames.UtilFrameLiquidacion;

public class GeneradorRecibo {

    private static DBReciboSueldo dbReciboSueldo;
    private static DBEmpleados dbEmpleados;
    private static DBLiquidacion dbLiquidacion;

    public static void generarRecibo(int legajo, JTable tablaConceptos) throws JRException, SQLException, ClassNotFoundException, ParseException {
        ReciboSueldo recibo;
        ArrayList<ReciboSueldo> coll = new ArrayList<ReciboSueldo>();

        float totalHaberes = 0;
        float totalDeducciones = 0;
        float totalHaberesSinDeduc = 0;

        dbReciboSueldo = new DBReciboSueldo();
        dbEmpleados = new DBEmpleados();
        ResultSet datosRecibo = dbReciboSueldo.resultSetDatosRecibo(legajo);

        int cantidadDeFilas = UtilFrameLiquidacion.obtenerLaCantidadDeFilasDeLaTabla(tablaConceptos);

        for (int i = 0; i < cantidadDeFilas; i++) {
            String obraSocial = "" + tablaConceptos.getModel().getValueAt(i, 0);
        }

        while (datosRecibo.next()) {
            int antiguedad = dbEmpleados.obtenerAntiguedadDelEmpleado(legajo);

            recibo = new ReciboSueldo(datosRecibo.getString("apellido") + ", " + datosRecibo.getString("nombre"),
                    "" + legajo,
                    datosRecibo.getString("cuil"),
                    "" + datosRecibo.getDouble("salario"),
                    datosRecibo.getString("fechaDeIngreso"),
                    "Empleados de comercio",
                    datosRecibo.getString("puesto"),
                    "San Miguel de tucuman",
                    datosRecibo.getString("obraSocial"),
                    antiguedad + " años",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0");

            for (int j = 0; j < cantidadDeFilas; j++) {
                String haber = "";
                String deduc = "";
                String haberSinDes = "";
                /**
                 * ****************Haberes******************
                 */
                if (tablaConceptos.getModel().getValueAt(j, 3) == null) {
                    haber = "";
                } else {
                    String aux1 = "" + tablaConceptos.getModel().getValueAt(j, 3);
                    if (!aux1.equals("")) {
                        float haberFloat = Float.valueOf(aux1);
                        totalHaberes = totalHaberes + haberFloat;
                    }
                    haber = "" + tablaConceptos.getModel().getValueAt(j, 3);
                }
                /**
                 * ****************Deducciones******************
                 */
                if (tablaConceptos.getModel().getValueAt(j, 4) == null) {
                    deduc = "";
                } else {
                    String aux2 = "" + tablaConceptos.getModel().getValueAt(j, 4);
                    if (!aux2.equals("")) {
                        float valor = Float.valueOf(aux2);
                        totalDeducciones = totalDeducciones + valor;
                    }
                    deduc = "" + tablaConceptos.getModel().getValueAt(j, 4);
                }
                /**
                 * ****************Haberes sin descuento******************
                 */
                if (tablaConceptos.getModel().getValueAt(j, 5) == null) {
                    haberSinDes = "";
                } else {
                    String aux3 = "" + tablaConceptos.getModel().getValueAt(j, 5);
                    if (!aux3.equals("")) {
                        float valor = Float.valueOf(aux3);
                        totalHaberesSinDeduc = totalHaberesSinDeduc + valor;
                    }
                    haberSinDes = "" + tablaConceptos.getModel().getValueAt(j, 5);
                }

                if ((Integer.parseInt((String) tablaConceptos.getModel().getValueAt(j, 0)) == 7
                        && tablaConceptos.getModel().getValueAt(j, 3) == null) || 
                        (Integer.parseInt((String) tablaConceptos.getModel().getValueAt(j, 0)) == 8
                        && tablaConceptos.getModel().getValueAt(j, 3) == null)) {

                } else {
                    ConceptoLiquidado c = new ConceptoLiquidado("" + tablaConceptos.getModel().getValueAt(j, 0),
                            "" + tablaConceptos.getModel().getValueAt(j, 1),
                            "" + tablaConceptos.getModel().getValueAt(j, 2),
                            haber,
                            haberSinDes,
                            deduc);
                    recibo.addConcepto(c);
                }

            }//fin del for externo

            recibo.setTotalHaberes("" + formatearNumero(totalHaberes));
            recibo.setTotalDeducciones("" + formatearNumero(totalDeducciones));
            recibo.setTotalHaberesSinDeducciones("" + totalHaberesSinDeduc);
            float sueldoNeto = (float) formatearNumero((float) (((totalHaberes) - totalDeducciones) + totalHaberesSinDeduc));
            recibo.setSueldoNeto("" + sueldoNeto);
            String pesos = NumberToLetterConverter.convertNumberToLetter("" + sueldoNeto);

            recibo.setPesos(pesos);
            coll.add(recibo);

        }//fin del while

        String path = "src\\reportes\\ReciboSueldo.jasper";
        JasperReport reporte = (JasperReport) JRLoader.loadObject(path);
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, null, new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(coll));

//        File directorio = new File("C://Recibos De Sueldo");
//        directorio.mkdirs();
        dbLiquidacion = new DBLiquidacion();
        int codigo = dbLiquidacion.obtenerElUltimoIdLiquidacion();

//        JasperExportManager.exportReportToPdfFile(jasperPrint, "C://Recibos De Sueldo/recibo_original_" + codigo + ".pdf");
        JasperViewer jv = new JasperViewer(jasperPrint, false);
        jv.setTitle("Recibo de sueldo - Original");
        jv.setVisible(true);

        /*------------------------DUPLICADO---------------------------------*/
        String pathDuplicado = "src\\reportes\\ReciboSueldoDuplicado.jasper";
        JasperReport reporteDuplicado = (JasperReport) JRLoader.loadObject(pathDuplicado);
        JasperPrint jasperPrintDuplicado = JasperFillManager.fillReport(reporteDuplicado, null, new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(coll));

//        JasperExportManager.exportReportToPdfFile(jasperPrintDuplicado, "C://Recibos De Sueldo/recibo_duplicado_" + codigo + ".pdf");
        JasperViewer jvDuplicado = new JasperViewer(jasperPrintDuplicado, false);
        jvDuplicado.setTitle("Recibo de sueldo - Duplicado");
        jvDuplicado.setVisible(true);

        /*------------------------TRIPLICADO---------------------------------*/
        String pathTriplicado = "src\\reportes\\ReciboSueldoTriplicado.jasper";
        JasperReport reporteTriplicado = (JasperReport) JRLoader.loadObject(pathTriplicado);
        JasperPrint jasperPrintTriplicado = JasperFillManager.fillReport(reporteTriplicado, null, new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource(coll));

//        JasperExportManager.exportReportToPdfFile(jasperPrintTriplicado, "C://Recibos De Sueldo/recibo_triplicado_" + codigo + ".pdf");
        JasperViewer jvTriplicado = new JasperViewer(jasperPrintTriplicado, false);
        jvTriplicado.setTitle("Recibo de sueldo - Triplicado");
        jvTriplicado.setVisible(true);

    }

    public static double formatearNumero(float numero) throws ParseException {
        DecimalFormat df = new DecimalFormat("0.00");
        String formate = df.format(numero);
        double finalValue = (double) df.parse(formate);
        return finalValue;
    }
}
