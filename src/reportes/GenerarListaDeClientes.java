package reportes;

import com.mysql.jdbc.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.ConexionReportes;

public class GenerarListaDeClientes {
    
    public static void generarReporteListaDeClientes() throws JRException, ClassNotFoundException, SQLException {
        ConexionReportes con = new ConexionReportes();
        Connection conn = (Connection) con.getConexion();

        String path = "src\\reportes\\ListaDeClientes.jasper";
        JasperReport reporte = (JasperReport) JRLoader.loadObject(path);
        Map parametro = new HashMap();
        
        JasperPrint jp = JasperFillManager.fillReport(reporte, null, conn);
        JasperViewer jv = new JasperViewer(jp, false);
        jv.setTitle("Lista de clientes");
        jv.setVisible(true);

        jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }    
}
