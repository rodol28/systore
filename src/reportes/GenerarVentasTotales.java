package reportes;

import com.mysql.jdbc.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.ConexionReportes;

public class GenerarVentasTotales {
    
    public static void generarReporteDeVentasTotales(Date desde, Date hasta) throws JRException, ClassNotFoundException, SQLException {
        ConexionReportes con = new ConexionReportes();
        Connection conn = (Connection) con.getConexion();

        String path = "src\\reportes\\VentasTotales.jasper";
        JasperReport reporte = (JasperReport) JRLoader.loadObject(path);
        Map parametros = new HashMap();
        parametros.put("desde", desde);
        parametros.put("hasta", hasta);
        
        JasperPrint jp = JasperFillManager.fillReport(reporte, parametros, conn);
        JasperViewer jv = new JasperViewer(jp, false);
        jv.setTitle("Ventas totales");
        jv.setVisible(true);

        jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }    
}
