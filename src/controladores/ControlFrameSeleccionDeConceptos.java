package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import utilidadesParaFrames.UtilFrameSeleccionDeConceptos;
import vistas.FrameSeleccionDeConceptos;
import vistas.MenuPrincipal;

public class ControlFrameSeleccionDeConceptos implements ActionListener {

    private FrameSeleccionDeConceptos frameSeleccionDeConceptos;
    private int idPuesto;
    private int legajo;
    private ArrayList<Integer> concepSelec = new ArrayList<Integer>();

    public ControlFrameSeleccionDeConceptos(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameSeleccionDeConceptos = new FrameSeleccionDeConceptos(menuPrincipal, true, this);
        frameSeleccionDeConceptos.setControlador(this);
        UtilFrameSeleccionDeConceptos.mostrarConceptosEnTabla(frameSeleccionDeConceptos.getTablaConceptos());
        frameSeleccionDeConceptos.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameSeleccionDeConceptos.BTN_BUSCAR)) {
            try {
                this.idPuesto = UtilFrameSeleccionDeConceptos.mostrarApeNombreYConceptos(frameSeleccionDeConceptos, frameSeleccionDeConceptos.getTxtLegajo(), frameSeleccionDeConceptos.getTablaConceptos());
                this.legajo = frameSeleccionDeConceptos.getTxtLegajo();
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameSeleccionDeConceptos.BTN_ACEPTAR)) {
            concepSelec.clear();
            boolean sel = false;
            DefaultTableModel modelo = (DefaultTableModel) frameSeleccionDeConceptos.getTablaConceptos().getModel();
            for (int fila = 0; fila < frameSeleccionDeConceptos.getTablaConceptos().getRowCount(); fila++) {
                sel = (boolean) frameSeleccionDeConceptos.getTablaConceptos().getModel().getValueAt(fila, 0);
                if (sel) {
                    this.concepSelec.add(Integer.parseInt("" + frameSeleccionDeConceptos.getTablaConceptos().getModel().getValueAt(fila, 1)));
                }
            }
            try {
                UtilFrameSeleccionDeConceptos.registrarConceptosSeleccionados(this.concepSelec, this.idPuesto, this.legajo);
                frameSeleccionDeConceptos.dispose();
//            mostrarSeleccionados(this.concepSelec);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameSeleccionDeConceptos.BTN_CANCELAR)) {
            frameSeleccionDeConceptos.dispose();
        }
    }//fin del actionEvent

    public void mostrarSeleccionados(ArrayList<Integer> selec) {
        for (int i = 0; i < selec.size(); i++) {
            System.out.println(selec.get(i));
        }
    }

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

    public int getLegajo() {
        return legajo;
    }

    public void setLegajo(int legajo) {
        this.legajo = legajo;
    } 
}
