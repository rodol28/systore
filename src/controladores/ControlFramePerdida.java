package controladores;

import controladoresAuxiliares.ControlFrameBuscarArticulo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFramePerdida;
import vistas.FramePerdida;
import vistas.MenuPrincipal;

public class ControlFramePerdida implements ActionListener {

    private FramePerdida framePerdida;
    private ControlFrameBuscarArticulo controlFrameBuscarArticulo;

    public ControlFramePerdida(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        framePerdida = new FramePerdida(menuPrincipal, true);
        framePerdida.setContralador(this);
        UtilAuxiliares.mostraElCodigoDelUsuarioActivo(framePerdida.getJTextFieldTxtVendedor());
        framePerdida.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(framePerdida.BTN_ACEPTAR)) {
            try {
                UtilFramePerdida.registrarPerdida(framePerdida);
                framePerdida.dispose();
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        if (e.getActionCommand().equals(framePerdida.BTN_BUSCAR)) {
            try {
                controlFrameBuscarArticulo = new ControlFrameBuscarArticulo(framePerdida);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(framePerdida.BTN_MODIFICAR_CANTIDAD)) {
            int filaSeleccionada = UtilFramePerdida.getFilaSeleccionada(framePerdida);
            if (filaSeleccionada > -1) {
                int cantidad = UtilFramePerdida.getCantidadIngresada(filaSeleccionada);
                UtilFramePerdida.actualizarCantidadStockActual(cantidad, filaSeleccionada, framePerdida.getTablaLineasDePerdida());
            }
        }
        if (e.getActionCommand().equals(framePerdida.BTN_ELIMINAR_RENGLON)) {
            int filaSeleccionada = UtilFramePerdida.getFilaSeleccionada(framePerdida);
            if (filaSeleccionada > -1) {
                UtilFramePerdida.eliminarUnItem(filaSeleccionada, framePerdida.getTablaLineasDePerdida());
            }
        }
        if (e.getActionCommand().equals(framePerdida.BTN_SALIR)) {
            UtilFramePerdida.setContadorDeArticulo(0);
            framePerdida.dispose();
        }
    }

}
