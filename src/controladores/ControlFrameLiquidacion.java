package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import otros.HorasExtra;
import reportes.GeneradorRecibo;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameLiquidacion;
import vistas.FrameLiquidacion;
import vistas.MenuPrincipal;

public class ControlFrameLiquidacion implements ActionListener, ItemListener {

    private FrameLiquidacion frameLiquidacion;
    private HorasExtra horasExtra;
    private ArrayList<HorasExtra> horasExt = new ArrayList<HorasExtra>();

    public ControlFrameLiquidacion(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException, ParseException {
        frameLiquidacion = new FrameLiquidacion(menuPrincipal, true);
        frameLiquidacion.setControlador(this);
        UtilFrameLiquidacion.mostraElCodigoDeLaNuevaLiquidacion(frameLiquidacion);
        UtilAuxiliares.llenarComboPuestos(frameLiquidacion.getComboPuestos());
        UtilFrameLiquidacion.mostrarEmpleadosEnTabla(frameLiquidacion.getTablaLegajos());
        frameLiquidacion.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameLiquidacion.BTN_ACEPTAR)) {
            int filaSeleccionada = UtilFrameLiquidacion.getFilaSeleccionada(frameLiquidacion);
            if (filaSeleccionada > -1) {
                int legajo = Integer.parseInt("" + frameLiquidacion.getTablaLegajos().getModel().getValueAt(filaSeleccionada, 1));
                try {
                    GeneradorRecibo.generarRecibo(legajo, frameLiquidacion.getTablaConceptos());
                    int idConcepto = Integer.parseInt("" + frameLiquidacion.getTablaConceptos().getModel().getValueAt(0, 0));
                    if (idConcepto == 0) {
                        UtilFrameLiquidacion.registrarLiquidacion(legajo, frameLiquidacion, frameLiquidacion.getTablaConceptos(), "Mensual");
                    } else {
                        UtilFrameLiquidacion.registrarLiquidacion(legajo, frameLiquidacion, frameLiquidacion.getTablaConceptos(), "Especial");
                    }
                    frameLiquidacion.dispose();
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                } catch (ParseException ex) {
                    Logger.getLogger(ControlFrameLiquidacion.class.getName()).log(Level.SEVERE, null, ex);
                } catch (JRException ex) {
                    Logger.getLogger(ControlFrameLiquidacion.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

//            for (int i = 0; i < horasExt.size(); i++) {
//                System.out.println("Legajo: " + horasExt.get(i).getLegajo());
//                System.out.println("Horas50: " + horasExt.get(i).getHoras50());
//                System.out.println("Horas100: " + horasExt.get(i).getHoras100());
//                System.out.println("=============================");
//            }
        }
        if (e.getActionCommand().equals(frameLiquidacion.BTN_CARGAR_HORAS_EXTRA)) {
            int filaSeleccionada = UtilFrameLiquidacion.getFilaSeleccionada(frameLiquidacion);
            if (filaSeleccionada > -1) {
                int legajo = Integer.parseInt("" + frameLiquidacion.getTablaLegajos().getModel().getValueAt(filaSeleccionada, 1));
                int horas50 = frameLiquidacion.getTxtHrs50();
                int horas100 = frameLiquidacion.getTxtHrs100();
                horasExtra = new HorasExtra(legajo, horas50, horas100);
                if (verificarSiExiste(horasExt, legajo)) {
                    removerItemDelArray(horasExt, legajo);
                    horasExt.add(horasExtra);
                } else {
                    horasExt.add(horasExtra);
                }

                frameLiquidacion.limpiar();
                try {
                    UtilFrameLiquidacion.cargarCantidadDeHoras(frameLiquidacion.getTablaConceptos(), horasExtra);
                } catch (ParseException ex) {
                }
            }
        }
        if (e.getActionCommand().equals(frameLiquidacion.BTN_CANCELAR)) {
            frameLiquidacion.dispose();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (ie.getStateChange() == 1) {
            try {
                UtilFrameLiquidacion.mostrarLosEmpleadosDeUnPuestoEnTabla(frameLiquidacion.getTablaLegajos(), frameLiquidacion.getPuestoSeleccionado(), frameLiquidacion.getTablaConceptos());
                frameLiquidacion.setTxtBasico("" + UtilFrameLiquidacion.obtenerElBasicoDelPuesto(UtilFrameLiquidacion.buscarElPuestoPorDescripcion(frameLiquidacion.getPuestoSeleccionado())));
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
    }

    public static void removerItemDelArray(ArrayList<HorasExtra> horasExt, int legajo) {
        int idx = 0;
        for (int i = 0; i < horasExt.size(); i++) {
            if (horasExt.get(i).getLegajo() == legajo) {
                idx = i;
            }
        }
        horasExt.remove(idx);
    }

    public boolean verificarSiExiste(ArrayList<HorasExtra> horasExt, int legajo) {
        boolean sonIguales = false;
        for (int i = 0; i < horasExt.size(); i++) {
            if (horasExt.get(i).getLegajo() == legajo) {
                sonIguales = true;
            }
        }
        return sonIguales;
    }
}
