package controladores;

import controladoresAuxiliares.ControlFrameModificarEleccionDeObrasSociales;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import objetosDelDominio.Empleado;
import utilidadesParaFrames.UtilFrameMenuEmpleados;
import utilidadesParaFrames.UtilFrameModificarEmpleado;
import vistas.FrameModificarEmpleado;
import vistas.MenuEmpleados;

public class ControlFrameModificarEmpleado implements ActionListener {

    private FrameModificarEmpleado frameModificarEmpleado;
    private JTable tablaEmpleados;
    private ControlFrameModificarGrupoFamiliar controlFrameModificarGrupoFamiliar;
    private ControlFrameModificarEleccionDeObrasSociales controlFrameModificarEleccionDeObrasSociales;
    private Empleado empleado;

    public ControlFrameModificarEmpleado(MenuEmpleados menuEmpleados,
            JTable tablaEmpleados,
            int legajo) throws SQLException, ClassNotFoundException {
        frameModificarEmpleado = new FrameModificarEmpleado(menuEmpleados, true);
        frameModificarEmpleado.setControlador(this);
        this.empleado = UtilFrameModificarEmpleado.cargarLosCampos(frameModificarEmpleado, legajo);
        this.tablaEmpleados = tablaEmpleados;
        frameModificarEmpleado.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarEmpleado.BTN_ACTUALIZAR)) {
            try {
                UtilFrameModificarEmpleado.actualizarElEmpleado(this.empleado, frameModificarEmpleado);
                UtilFrameMenuEmpleados.mostrarEmpleadosEnTabla(this.tablaEmpleados, false);
                frameModificarEmpleado.dispose();
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameModificarEmpleado.BTN_ACTUALIZAR_PARIENTES)) {
            try {
                controlFrameModificarGrupoFamiliar = new ControlFrameModificarGrupoFamiliar(frameModificarEmpleado, this.empleado, this);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameModificarEmpleado.BTN_ACTUALIZAR_OBRA_SOCIAL)) {
//            try {
//                controlFrameModificarEleccionDeObrasSociales = new ControlFrameModificarEleccionDeObrasSociales(frameModificarEmpleado, this.empleado, this);
//            } catch (SQLException ex) {
//            } catch (ClassNotFoundException ex) {
//            }
        }
        if (e.getActionCommand().equals(frameModificarEmpleado.BTN_CANCELAR)) {
            frameModificarEmpleado.dispose();
        }
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

//    public void mostrar(Empleado empleado) {
//        for (int i = 0; i < empleado.getGrupoFamiliar().size(); i++) {
//            System.out.println("==========================");
//            System.out.println(empleado.getGrupoFamiliar().get(i).getDni());
//            System.out.println(empleado.getGrupoFamiliar().get(i).getApellido());
//            System.out.println(empleado.getGrupoFamiliar().get(i).getNombre());
//            String dia = "" + empleado.getGrupoFamiliar().get(i).getFechaNac().getDia();
//            String mes = "" + empleado.getGrupoFamiliar().get(i).getFechaNac().getMes();
//            String año = "" + empleado.getGrupoFamiliar().get(i).getFechaNac().getAño();
//            String fechaNac = dia + "/" + mes + "/" + año;
//            System.out.println(fechaNac);
//            System.out.println(empleado.getGrupoFamiliar().get(i).getParentesco());
//        }
//    }
}
