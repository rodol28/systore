package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.DBClientes;
import utilidadesParaFrames.UtilFrameMenuClientes;
import vistas.MenuClientes;
import vistas.MenuPrincipal;

public class ControlMenuClientes implements ActionListener {

    private MenuClientes menuClientes;
    private ControlFrameNuevoCliente controlFrameNuevoCliente;
    private ControlFrameModificarCliente controlFrameModificarCliente;
    private DBClientes dbClientes;

    public ControlMenuClientes(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        menuClientes = new MenuClientes(menuPrincipal, true);
        menuClientes.setControlador(this);
        UtilFrameMenuClientes.mostrarClientesEnTabla(menuClientes.getTablaClientes());
        menuClientes.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(menuClientes.BTN_NUEVO)) {
            try {
                controlFrameNuevoCliente = new ControlFrameNuevoCliente(menuClientes, menuClientes.getTablaClientes());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuClientes.BTN_MODIFICAR)) {
            int i = menuClientes.getTablaClientes().getSelectedRow();
            if (i > -1) {
                int idCliente = Integer.parseInt((String) menuClientes.getTablaClientes().getModel().getValueAt(i, 0));
                String tipoDeCuit = (String) menuClientes.getTablaClientes().getModel().getValueAt(i, 9);
                try {
                    controlFrameModificarCliente = new ControlFrameModificarCliente(menuClientes, tipoDeCuit, menuClientes.getTablaClientes(), idCliente);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuClientes.BTN_ELIMINAR)) {
            int i = menuClientes.getTablaClientes().getSelectedRow();
            if (i > -1) {
                int idCliente = Integer.parseInt((String) menuClientes.getTablaClientes().getModel().getValueAt(i, 0));
                int op = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar el cliente?");
                if (op == 0) {
                    try {
                        dbClientes = new DBClientes();
                        dbClientes.bajaDeCliente(idCliente);
                        UtilFrameMenuClientes.mostrarClientesEnTabla(menuClientes.getTablaClientes());
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuClientes.BTN_BUSCAR)) {
            int nroCliente = UtilFrameMenuClientes.getCodigoIngresado();
            if (nroCliente == -1) {
                try {
                    UtilFrameMenuClientes.mostrarClientesEnTabla(menuClientes.getTablaClientes());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                try {
                    UtilFrameMenuClientes.mostrarClienteXEnTabla(menuClientes.getTablaClientes(), nroCliente);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        }
        if (e.getActionCommand().equals(menuClientes.BTN_SALIR)) {
            menuClientes.dispose();
        }
    }

}
