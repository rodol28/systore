package controladores;

import controladoresAuxiliares.ControlFrameBuscarArticulo;
import controladoresAuxiliares.ControlFramePrecioCosto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import utilidadesParaFrames.UtilFrameIngresoDeMercaderia;
import vistas.FrameIngresoDeMercaderia;
import vistas.MenuPrincipal;

public class ControlFrameIngresoDeMercaderia implements ActionListener {

    private FrameIngresoDeMercaderia frameIngresoDeMercaderia;
    private ControlFrameBuscarArticulo controlFrameBuscarArticulo;
    private ControlFramePrecioCosto controlFramePrecioCosto;
    private ControlFrameNuevoArticulo controlFrameNuevoArticulo;
    private float precio = 0;
    private float costo = 0;
    /*Variable que permite determinar si se presiono o no el boton "Cancelar"
    en la vista "FramePrecioCosto" y de esta manera llamar o no al metodo que 
    actualiza el precio y costo en la tabla.*/
    private boolean botonPress = false;

    public ControlFrameIngresoDeMercaderia(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameIngresoDeMercaderia = new FrameIngresoDeMercaderia(menuPrincipal, true);
        frameIngresoDeMercaderia.setControlador(this);
        UtilFrameIngresoDeMercaderia.mostraElCodigoDelNuevoArticulo(frameIngresoDeMercaderia);
        UtilFrameIngresoDeMercaderia.llenarComboProveedores(frameIngresoDeMercaderia.getComboProvedores());
        frameIngresoDeMercaderia.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameIngresoDeMercaderia.BTN_BUSCAR)) {
            try {
                controlFrameBuscarArticulo = new ControlFrameBuscarArticulo(frameIngresoDeMercaderia);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameIngresoDeMercaderia.BTN_MODIFICAR_CANTIDAD)) {
            int filaSeleccionada = UtilFrameIngresoDeMercaderia.getFilaSeleccionada(frameIngresoDeMercaderia);
            if (filaSeleccionada > -1) {
                int cantidad = UtilFrameIngresoDeMercaderia.getCantidadIngresada(filaSeleccionada);
                UtilFrameIngresoDeMercaderia.actualizarCantidadStockActual(cantidad, filaSeleccionada, frameIngresoDeMercaderia.getTablaArticulos());
            }
        }
        if (e.getActionCommand().equals(frameIngresoDeMercaderia.BTN_MODIFICAR_PRECIO)) {
            if (comprobarArticuloSeleccionado()) {
                int filaSeleccionada = UtilFrameIngresoDeMercaderia.getFilaSeleccionada(frameIngresoDeMercaderia);
                if (filaSeleccionada > -1) {
                    if (!botonPress) {
                        UtilFrameIngresoDeMercaderia.actualizarPrecioCosto(this.costo, this.precio, filaSeleccionada, frameIngresoDeMercaderia.getTablaArticulos());
                    } else {
                        botonPress = false;
                    }
                }
            }
        }
        if (e.getActionCommand().equals(frameIngresoDeMercaderia.BTN_NUEVO_ARTICULO)) {
            try {
                controlFrameNuevoArticulo = new ControlFrameNuevoArticulo(frameIngresoDeMercaderia);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameIngresoDeMercaderia.BTN_SUPRIMIR_FILA)) {
            int filaSeleccionada = UtilFrameIngresoDeMercaderia.getFilaSeleccionada(frameIngresoDeMercaderia);
            if (filaSeleccionada > -1) {
                UtilFrameIngresoDeMercaderia.eliminarUnItem(filaSeleccionada, frameIngresoDeMercaderia.getTablaArticulos());
            }
        }
        if (e.getActionCommand().equals(frameIngresoDeMercaderia.BTN_ACEPTAR)) {
            UtilFrameIngresoDeMercaderia.setContadorDeArticulo(0);
            if (frameIngresoDeMercaderia.getJdcFechaDeCompra() != null) {
                try {
                    UtilFrameIngresoDeMercaderia.registrarCompra(frameIngresoDeMercaderia);
                    frameIngresoDeMercaderia.dispose();
                } catch (ClassNotFoundException ex) {
                } catch (SQLException ex) {
                }
            }

        }
        if (e.getActionCommand().equals(frameIngresoDeMercaderia.BTN_SALIR)) {
            frameIngresoDeMercaderia.dispose();
            UtilFrameIngresoDeMercaderia.setContadorDeArticulo(0);
        }
    }

    /*Comprueba que se haya seleccionado un articulo*/
    public boolean comprobarArticuloSeleccionado() {
        int i = frameIngresoDeMercaderia.getTablaArticulos().getSelectedRow();
        if (i > -1) {
            controlFramePrecioCosto = new ControlFramePrecioCosto(frameIngresoDeMercaderia, this);
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public void setBotonPress(boolean botonPress) {
        this.botonPress = botonPress;
    }
}
