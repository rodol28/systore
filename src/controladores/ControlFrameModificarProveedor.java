package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import utilidadesParaFrames.UtilFrameMenuProveedores;
import utilidadesParaFrames.UtilFrameModificarProveedor;
import vistas.FrameModificarProveedor;
import vistas.MenuProveedores;

public class ControlFrameModificarProveedor implements ActionListener {

    private FrameModificarProveedor frameModificarProveedor;
    private JTable tablaProveedores;
    
    public ControlFrameModificarProveedor(MenuProveedores menuProveedores, String provincia, String rubroPro, int idProveedor, JTable tablaProveedores) throws SQLException, ClassNotFoundException {
        frameModificarProveedor = new FrameModificarProveedor(menuProveedores, true);
        frameModificarProveedor.setControlador(this);
        this.tablaProveedores = tablaProveedores;
        UtilFrameModificarProveedor.cargarLosCampos(frameModificarProveedor, rubroPro, provincia, idProveedor);
        frameModificarProveedor.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarProveedor.BTN_ACTUALIZAR)) {
            try {
                UtilFrameModificarProveedor.actualizarElProveedor(frameModificarProveedor);
                frameModificarProveedor.dispose();
                UtilFrameMenuProveedores.mostrarProveedoresEnTabla(tablaProveedores);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameModificarProveedor.BTN_CANCELAR)) {
            frameModificarProveedor.dispose();
        }
    }

}
