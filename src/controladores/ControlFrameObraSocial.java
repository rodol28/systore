package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import persistencia.DBAuxiliares;
import vistas.FrameObraSocial;
import vistas.MenuPrincipal;

public class ControlFrameObraSocial implements ActionListener {

    private FrameObraSocial frameObraSocial;
    private DBAuxiliares dbAuxiliares;
    private ControlFrameNuevaObraSocial controlFrameNuevaObraSocial;

    public ControlFrameObraSocial(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameObraSocial = new FrameObraSocial(menuPrincipal, true);
        frameObraSocial.setControlador(this);
        mostrarObrasSocialesEnTabla(frameObraSocial.getTablaObrasSociales());
        frameObraSocial.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameObraSocial.BTN_NUEVO)) {
            controlFrameNuevaObraSocial = new ControlFrameNuevaObraSocial(frameObraSocial, this, frameObraSocial.getTablaObrasSociales());
        }
        if (e.getActionCommand().equals(frameObraSocial.BTN_ELIMINAR)) {
            int i = frameObraSocial.getTablaObrasSociales().getSelectedRow();
            if (i > -1) {
                String obraSocial = (String) frameObraSocial.getTablaObrasSociales().getModel().getValueAt(i, 0);
                int op = JOptionPane.showConfirmDialog(null, "Esta seguro de borrar la obra social " + obraSocial + "?");
                if (op == 0) {
                    try {
                        dbAuxiliares = new DBAuxiliares();
                        dbAuxiliares.bajaDeObraSocial(obraSocial);
                        mostrarObrasSocialesEnTabla(frameObraSocial.getTablaObrasSociales());
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar una marca", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(frameObraSocial.BTN_SALIR)) {
            frameObraSocial.dispose();
        }
    }

    public void mostrarObrasSocialesEnTabla(JTable tablaMarcas) throws SQLException, ClassNotFoundException {

        dbAuxiliares = new DBAuxiliares();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Razon social"};
        modelo.setColumnIdentifiers(titulos);
        tablaMarcas.setModel(modelo);
        String registro[] = new String[1];

        ResultSet obrasSociales = dbAuxiliares.resultSetObrasSociales();

        while (obrasSociales.next()) {
            registro[0] = obrasSociales.getString("razonSocialOb");
            modelo.addRow(registro);
        }
    }
}
