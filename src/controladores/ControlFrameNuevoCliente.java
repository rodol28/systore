package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameMenuClientes;
import utilidadesParaFrames.UtilFrameNuevoCliente;
import vistas.FrameNuevoCliente;
import vistas.MenuClientes;

public class ControlFrameNuevoCliente implements ActionListener {

    private FrameNuevoCliente frameNuevoCliente;
    private JTable tablaClientes;

    public ControlFrameNuevoCliente(MenuClientes menuClientes, JTable tablaClientes) throws SQLException, ClassNotFoundException {
        frameNuevoCliente = new FrameNuevoCliente(menuClientes, true);
        frameNuevoCliente.setControlador(this);
        UtilAuxiliares.llenarComboTipoDeCuit(frameNuevoCliente.getComboCuit());
        this.tablaClientes = tablaClientes;
        frameNuevoCliente.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoCliente.BTN_GUARDAR)) {
            try {
                UtilFrameNuevoCliente.registraCliente(frameNuevoCliente);
                frameNuevoCliente.dispose();
                UtilFrameMenuClientes.mostrarClientesEnTabla(tablaClientes);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoCliente.BTN_CANCELAR)) {
            frameNuevoCliente.dispose();
            try {
                UtilFrameMenuClientes.mostrarClientesEnTabla(tablaClientes);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
    }

}
