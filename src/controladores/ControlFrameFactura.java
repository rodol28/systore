package controladores;

import controladoresAuxiliares.ControlFrameBuscarArticulo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameFactura;
import vistas.FrameFactura;
import vistas.MenuPrincipal;

public class ControlFrameFactura implements ActionListener {

    private FrameFactura frameFactura;
    private ControlFrameBuscarArticulo controlFrameBuscarArticulo;
    private boolean ventaCompletada = false;

    public ControlFrameFactura(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameFactura = new FrameFactura(menuPrincipal, true);
        frameFactura.setContralador(this);
        UtilFrameFactura.mostraElCodigoDeLaNuevaVenta(frameFactura);
        UtilAuxiliares.mostraElCodigoDelUsuarioActivo(frameFactura.getJTextFieldVendedor());
        frameFactura.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameFactura.BTN_ACEPTAR)) {
            UtilFrameFactura.setContadorDeArticulo(0);
            try {
                if (UtilFrameFactura.registrarVenta(frameFactura)) {
                    ventaCompletada = false;
                    frameFactura.dispose();
                }
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        if (e.getActionCommand().equals(frameFactura.BTN_MODIFICAR_CANTIDAD)) {
            int filaSeleccionada = UtilFrameFactura.getFilaSeleccionada(frameFactura);
            if (filaSeleccionada > -1) {
                int cantidad = UtilFrameFactura.getCantidadIngresada(filaSeleccionada);
                UtilFrameFactura.actualizarCantidad(cantidad, filaSeleccionada, frameFactura);
            }
        }
        if (e.getActionCommand().equals(frameFactura.BTN_ELIMINAR_RENGLON)) {
            int filaSeleccionada = UtilFrameFactura.getFilaSeleccionada(frameFactura);
            if (filaSeleccionada > -1) {
                UtilFrameFactura.eliminarUnItem(filaSeleccionada, frameFactura);
            }
        }
        if (e.getActionCommand().equals(frameFactura.BTN_BUSCAR)) {
            try {
                controlFrameBuscarArticulo = new ControlFrameBuscarArticulo(frameFactura);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameFactura.BTN_SALIR)) {
            UtilFrameFactura.setContadorDeArticulo(0);
            frameFactura.dispose();
        }
    }
}
