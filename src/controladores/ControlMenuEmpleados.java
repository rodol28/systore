package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.DBEmpleados;
import utilidadesParaFrames.UtilFrameMenuEmpleados;
import vistas.MenuEmpleados;
import vistas.MenuPrincipal;

public class ControlMenuEmpleados implements ActionListener {

    private MenuEmpleados menuEmpleados;
    private ControlFrameNuevoEmpleado controlFrameNuevoEmpleado;
    private DBEmpleados dbEmpleados;
    private ControlFrameModificarEmpleado controlFrameModificarEmpleado;

    public ControlMenuEmpleados(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        menuEmpleados = new MenuEmpleados(menuPrincipal, true);
        menuEmpleados.setControlador(this);
        UtilFrameMenuEmpleados.mostrarEmpleadosEnTabla(menuEmpleados.getTablaEmpleados(), false);
        menuEmpleados.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(menuEmpleados.BTN_NUEVO)) {
            try {
                controlFrameNuevoEmpleado = new ControlFrameNuevoEmpleado(menuEmpleados, menuEmpleados.getTablaEmpleados());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuEmpleados.BTN_MODIFICAR)) {

            int i = menuEmpleados.getTablaEmpleados().getSelectedRow();
            if (i > -1) {
                int legajo = Integer.parseInt((String) menuEmpleados.getTablaEmpleados().getModel().getValueAt(i, 0));
                try {
                    controlFrameModificarEmpleado = new ControlFrameModificarEmpleado(menuEmpleados, menuEmpleados.getTablaEmpleados(), legajo);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un empleado.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuEmpleados.BTN_ELIMINAR)) {
            int i = menuEmpleados.getTablaEmpleados().getSelectedRow();
            if (i > -1) {
                int legajo = Integer.parseInt((String) menuEmpleados.getTablaEmpleados().getModel().getValueAt(i, 0));
                int op = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar el empleado?");
                if (op == 0) {
                    dbEmpleados = new DBEmpleados();
                    try {
                        dbEmpleados.bajaDelEmpleado(legajo);
                        UtilFrameMenuEmpleados.mostrarEmpleadosEnTabla(menuEmpleados.getTablaEmpleados(), false);
                    } catch (ClassNotFoundException ex) {
                    } catch (SQLException ex) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un empleado", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuEmpleados.BTN_BUSCAR)) {
            int legajo = UtilFrameMenuEmpleados.getLegajoIngresado();
            if (legajo == -1) {
                try {
                    UtilFrameMenuEmpleados.mostrarEmpleadosEnTabla(menuEmpleados.getTablaEmpleados(), true);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                try {
                    UtilFrameMenuEmpleados.mostrarEmpleadoX(menuEmpleados.getTablaEmpleados(), legajo);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        }
        if (e.getActionCommand().equals(menuEmpleados.BTN_VER)) {
            System.out.println("VER");
        }
        if (e.getActionCommand().equals(menuEmpleados.BTN_SALIR)) {
            menuEmpleados.dispose();
        }
    }

}
