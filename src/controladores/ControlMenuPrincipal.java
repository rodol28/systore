package controladores;

import controladoresAuxiliares.ControlFrameFechas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import persistencia.Conector;
import persistencia.DBUsuarios;
import reportes.GenerarListaBajoStock;
import reportes.GenerarListaDeClientes;
import reportes.GenerarListaDeProveedores;
import reportes.GenerarListaGralArticulos;
import servicios.GeneradorDeReporte;
import vistas.ActualizarPrecios;
import vistas.MenuPrincipal;

public class ControlMenuPrincipal implements ActionListener {

    private MenuPrincipal menuPrincipal;
    private ActualizarPrecios actulizarPrecios;
    private ControlMenuArticulos controlMenuArticulos;
    private ControlActualizarPrecios controlActualizarPrecios;
    private ControlMenuClientes controlMenuClientes;
    private ControlMenuProveedores controlMenuProveedores;
    private ControlFrameLiquidacion controlFrameLiquidacion;
    private ControlMenuConceptos controlMenuConceptos;
    private ControlFrameSeleccionDeConceptos controlFrameSeleccionDeConceptos;
    private ControlMenuEmpleados controlMenuEmpleados;
    private ControlMenuPuestos controlMenuPuestos;
    private ControlMenuUsuarios controlMenuUsuarios;
    private ControlFrameIngresoDeMercaderia controlFrameIngresoDeMercaderia;
    private ControlFrameConsultarCompras controlFrameConsultarCompras;
    private ControlFrameFactura controlFrameFactura;
    private ControlFrameConsultarVentas controlFrameConsultarVentas;
    private ControlFramePerdida controlFrameSalida;
    private ControlFrameConsultarPerdidas controlFrameConsultarSalidas;
    private ControlFrameMarcas controlFrameMarcas;
    private ControlFrameObraSocial controlFrameObraSocial;
    private ControlFrameRubros controlFrameRubros;
    private ControlFrameMotivoDePerdida controlFrameMotivoDePerdida;
    private ControlFrameFechas controlFrameFechas;
    private Conector con;
    private DBUsuarios dbUsuarios;
    private String descripcionTipoDeUsuario = "";

    public ControlMenuPrincipal(Conector con, String usuario, int idTipoDeUsuario) throws SQLException, ClassNotFoundException {
        this.menuPrincipal = new MenuPrincipal();
        menuPrincipal.setControlador(this);
        dbUsuarios = new DBUsuarios();
        descripcionTipoDeUsuario = dbUsuarios.buscarDecripcionPorIdTipoDeUsuario(idTipoDeUsuario);
        menuPrincipal.setLabelUsuario(usuario + " - " + descripcionTipoDeUsuario);
        if (idTipoDeUsuario == 2) {
            menuPrincipal.desactivarOpcionesDeAdministrador();
        }
        this.menuPrincipal.ejecutar();
        this.con = con;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        /*BOTONES*/
        if (e.getActionCommand().equals(menuPrincipal.BTN_ARTICULOS)) {
            try {
                controlMenuArticulos = new ControlMenuArticulos(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.BTN_COMPRAS)) {
            try {
                controlFrameIngresoDeMercaderia = new ControlFrameIngresoDeMercaderia(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.BTN_VENTAS)) {
            try {
                controlFrameFactura = new ControlFrameFactura(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.BTN_EMPLEADOS)) {
            try {
                controlMenuEmpleados = new ControlMenuEmpleados(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.BTN_LIQUIDACION)) {
            try {
                controlFrameLiquidacion = new ControlFrameLiquidacion(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (ParseException ex) {
                Logger.getLogger(ControlMenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        /*MENUES*/
        if (e.getActionCommand().equals(menuPrincipal.MENU_ABM_ARTICULO)) {
            try {
                controlMenuArticulos = new ControlMenuArticulos(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
//        if (e.getActionCommand().equals(menuPrincipal.MENU_ACTUALIZAR_PRECIO_ARTICULO)) {
//            controlActualizarPrecios = new ControlActualizarPrecios(menuPrincipal);
//        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_ABM_CLIENTE)) {
            try {
                controlMenuClientes = new ControlMenuClientes(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_ABM_PROVEEDORES)) {
            try {
                controlMenuProveedores = new ControlMenuProveedores(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_GESTION_CONCEPTOS)) {
            try {
                controlMenuConceptos = new ControlMenuConceptos(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_SELECCION_CONCEPTOS)) {
            try {
                controlFrameSeleccionDeConceptos = new ControlFrameSeleccionDeConceptos(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_GESTION_EMPLEADOS)) {
            try {
                controlMenuEmpleados = new ControlMenuEmpleados(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_GESTION_PUESTOS)) {
            try {
                controlMenuPuestos = new ControlMenuPuestos(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_GESTION_USUARIOS)) {
            try {
                controlMenuUsuarios = new ControlMenuUsuarios(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_INGRESO_DE_MERCADERIA)) {
            try {
                controlFrameIngresoDeMercaderia = new ControlFrameIngresoDeMercaderia(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_CONSULTAR_COMPRAS)) {
            try {
                controlFrameConsultarCompras = new ControlFrameConsultarCompras(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_FACTURA)) {
            try {
                controlFrameFactura = new ControlFrameFactura(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_CONSULTAR_VENTAS)) {
            try {
                controlFrameConsultarVentas = new ControlFrameConsultarVentas(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_SALIDAS)) {
            try {
                controlFrameSalida = new ControlFramePerdida(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_CONSULTAR_SALIDAS)) {
            try {
                controlFrameConsultarSalidas = new ControlFrameConsultarPerdidas(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_ABM_MARCAS)) {
            try {
                controlFrameMarcas = new ControlFrameMarcas(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_ABM_OBRAS_SOCIALES)) {
            try {
                controlFrameObraSocial = new ControlFrameObraSocial(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_ABM_RUBROS)) {
            try {
                controlFrameRubros = new ControlFrameRubros(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuPrincipal.MENU_ABM_MOTIVOS_PERDIDA)) {
            try {
                controlFrameMotivoDePerdida = new ControlFrameMotivoDePerdida(menuPrincipal);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        
        if (e.getActionCommand().equals(menuPrincipal.MENU_SALIR)) {
            dbUsuarios = new DBUsuarios();
            try {
                dbUsuarios.registrarFinDeLaSesion(dbUsuarios.obtenerElIdDeLaSesionActiva());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (Exception ex) {
            }
            System.exit(0);
        }
        
        if (e.getActionCommand().equals(menuPrincipal.MENU_REPORTE_DE_PRUEBA)) {
            System.out.println("Hello, my name is Jasper");
            try {
                String id = JOptionPane.showInputDialog("Ingreso un idPersona:", "");
                int idPersona = Integer.parseInt(id);
                GeneradorDeReporte.reporteDePersonas(idPersona);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (JRException ex) {
            }
        }

        if (e.getActionCommand().equals(menuPrincipal.MENU_REPORTE_LISTA_ARTICULOS_GRAL)) {
            try {
                GenerarListaGralArticulos.generarReporteListaDeArticulos();
            } catch (JRException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        
        if (e.getActionCommand().equals(menuPrincipal.MENU_REPORTE_BAJO_STOCK_GRAL)) {
            try {
                GenerarListaBajoStock.generarReporteListaDeArticulosConBajoStock();
            } catch (JRException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        
        if (e.getActionCommand().equals(menuPrincipal.MENU_REPORTE_LISTA_DE_PROVEEDORES)) {
            try {
                GenerarListaDeProveedores.generarReporteListaDePreveedores();
            } catch (JRException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        
        if (e.getActionCommand().equals(menuPrincipal.MENU_REPORTE_LISTA_DE_CLIENTES)) {
            try {
                GenerarListaDeClientes.generarReporteListaDeClientes();
            } catch (JRException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        
        if (e.getActionCommand().equals(menuPrincipal.MENU_REPORTE_VENTAS_TOTALES)) {
            controlFrameFechas = new ControlFrameFechas(menuPrincipal);
        }
    }
}
