package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.Conector;
import persistencia.DBUsuarios;
import persistencia.MySqlConexion;
import servicios.Validador;
import vistas.Log;

public class ControlLog implements ActionListener {

    private Log log;
    private Conector con;
    private DBUsuarios dbUsuarios;
    private ControlMenuPrincipal controlMP;
    private int idTipoDeUsuario = 0;

    public ControlLog() {
        this.log = new Log(this);
        log.ejecutar();
        log.setControlador(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(log.BTN_ENTRAR)) {
            try {
                verificarLogueo();
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(log.BTN_SALIR)) {
            System.exit(0);
        }
    }

    public void verificarLogueo() throws SQLException, ClassNotFoundException {
        Conector con = new MySqlConexion();
        con.conectar();

        Validador validador = new Validador(con);
        boolean usuarioValido = false;
        try {
            usuarioValido = validador.validarUsuario(log.getTxtUsuario(), log.getTxtPassword());
        } catch (Exception ex) {
        }
        /*Si la variable "usuarioValido" es true el usuario es valido*/
        if (usuarioValido == true) {
            dbUsuarios = new DBUsuarios();
            try {
                dbUsuarios.registrarInicioDeLaSesion(log.getTxtUsuario());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (Exception ex) {
            }
            idTipoDeUsuario = dbUsuarios.obtenerElTipoDeUsuario(log.getTxtUsuario());
            log.dispose();

            controlMP = new ControlMenuPrincipal(this.con, log.getTxtUsuario(), idTipoDeUsuario);
        } else {
            JOptionPane.showMessageDialog(null, "Usuario no valido.");
            log.limpiar();
        }
    }
}
