package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import utilidadesParaFrames.UtilFrameMenuClientes;
import utilidadesParaFrames.UtilFrameModificarCliente;
import vistas.FrameModificarCliente;
import vistas.MenuClientes;

public class ControlFrameModificarCliente implements ActionListener {

    private FrameModificarCliente frameModificarCliente;
    private JTable tablaClientes;

    public ControlFrameModificarCliente(MenuClientes menuCliente, String tipoDeCuit, JTable tablaClientes, int idCliente) throws SQLException, ClassNotFoundException {
        frameModificarCliente = new FrameModificarCliente(menuCliente, true);
        frameModificarCliente.setControlador(this);
        this.tablaClientes = tablaClientes;
        UtilFrameModificarCliente.cargarLosCampos(frameModificarCliente, tipoDeCuit, idCliente);
        frameModificarCliente.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarCliente.BTN_GUARDAR)) {
            try {
                UtilFrameModificarCliente.actualizarElCliente(frameModificarCliente);
                frameModificarCliente.dispose();
                UtilFrameMenuClientes.mostrarClientesEnTabla(tablaClientes);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameModificarCliente.BTN_CANCELAR)) {
            frameModificarCliente.dispose();
        }
    }

}
