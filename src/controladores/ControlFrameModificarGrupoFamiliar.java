package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JTable;
import objetosDelDominio.Empleado;
import objetosDelDominio.Pariente;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameModificarGrupoFamiliar;
import vistas.FrameModificarEmpleado;
import vistas.FrameModificarGrupoFamiliar;

public class ControlFrameModificarGrupoFamiliar implements ActionListener {

    private FrameModificarGrupoFamiliar frameModificarGrupoFamiliar;
    private ControlFrameModificarEmpleado controlFrameModificarEmpleado;
    private Pariente pariente;
    private Empleado empleado;
    private ArrayList<Pariente> grupoFamiliarLocal;

    public ControlFrameModificarGrupoFamiliar(FrameModificarEmpleado frameModificarEmpleado,
            Empleado empleado,
            ControlFrameModificarEmpleado controlFrameModificarEmpleado) throws SQLException, ClassNotFoundException {

        frameModificarGrupoFamiliar = new FrameModificarGrupoFamiliar(frameModificarEmpleado, true);
        frameModificarGrupoFamiliar.setControlador(this);

        this.empleado = empleado;
        this.grupoFamiliarLocal = new ArrayList<Pariente>();
        igualarArraysInicial(this.grupoFamiliarLocal);
        this.controlFrameModificarEmpleado = controlFrameModificarEmpleado;

        UtilAuxiliares.llenarComboParentescos(frameModificarGrupoFamiliar.getComboParentesco());
        UtilFrameModificarGrupoFamiliar.mostrarElGrupoFamiliar(frameModificarGrupoFamiliar.getTablaParientes(), empleado);
        frameModificarGrupoFamiliar.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarGrupoFamiliar.BTN_ACTUALIZAR_GRUPO_FAMILIAR)) {
            igualarArraysFinal(this.grupoFamiliarLocal);
            controlFrameModificarEmpleado.setEmpleado(empleado);
            frameModificarGrupoFamiliar.dispose();
        }
        if (e.getActionCommand().equals(frameModificarGrupoFamiliar.BTN_AGREGAR)) {
            int dniPariente = Integer.parseInt("" + frameModificarGrupoFamiliar.getTxtDni());
            boolean existe = verificarSiExiste(frameModificarGrupoFamiliar.getTablaParientes(), dniPariente);

            if (existe) {
                cargarDatosEnElModeloExistente(frameModificarGrupoFamiliar, this.grupoFamiliarLocal);
                try {
                    UtilFrameModificarGrupoFamiliar.agregarPariente(this.grupoFamiliarLocal, frameModificarGrupoFamiliar.getTablaParientes());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
                frameModificarGrupoFamiliar.limpiarCampos();
            } else {
                cargarDatosEnElModelo(frameModificarGrupoFamiliar, this.grupoFamiliarLocal);
                try {
                    UtilFrameModificarGrupoFamiliar.agregarPariente(this.grupoFamiliarLocal, frameModificarGrupoFamiliar.getTablaParientes());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
                frameModificarGrupoFamiliar.limpiarCampos();
            }
//            mostrar(this.grupoFamiliarLocal);
        }
        if (e.getActionCommand().equals(frameModificarGrupoFamiliar.BTN_SUPRIMIR_FILA)) {
            int filaSeleccionada = UtilFrameModificarGrupoFamiliar.getFilaSeleccionada(frameModificarGrupoFamiliar);
            if (filaSeleccionada > -1) {
                int dniPariente = Integer.parseInt("" + frameModificarGrupoFamiliar.getTablaParientes().getModel().getValueAt(filaSeleccionada, 0));
                removerDelArrayGrupoFamiliarUnPariente(this.grupoFamiliarLocal, dniPariente);
                UtilFrameModificarGrupoFamiliar.eliminarUnItem(filaSeleccionada, frameModificarGrupoFamiliar.getTablaParientes());
                frameModificarGrupoFamiliar.limpiarCampos();
//                mostrar(this.grupoFamiliarLocal);
            }
        }
        if (e.getActionCommand().equals(frameModificarGrupoFamiliar.BTN_CANCELAR)) {
            frameModificarGrupoFamiliar.dispose();
        }
    }

    public static void removerDelArrayGrupoFamiliarUnPariente(ArrayList<Pariente> grupoFamiliarLocal, int dniPariente) {
        int idx = 0;
        for (int i = 0; i < grupoFamiliarLocal.size(); i++) {
            if (grupoFamiliarLocal.get(i).getDni() == dniPariente) {
                idx = i;
            }
        }
        grupoFamiliarLocal.remove(idx);
    }

    public void cargarDatosEnElModeloExistente(FrameModificarGrupoFamiliar frameModificarGrupoFamiliar, ArrayList<Pariente> grupoFamiliarLocal) {

        pariente = new Pariente(frameModificarGrupoFamiliar.getTxtDni(),
                frameModificarGrupoFamiliar.getTxtNombre(),
                frameModificarGrupoFamiliar.getTxtApellido(),
                "" + frameModificarGrupoFamiliar.getJdcFechaDeNac(),
                frameModificarGrupoFamiliar.getParentescoSeleccionado());

        int posicion = 0;

        for (int i = 0; i < grupoFamiliarLocal.size(); i++) {
            if (grupoFamiliarLocal.get(i).getDni() == frameModificarGrupoFamiliar.getTxtDni()) {
                posicion = i;
            }
        }
        grupoFamiliarLocal.remove(posicion);
        grupoFamiliarLocal.add(pariente);
    }

    public void cargarDatosEnElModelo(FrameModificarGrupoFamiliar frameModificarGrupoFamiliar, ArrayList<Pariente> grupoFamiliarLocal) {

        pariente = new Pariente(frameModificarGrupoFamiliar.getTxtDni(),
                frameModificarGrupoFamiliar.getTxtNombre(),
                frameModificarGrupoFamiliar.getTxtApellido(),
                "" + frameModificarGrupoFamiliar.getJdcFechaDeNac(),
                frameModificarGrupoFamiliar.getParentescoSeleccionado());

        /*Se agrega el pariente en el array grupo familiar del empleado*/
        grupoFamiliarLocal.add(pariente);
    }

    public boolean verificarSiExiste(JTable tablaParientes, int dniRec) {
        int cantidadDeFilas = UtilFrameModificarGrupoFamiliar.obtenerLaCantidadDeFilasDeLaTabla(tablaParientes);
        boolean sonIguales = false;
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            int dni = Integer.parseInt("" + tablaParientes.getModel().getValueAt(fila, 0));
            if (dniRec == dni) {
                sonIguales = true;
            }
        }
        return sonIguales;
    }

    public void igualarArraysFinal(ArrayList<Pariente> grupoFamiliarLocal) {

        for (int i = 0; i < empleado.getGrupoFamiliar().size(); i++) {
            empleado.getGrupoFamiliar().remove(i);
        }
        if (empleado.getGrupoFamiliar().size() > 0) {
            empleado.getGrupoFamiliar().remove(0);
        }

        for (int j = 0; j < this.grupoFamiliarLocal.size(); j++) {
            empleado.getGrupoFamiliar().add(grupoFamiliarLocal.get(j));
        }
    }

    public void igualarArraysInicial(ArrayList<Pariente> grupoFamiliarLocal) {

        for (int j = 0; j < empleado.getGrupoFamiliar().size(); j++) {
            grupoFamiliarLocal.add(empleado.getGrupoFamiliar().get(j));
        }
    }

//    public void mostrar(ArrayList<Pariente> grupoFamiliarLocal) {
//        for (int i = 0; i < grupoFamiliarLocal.size(); i++) {
//            System.out.println("==========================");
//            System.out.println(grupoFamiliarLocal.get(i).getDni());
//            System.out.println(grupoFamiliarLocal.get(i).getApellido());
//            System.out.println(grupoFamiliarLocal.get(i).getNombre());
//            String dia = "" + grupoFamiliarLocal.get(i).getFechaNac().getDia();
//            String mes = "" + grupoFamiliarLocal.get(i).getFechaNac().getMes();
//            String año = "" + grupoFamiliarLocal.get(i).getFechaNac().getAño();
//            String fechaNac = dia + "/" + mes + "/" + año;
//            System.out.println(fechaNac);
//            System.out.println(grupoFamiliarLocal.get(i).getParentesco());
//        }
//    }
}
