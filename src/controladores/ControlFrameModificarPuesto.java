package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import utilidadesParaFrames.UtilFrameMenuPuestos;
import utilidadesParaFrames.UtilFrameModificarPuesto;
import vistas.FrameModificarPuesto;
import vistas.MenuPuestos;

public class ControlFrameModificarPuesto implements ActionListener {

    private FrameModificarPuesto frameModificarPuesto;
    private int idPuesto;
    private JTable tablaPuestos;
    private String descripcionDelPuesto;

    public ControlFrameModificarPuesto(MenuPuestos menuPuestos,
            JTable tablaPuestos,
            int idPuesto,
            float sueldoBasico, String descripcionDelPuesto) throws ClassNotFoundException {

        frameModificarPuesto = new FrameModificarPuesto(menuPuestos, true);
        frameModificarPuesto.setControlador(this);
        this.idPuesto = idPuesto;
        this.tablaPuestos = tablaPuestos;
        this.descripcionDelPuesto = descripcionDelPuesto;
        UtilFrameModificarPuesto.cargarCamposDelPuesto(frameModificarPuesto, idPuesto, descripcionDelPuesto,  sueldoBasico);
        frameModificarPuesto.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarPuesto.BTN_ACTUALIZAR)) {
            try {
                UtilFrameModificarPuesto.modificarElPuesto(this.idPuesto, frameModificarPuesto.getTxtSueldoBasico());
                UtilFrameMenuPuestos.mostrarPuestosEnTabla(this.tablaPuestos);
                frameModificarPuesto.dispose();
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        if (e.getActionCommand().equals(frameModificarPuesto.BTN_CANCELAR)) {
            frameModificarPuesto.dispose();
        }
    }

}
