package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import persistencia.DBAuxiliares;
import vistas.FrameMotivoDePerdida;
import vistas.MenuPrincipal;

public class ControlFrameMotivoDePerdida implements ActionListener {

    private FrameMotivoDePerdida frameMotivoDePerdida;
    private ControlFrameNuevoMotivoDePerdida controlFrameNuevoMotivoDePerdida;
    private DBAuxiliares dbAuxiliares;

    public ControlFrameMotivoDePerdida(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameMotivoDePerdida = new FrameMotivoDePerdida(menuPrincipal, true);
        frameMotivoDePerdida.setControlador(this);
        mostrarMotivosEnTabla(frameMotivoDePerdida.getTablaMotivosDePerdida());
        frameMotivoDePerdida.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameMotivoDePerdida.BTN_NUEVO)) {
            controlFrameNuevoMotivoDePerdida = new ControlFrameNuevoMotivoDePerdida(frameMotivoDePerdida, 
                    this, frameMotivoDePerdida.getTablaMotivosDePerdida());
        }
        if (e.getActionCommand().equals(frameMotivoDePerdida.BTN_ELIMINAR)) {
            int i = frameMotivoDePerdida.getTablaMotivosDePerdida().getSelectedRow();
            if (i > -1) {
                String motivo = (String) frameMotivoDePerdida.getTablaMotivosDePerdida().getModel().getValueAt(i, 0);
                int op = JOptionPane.showConfirmDialog(null, "Esta seguro de borrar el motivo de salida: " + motivo + "?");
                if (op == 0) {
                    try {
                        dbAuxiliares = new DBAuxiliares();
                        dbAuxiliares.bajaDeMotivoDePerdida(motivo);
                        mostrarMotivosEnTabla(frameMotivoDePerdida.getTablaMotivosDePerdida());
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar alguno de los motivos.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(frameMotivoDePerdida.BTN_SALIR)) {
            frameMotivoDePerdida.dispose();
        }
    }

    public void mostrarMotivosEnTabla(JTable tablaMotivosDePerdida) throws SQLException, ClassNotFoundException {

        dbAuxiliares = new DBAuxiliares();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Motivo"};
        modelo.setColumnIdentifiers(titulos);
        tablaMotivosDePerdida.setModel(modelo);
        String registro[] = new String[1];

        ResultSet motivos = dbAuxiliares.resultSetMotivosDePerdida();

        while (motivos.next()) {
            registro[0] = motivos.getString("descripcionMotivo");
            modelo.addRow(registro);
        }
    }
}
