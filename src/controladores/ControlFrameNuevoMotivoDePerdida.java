package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import persistencia.DBAuxiliares;
import vistas.FrameMotivoDePerdida;
import vistas.FrameNuevoMotivoDePerdida;

public class ControlFrameNuevoMotivoDePerdida implements ActionListener {

    private FrameNuevoMotivoDePerdida frameNuevoMotivoDePerdida;
    private DBAuxiliares dbAuxiliares;
    private ControlFrameMotivoDePerdida controlFrameMotivoDePerdida;
    private JTable tablaMotivosDePerdida;

    public ControlFrameNuevoMotivoDePerdida(FrameMotivoDePerdida frameMotivoDePerdida,
            ControlFrameMotivoDePerdida controlFrameMotivoDePerdida,
            JTable tablaMotivosDePerdida) {
        frameNuevoMotivoDePerdida = new FrameNuevoMotivoDePerdida(frameMotivoDePerdida, true);
        frameNuevoMotivoDePerdida.setControlador(this);
        this.controlFrameMotivoDePerdida = controlFrameMotivoDePerdida;
        this.tablaMotivosDePerdida = tablaMotivosDePerdida;
        frameNuevoMotivoDePerdida.ejecutar();
    }

        @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoMotivoDePerdida.BTN_AGREGAR)) {
            try {
                registarMarca();
                controlFrameMotivoDePerdida.mostrarMotivosEnTabla(tablaMotivosDePerdida);
                frameNuevoMotivoDePerdida.dispose();
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoMotivoDePerdida.BTN_CANCELAR)) {
            frameNuevoMotivoDePerdida.dispose();
        }
    }

    public void registarMarca() throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();
        dbAuxiliares.altaDeMotivoDePerdida(frameNuevoMotivoDePerdida.getTxtDescripcion());
    }
}
