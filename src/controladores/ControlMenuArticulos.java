package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.DBArticulos;
import utilidadesParaFrames.UtilFrameMenuArticulos;
import vistas.MenuArticulos;
import vistas.MenuPrincipal;

public class ControlMenuArticulos implements ActionListener {

    private MenuArticulos menuArticulos;
    private ControlFrameNuevoArticulo controlFrameNuevoArticulo;
    private DBArticulos dbarticulos;
    private ControlFrameModificarArticulo controlFrameModificarArticulo;

    public ControlMenuArticulos(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        menuArticulos = new MenuArticulos(menuPrincipal, true, this);
        menuArticulos.setControlador(this);
        UtilFrameMenuArticulos.mostrarArticulosEnTabla(menuArticulos.getTablaArticulos());
        menuArticulos.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(menuArticulos.BTN_NUEVO)) {
            try {
                controlFrameNuevoArticulo = new ControlFrameNuevoArticulo(menuArticulos, this, menuArticulos.getTablaArticulos());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuArticulos.BTN_MODIFICAR)) {
            int i = menuArticulos.getTablaArticulos().getSelectedRow();
            if (i > -1) {
                int idArticulo = Integer.parseInt((String) menuArticulos.getTablaArticulos().getModel().getValueAt(i, 0));
                String marca = (String) menuArticulos.getTablaArticulos().getModel().getValueAt(i, 2);
                String rubro = (String) menuArticulos.getTablaArticulos().getModel().getValueAt(i, 3);
                try {
                    controlFrameModificarArticulo = new ControlFrameModificarArticulo(menuArticulos, idArticulo, marca,
                            rubro, menuArticulos.getTablaArticulos());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuArticulos.BTN_ELIMINAR)) {
            int i = menuArticulos.getTablaArticulos().getSelectedRow();
            if (i > -1) {
                int idArticulo = Integer.parseInt((String) menuArticulos.getTablaArticulos().getModel().getValueAt(i, 0));
                int op = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar el articulo?");
                if (op == 0) {
                    try {
                        dbarticulos = new DBArticulos();
                        dbarticulos.bajaDeArticulo(idArticulo);
                        UtilFrameMenuArticulos.mostrarArticulosEnTabla(menuArticulos.getTablaArticulos());
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuArticulos.BTN_BUSCAR)) {
            int codigo = UtilFrameMenuArticulos.getCodigoIngresado();
            if (codigo == -1) {
                try {
                    UtilFrameMenuArticulos.mostrarArticulosEnTabla(menuArticulos.getTablaArticulos());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                try {
                    UtilFrameMenuArticulos.mostrarArticuloXEnTabla(menuArticulos.getTablaArticulos(), codigo);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }

        }
        if (e.getActionCommand().equals(menuArticulos.BTN_SALIR)) {
            menuArticulos.dispose();
        }
    }
}
