package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import objetosDelDominio.Empleado;
import objetosDelDominio.Pariente;
import persistencia.DBAuxiliares;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameNuevoGrupoFamiliar;
import vistas.FrameNuevoEmpleado;
import vistas.FrameNuevoGrupoFamiliar;

public class ControlFrameNuevoGrupoFamiliar implements ActionListener {

    private FrameNuevoGrupoFamiliar frameNuevoGrupoFamiliar;
    private Pariente pariente;
    private Empleado empleado;
    private ControlFrameNuevoEmpleado controlFrameNuevoEmpleado;
    private DBAuxiliares dbAuxiliares;

    public ControlFrameNuevoGrupoFamiliar(FrameNuevoEmpleado frameNuevoEmpleado,
            ControlFrameNuevoEmpleado controlFrameNuevoEmpleado,
            Empleado empleado,
            boolean grupoFamiliarAgregado) throws SQLException, ClassNotFoundException {

        frameNuevoGrupoFamiliar = new FrameNuevoGrupoFamiliar(frameNuevoEmpleado, true);
        frameNuevoGrupoFamiliar.setControlador(this);
        this.controlFrameNuevoEmpleado = controlFrameNuevoEmpleado;
        this.empleado = empleado;

        if (grupoFamiliarAgregado == true) {
            frameNuevoGrupoFamiliar.mostrarBotonLimpiar();
            UtilFrameNuevoGrupoFamiliar.mostrarLosParientesCargados(empleado.getGrupoFamiliar(), frameNuevoGrupoFamiliar.getTablaParientes());
        } else {
            frameNuevoGrupoFamiliar.ocultarBotonLimpiar();
        }
        UtilAuxiliares.llenarComboParentescos(frameNuevoGrupoFamiliar.getComboParentesco());
        this.dbAuxiliares = new DBAuxiliares();
        frameNuevoGrupoFamiliar.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoGrupoFamiliar.BTN_AGREGAR)) {
            try {
                UtilFrameNuevoGrupoFamiliar.agregarPariente(cargarDatosEnElModelo(frameNuevoGrupoFamiliar, this.dbAuxiliares), frameNuevoGrupoFamiliar.getTablaParientes());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
            frameNuevoGrupoFamiliar.limpiarCampos();
        }
        if (e.getActionCommand().equals(frameNuevoGrupoFamiliar.BTN_ACEPTAR)) {
            UtilFrameNuevoGrupoFamiliar.setContadorDeParientes(0);
            controlFrameNuevoEmpleado.setEmpleado(empleado);
            controlFrameNuevoEmpleado.setGrupoFamiliarAgregado(true);
            frameNuevoGrupoFamiliar.dispose();
        }
        if (e.getActionCommand().equals(frameNuevoGrupoFamiliar.BTN_SUPRIMIR_FILA)) {
            int filaSeleccionada = UtilFrameNuevoGrupoFamiliar.getFilaSeleccionada(frameNuevoGrupoFamiliar);
            if (filaSeleccionada > -1) {
                int dniPariente = Integer.parseInt("" + frameNuevoGrupoFamiliar.getTablaParientes().getModel().getValueAt(filaSeleccionada, 0));
                removerDelArrayGrupoFamiliarUnPariente(empleado, dniPariente);
                UtilFrameNuevoGrupoFamiliar.eliminarUnItem(filaSeleccionada, frameNuevoGrupoFamiliar.getTablaParientes());
            }
            if (empleado.getGrupoFamiliar().size() == 0) {
                frameNuevoGrupoFamiliar.ocultarBotonLimpiar();
                controlFrameNuevoEmpleado.setGrupoFamiliarAgregado(false);
                controlFrameNuevoEmpleado.getFrameNuevoEmpleado().ocultarMsjGrupoFamiliarAgregado();
            }
        }
        if (e.getActionCommand().equals(frameNuevoGrupoFamiliar.BTN_LIMPIAR_ARRAY)) {
            limpiarElArrayGrupoFamiliar(empleado);
            controlFrameNuevoEmpleado.setGrupoFamiliarAgregado(false);
            JOptionPane.showMessageDialog(null, "Se quito el grupo familiar.");
            controlFrameNuevoEmpleado.getFrameNuevoEmpleado().ocultarMsjGrupoFamiliarAgregado();
            frameNuevoGrupoFamiliar.dispose();
        }
        if (e.getActionCommand().equals(frameNuevoGrupoFamiliar.BTN_CANCELAR)) {
            UtilFrameNuevoGrupoFamiliar.setContadorDeParientes(0);
            frameNuevoGrupoFamiliar.dispose();
        }
    }

    public Pariente cargarDatosEnElModelo(FrameNuevoGrupoFamiliar frameNuevoGrupoFamiliar, DBAuxiliares dbAuxiliares) throws SQLException, ClassNotFoundException {

        pariente = new Pariente(frameNuevoGrupoFamiliar.getTxtDni(),
                frameNuevoGrupoFamiliar.getTxtNombre(),
                frameNuevoGrupoFamiliar.getTxtApellido(),
                "" + frameNuevoGrupoFamiliar.getJdcFechaDeNac(),
                frameNuevoGrupoFamiliar.getParentescoSeleccionado(),
                dbAuxiliares.buscarParentescoPorDescripcion(frameNuevoGrupoFamiliar.getParentescoSeleccionado()));

        /*Se agrega el pariente en el array grupo familiar del empleado*/
        this.empleado.agregarPariente(pariente);

        /*Este mismo paciente es retornado para ser 
        mostrado en la tabla de parientes de la vista*/
        return pariente;
    }

    /*Este metodo permite limpiar el array de parientes
     del empleado en el caso de que el usuario presione 
    el boton "Quitar grupo familiar"*/
    public static void limpiarElArrayGrupoFamiliar(Empleado empleado) {
        empleado.getGrupoFamiliar().clear();
    }

    public static void removerDelArrayGrupoFamiliarUnPariente(Empleado empleado, int dniPariente) {
        int idx = 0;
        for (int i = 0; i < empleado.getGrupoFamiliar().size(); i++) {
            if (empleado.getGrupoFamiliar().get(i).getDni() == dniPariente) {
                idx = i;
            }
        }
        empleado.getGrupoFamiliar().remove(idx);
    }
}
