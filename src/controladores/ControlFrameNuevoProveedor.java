package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameMenuProveedores;
import utilidadesParaFrames.UtilFrameNuevoProveedor;
import vistas.FrameNuevoProveedor;
import vistas.MenuProveedores;

public class ControlFrameNuevoProveedor implements ActionListener {

    private FrameNuevoProveedor frameNuevoProveedor;
    private JTable tablaProveedores;

    public ControlFrameNuevoProveedor(MenuProveedores menuProveedores, JTable tablaProveedores) throws SQLException, ClassNotFoundException {
        frameNuevoProveedor = new FrameNuevoProveedor(menuProveedores, true);
        frameNuevoProveedor.setControlador(this);
        this.tablaProveedores = tablaProveedores;
        UtilAuxiliares.llenarComboRubro(frameNuevoProveedor.getComboRubros());
        UtilAuxiliares.llenarComboTipoDeCuit(frameNuevoProveedor.getComboCuit());
        frameNuevoProveedor.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoProveedor.BTN_GUARDAR)) {
            try {
                UtilFrameNuevoProveedor.registarProveedor(frameNuevoProveedor);
                frameNuevoProveedor.dispose();
                UtilFrameMenuProveedores.mostrarProveedoresEnTabla(tablaProveedores);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoProveedor.BTN_CANCELAR)) {
            frameNuevoProveedor.dispose();
        }
    }

}
