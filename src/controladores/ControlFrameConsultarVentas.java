package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import utilidadesParaFrames.UtilFrameConsultarVentas;
import vistas.FrameConsultarVentas;
import vistas.MenuPrincipal;

public class ControlFrameConsultarVentas implements ActionListener {

    private FrameConsultarVentas frameConsultarVentas;

    public ControlFrameConsultarVentas(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameConsultarVentas = new FrameConsultarVentas(menuPrincipal, true);
        frameConsultarVentas.setControlador(this);
        UtilFrameConsultarVentas.mostrarVentasEnTabla(frameConsultarVentas.getTablaVentas(), frameConsultarVentas.getTablaLineasDeVenta(), null, null, null, 0);
        frameConsultarVentas.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameConsultarVentas.BTN_BUSCAR_NRO)) {
            int nroFactura = UtilFrameConsultarVentas.getCodigoIngresado();
            if (nroFactura == -1) {
                try {
                    UtilFrameConsultarVentas.mostrarVentasEnTabla(frameConsultarVentas.getTablaVentas(),
                            frameConsultarVentas.getTablaLineasDeVenta(),
                            frameConsultarVentas.getJTextFieldNroFactura(),
                            frameConsultarVentas.getJTextFieldFechaVenta(),
                            frameConsultarVentas.getJTextFieldVendedor(),
                            nroFactura);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                try {
                    UtilFrameConsultarVentas.mostrarVentaXEnTabla(frameConsultarVentas.getTablaVentas(), frameConsultarVentas.getTablaLineasDeVenta(), frameConsultarVentas.getJTextFieldFechaVenta(), nroFactura);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        }

        if (e.getActionCommand().equals(frameConsultarVentas.BTN_SALIR)) {
            frameConsultarVentas.dispose();
        }
    }

}
