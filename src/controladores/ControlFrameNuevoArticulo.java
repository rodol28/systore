package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameMenuArticulos;
import utilidadesParaFrames.UtilFrameNuevoArticulo;
import vistas.FrameIngresoDeMercaderia;
import vistas.FrameNuevoArticulo;
import vistas.MenuArticulos;

public class ControlFrameNuevoArticulo implements ActionListener {

    private FrameNuevoArticulo frameNuevoArticulo;
    private ControlFrameMarcas controlFrameMarcas;
    private ControlFrameRubros controlFrameRubros;
    private ControlMenuArticulos controlMenuArticulos;
    private JTable tablaArticulos;
    private int frameActivo = -1; //indica con que contructor se creo el frame para saber si hay que mostrar o no la actulizacion de la tabla de articulos

    public ControlFrameNuevoArticulo(MenuArticulos menuArticulos,
            ControlMenuArticulos controlMenuArticulos, JTable tablaArticulos) throws SQLException, ClassNotFoundException {
        frameNuevoArticulo = new FrameNuevoArticulo(menuArticulos, true);
        frameNuevoArticulo.setControlador(this);
        this.controlMenuArticulos = controlMenuArticulos;
        this.tablaArticulos = tablaArticulos;
        UtilFrameNuevoArticulo.mostraElCodigoDelNuevoArticulo(frameNuevoArticulo);
        UtilAuxiliares.llenarComboMarca(frameNuevoArticulo.getComboMarca());
        UtilAuxiliares.llenarComboRubro(frameNuevoArticulo.getComboRubro());
        frameActivo = 0;
        frameNuevoArticulo.ejecutar();
    }

    public ControlFrameNuevoArticulo(FrameIngresoDeMercaderia frameIngresoDeMercaderia) throws SQLException, ClassNotFoundException {
        frameNuevoArticulo = new FrameNuevoArticulo(frameIngresoDeMercaderia, true);
        frameNuevoArticulo.setControlador(this);
        UtilFrameNuevoArticulo.mostraElCodigoDelNuevoArticulo(frameNuevoArticulo);
        UtilAuxiliares.llenarComboMarca(frameNuevoArticulo.getComboMarca());
        UtilAuxiliares.llenarComboRubro(frameNuevoArticulo.getComboRubro());
        frameActivo = 1;
        frameNuevoArticulo.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoArticulo.BTN_NUEVO_RUBRO)) {
            try {
                controlFrameRubros = new ControlFrameRubros(frameNuevoArticulo, this, frameNuevoArticulo.getComboRubro());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoArticulo.BTN_NUEVA_MARCA)) {
            try {
                controlFrameMarcas = new ControlFrameMarcas(frameNuevoArticulo, this, frameNuevoArticulo.getComboMarca());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoArticulo.BTN_GUARDAR)) {
            try {
                UtilFrameNuevoArticulo.registarElArticulo(frameNuevoArticulo);
                frameNuevoArticulo.limpiarPantalla();
                UtilFrameMenuArticulos.mostrarArticulosEnTabla(this.tablaArticulos);       
                if (frameActivo == 1) {
                    frameNuevoArticulo.dispose();
                }
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoArticulo.BTN_CANCELAR)) {
            if (frameActivo == 0) {
                frameNuevoArticulo.dispose();
                try {
                    UtilFrameMenuArticulos.mostrarArticulosEnTabla(tablaArticulos);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else if (frameActivo == 1) {
                frameNuevoArticulo.dispose();
            }
        }
    }
}
