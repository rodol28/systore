package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import objetosDelDominio.Concepto;
import objetosDelDominio.TipoDeConcepto;
import utilidadesParaFrames.UtilFrameMenuConceptos;
import utilidadesParaFrames.UtilFrameModificarConcepto;
import vistas.FrameModificarConcepto;
import vistas.MenuConceptos;

public class ControlFrameModificarConcepto implements ActionListener {

    private FrameModificarConcepto frameModificarConcepto;
    private int idConcepto;
    private JTable tablaConceptos;
    private Concepto concepto;

    public ControlFrameModificarConcepto(MenuConceptos menuConceptos, JTable tablaConceptos, int idConcepto) throws SQLException, ClassNotFoundException {
        frameModificarConcepto = new FrameModificarConcepto(menuConceptos, true);
        frameModificarConcepto.setControlador(this);
        this.idConcepto = idConcepto;
        this.tablaConceptos = tablaConceptos;
        UtilFrameModificarConcepto.cargarLosCampos(frameModificarConcepto, idConcepto);
        this.concepto = new Concepto(idConcepto);
        frameModificarConcepto.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarConcepto.BTN_ACTUALIZAR)) {
            deLaVistaAlConcepto();
            try {
                UtilFrameModificarConcepto.actualizarConcepto(this.concepto);
                UtilFrameMenuConceptos.mostrarConceptosEnTabla(this.tablaConceptos);
                this.frameModificarConcepto.dispose();
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {

            }
        }
        if (e.getActionCommand().equals(frameModificarConcepto.BTN_CANCELAR)) {
            this.frameModificarConcepto.dispose();
        }
    }

    public void deLaVistaAlConcepto() {
        this.concepto.setAbreviatura(frameModificarConcepto.getTxtAbrev());
        this.concepto.setDescripcion(frameModificarConcepto.getTxtDescripcion());

        if (frameModificarConcepto.getCheckHaber()) {
            this.concepto.setTipoDeConcepto("Haber");
            this.concepto.setCantidadFija(frameModificarConcepto.getTxtFijohaber());
            this.concepto.setCantidadPorcentual(frameModificarConcepto.getTxtPorcHaber());
        }

        if (frameModificarConcepto.getCheckRetencion()) {
            this.concepto.setTipoDeConcepto("Retención");
            this.concepto.setCantidadFija(frameModificarConcepto.getTxtFijoRetencion());
            this.concepto.setCantidadPorcentual(frameModificarConcepto.getTxtPorcRetencion());
        }

        this.concepto.setEsRemunerativo(frameModificarConcepto.getCheckHaberRemunerativo());
    }
}
