package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import utilidadesParaFrames.UtilFrameMenuUsuarios;
import utilidadesParaFrames.UtilFrameNuevoUsuario;
import vistas.FrameNuevoUsuario;
import vistas.MenuUsuarios;

public class ControlFrameNuevoUsuario implements ActionListener {

    private FrameNuevoUsuario frameNuevoUsuario;
    private MenuUsuarios menuUsuarios;

    public ControlFrameNuevoUsuario(MenuUsuarios menuUsuarios) throws SQLException, ClassNotFoundException {
        frameNuevoUsuario = new FrameNuevoUsuario(menuUsuarios, true);
        frameNuevoUsuario.setControlador(this);
        this.menuUsuarios = menuUsuarios;
        UtilFrameNuevoUsuario.llenarComboTipoDeUsuario(frameNuevoUsuario.getComboTipoDeUsuario());
        UtilFrameNuevoUsuario.llenarElComboEmpleados(frameNuevoUsuario.getComboEmpleado());
        frameNuevoUsuario.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoUsuario.BTN_ACEPTAR)) {
            try {
                if (UtilFrameNuevoUsuario.registrarUsuario(frameNuevoUsuario)) {
                    frameNuevoUsuario.dispose();
                    UtilFrameMenuUsuarios.mostrarUsuarriosEnTabla(this.menuUsuarios.getTablaListaDeUsuarios());
                }
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            } catch (Exception ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoUsuario.BTN_SALIR)) {
            frameNuevoUsuario.dispose();
        }
    }
}
