package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import utilidadesParaFrames.UtilFrameModificarUsuario;
import vistas.FrameModificarUsuario;
import vistas.MenuUsuarios;

public class ControlFrameModificarUsuario implements ActionListener {

    private FrameModificarUsuario frameModificarUsuario;

    public ControlFrameModificarUsuario(MenuUsuarios menuUsuarios, String nombreDeUsuario) {
        frameModificarUsuario = new FrameModificarUsuario(menuUsuarios, true);
        frameModificarUsuario.setControlador(this);
        frameModificarUsuario.setTxtNombre(nombreDeUsuario);
        frameModificarUsuario.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarUsuario.BTN_ACTUALIZAR)) {
            try {
                if (UtilFrameModificarUsuario.actualizarElUsuario(frameModificarUsuario.getTxtNombre(), frameModificarUsuario.getTxtClave(), frameModificarUsuario.getTxtRepetirClave())) {
                    frameModificarUsuario.dispose();
                }
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            } catch (Exception ex) {
            }
        }
        if (e.getActionCommand().equals(frameModificarUsuario.BTN_SALIR)) {
            frameModificarUsuario.dispose();
        }
    }

}
