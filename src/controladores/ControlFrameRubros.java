package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import persistencia.DBAuxiliares;
import utilidadesParaFrames.UtilAuxiliares;
import vistas.FrameNuevoArticulo;
import vistas.FrameRubros;
import vistas.MenuPrincipal;

public class ControlFrameRubros implements ActionListener {

    private FrameRubros frameRubros;
    private DBAuxiliares dbAuxiliares;
    private ControlFrameNuevoRubro controlFrameNuevoRubro;
    private ControlFrameNuevoArticulo controlFrameNuevoArticulo;
    private JComboBox comboRubros;
    /*Variable que indica desde que frame se creo la pantalla de Rubros. Permite determinar
    si se debe actualizar o no el combo de rubros*/
    private boolean frameOrigen = false;

    public ControlFrameRubros(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameRubros = new FrameRubros(menuPrincipal, true);
        frameRubros.setControlador(this);
        mostrarRubrosEnTabla(frameRubros.getTablaRubros());
        frameOrigen = false;
        frameRubros.ejecutar();
    }

    ControlFrameRubros(FrameNuevoArticulo frameNuevoArticulo,
            ControlFrameNuevoArticulo controlFrameNuevoArticulo, JComboBox comboRubros) throws SQLException, ClassNotFoundException {
        frameRubros = new FrameRubros(frameNuevoArticulo, true);
        frameRubros.setControlador(this);
        this.controlFrameNuevoArticulo = controlFrameNuevoArticulo;
        this.comboRubros = comboRubros;
        mostrarRubrosEnTabla(frameRubros.getTablaRubros());
        frameOrigen = true;
        frameRubros.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameRubros.BTN_NUEVO)) {
            controlFrameNuevoRubro = new ControlFrameNuevoRubro(frameRubros, this, frameRubros.getTablaRubros());
        }
        if (e.getActionCommand().equals(frameRubros.BTN_ELIMINAR)) {
            int i = frameRubros.getTablaRubros().getSelectedRow();
            if (i > -1) {
                String rubro = (String) frameRubros.getTablaRubros().getModel().getValueAt(i, 0);
                int op = JOptionPane.showConfirmDialog(null, "Esta seguro de borrar el rubro" + rubro + "?");
                if (op == 0) {
                    try {
                        dbAuxiliares = new DBAuxiliares();
                        dbAuxiliares.bajaDeRubro(rubro);
                        mostrarRubrosEnTabla(frameRubros.getTablaRubros());
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar una marca", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(frameRubros.BTN_SALIR)) {
            frameRubros.dispose();
            if (frameOrigen) {
                try {
                    UtilAuxiliares.llenarComboRubro(comboRubros);
                    frameOrigen = false;
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        }
    }

    public void mostrarRubrosEnTabla(JTable tablaRubros) throws SQLException, ClassNotFoundException {

        dbAuxiliares = new DBAuxiliares();

        DefaultTableModel modelo = new DefaultTableModel();

        String titulos[] = {"Rubro"};
        modelo.setColumnIdentifiers(titulos);
        tablaRubros.setModel(modelo);
        String registro[] = new String[1];

        ResultSet rubros = dbAuxiliares.resultSetRubros();

        while (rubros.next()) {
            registro[0] = rubros.getString("descripcion");
            modelo.addRow(registro);
        }
    }
}
