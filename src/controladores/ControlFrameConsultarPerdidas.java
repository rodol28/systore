package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import utilidadesParaFrames.UtilFrameConsultarMotivoDePerdida;
import vistas.FrameConsultarPerdidas;
import vistas.MenuPrincipal;

public class ControlFrameConsultarPerdidas implements ActionListener {

    private FrameConsultarPerdidas frameConsultarPerdidas;

    public ControlFrameConsultarPerdidas(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameConsultarPerdidas = new FrameConsultarPerdidas(menuPrincipal, true);
        frameConsultarPerdidas.setContralador(this);
        UtilFrameConsultarMotivoDePerdida.mostrarPerdidasEnTabla(frameConsultarPerdidas.getTablaPerdidas(),
                frameConsultarPerdidas.getTablaLineasDePerdida(), null, null, 0);
        frameConsultarPerdidas.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameConsultarPerdidas.BTN_BUSCAR_POR_NRO_PERDIDA)) {
            int nroPerdida = UtilFrameConsultarMotivoDePerdida.getCodigoIngresado();
            if (nroPerdida == -1) {
                try {
                    UtilFrameConsultarMotivoDePerdida.mostrarPerdidasEnTabla(frameConsultarPerdidas.getTablaPerdidas(),
                            frameConsultarPerdidas.getTablaLineasDePerdida(),
                            frameConsultarPerdidas.getJTextFieldNroPerdida(),
                            frameConsultarPerdidas.getJTextFieldFecha(),
                            nroPerdida);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                try {
                    UtilFrameConsultarMotivoDePerdida.mostrarPerdidaNroXEnTabla(frameConsultarPerdidas.getTablaPerdidas(), frameConsultarPerdidas.getTablaLineasDePerdida(), frameConsultarPerdidas.getJTextFieldFecha(), nroPerdida);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        }
        if (e.getActionCommand().equals(frameConsultarPerdidas.BTN_SALIR)) {
            frameConsultarPerdidas.dispose();
        }
    }
}
