package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import utilidadesParaFrames.UtilFrameMenuArticulos;
import utilidadesParaFrames.UtilFrameModificarArticulo;
import vistas.FrameModificarArticulo;
import vistas.MenuArticulos;

public class ControlFrameModificarArticulo implements ActionListener {

    private FrameModificarArticulo frameModificarArticulo;
    private JTable tablaArticulos;

    public ControlFrameModificarArticulo(MenuArticulos menuArticulos, int idArticulo,
            String marca, String rubro, JTable tablaArticulos) throws SQLException, ClassNotFoundException {
        frameModificarArticulo = new FrameModificarArticulo(menuArticulos, true);
        frameModificarArticulo.setControlador(this);
        this.tablaArticulos = tablaArticulos;
        UtilFrameModificarArticulo.cargarLosCampos(frameModificarArticulo, marca, rubro, idArticulo);
        frameModificarArticulo.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarArticulo.BTN_ACTUALIZAR)) {
            try {
                UtilFrameModificarArticulo.actualizarElArticulo(frameModificarArticulo);
                frameModificarArticulo.dispose();
                UtilFrameMenuArticulos.mostrarArticulosEnTabla(tablaArticulos);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameModificarArticulo.BTN_CANCELAR)) {
            frameModificarArticulo.dispose();
        }
    }
}
