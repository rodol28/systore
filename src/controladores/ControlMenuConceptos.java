package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.DBConceptos;
import utilidadesParaFrames.UtilFrameMenuConceptos;
import vistas.MenuConceptos;
import vistas.MenuPrincipal;

public class ControlMenuConceptos implements ActionListener {

    private MenuConceptos menuConceptos;
    private ControlFrameNuevoConcepto controlFrameNuevoConcepto;
    private ControlFrameModificarConcepto controlFrameModificarConcepto;
    private DBConceptos dbConceptos;

    public ControlMenuConceptos(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        menuConceptos = new MenuConceptos(menuPrincipal, true);
        menuConceptos.setControlador(this);
        UtilFrameMenuConceptos.mostrarConceptosEnTabla(menuConceptos.getTablaConceptos());
        menuConceptos.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(menuConceptos.BTN_NUEVO)) {
            try {
                controlFrameNuevoConcepto = new ControlFrameNuevoConcepto(menuConceptos);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuConceptos.BTN_MODIFICAR)) {
            int i = menuConceptos.getTablaConceptos().getSelectedRow();
            if (i > -1) {
                int idConcepto = Integer.parseInt((String) menuConceptos.getTablaConceptos().getModel().getValueAt(i, 0));
                try {
                    controlFrameModificarConcepto = new ControlFrameModificarConcepto(menuConceptos, menuConceptos.getTablaConceptos(), idConcepto);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un concepto.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuConceptos.BTN_ELIMINAR)) {
            int i = menuConceptos.getTablaConceptos().getSelectedRow();
            if (i > -1) {
                int idConcepto = Integer.parseInt((String) menuConceptos.getTablaConceptos().getModel().getValueAt(i, 0));
                if (idConcepto == 0 || idConcepto == 1 || idConcepto == 2
                        || idConcepto == 3 || idConcepto == 4 || idConcepto == 5
                        || idConcepto == 6 || idConcepto == 7 || idConcepto == 8
                        || idConcepto == 9 || idConcepto == 10 || idConcepto == 11
                        || idConcepto == 12) {
                    JOptionPane.showMessageDialog(null, "No esta permitido eliminar este concepto.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    int op = JOptionPane.showConfirmDialog(null, "Esta seguro de dar de baja el concepto?");
                    if (op == 0) {
                        try {
                            dbConceptos = new DBConceptos();
                            dbConceptos.bajaDelConcepto(idConcepto);
                            UtilFrameMenuConceptos.mostrarConceptosEnTabla(menuConceptos.getTablaConceptos());
                        } catch (SQLException ex) {
                        } catch (ClassNotFoundException ex) {
                        }
                    }
                }

            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un concepto.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuConceptos.BTN_SALIR)) {
            menuConceptos.dispose();
        }
    }
}
