package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import objetosDelDominio.Concepto;
import objetosDelDominio.TipoDeConcepto;
import utilidadesParaFrames.UtilFrameMenuConceptos;
import utilidadesParaFrames.UtilFrameNuevoConcepto;
import vistas.FrameNuevoConcepto;
import vistas.MenuConceptos;

public class ControlFrameNuevoConcepto implements ActionListener {

    private FrameNuevoConcepto frameNuevoConcepto;
    private Concepto concepto;
    private MenuConceptos menuConceptos;

    public ControlFrameNuevoConcepto(MenuConceptos menuConceptos) throws SQLException, ClassNotFoundException {
        this.frameNuevoConcepto = new FrameNuevoConcepto(menuConceptos, true);
        this.frameNuevoConcepto.setControlador(this);

        UtilFrameNuevoConcepto.mostraElIdNuevo(frameNuevoConcepto.getJTextFieldTxtCodigo());

        this.concepto = new Concepto(frameNuevoConcepto.getTxtCodigo());

        this.menuConceptos = menuConceptos;

        this.frameNuevoConcepto.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoConcepto.BTN_AGREGAR)) {
            deLaVistaAlConcepto();
            try {
                UtilFrameNuevoConcepto.registrarConcepto(this.concepto);
                this.frameNuevoConcepto.dispose();
                UtilFrameMenuConceptos.mostrarConceptosEnTabla(this.menuConceptos.getTablaConceptos());
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoConcepto.BTN_CANCELAR)) {
            this.frameNuevoConcepto.dispose();
        }
    }

    public void deLaVistaAlConcepto() {
        this.concepto.setAbreviatura(frameNuevoConcepto.getTxtAbrev());
        this.concepto.setDescripcion(frameNuevoConcepto.getTxtDescripcion());

        if (frameNuevoConcepto.getCheckHaber()) {
            this.concepto.setTipoDeConcepto("Haber");
            this.concepto.setCantidadFija(frameNuevoConcepto.getTxtFijohaber());
            this.concepto.setCantidadPorcentual(frameNuevoConcepto.getTxtPorcHaber());
        }

        if (frameNuevoConcepto.getCheckRetencion()) {
            this.concepto.setTipoDeConcepto("Retención");
            this.concepto.setCantidadFija(frameNuevoConcepto.getTxtFijoRetencion());
            this.concepto.setCantidadPorcentual(frameNuevoConcepto.getTxtPorcRetencion());
        }

        this.concepto.setEsRemunerativo(frameNuevoConcepto.getCheckHaberRemunerativo());
    }
}
