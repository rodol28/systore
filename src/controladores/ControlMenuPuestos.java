package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.DBPuestos;
import utilidadesParaFrames.UtilFrameMenuPuestos;
import vistas.MenuPrincipal;
import vistas.MenuPuestos;

public class ControlMenuPuestos implements ActionListener {

    private MenuPuestos menuPuestos;
    private ControlFrameNuevoPuesto controlFrameNuevoPuesto;
    private ControlFrameModificarPuesto controlFrameModificarPuesto;
    private DBPuestos dbPuestos;

    public ControlMenuPuestos(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        menuPuestos = new MenuPuestos(menuPrincipal, true);
        menuPuestos.setControlador(this);
        UtilFrameMenuPuestos.mostrarPuestosEnTabla(menuPuestos.getTablaPuestos());
        menuPuestos.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(menuPuestos.BTN_AGREGAR)) {
            controlFrameNuevoPuesto = new ControlFrameNuevoPuesto(menuPuestos, menuPuestos.getTablaPuestos());
        }
        if (e.getActionCommand().equals(menuPuestos.BTN_MODIFICAR)) {
            int i = menuPuestos.getTablaPuestos().getSelectedRow();
            if (i > -1) {

                int idPuesto = Integer.parseInt((String) menuPuestos.getTablaPuestos().getModel().getValueAt(i, 0));
                String descripcionDelPuesto = (String) menuPuestos.getTablaPuestos().getModel().getValueAt(i, 1);
                float sueldoBasico = Float.parseFloat((String) menuPuestos.getTablaPuestos().getModel().getValueAt(i, 2));

                try {
                    controlFrameModificarPuesto = new ControlFrameModificarPuesto(menuPuestos,
                            menuPuestos.getTablaPuestos(),
                            idPuesto,
                            sueldoBasico,
                            descripcionDelPuesto);

                } catch (ClassNotFoundException ex) {
                }

            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un puesto", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuPuestos.BTN_ELIMINAR)) {
            int i = menuPuestos.getTablaPuestos().getSelectedRow();
            if (i > -1) {
                int idPuesto = Integer.parseInt((String) menuPuestos.getTablaPuestos().getModel().getValueAt(i, 0));
//                if (idPuesto == 23 || idPuesto == 9 || idPuesto == 15) {
//                    JOptionPane.showMessageDialog(null, "No esta permitido eliminar los puesto de Administrador o Vendedor.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
//                } else {
                    int op = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar el puesto?");
                    if (op == 0) {
                        try {
                            dbPuestos = new DBPuestos();
                            dbPuestos.bajaDePuesto(idPuesto);
                            UtilFrameMenuPuestos.mostrarPuestosEnTabla(menuPuestos.getTablaPuestos());
                        } catch (SQLException ex) {
                        } catch (ClassNotFoundException ex) {
                        }
                    }
//                }

            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un puesto.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuPuestos.BTN_CANCELAR)) {
            menuPuestos.dispose();
        }
    }
}
