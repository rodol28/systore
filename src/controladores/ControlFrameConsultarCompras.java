package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import utilidadesParaFrames.UtilFrameConsultarCompras;
import vistas.FrameConsultarCompras;
import vistas.MenuPrincipal;

public class ControlFrameConsultarCompras implements ActionListener {

    private FrameConsultarCompras frameConsultarCompras;

    public ControlFrameConsultarCompras(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        frameConsultarCompras = new FrameConsultarCompras(menuPrincipal, true);
        frameConsultarCompras.setControlador(this);
        UtilFrameConsultarCompras.mostrarComprasEnTabla(frameConsultarCompras.getTablaCompras(), frameConsultarCompras.getTablaLineasDeCompras(), null, null, 0);
        frameConsultarCompras.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameConsultarCompras.MENU_BTN_BUSCAR_POR_FAC)) {
            int nroFactura = UtilFrameConsultarCompras.getCodigoIngresado();
            if (nroFactura == -1) {
                try {
                    UtilFrameConsultarCompras.mostrarComprasEnTabla(frameConsultarCompras.getTablaCompras(),
                            frameConsultarCompras.getTablaLineasDeCompras(),
                            frameConsultarCompras.getJTextFieldFactura(),
                            frameConsultarCompras.getJTextFieldFecha(),
                            nroFactura);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                try {
                    UtilFrameConsultarCompras.mostrarCompraNroXEnTabla(frameConsultarCompras.getTablaCompras(), frameConsultarCompras.getTablaLineasDeCompras(), frameConsultarCompras.getJTextFieldFecha(), nroFactura);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        }
        if (e.getActionCommand().equals(frameConsultarCompras.BTN_SALIR)) {
            frameConsultarCompras.dispose();
        }
    }
}
