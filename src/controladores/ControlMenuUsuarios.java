package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.DBUsuarios;
import utilidadesParaFrames.UtilFrameMenuUsuarios;
import vistas.MenuPrincipal;
import vistas.MenuUsuarios;

public class ControlMenuUsuarios implements ActionListener {

    private MenuUsuarios menuUsuarios;
    private ControlFrameNuevoUsuario controlFrameNuevoUsuario;
    private ControlFrameModificarUsuario controlFrameModificarUsuario;
    private DBUsuarios dbUsuarios;

    public ControlMenuUsuarios(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        menuUsuarios = new MenuUsuarios(menuPrincipal, true);
        menuUsuarios.setControlador(this);
        UtilFrameMenuUsuarios.mostrarUsuarriosEnTabla(menuUsuarios.getTablaListaDeUsuarios());
        menuUsuarios.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(menuUsuarios.BTN_NUEVO)) {
            try {
                controlFrameNuevoUsuario = new ControlFrameNuevoUsuario(menuUsuarios);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuUsuarios.BTN_MODIFICAR)) {

            int i = menuUsuarios.getTablaListaDeUsuarios().getSelectedRow();
            if (i > -1) {
                String nombreDeUsuario = (String) menuUsuarios.getTablaListaDeUsuarios().getModel().getValueAt(i, 2);
                controlFrameModificarUsuario = new ControlFrameModificarUsuario(menuUsuarios, nombreDeUsuario);
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuUsuarios.BTN_ELIMINAR)) {
            int i = menuUsuarios.getTablaListaDeUsuarios().getSelectedRow();
            if (i > -1) {
                String nombreDeUsuario = (String) menuUsuarios.getTablaListaDeUsuarios().getModel().getValueAt(i, 2);
                int op = JOptionPane.showConfirmDialog(null, "Esta seguro de dar de baja el usuario?");
                if (op == 0) {
                    try {
                        dbUsuarios = new DBUsuarios();
                        dbUsuarios.bajaDelusuario(nombreDeUsuario);
                        UtilFrameMenuUsuarios.mostrarUsuarriosEnTabla(menuUsuarios.getTablaListaDeUsuarios());
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuUsuarios.BTN_SALIR)) {
            menuUsuarios.dispose();
        }
    }

}
