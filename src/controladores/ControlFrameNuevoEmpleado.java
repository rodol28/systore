package controladores;

import controladoresAuxiliares.ControlFrameElegirObraSocial;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JTable;
import objetosDelDominio.Empleado;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameMenuEmpleados;
import utilidadesParaFrames.UtilFrameNuevoEmpleado;
import vistas.FrameNuevoEmpleado;
import vistas.MenuEmpleados;

public class ControlFrameNuevoEmpleado implements ActionListener {

    private FrameNuevoEmpleado frameNuevoEmpleado;
    private ControlFrameNuevoGrupoFamiliar controlFrameNuevoGrupoFamiliar;
    private ControlFrameElegirObraSocial controlFrameElegirObraSocial;
    private Empleado empleado;
    private boolean grupoFamiliarAgregado = false;
    private JTable tablaEmpleados;

    public ControlFrameNuevoEmpleado(MenuEmpleados menuEmpleados, JTable tablaEmpleados) throws SQLException, ClassNotFoundException {
        frameNuevoEmpleado = new FrameNuevoEmpleado(menuEmpleados, true);
        frameNuevoEmpleado.setControlador(this);
        this.tablaEmpleados = tablaEmpleados;

        UtilFrameNuevoEmpleado.mostraElLegajoNuevo(frameNuevoEmpleado.getJTextFieldLegajo());
        UtilAuxiliares.llenarComboPuestosNuevoEmpleado(frameNuevoEmpleado.getComboPuestos());
        UtilAuxiliares.llenarComboEstadosCivil(frameNuevoEmpleado.getComboEstadoCivil());
        UtilAuxiliares.llenarComboObrasSociales(frameNuevoEmpleado.getComboObraSocial());
        this.empleado = new Empleado(frameNuevoEmpleado.getTxtLegajo());
        frameNuevoEmpleado.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoEmpleado.BTN_AGREGAR_PARIENTES)) {

            try {
                controlFrameNuevoGrupoFamiliar = new ControlFrameNuevoGrupoFamiliar(frameNuevoEmpleado,
                        this,
                        this.empleado,
                        this.grupoFamiliarAgregado);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }

            if (grupoFamiliarAgregado) {
                frameNuevoEmpleado.mostrarMsjGrupoFamiliarAgregado();
            }
        }
        if (e.getActionCommand().equals(frameNuevoEmpleado.BTN_ACEPTAR)) {
            try {
                UtilFrameNuevoEmpleado.registrarEmpleado(this.empleado, frameNuevoEmpleado);
                frameNuevoEmpleado.dispose();
                UtilFrameMenuEmpleados.mostrarEmpleadosEnTabla(this.tablaEmpleados, false);
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoEmpleado.BTN_AGREGAR_OBRAS_SOCIALES)) {
//            try {
//                controlFrameElegirObraSocial = new ControlFrameElegirObraSocial(frameNuevoEmpleado, this.empleado, this);
//            } catch (SQLException ex) {
//            } catch (ClassNotFoundException ex) {
//            }
        }
        if (e.getActionCommand().equals(frameNuevoEmpleado.BTN_CANCELAR)) {
            frameNuevoEmpleado.dispose();
        }
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public void setGrupoFamiliarAgregado(boolean grupoFamiliarAgregado) {
        this.grupoFamiliarAgregado = grupoFamiliarAgregado;
    }

    public FrameNuevoEmpleado getFrameNuevoEmpleado() {
        return this.frameNuevoEmpleado;
    }

    public void mostrarObrasSocialesCargadas(ArrayList<String> obrasSociales) {
        for (int i = 0; i < obrasSociales.size(); i++) {
            System.out.println("Razon social: " + obrasSociales.get(i));
        }
    }
}
