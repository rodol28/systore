package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import persistencia.DBAuxiliares;
import vistas.FrameNuevaObraSocial;
import vistas.FrameObraSocial;

public class ControlFrameNuevaObraSocial implements ActionListener {

    private FrameNuevaObraSocial frameNuevaObraSocial;
    private DBAuxiliares dbAuxiliares;
    private ControlFrameObraSocial controlFrameObraSocial;
    private JTable tablaObrasSociales;

    public ControlFrameNuevaObraSocial(FrameObraSocial frameObraSocial,
            ControlFrameObraSocial controlFrameObraSocial,
            JTable tablaObrasSociales) {
        frameNuevaObraSocial = new FrameNuevaObraSocial(frameObraSocial, true);
        this.controlFrameObraSocial = controlFrameObraSocial;
        this.tablaObrasSociales = tablaObrasSociales;
        frameNuevaObraSocial.setControlador(this);
        frameNuevaObraSocial.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevaObraSocial.BTN_AGREGAR)) {
            try {
                registarObraSocial();
                controlFrameObraSocial.mostrarObrasSocialesEnTabla(tablaObrasSociales);
                frameNuevaObraSocial.dispose();
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevaObraSocial.BTN_CANCELAR)) {
            frameNuevaObraSocial.dispose();
        }
    }

    public void registarObraSocial() throws SQLException, ClassNotFoundException {
        dbAuxiliares = new DBAuxiliares();
        dbAuxiliares.altaDeObraSocial(frameNuevaObraSocial.getTxtRazonSocial());
    }
}
