package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import persistencia.DBProveedores;
import utilidadesParaFrames.UtilFrameMenuProveedores;
import vistas.MenuPrincipal;
import vistas.MenuProveedores;

public class ControlMenuProveedores implements ActionListener {

    private MenuProveedores menuProveedores;
    private ControlFrameNuevoProveedor controlFrameNuevoProveedor;
    private ControlFrameModificarProveedor controlFrameModificarProveedor;
    private DBProveedores dbProveedores;

    public ControlMenuProveedores(MenuPrincipal menuPrincipal) throws SQLException, ClassNotFoundException {
        menuProveedores = new MenuProveedores(menuPrincipal, true);
        menuProveedores.setControlador(this);
        UtilFrameMenuProveedores.mostrarProveedoresEnTabla(menuProveedores.getTablaProveedores());
        menuProveedores.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(menuProveedores.BTN_NUEVO)) {
            try {
                controlFrameNuevoProveedor = new ControlFrameNuevoProveedor(menuProveedores, menuProveedores.getTablaProveedores());
            } catch (SQLException ex) {
            } catch (ClassNotFoundException ex) {
            }
        }
        if (e.getActionCommand().equals(menuProveedores.BTN_MODIFICAR)) {
            int i = menuProveedores.getTablaProveedores().getSelectedRow();
            if (i > -1) {
                int idProveedor = Integer.parseInt((String) menuProveedores.getTablaProveedores().getModel().getValueAt(i, 0));
                String rubro = (String) menuProveedores.getTablaProveedores().getModel().getValueAt(i, 6);
                String provincia = (String) menuProveedores.getTablaProveedores().getModel().getValueAt(i, 9);
                try {
                    controlFrameModificarProveedor = new ControlFrameModificarProveedor(menuProveedores, provincia, rubro, idProveedor, menuProveedores.getTablaProveedores());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un proveedor.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuProveedores.BTN_ELIMINAR)) {
            int i = menuProveedores.getTablaProveedores().getSelectedRow();
            if (i > -1) {
                int idProveedor = Integer.parseInt((String) menuProveedores.getTablaProveedores().getModel().getValueAt(i, 0));
                int op = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar el proveedor?");
                if (op == 0) {
                    try {
                        dbProveedores = new DBProveedores();
                        dbProveedores.bajaDeProveedor(idProveedor);
                        UtilFrameMenuProveedores.mostrarProveedoresEnTabla(menuProveedores.getTablaProveedores());
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccionar un proveedor.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getActionCommand().equals(menuProveedores.BTN_BUSCAR)) {
            int nroProveedor = UtilFrameMenuProveedores.getCodigoIngresado();
            if (nroProveedor == -1) {
                try {
                    UtilFrameMenuProveedores.mostrarProveedoresEnTabla(menuProveedores.getTablaProveedores());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            } else {
                try {
                    UtilFrameMenuProveedores.mostrarElProveedorX(menuProveedores.getTablaProveedores(), nroProveedor);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        }
        if (e.getActionCommand().equals(menuProveedores.BTN_SALIR)) {
            menuProveedores.dispose();
        }
    }

}
