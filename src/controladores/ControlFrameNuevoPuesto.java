package controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JTable;
import utilidadesParaFrames.UtilFrameMenuPuestos;
import utilidadesParaFrames.UtilFrameNuevoPuesto;
import vistas.FrameNuevoPuesto;
import vistas.MenuPuestos;

public class ControlFrameNuevoPuesto implements ActionListener {

    private FrameNuevoPuesto frameNuevoPuesto;
    private JTable tablaPuestos;

    public ControlFrameNuevoPuesto(MenuPuestos menuPuestos, JTable tablaPuestos) {
        frameNuevoPuesto = new FrameNuevoPuesto(menuPuestos, true);
        frameNuevoPuesto.setControlador(this);
        this.tablaPuestos = tablaPuestos;
        frameNuevoPuesto.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameNuevoPuesto.BTN_AGREGAR)) {
            try {
                UtilFrameNuevoPuesto.registrarPuesto(frameNuevoPuesto);
                UtilFrameMenuPuestos.mostrarPuestosEnTabla(tablaPuestos);
                frameNuevoPuesto.dispose();
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        if (e.getActionCommand().equals(frameNuevoPuesto.BTN_CANCELAR)) {
            frameNuevoPuesto.dispose();
        }
    }
}
