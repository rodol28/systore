package controladoresAuxiliares;

import controladores.ControlFrameIngresoDeMercaderia;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import vistas.FrameIngresoDeMercaderia;
import vistasAuxiliares.FramePrecioCosto;

public class ControlFramePrecioCosto implements ActionListener {

    private FramePrecioCosto framePrecioCosto;
    private ControlFrameIngresoDeMercaderia controlFrameIngresoDeMercaderia;

    public ControlFramePrecioCosto(FrameIngresoDeMercaderia frameIngresoDeMercaderia,
            ControlFrameIngresoDeMercaderia controlFrameIngresoDeMercaderia) {
        framePrecioCosto = new FramePrecioCosto(frameIngresoDeMercaderia, true);
        this.controlFrameIngresoDeMercaderia = controlFrameIngresoDeMercaderia;
        framePrecioCosto.setControlador(this);
        framePrecioCosto.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(framePrecioCosto.BTN_MODIFICAR)) {
            ArrayList<Float> pc = new ArrayList<Float>();
            pc = getPrecioCosto();
            controlFrameIngresoDeMercaderia.setCosto(pc.get(0));
            controlFrameIngresoDeMercaderia.setPrecio(pc.get(1));
            controlFrameIngresoDeMercaderia.setBotonPress(false);
            framePrecioCosto.dispose();
        }
        if (e.getActionCommand().equals(framePrecioCosto.BTN_CANCELAR)) {
            framePrecioCosto.dispose();
            controlFrameIngresoDeMercaderia.setBotonPress(true);
        }
    }

    public ArrayList getPrecioCosto() {
        ArrayList pc = new ArrayList();
        pc.add(framePrecioCosto.getTxtCosto());
        pc.add(framePrecioCosto.getTxtPrecio());
        return pc;
    }
}
