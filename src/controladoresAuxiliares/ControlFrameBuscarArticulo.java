package controladoresAuxiliares;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import utilidadesParaFrames.UtilFrameBuscarArticulo;
import utilidadesParaFrames.UtilFrameFactura;
import utilidadesParaFrames.UtilFrameIngresoDeMercaderia;
import utilidadesParaFrames.UtilFramePerdida;
import vistas.FrameFactura;
import vistas.FrameIngresoDeMercaderia;
import vistas.FramePerdida;
import vistasAuxiliares.FrameBuscarArticulo;

public class ControlFrameBuscarArticulo implements ActionListener {

    private FrameBuscarArticulo frameBuscarArticulo;
    private FrameIngresoDeMercaderia frameIngresoDeMercaderia;
    private FrameFactura frameFactura;
    private FramePerdida framePerdida;
    private int frameOrigen = 0;

    public ControlFrameBuscarArticulo(FrameIngresoDeMercaderia frameIngresoDeMercaderia) throws SQLException, ClassNotFoundException {
        frameBuscarArticulo = new FrameBuscarArticulo(frameIngresoDeMercaderia, true);
        frameBuscarArticulo.setControlador(this);
        this.frameIngresoDeMercaderia = frameIngresoDeMercaderia;
        UtilFrameBuscarArticulo.mostrarArticulosEnTabla(frameBuscarArticulo.getTablaArticulos());
        frameOrigen = 0;
        frameBuscarArticulo.ejecutar();
    }

    public ControlFrameBuscarArticulo(FrameFactura frameFactura) throws SQLException, ClassNotFoundException {
        frameBuscarArticulo = new FrameBuscarArticulo(frameFactura, true);
        frameBuscarArticulo.setControlador(this);
        this.frameFactura = frameFactura;
        UtilFrameBuscarArticulo.mostrarArticulosEnTabla(frameBuscarArticulo.getTablaArticulos());
        frameOrigen = 1;
        frameBuscarArticulo.ejecutar();
    }

    public ControlFrameBuscarArticulo(FramePerdida framePerdida) throws SQLException, ClassNotFoundException {
        frameBuscarArticulo = new FrameBuscarArticulo(framePerdida, true);
        frameBuscarArticulo.setControlador(this);
        this.framePerdida = framePerdida;
        UtilFrameBuscarArticulo.mostrarArticulosEnTabla(frameBuscarArticulo.getTablaArticulos());
        frameOrigen = 2;
        frameBuscarArticulo.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameBuscarArticulo.BTN_AGREGAR)) {

            if (frameOrigen == 1) {
                int i = frameBuscarArticulo.getTablaArticulos().getSelectedRow();
                if (i > -1) {
                    int idArticulo = Integer.parseInt((String) frameBuscarArticulo.getTablaArticulos().getModel().getValueAt(i, 0));
                    try {
                        boolean existe = verificarSiExiste(frameFactura.getTablaLineasDeVenta(), idArticulo);
                        if (existe) {
                            JOptionPane.showMessageDialog(null, "Este articulo ya fue seleccionado.");
                        } else {
                            UtilFrameFactura.agregarArticuloEnTabla(idArticulo, frameFactura.getTablaLineasDeVenta(), frameFactura);
                            frameOrigen = 1;
                        }
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
                }

            } else if (frameOrigen == 0) {
                int i = frameBuscarArticulo.getTablaArticulos().getSelectedRow();
                if (i > -1) {
                    int idArticulo = Integer.parseInt((String) frameBuscarArticulo.getTablaArticulos().getModel().getValueAt(i, 0));
                    try {
                        boolean existe = verificarSiExiste(frameIngresoDeMercaderia.getTablaArticulos(), idArticulo);
                        if (existe) {
                            JOptionPane.showMessageDialog(null, "Este articulo ya fue seleccionado.");
                        } else {
                            UtilFrameIngresoDeMercaderia.agregarArticuloEnTabla(idArticulo, frameIngresoDeMercaderia.getTablaArticulos(), false);
                            frameOrigen = 0;
                        }
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
                }

            } else if (frameOrigen == 2) {
                int i = frameBuscarArticulo.getTablaArticulos().getSelectedRow();
                if (i > -1) {
                    int idArticulo = Integer.parseInt((String) frameBuscarArticulo.getTablaArticulos().getModel().getValueAt(i, 0));
                    try {
                        UtilFramePerdida.agregarArticuloEnTabla(idArticulo, framePerdida.getTablaLineasDePerdida(), false, frameBuscarArticulo);
                        frameOrigen = 2;
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un articulo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        if (e.getActionCommand().equals(frameBuscarArticulo.BTN_CANCELAR)) {
            frameBuscarArticulo.dispose();
        }
    }

    public boolean verificarSiExiste(JTable tablaArticulos, int idArticulo) {
        int cantidadDeFilas = UtilFrameIngresoDeMercaderia.obtenerLaCantidadDeFilasDeLaTabla(tablaArticulos);
        boolean sonIguales = false;
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            int codigo = Integer.parseInt("" + tablaArticulos.getModel().getValueAt(fila, 1));
            if (codigo == idArticulo) {
                sonIguales = true;
            }
        }
        return sonIguales;
    }
}
