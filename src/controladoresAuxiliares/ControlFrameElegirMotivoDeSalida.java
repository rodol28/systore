package controladoresAuxiliares;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import utilidadesParaFrames.UtilFramePerdida;
import vistasAuxiliares.FrameBuscarArticulo;
import vistasAuxiliares.FrameElegirMotivoSalida;

public class ControlFrameElegirMotivoDeSalida implements ActionListener {

    private FrameElegirMotivoSalida frameElegirMotivoSalida;

    public ControlFrameElegirMotivoDeSalida(FrameBuscarArticulo frameBuscarArticulo) throws SQLException, ClassNotFoundException {
        frameElegirMotivoSalida = new FrameElegirMotivoSalida(frameBuscarArticulo, true);
        frameElegirMotivoSalida.setControlador(this);
        UtilFramePerdida.llenarComboMotivos(frameElegirMotivoSalida.getComboMotivo());
        frameElegirMotivoSalida.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameElegirMotivoSalida.BTN_ACEPTAR)) {
            UtilFramePerdida.getMotivoDeSalida("" + frameElegirMotivoSalida.getComboMotivo().getSelectedItem());
            frameElegirMotivoSalida.dispose();
        }
    }

}
