package controladoresAuxiliares;

import controladores.ControlFrameModificarEmpleado;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import objetosDelDominio.Empleado;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameModificarEleccionDeObrasSociales;
import vistas.FrameModificarEmpleado;
import vistasAuxiliares.FrameModificarEleccionDeObrasSociales;

public class ControlFrameModificarEleccionDeObrasSociales implements ActionListener {

    private FrameModificarEleccionDeObrasSociales frameModificarEleccionDeObrasSociales;
    private ControlFrameModificarEmpleado controlFrameModificarEmpleado;
    private Empleado empleado;
    private ArrayList<String> obrasSocialesLocal;

    public ControlFrameModificarEleccionDeObrasSociales(FrameModificarEmpleado frameModificarEmpleado,
            Empleado empleado,
            ControlFrameModificarEmpleado controlFrameModificarEmpleado) throws SQLException, ClassNotFoundException {
        frameModificarEleccionDeObrasSociales = new FrameModificarEleccionDeObrasSociales(frameModificarEmpleado, true);
        frameModificarEleccionDeObrasSociales.setControlador(this);

        this.empleado = empleado;
        this.controlFrameModificarEmpleado = controlFrameModificarEmpleado;
        this.obrasSocialesLocal = new ArrayList<String>();
        igualarArraysInicial(this.obrasSocialesLocal);
        

        UtilAuxiliares.llenarComboObrasSociales(frameModificarEleccionDeObrasSociales.getComboObrasSociales());
        UtilFrameModificarEleccionDeObrasSociales.mostrarObrasSocialesDelEmpleado(frameModificarEleccionDeObrasSociales.getTablaObrasSociales(), empleado);
        frameModificarEleccionDeObrasSociales.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameModificarEleccionDeObrasSociales.BTN_ACEPTAR)) {
            igualarArraysFinal(this.obrasSocialesLocal);
            controlFrameModificarEmpleado.setEmpleado(empleado);
            frameModificarEleccionDeObrasSociales.dispose();
        }
        if (e.getActionCommand().equals(frameModificarEleccionDeObrasSociales.BTN_AGREGAR)) {
            String obraSocial = frameModificarEleccionDeObrasSociales.getObraSocialSeleccionada();
            boolean existe = verificarSiExiste(frameModificarEleccionDeObrasSociales.getTablaObrasSociales(), obraSocial);

            if (existe) {
                JOptionPane.showMessageDialog(null, "Esta obra social ya fue seleccionado.", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
            } else {
                this.obrasSocialesLocal.add(obraSocial);
                try {
                    UtilFrameModificarEleccionDeObrasSociales.agregarObraSocial(this.obrasSocialesLocal, frameModificarEleccionDeObrasSociales.getTablaObrasSociales());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }

        }
        if (e.getActionCommand().equals(frameModificarEleccionDeObrasSociales.BTN_QUITAR)) {
            int filaSeleccionada = UtilFrameModificarEleccionDeObrasSociales.getFilaSeleccionada(frameModificarEleccionDeObrasSociales);
            if (filaSeleccionada > -1) {
                String obraSocial = "" + frameModificarEleccionDeObrasSociales.getTablaObrasSociales().getModel().getValueAt(filaSeleccionada, 0);
                removerDelArrayUnaObraSocial(this.obrasSocialesLocal, obraSocial);
                UtilFrameModificarEleccionDeObrasSociales.eliminarUnItem(filaSeleccionada, frameModificarEleccionDeObrasSociales.getTablaObrasSociales());
                frameModificarEleccionDeObrasSociales.getComboObrasSociales().setSelectedIndex(0);
            }
        }
        if (e.getActionCommand().equals(frameModificarEleccionDeObrasSociales.BTN_CANCELAR)) {
            frameModificarEleccionDeObrasSociales.dispose();
        }
    }

    public void igualarArraysInicial(ArrayList<String> obrasSocialesLocal) {

//        for (int j = 0; j < empleado.getObrasSociales().size(); j++) {
//            obrasSocialesLocal.add(empleado.getObrasSociales().get(j));
//        }
    }

    public void igualarArraysFinal(ArrayList<String> obrasSocialesLocal) {

//        for (int i = 0; i < empleado.getObrasSociales().size(); i++) {
//            empleado.getObrasSociales().remove(i);
//        }
//        if (empleado.getObrasSociales().size() > 0) {
//            empleado.getObrasSociales().remove(0);
//        }
//
//        for (int j = 0; j < this.obrasSocialesLocal.size(); j++) {
//            empleado.getObrasSociales().add(obrasSocialesLocal.get(j));
//        }
    }

    public boolean verificarSiExiste(JTable tablaObrasSociales, String obraSocial) {
        int cantidadDeFilas = UtilFrameModificarEleccionDeObrasSociales.obtenerLaCantidadDeFilasDeLaTabla(tablaObrasSociales);
        boolean sonIguales = false;
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            String razonSocial = "" + tablaObrasSociales.getModel().getValueAt(fila, 0);
            if (razonSocial.equals(obraSocial)) {
                sonIguales = true;
            }
        }
        return sonIguales;
    }

    public static void removerDelArrayUnaObraSocial(ArrayList<String> obrasSocialesLocal, String obraSocial) {
        int idx = 0;
        for (int i = 0; i < obrasSocialesLocal.size(); i++) {
            if (obrasSocialesLocal.get(i).equals(obraSocial)) {
                idx = i;
            }
        }
        obrasSocialesLocal.remove(idx);
    }

}
