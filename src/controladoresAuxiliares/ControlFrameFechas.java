package controladoresAuxiliares;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import net.sf.jasperreports.engine.JRException;
import reportes.GenerarVentasTotales;
import vistas.MenuPrincipal;
import vistasAuxiliares.FrameFechas;

public class ControlFrameFechas implements ActionListener {

    private FrameFechas frameFechas;

    public ControlFrameFechas(MenuPrincipal menuPrincipal) {
        frameFechas = new FrameFechas(menuPrincipal, true);
        frameFechas.setControlador(this);
        frameFechas.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameFechas.BTN_ACEPTAR)) {
            try {
                GenerarVentasTotales.generarReporteDeVentasTotales(frameFechas.getJdcDesde(), frameFechas.getJdcHasta());
                frameFechas.dispose();
            } catch (JRException ex) {
            } catch (ClassNotFoundException ex) {
            } catch (SQLException ex) {
            }
        }
        if (e.getActionCommand().equals(frameFechas.BTN_CANCELAR)) {
            frameFechas.dispose();
        }
    }

}
