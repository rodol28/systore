package controladoresAuxiliares;

import controladores.ControlFrameNuevoEmpleado;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import objetosDelDominio.Empleado;
import utilidadesParaFrames.UtilAuxiliares;
import utilidadesParaFrames.UtilFrameElegirObraSocial;
import utilidadesParaFrames.UtilFrameModificarEleccionDeObrasSociales;
import vistas.FrameNuevoEmpleado;
import vistasAuxiliares.FrameElegirObraSocial;

public class ControlFrameElegirObraSocial implements ActionListener {

    private FrameElegirObraSocial frameElegirObraSocial;
    private ControlFrameNuevoEmpleado controlFrameNuevoEmpleado;
    private Empleado empleado;
    private ArrayList<String> obrasSocialesLocal;

    public ControlFrameElegirObraSocial(FrameNuevoEmpleado frameNuevoEmpleado,
            Empleado empleado,
            ControlFrameNuevoEmpleado controlFrameNuevoEmpleado) throws SQLException, ClassNotFoundException {

        frameElegirObraSocial = new FrameElegirObraSocial(frameNuevoEmpleado, true);
        frameElegirObraSocial.setControlador(this);

        this.empleado = empleado;
        this.controlFrameNuevoEmpleado = controlFrameNuevoEmpleado;
        this.obrasSocialesLocal = new ArrayList<String>();
        igualarArraysInicial(this.obrasSocialesLocal);

        UtilAuxiliares.llenarComboObrasSociales(frameElegirObraSocial.getComboObrasSociales());
        UtilFrameElegirObraSocial.mostrarObrasSocialesDelEmpleado(frameElegirObraSocial.getTablaObrasSociales(), empleado);
        frameElegirObraSocial.ejecutar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(frameElegirObraSocial.BTN_ACEPTAR)) {
            igualarArraysFinal(this.obrasSocialesLocal);   
            controlFrameNuevoEmpleado.setEmpleado(empleado);
            frameElegirObraSocial.dispose();
        }
        if (e.getActionCommand().equals(frameElegirObraSocial.BTN_AGREGAR)) {
            String obraSocial = frameElegirObraSocial.getObraSocialSeleccionada();
            boolean existe = verificarSiExiste(frameElegirObraSocial.getTablaObrasSociales(), obraSocial);

            if (existe) {
                JOptionPane.showMessageDialog(null, "Esta obra social ya fue seleccionado.");
            } else {
                this.obrasSocialesLocal.add(obraSocial);
                try {
                    UtilFrameElegirObraSocial.agregarObraSocial(this.obrasSocialesLocal,
                            frameElegirObraSocial.getTablaObrasSociales());
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        }
        if (e.getActionCommand().equals(frameElegirObraSocial.BTN_QUITAR)) {
            int filaSeleccionada = UtilFrameElegirObraSocial.getFilaSeleccionada(frameElegirObraSocial);
            if (filaSeleccionada > -1) {
                String obraSocial = "" + frameElegirObraSocial.getTablaObrasSociales().getModel().getValueAt(filaSeleccionada, 0);
                removerDelArrayUnaObraSocial(this.obrasSocialesLocal, obraSocial);
                UtilFrameElegirObraSocial.eliminarUnItem(filaSeleccionada, frameElegirObraSocial.getTablaObrasSociales());
                frameElegirObraSocial.getComboObrasSociales().setSelectedIndex(0);
            }
        }
        if (e.getActionCommand().equals(frameElegirObraSocial.BTN_CANCELAR)) {
            frameElegirObraSocial.dispose();
        }
    }

    public void agregarObrasSocialesAlEmpleado(JTable obrasSociales) {
        int cantidadDeFilas = UtilFrameElegirObraSocial.obtenerLaCantidadDeFilasDeLaTabla(obrasSociales);

//        for (int i = 0; i < cantidadDeFilas; i++) {
//            String obraSocial = "" + frameElegirObraSocial.getTablaObrasSociales().getModel().getValueAt(i, 0);
//            this.empleado.agregarObraSocial(obraSocial);
//        }
    }

    public void igualarArraysInicial(ArrayList<String> obrasSocialesLocal) {

//        for (int j = 0; j < empleado.getObrasSociales().size(); j++) {
//            obrasSocialesLocal.add(empleado.getObrasSociales().get(j));
//        }
    }

    public void igualarArraysFinal(ArrayList<String> obrasSocialesLocal) {

//        for (int i = 0; i < empleado.getObrasSociales().size(); i++) {
//            empleado.getObrasSociales().remove(i);
//        }
//        if (empleado.getObrasSociales().size() > 0) {
//            empleado.getObrasSociales().remove(0);
//        }
//
//        for (int j = 0; j < this.obrasSocialesLocal.size(); j++) {
//            empleado.getObrasSociales().add(obrasSocialesLocal.get(j));
//        }
    }

    public boolean verificarSiExiste(JTable tablaObrasSociales, String obraSocial) {
        int cantidadDeFilas = UtilFrameModificarEleccionDeObrasSociales.obtenerLaCantidadDeFilasDeLaTabla(tablaObrasSociales);
        boolean sonIguales = false;
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            String razonSocial = "" + tablaObrasSociales.getModel().getValueAt(fila, 0);
            if (razonSocial.equals(obraSocial)) {
                sonIguales = true;
            }
        }
        return sonIguales;
    }

    public static void removerDelArrayUnaObraSocial(ArrayList<String> obrasSocialesLocal, String obraSocial) {
        int idx = 0;
        for (int i = 0; i < obrasSocialesLocal.size(); i++) {
            if (obrasSocialesLocal.get(i).equals(obraSocial)) {
                idx = i;
            }
        }
        obrasSocialesLocal.remove(idx);
    }
}
