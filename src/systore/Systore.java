package systore;

import controladores.ControlLog;
import java.text.DecimalFormat;
import java.text.ParseException;

public class Systore {

    private static int usuario;

    public static void main(String[] args) throws Exception {
        ControlLog controlLog = new ControlLog(); 
//        float nro = (float) 5.5;
//        formatearNumero(nro);
    }

    public static int getUsuario() {
        return usuario;
    }

    public static void setUsuario(int usuario) {
        Systore.usuario = usuario;
    }

    public static double formatearNumero(float numero) throws ParseException {
        DecimalFormat df = new DecimalFormat("0.00");
        String formate = df.format(numero);
        double finalValue = (double) df.parse(formate);
        return finalValue;
    }
}
