package objetosDelDominio;

public class Puesto {
    private int idPuesto;
    private String descripcionPuesto;
    private float sueldoBasico;

    public Puesto(int idPuesto, float sueldoBasico) {
        this.idPuesto = idPuesto;
        this.sueldoBasico = sueldoBasico;
    }

    public Puesto(int idPuesto, String descripcionPuesto,  float sueldoBasico) {
        this.idPuesto = idPuesto;
        this.descripcionPuesto = descripcionPuesto;
        this.sueldoBasico = sueldoBasico;
    }

    public Puesto(String descripcionPuesto, float sueldoBasico) {
        this.descripcionPuesto = descripcionPuesto;
        this.sueldoBasico = sueldoBasico;
    }

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

    public String getDescripcionPuesto() {
        return descripcionPuesto;
    }

    public void setDescripcionPuesto(String descripcionPuesto) {
        this.descripcionPuesto = descripcionPuesto;
    }

    public float getSueldoBasico() {
        return sueldoBasico;
    }

    public void setSueldoBasico(float sueldoBasico) {
        this.sueldoBasico = sueldoBasico;
    }
}
