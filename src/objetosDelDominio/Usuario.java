package objetosDelDominio;

public class Usuario {

    private int idUsuario;
    private String user;
    private String pass;
    private int idEmpleado;
    private int idTipo;

    public Usuario() {
    }

    public Usuario(int idUsuario, String user, String pass, int idEmpleado,int idTipo) {
        this.idUsuario = idUsuario;
        this.user = user;
        this.pass = pass;
        this.idEmpleado = idEmpleado;
        this.idTipo = idTipo;
    }
    
    public Usuario(String user, String pass, int idEmpleado,int idTipo) {
        this.user = user;
        this.pass = pass;
        this.idEmpleado = idEmpleado;
        this.idTipo = idTipo;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }      
}
