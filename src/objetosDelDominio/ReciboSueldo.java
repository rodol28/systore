package objetosDelDominio;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ReciboSueldo {

    private String empleado;
    private String legajo;
    private String cuil;
    private String basico;
    private String ingreso;
    private String convenio;
    private String puesto;
    private String sucursal;
    private String obraSocial;
    private String antiguedad;

    private String sueldoNeto;
    private String totalHaberes;
    private String totalHaberesSinDeducciones;
    private String totalDeducciones;
    private String pesos;

    private List<ConceptoLiquidado> conceptos = new ArrayList<ConceptoLiquidado>();

    public ReciboSueldo() {
    }

    public ReciboSueldo(String empleado, String legajo) {
        this.empleado = empleado;
        this.legajo = legajo;
    }

    public ReciboSueldo(String empleado, String legajo, String cuil, String basico, String ingreso, String convenio, String puesto, String sucursal, String obraSocial, String antiguedad, String sueldoNeto, String totalHaberes, String totalHaberesSinDeducciones, String totalDeducciones, String pesos) {
        this.empleado = empleado;
        this.legajo = legajo;
        this.cuil = cuil;
        this.basico = basico;
        this.ingreso = ingreso;
        this.convenio = convenio;
        this.puesto = puesto;
        this.sucursal = sucursal;
        this.obraSocial = obraSocial;
        this.antiguedad = antiguedad;
        this.sueldoNeto = sueldoNeto;
        this.totalHaberes = totalHaberes;
        this.totalHaberesSinDeducciones = totalHaberesSinDeducciones;
        this.totalDeducciones = totalDeducciones;
        this.pesos = pesos;
    }

    public void addConcepto(ConceptoLiquidado concepto) {
        this.conceptos.add(concepto);
    }

    public JRDataSource getConceptosDS() {
        return new JRBeanCollectionDataSource(conceptos);
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public List getConceptos() {
        return conceptos;
    }

    public void setConceptos(List conceptos) {
        this.conceptos = conceptos;
    }

    public String getCuil() {
        return cuil;
    }

    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    public String getBasico() {
        return basico;
    }

    public void setBasico(String basico) {
        this.basico = basico;
    }

    public String getIngreso() {
        return ingreso;
    }

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getConvenio() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getObraSocial() {
        return obraSocial;
    }

    public void setObraSocial(String obraSocial) {
        this.obraSocial = obraSocial;
    }

    public String getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(String antiguedad) {
        this.antiguedad = antiguedad;
    }

    public String getSueldoNeto() {
        return sueldoNeto;
    }

    public void setSueldoNeto(String sueldoNeto) {
        this.sueldoNeto = sueldoNeto;
    }

    public String getTotalHaberes() {
        return totalHaberes;
    }

    public void setTotalHaberes(String totalHaberes) {
        this.totalHaberes = totalHaberes;
    }

    public String getTotalHaberesSinDeducciones() {
        return totalHaberesSinDeducciones;
    }

    public void setTotalHaberesSinDeducciones(String totalHaberesSinDeducciones) {
        this.totalHaberesSinDeducciones = totalHaberesSinDeducciones;
    }

    public String getTotalDeducciones() {
        return totalDeducciones;
    }

    public void setTotalDeducciones(String totalDeducciones) {
        this.totalDeducciones = totalDeducciones;
    }

    public String getPesos() {
        return pesos;
    }

    public void setPesos(String pesos) {
        this.pesos = pesos;
    }
}
