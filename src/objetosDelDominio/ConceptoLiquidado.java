package objetosDelDominio;

public class ConceptoLiquidado {
    private String codigo;
    private String descripcionConcepto;
    private String cantidad;
    private String haberes;
    private String haberesSinDescuento;
    private String deducciones;

    public ConceptoLiquidado() {
    }

    public ConceptoLiquidado(String codigo, String descripcionConcepto) {
        this.codigo = codigo;
        this.descripcionConcepto = descripcionConcepto;
    }

    public ConceptoLiquidado(String codigo, String descripcionConcepto, String cantidad, String haberes, String haberesSinDescuento, String deducciones) {
        this.codigo = codigo;
        this.descripcionConcepto = descripcionConcepto;
        this.cantidad = cantidad;
        this.haberes = haberes;
        this.haberesSinDescuento = haberesSinDescuento;
        this.deducciones = deducciones;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcionConcepto() {
        return descripcionConcepto;
    }

    public void setDescripcionConcepto(String descripcionConcepto) {
        this.descripcionConcepto = descripcionConcepto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getHaberes() {
        return haberes;
    }

    public void setHaberes(String haberes) {
        this.haberes = haberes;
    }

    public String getHaberesSinDescuento() {
        return haberesSinDescuento;
    }

    public void setHaberesSinDescuento(String haberesSinDescuento) {
        this.haberesSinDescuento = haberesSinDescuento;
    }

    public String getDeducciones() {
        return deducciones;
    }

    public void setDeducciones(String deducciones) {
        this.deducciones = deducciones;
    }
}
