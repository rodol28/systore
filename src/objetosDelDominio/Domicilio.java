package objetosDelDominio;

public class Domicilio {

    private int codigo;
    private String calle;
    private String numero;
    private String localidad;
    private int codigoPostal;
    private int provincia;
    private int pais;
    
    private String nroDepartamento;
    private String piso;

    public Domicilio(String calle, String numero, String localidad, int codigoPostal, int provincia, int pais) {
        this.calle = calle;
        this.numero = numero;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
        this.provincia = provincia;
        this.pais = pais;
    }

    public Domicilio(String calle, String numero, String localidad, int codigoPostal, int provincia, int pais, String nroDepartamento, String piso) {
        this.calle = calle;
        this.numero = numero;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
        this.provincia = provincia;
        this.pais = pais;
        this.nroDepartamento = nroDepartamento;
        this.piso = piso;
    }
    
    public String getNroDepartamento() {
        return nroDepartamento;
    }

    public void setNroDepartamento(String nroDepartamento) {
        this.nroDepartamento = nroDepartamento;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }  

    public Domicilio() {
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public int getProvincia() {
        return provincia;
    }

    public void setProvincia(int provincia) {
        this.provincia = provincia;
    }

    public int getPais() {
        return pais;
    }

    public void setPais(int pais) {
        this.pais = pais;
    }

}
