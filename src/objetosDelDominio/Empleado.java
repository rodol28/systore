package objetosDelDominio;

import java.util.ArrayList;

public class Empleado extends Persona {

    private int idEmpleado;
    private int legajo;
    private String nroCuil;
    private Fecha fechaDeIngreso;
    private int estadoDelEmpleado;
    private int idPersona;
    private Persona persona;  
    private int idPuesto;
    private int idObraSocial;
    private String obraSocial;
    private String domicilio;
    private String pais;
    private String provincia;
    private String localidad;
    private int codigoPostal;
    private String nacionalidad;
    private ArrayList<Pariente> grupoFamiliar = new ArrayList<Pariente>();

    private String razonSocialBanco;
    private String cuentaBancaria;
    
    
    private String fechaIngString;

    public Empleado(int legajo) {
        this.legajo = legajo;
    }

    public Empleado(int legajo, Fecha fechaDeIngreso, int estadoDelEmpleado, Domicilio domicilio, Persona persona, int idPuesto, String nombre, String apellido, String email, String telefono, int dni, String sexo, Fecha fechaNac, int estadoCivil) {
        super(nombre, apellido, email, telefono, dni, sexo, fechaNac, estadoCivil);
        this.legajo = legajo;
        this.fechaDeIngreso = fechaDeIngreso;
        this.estadoDelEmpleado = estadoDelEmpleado;
        this.persona = persona;
        this.idPuesto = idPuesto;
    }

    public Empleado(int idEmpleado, int legajo, Fecha fechaDeIngreso, int estadoDelEmpleado, int idDomicilio, Domicilio domicilio, int idPersona, Persona persona, int idPuesto, String nroCuil, String nombre, String apellido, String email, String telefono, int estadoCivil) {
        super(nombre, apellido, email, telefono, estadoCivil);
        this.idEmpleado = idEmpleado;
        this.legajo = legajo;
        this.fechaDeIngreso = fechaDeIngreso;
        this.estadoDelEmpleado = estadoDelEmpleado;
        this.idPersona = idPersona;
        this.persona = persona;
        this.idPuesto = idPuesto;
        this.nroCuil = nroCuil;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public int getLegajo() {
        return legajo;
    }

    public void setLegajo(int legajo) {
        this.legajo = legajo;
    }

    public Fecha getFechaDeIngreso() {
        return fechaDeIngreso;
    }

    public void setFechaDeIngreso(Fecha fechaDeIngreso) {
        this.fechaDeIngreso = fechaDeIngreso;
    }

    public int getEstadoDelEmpleado() {
        return estadoDelEmpleado;
    }

    public void setEstadoDelEmpleado(int estadoDelEmpleado) {
        this.estadoDelEmpleado = estadoDelEmpleado;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public int getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(int idPuesto) {
        this.idPuesto = idPuesto;
    }

    public ArrayList<Pariente> getGrupoFamiliar() {
        return grupoFamiliar;
    }

    public void setGrupoFamiliar(ArrayList<Pariente> grupoFamiliar) {
        this.grupoFamiliar = grupoFamiliar;
    }

    public void agregarPariente(Pariente pariente) {
        grupoFamiliar.add(pariente);
    }

    public void removerPariente(int index) {
        grupoFamiliar.remove(index);
    }

    public String getNroCuil() {
        return nroCuil;
    }

    public void setNroCuil(String nroCuil) {
        this.nroCuil = nroCuil;
    }

    public int getIdObraSocial() {
        return idObraSocial;
    }

    public void setIdObraSocial(int idObraSocial) {
        this.idObraSocial = idObraSocial;
    }

    public String getRazonSocialBanco() {
        return razonSocialBanco;
    }

    public void setRazonSocialBanco(String razonSocialBanco) {
        this.razonSocialBanco = razonSocialBanco;
    }

    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

    public void setCuentaBancaria(String cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }

    public String getFechaIngString() {
        return fechaIngString;
    }

    public void setFechaIngString(String fechaIngString) {
        this.fechaIngString = fechaIngString;
    }

    public String getObraSocial() {
        return obraSocial;
    }

    public void setObraSocial(String obraSocial) {
        this.obraSocial = obraSocial;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
    
    
}
