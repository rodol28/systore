package objetosDelDominio;

public class TipoDeConcepto {
    private String tipoDeConceptoDescripcion;
    private float fijo;
    private float porcentaje;

    public TipoDeConcepto() {
    }

    public TipoDeConcepto(String tipoDeConceptoDescripcion, float fijo, float porcentaje) {
        this.tipoDeConceptoDescripcion = tipoDeConceptoDescripcion;
        this.fijo = fijo;
        this.porcentaje = porcentaje;
    }

    public String getTipoDeConceptoDescripcion() {
        return tipoDeConceptoDescripcion;
    }

    public void setTipoDeConceptoDescripcion(String tipoDeConceptoDescripcion) {
        this.tipoDeConceptoDescripcion = tipoDeConceptoDescripcion;
    }

    public float getFijo() {
        return fijo;
    }

    public void setFijo(float fijo) {
        this.fijo = fijo;
    }

    public float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }
}
