package objetosDelDominio;

public class Cliente extends Persona {

    private int codigo;
    private String nroCuit;
    private String observacion;
    private int tipoDeCuit;
    private String domicilio;
    private String pais;
    private String provincia;
    private String localidad;
    private int codigoPostal;

    /*Crear para alta*/
    public Cliente(String nroCuit, String observacion, int tipoDeCuit,
            String domicilio, String pais, String provincia,
            String localidad, int codigoPostal, String nombre,
            String apellido, String email, String telefono) {
        super(nombre, apellido, email, telefono);
        this.nroCuit = nroCuit;
        this.observacion = observacion;
        this.tipoDeCuit = tipoDeCuit;
        this.domicilio = domicilio;
        this.pais = pais;
        this.provincia = provincia;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
    }
    
    /*Crear para despues modificar*/
    public Cliente(int codigo, String nroCuit, String observacion, int tipoDeCuit,
            String domicilio, String pais, String provincia,
            String localidad, int codigoPostal, String nombre,
            String apellido, String email, String telefono) {
        super(nombre, apellido, email, telefono);
        this.codigo = codigo;
        this.nroCuit = nroCuit;
        this.observacion = observacion;
        this.tipoDeCuit = tipoDeCuit;
        this.domicilio = domicilio;
        this.pais = pais;
        this.provincia = provincia;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNroCuit() {
        return nroCuit;
    }

    public void setNroCuit(String nroCuit) {
        this.nroCuit = nroCuit;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getTipoDeCuit() {
        return tipoDeCuit;
    }

    public void setTipoDeCuit(int tipoDeCuit) {
        this.tipoDeCuit = tipoDeCuit;
    }
}
