package objetosDelDominio;

import java.util.ArrayList;

public class Venta {
    
    private int idventa;
    private Fecha fecha;
    private float total;
    private Empleado empleado;
    private int idEmpleado;
    private Cliente cliente;
    private ArrayList<LineaDeVenta> lineasDeVenta = new ArrayList<LineaDeVenta>();

    public Venta(Fecha fecha, float total, Empleado empleado, Cliente cliente) {
        this.fecha = fecha;
        this.total = total;
        this.empleado = empleado;
        this.cliente = cliente;
    }

    public Venta(int idVenta, Fecha fecha, float total, int idEmpleado) {
        this.idventa = idVenta;
        this.fecha = fecha;
        this.total = total;
        this.idEmpleado = idEmpleado;
    }

    public int getIdventa() {
        return idventa;
    }

    public void setIdventa(int idventa) {
        this.idventa = idventa;
    }

    public Fecha getFecha() {
        return fecha;
    }

    public void setFecha(Fecha fecha) {
        this.fecha = fecha;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public ArrayList<LineaDeVenta> getLineasDeVenta() {
        return lineasDeVenta;
    }

    public void setLineasDeVenta(ArrayList<LineaDeVenta> lineasDeVenta) {
        this.lineasDeVenta = lineasDeVenta;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public void agregarLineaDeVenta(LineaDeVenta linea) {
        lineasDeVenta.add(linea);
    }
    
}
