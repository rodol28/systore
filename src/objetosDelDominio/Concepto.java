package objetosDelDominio;

public class Concepto {
    
    private int idConcepto;
    private String abreviatura;
    private String descripcion;
    private boolean esRemunerativo;
    private String tipoDeConcepto;
    private double cantidadPorcentual;
    private double cantidadFija;

    public Concepto() {
    }

    public Concepto(int idConcepto) {
        this.idConcepto = idConcepto;
    }
    
    public int getIdConcepto() {
        return idConcepto;
    }

    public void setIdConcepto(int idConcepto) {
        this.idConcepto = idConcepto;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isEsRemunerativo() {
        return esRemunerativo;
    }

    public void setEsRemunerativo(boolean esRemunerativo) {
        this.esRemunerativo = esRemunerativo;
    }

    public String getTipoDeConcepto() {
        return tipoDeConcepto;
    }

    public void setTipoDeConcepto(String tipoDeConcepto) {
        this.tipoDeConcepto = tipoDeConcepto;
    }

    public double getCantidadPorcentual() {
        return cantidadPorcentual;
    }

    public void setCantidadPorcentual(double cantidadPorcentual) {
        this.cantidadPorcentual = cantidadPorcentual;
    }

    public double getCantidadFija() {
        return cantidadFija;
    }

    public void setCantidadFija(double cantidadFija) {
        this.cantidadFija = cantidadFija;
    }
}
