package objetosDelDominio;

public class Proveedor {

    private int codigo;
    private String razonSocial;
    private String telefono;
    private String celular;
    private String email;
    private String nroCUIT;
    private String web;
    private String observacion;
    private int tipoDeCuit;
    private int rubro;
    private String domicilio;
    private String pais;
    private String provincia;
    private String localidad;
    private int codigoPostal;

    public Proveedor(String razonSocial, String telefono, String celular,
            String email, String nroCUIT, String web, String observacion,
            int tipoDeCuit, int rubro, String domicilio, String pais,
            String provincia, String localidad, int codigoPostal) {
        this.razonSocial = razonSocial;
        this.telefono = telefono;
        this.celular = celular;
        this.email = email;
        this.nroCUIT = nroCUIT;
        this.web = web;
        this.observacion = observacion;
        this.tipoDeCuit = tipoDeCuit;
        this.rubro = rubro;
        this.domicilio = domicilio;
        this.pais = pais;
        this.provincia = provincia;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
    }

    public Proveedor(int codigo, String razonSocial, String telefono, String celular,
            String email, String nroCUIT, String web, String observacion,
            int tipoDeCuit, int rubro, String domicilio, String pais,
            String provincia, String localidad, int codigoPostal) {
        this.codigo = codigo;
        this.razonSocial = razonSocial;
        this.telefono = telefono;
        this.celular = celular;
        this.email = email;
        this.nroCUIT = nroCUIT;
        this.web = web;
        this.observacion = observacion;
        this.tipoDeCuit = tipoDeCuit;
        this.rubro = rubro;
        this.domicilio = domicilio;
        this.pais = pais;
        this.provincia = provincia;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
    }

    public Proveedor() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNroCUIT() {
        return nroCUIT;
    }

    public void setNroCUIT(String nroCUIT) {
        this.nroCUIT = nroCUIT;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getTipoDeCuit() {
        return tipoDeCuit;
    }

    public void setTipoDeCuit(int tipoDeCuit) {
        this.tipoDeCuit = tipoDeCuit;
    }

    public int getRubro() {
        return rubro;
    }

    public void setRubro(int rubro) {
        this.rubro = rubro;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
}
