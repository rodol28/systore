package objetosDelDominio;

import java.util.ArrayList;

public class Sueldo {

    private int idEmpleado;
    private String fechaDePago;
    private int antiguedad;
    private String categoria;
    private double sueldoBruto;
    private double sueldoNeto;
    private String tipo;
    private ArrayList<DetalleSueldo> detalleSueldo = new ArrayList<DetalleSueldo>();

    public Sueldo(int idEmpleado, String fechaDePago, int antiguedad, String categoria, double sueldoBruto, double sueldoNeto, String tipo) {
        this.idEmpleado = idEmpleado;
        this.fechaDePago = fechaDePago;
        this.antiguedad = antiguedad;
        this.categoria = categoria;
        this.sueldoBruto = sueldoBruto;
        this.sueldoNeto = sueldoNeto;
        this.tipo = tipo;
    }

    public void addDetalleSueldo(DetalleSueldo detalleSueldo) {
        this.detalleSueldo.add(detalleSueldo);
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmplado) {
        this.idEmpleado = idEmplado;
    }

    public String getFechaDePago() {
        return fechaDePago;
    }

    public void setFechaDePago(String fechaDePago) {
        this.fechaDePago = fechaDePago;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public double getSueldoBruto() {
        return sueldoBruto;
    }

    public void setSueldoBruto(double sueldoBruto) {
        this.sueldoBruto = sueldoBruto;
    }

    public double getSueldoNeto() {
        return sueldoNeto;
    }

    public void setSueldoNeto(double sueldoNeto) {
        this.sueldoNeto = sueldoNeto;
    }

    public ArrayList<DetalleSueldo> getDetalleSueldo() {
        return detalleSueldo;
    }

    public void setDetalleSueldo(ArrayList<DetalleSueldo> detalleSueldo) {
        this.detalleSueldo = detalleSueldo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
