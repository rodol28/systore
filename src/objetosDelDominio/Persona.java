package objetosDelDominio;

public class Persona {

    private int idPersona;
    private String nombre;
    private String apellido;
    private String email;
    private String telefono;

    private int dni;
    private String sexo;
    private int idSexo;
    private Fecha fechaNac;
    private int idEstadoCivil;

    private int idPaisNacionalidad;
    private String nacionalidad;

    private String fechaNacString;

    public Persona() {
    }

    public Persona(String nombre, String apellido, String email, String telefono, int estadoCivil) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.telefono = telefono;
        this.idEstadoCivil = estadoCivil;
    }

    public Persona(String nombre, String apellido, String email, String telefono, int dni, String sexo, Fecha fechaNac, int estadoCivil) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.telefono = telefono;
        this.dni = dni;
        this.sexo = sexo;
        this.fechaNac = fechaNac;
        this.idEstadoCivil = estadoCivil;
    }

    public Persona(String nombre, String apellido, String email, String telefono) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.telefono = telefono;
    }

    public Persona(String nombre, String apellido, String email, String telefono, int dni, String sexo, Fecha fechaNac, int idEstadoCivil, int idPaisNacionalidad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.telefono = telefono;
        this.dni = dni;
        this.sexo = sexo;
        this.fechaNac = fechaNac;
        this.idEstadoCivil = idEstadoCivil;
        this.idPaisNacionalidad = idPaisNacionalidad;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Fecha getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Fecha fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getIdEstadoCivil() {
        return idEstadoCivil;
    }

    public void setEstadoCivil(int estadoCivil) {
        this.idEstadoCivil = estadoCivil;
    }

    public int getIdPaisNacionalidad() {
        return idPaisNacionalidad;
    }

    public void setIdPaisNacionalidad(int idPaisNacionalidad) {
        this.idPaisNacionalidad = idPaisNacionalidad;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public int getIdSexo() {
        return idSexo;
    }

    public void setIdSexo(int idSexo) {
        this.idSexo = idSexo;
    }

    public String getFechaNacString() {
        return fechaNacString;
    }

    public void setFechaNacString(String fechaNacString) {
        this.fechaNacString = fechaNacString;
    }
}
