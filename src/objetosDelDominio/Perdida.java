package objetosDelDominio;

import java.util.ArrayList;

public class Perdida {

    private int idPerdida;
    private String observacion;
    private Empleado empleado;
    private int idEmpleado;
    private ArrayList<LineaDePerdida> lineasDePerdida = new ArrayList<LineaDePerdida>();

    public Perdida(int idPerdida, String observacion, int idEmpleado) {
        this.idPerdida = idPerdida;
        this.observacion = observacion;
        this.idEmpleado = idEmpleado;
    }

    public Perdida(String observacion, int idEmpleado) {
        this.observacion = observacion;
        this.idEmpleado = idEmpleado;
    }
    
    public int getIdPerdida() {
        return idPerdida;
    }

    public void setIdPerdida(int idPerdida) {
        this.idPerdida = idPerdida;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }
    
    public void agregarLineaDePerdida(LineaDePerdida linea) {
        lineasDePerdida.add(linea);
    }

    public ArrayList<LineaDePerdida> getLineasDePerdida() {
        return lineasDePerdida;
    }
    
    public void setLineasDePerdida(ArrayList<LineaDePerdida> lineasDePerdida) {
        this.lineasDePerdida = lineasDePerdida;
    }
}
