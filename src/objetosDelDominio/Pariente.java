package objetosDelDominio;

public class Pariente {

    private int dni;
    private String nombre;
    private String apellido;
    private String fechaNacString;
    private Fecha fechaNac;
    private String parentesco;
    private int idParentesco;
    private int idEmpleado;

    public Pariente(int dni, String nombre, String apellido, Fecha fechaNac, int idParentesco) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNac = fechaNac;
        this.idParentesco = idParentesco;
    }

    public Pariente(int dni, String nombre, String apellido, Fecha fechaNac, String parentesco) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNac = fechaNac;
        this.parentesco = parentesco;
    }

    public Pariente(int dni, String nombre, String apellido, Fecha fechaNac, String parentesco, int idParentesco) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNac = fechaNac;
        this.parentesco = parentesco;
        this.idParentesco = idParentesco;
    }

    public Pariente(int dni, String nombre, String apellido, String fechaNac, String parentesco) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNacString = fechaNac;
        this.parentesco = parentesco;
    }

    public Pariente(int dni, String nombre, String apellido, String fechaNac, String parentesco, int idParentesco) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNacString = fechaNac;
        this.parentesco = parentesco;
        this.idParentesco = idParentesco;
    }

    public Pariente(int dni, String nombre, String apellido, String fechaNac, int idParentesco) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNacString = fechaNac;
        this.idParentesco = idParentesco;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Fecha getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Fecha fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public int getIdParentesco() {
        return idParentesco;
    }

    public void setIdParentesco(int idParentesco) {
        this.idParentesco = idParentesco;
    }

    public String getFechaNacString() {
        return fechaNacString;
    }

    public void setFechaNacString(String fechaNacString) {
        this.fechaNacString = fechaNacString;
    }
}
