package objetosDelDominio;

public class LineaDePerdida {
    private Articulo articulo;
    private int cantidad;
    private int idMotivo;

    public LineaDePerdida(Articulo articulo, int cantidad, int idMotivo) {
        this.articulo = articulo;
        this.cantidad = cantidad;
        this.idMotivo = idMotivo;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(int idMotivo) {
        this.idMotivo = idMotivo;
    }
}
