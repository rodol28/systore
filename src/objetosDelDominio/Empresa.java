package objetosDelDominio;

public class Empresa {
    private static final String razon_social = "Nombre de la empresa";
    private static final String domicilio = "domicilio";
    private static final String nro_cuit = "numero de cuit";

    public static String getRazon_social() {
        return razon_social;
    }

    public static String getDomicilio() {
        return domicilio;
    }

    public static String getNro_cuit() {
        return nro_cuit;
    }
}
