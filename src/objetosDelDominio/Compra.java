package objetosDelDominio;

import java.util.ArrayList;

public class Compra {

    private int codigo;
    private Fecha fecha;
    private float total;
    private Proveedor proveedor;
    private int idProveedor;
    private ArrayList<LineaDeCompra> lineasDeCompra = new ArrayList<LineaDeCompra>();
    
    private String fechaDeCompraString;

    public Compra(int codigo, Fecha fecha, Proveedor proveedor) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.proveedor = proveedor;
    }

    public Compra(int codigo, Fecha fecha, int idProveedor) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.idProveedor = idProveedor;
    }
    
    public Compra(int codigo, String fecha, int idProveedor) {
        this.codigo = codigo;
        this.fechaDeCompraString = fecha;
        this.idProveedor = idProveedor;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Fecha getFecha() {
        return fecha;
    }

    public void setFecha(Fecha fecha) {
        this.fecha = fecha;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public ArrayList<LineaDeCompra> getLineasDeCompra() {
        return lineasDeCompra;
    }

    public void setLineasDeCompra(ArrayList<LineaDeCompra> lineasDeCompra) {
        this.lineasDeCompra = lineasDeCompra;
    }

    public void agregarLineaDeCompra(LineaDeCompra linea) {
        lineasDeCompra.add(linea);
    }

    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public void calcularTotal() {
        float total = 0;
        for (int i = 0; i < getLineasDeCompra().size(); i++) {
            total = total + (this.getLineasDeCompra().get(i).getCantidad() * this.getLineasDeCompra().get(i).getArticulo().getCosto());
        }
        this.setTotal(total);
    }

    public String getFechaDeCompraString() {
        return fechaDeCompraString;
    }

    public void setFechaDeCompraString(String fechaDeCompraString) {
        this.fechaDeCompraString = fechaDeCompraString;
    }
}
