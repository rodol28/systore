package otros;

public class HorasExtra {
    private int legajo;
    private int horas50;
    private int horas100;

    public HorasExtra() {
    }

    public HorasExtra(int legajo, int horas50, int horas100) {
        this.legajo = legajo;
        this.horas50 = horas50;
        this.horas100 = horas100;
    }

    public int getLegajo() {
        return legajo;
    }

    public void setLegajo(int legajo) {
        this.legajo = legajo;
    }

    public int getHoras50() {
        return horas50;
    }

    public void setHoras50(int horas50) {
        this.horas50 = horas50;
    }

    public int getHoras100() {
        return horas100;
    }

    public void setHoras100(int horas100) {
        this.horas100 = horas100;
    }  
}
