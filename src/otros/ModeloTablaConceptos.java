package otros;

import javax.swing.table.DefaultTableModel;

public class ModeloTablaConceptos extends DefaultTableModel {


    public Class getColumnClass(int columna) {
        if (columna == 0) {
            return Boolean.class;
        }
        if (columna == 1) {
            return Object.class;
        }
        if (columna == 2) {
            return Object.class;
        }
        if (columna == 3) {
            return Object.class;
        }
        if (columna == 4) {
            return Object.class;
        }
        if (columna == 5) {
            return Object.class;
        }
        if (columna == 6) {
            return Object.class;
        }
        if (columna == 7) {
            return Object.class;
        }
        if (columna == 8) {
            return Object.class;
        }
        return Object.class;
    }

}
