package servicios;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Reportes {

    InputStream inputStream = null;
    JasperPrint jasperPrint = null;
//    AsistenteDataSource dataSource = new AsistenteDataSource();

    public void cargarAsistentes() throws FileNotFoundException, JRException {
//        for (int i = 0; i <= 5; i++) {
//            Asistente asist;
//            asist = new Asistente("" + i, "AsistenteNombre de: " + i, "AsistenteApellido de: " + i, "AsistenteDni de: " + i);
//            dataSource.addAsistente(asist);
//        }

        inputStream = new FileInputStream("src/reportes/reporte1.jrxml");

        JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
//        jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource);

        File directorio = new File("C://Users//Sherlock28//Desktop//Reportes");
        directorio.mkdirs();
        
        JasperExportManager.exportReportToPdfFile(jasperPrint, "C://Users//Sherlock28//Desktop//reportes/reporte1.pdf");

        JasperViewer jv = new JasperViewer(jasperPrint, false);
        jv.setTitle("Lista de asistentes");
        jv.setVisible(true);
    }
}
