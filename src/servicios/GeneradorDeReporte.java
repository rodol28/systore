package servicios;

import com.mysql.jdbc.Connection;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.ConexionReportes;

public class GeneradorDeReporte {

    public static void reporteDePersonas(int idPersona) throws JRException, ClassNotFoundException, SQLException {
        ConexionReportes con = new ConexionReportes();
        Connection conn = (Connection) con.getConexion();

        JasperReport reporte = (JasperReport) JRLoader.loadObject("ListaDeUsuarios.jasper");
        Map parametro = new HashMap();

        parametro.putIfAbsent("idPersona", idPersona);

        JasperPrint jp = JasperFillManager.fillReport(reporte, parametro, conn);
        JasperViewer jv = new JasperViewer(jp, false);
        jv.setTitle("Lista de persona");
        jv.setVisible(true);

        jv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
