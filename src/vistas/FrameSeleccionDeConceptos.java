package vistas;

import controladores.ControlFrameSeleccionDeConceptos;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import utilidadesParaFrames.UtilFrameSeleccionDeConceptos;

public class FrameSeleccionDeConceptos extends javax.swing.JDialog {

    public static final String BTN_BUSCAR = "buscar";
    public static final String BTN_ACEPTAR = "aceptar";
    public static final String BTN_CANCELAR = "cancelar";

    public FrameSeleccionDeConceptos(java.awt.Frame parent, boolean modal, ControlFrameSeleccionDeConceptos control) {
        super(parent, modal);
        initComponents();
        btnBuscar.setActionCommand(BTN_BUSCAR);
        btnAceptar.setActionCommand(BTN_ACEPTAR);
        btnCancelar.setActionCommand(BTN_CANCELAR);
        this.setLocationRelativeTo(null);

        btnBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                try {
                    keyEnterReleased(evt, control);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        });

        txtLegajo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                try {
                    keyEnterReleased(evt, control);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        });
    }

    private void keyEnterReleased(java.awt.event.KeyEvent evt, ControlFrameSeleccionDeConceptos control) throws SQLException, ClassNotFoundException {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            control.setIdPuesto(UtilFrameSeleccionDeConceptos.mostrarApeNombreYConceptos(this, this.getTxtLegajo(), this.getTablaConceptos()));
            control.setLegajo(this.getTxtLegajo());
        }
    }

    public void ejecutar() {
        this.setVisible(true);
    }

    public void setControlador(ControlFrameSeleccionDeConceptos control) {
        btnBuscar.addActionListener(control);
        btnAceptar.addActionListener(control);
        btnCancelar.addActionListener(control);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAceptar = new javax.swing.JButton();
        txtLegajo = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtApeNombre = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaConceptos = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txtPuesto = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SELECCIÓN DE CONCEPTOS");
        setResizable(false);

        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tick.png"))); // NOI18N
        btnAceptar.setText("Aceptar");

        jLabel1.setText("Legajo:");

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar.setText("Cancelar");

        jLabel2.setText("Apellido y nombre:");

        txtApeNombre.setEditable(false);
        txtApeNombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        tablaConceptos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "", "HR", "Codigo", "Abreviatura", "Descripcion"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tablaConceptos);

        jLabel3.setText("Puesto:");

        txtPuesto.setEditable(false);
        txtPuesto.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/find.png"))); // NOI18N
        btnBuscar.setText("Buscar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtLegajo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnBuscar))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtApeNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(41, 41, 41)
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(txtPuesto, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(201, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(btnAceptar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCancelar)))
                        .addGap(30, 30, 30))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLegajo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtApeNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(txtPuesto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancelar))
                .addGap(16, 16, 16))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaConceptos;
    private javax.swing.JTextField txtApeNombre;
    private javax.swing.JTextField txtLegajo;
    private javax.swing.JTextField txtPuesto;
    // End of variables declaration//GEN-END:variables

    public JTable getTablaConceptos() {
        return tablaConceptos;
    }

    public TableModel getModelo() {
        return tablaConceptos.getModel();
    }

    public boolean getValorFila(int fila) {
        return (boolean) tablaConceptos.getModel().getValueAt(fila, 0);
    }

    public void setTablaConceptos(JTable tablaConceptos) {
        this.tablaConceptos = tablaConceptos;
    }

    public int getTxtLegajo() {
        return Integer.parseInt(txtLegajo.getText());
    }

    public void setTxtLegajo(String txtLegajo) {
        this.txtLegajo.setText(txtLegajo);
    }

    public String getTxtPuesto() {
        return txtPuesto.getText();
    }

    public void setTxtPuesto(String txtPuesto) {
        this.txtPuesto.setText(txtPuesto);
    }

    public String getTxtApeNombre() {
        return txtApeNombre.getText();
    }

    public void setTxtApeNombre(String txtApeNombre) {
        this.txtApeNombre.setText(txtApeNombre);
    }
}
