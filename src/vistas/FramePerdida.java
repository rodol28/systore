package vistas;

import controladores.ControlFramePerdida;
import java.util.Calendar;
import javax.swing.JTable;
import javax.swing.JTextField;

public class FramePerdida extends javax.swing.JDialog {

    public static final String BTN_ACEPTAR = "aceptar";
    public static final String BTN_BUSCAR = "buscar";
    public static final String BTN_MODIFICAR_CANTIDAD = "modificar_cantidad";
    public static final String BTN_ELIMINAR_RENGLON = "eliminar_renglon";
    public static final String BTN_SALIR = "salir";

    public FramePerdida(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnAceptar.setActionCommand(BTN_ACEPTAR);
        btnBuscar.setActionCommand(BTN_BUSCAR);
        btnModificarCantidad.setActionCommand(BTN_MODIFICAR_CANTIDAD);
        btnEliminarRenglon.setActionCommand(BTN_ELIMINAR_RENGLON);
        btnSalir.setActionCommand(BTN_SALIR);
        fecha();
        this.setLocationRelativeTo(null);
    }

    public void ejecutar() {
        this.setVisible(true);
    }

    public void setContralador(ControlFramePerdida control) {
        btnAceptar.addActionListener(control);
        btnBuscar.addActionListener(control);
        btnModificarCantidad.addActionListener(control);
        btnEliminarRenglon.addActionListener(control);
        btnSalir.addActionListener(control);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtObservacion = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtVendedor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaLineasDePerdida = new javax.swing.JTable();
        btnModificarCantidad = new javax.swing.JButton();
        btnEliminarRenglon = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("NUEVA PERDIDA");
        setResizable(false);

        jLabel1.setText("Obs.:");

        txtObservacion.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel2.setText("Empleado:");

        txtVendedor.setEditable(false);
        txtVendedor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtVendedor.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel3.setText("Fecha:");

        txtFecha.setEditable(false);
        txtFecha.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel4.setText("Código:");

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/find.png"))); // NOI18N
        btnBuscar.setText("Buscar");

        tablaLineasDePerdida.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Motivo", "Cant.", "Cod.", "Descripcion", "Precio", "Stock ant.", "Actual"
            }
        ));
        jScrollPane1.setViewportView(tablaLineasDePerdida);

        btnModificarCantidad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/table_edit.png"))); // NOI18N

        btnEliminarRenglon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/delete.png"))); // NOI18N

        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tick.png"))); // NOI18N
        btnAceptar.setText("Aceptar");

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnSalir.setText("Cancelar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnBuscar)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(txtObservacion, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnModificarCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnEliminarRenglon)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnSalir)
                                .addGap(6, 6, 6)))
                        .addGap(39, 39, 39))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(37, 37, 37)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE)
                    .addGap(38, 38, 38)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(txtObservacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 351, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAceptar)
                        .addComponent(btnSalir))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnModificarCantidad)
                        .addComponent(btnEliminarRenglon)))
                .addGap(19, 19, 19))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(104, 104, 104)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(67, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEliminarRenglon;
    private javax.swing.JButton btnModificarCantidad;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaLineasDePerdida;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtObservacion;
    private javax.swing.JTextField txtVendedor;
    // End of variables declaration//GEN-END:variables

    public JTable getTablaLineasDePerdida() {
        return tablaLineasDePerdida;
    }

    public void setTablaLineasDePerdida(JTable tablaLineasDePerdida) {
        this.tablaLineasDePerdida = tablaLineasDePerdida;
    }

    public int getTxtCodigo() {
        return Integer.parseInt(txtCodigo.getText());
    }

    public void setTxtCodigo(String txtCodigo) {
        this.txtCodigo.setText(txtCodigo);
    }

    public String getTxtFecha() {
        return txtFecha.getText();
    }

    public void setTxtFecha(JTextField txtFecha) {
        this.txtFecha = txtFecha;
    }

    public String getTxtObservacion() {
        return txtObservacion.getText();
    }

    public void setTxtObservacion(String txtObservacion) {
        this.txtObservacion.setText(txtObservacion);
    }

    public int getTxtVendedor() {
        return Integer.parseInt(txtVendedor.getText());
    }

    public JTextField getJTextFieldTxtVendedor() {
        return txtVendedor;
    }

    public void setTxtVendedor(String txtVendedor) {
        this.txtVendedor.setText(txtVendedor);
    }

    public void fecha() {
        Calendar hoy = Calendar.getInstance();

        int dia = hoy.get(Calendar.DAY_OF_MONTH);
        int mes = hoy.get(Calendar.MONTH) + 1;
        int año = hoy.get(Calendar.YEAR);

        txtFecha.setText(dia + "/" + mes + "/" + año);
    }
}
