package vistas;

import controladores.ControlMenuPrincipal;
import java.util.Calendar;
import modelo.ImagenMDI;

public class MenuPrincipal extends javax.swing.JFrame {

    /*botones*/
    public static final String BTN_ARTICULOS = "articulos";
    public static final String BTN_COMPRAS = "compras";
    public static final String BTN_VENTAS = "ventas";
    public static final String BTN_CONSULTAR_VENTAS = "consultarVentas";
    public static final String BTN_LIQUIDACION = "liquidacion";
    public static final String BTN_EMPLEADOS = "empleados";
    /*menues*/
    public static final String MENU_ABM_ARTICULO = "abmArticulo";
//    public static final String MENU_ACTUALIZAR_PRECIO_ARTICULO = "actualizarPrecios";
    public static final String MENU_ABM_CLIENTE = "abmCliente";
    public static final String MENU_ABM_PROVEEDORES = "abmProveedores";
    public static final String MENU_INGRESO_DE_MERCADERIA = "IngresoDeCompra";
    public static final String MENU_CONSULTAR_COMPRAS = "consultarCompra";
    public static final String MENU_FACTURA = "factura";
    public static final String MENU_CONSULTAR_VENTAS = "consultarVentas";
    public static final String MENU_SALIDAS = "salidas";
    public static final String MENU_CONSULTAR_SALIDAS = "consultarSalidas";
    public static final String MENU_LIQUIDACION = "liquidacion";
    public static final String MENU_GESTION_CONCEPTOS = "gestionConceptos";
    public static final String MENU_SELECCION_CONCEPTOS = "seleccionConceptos";
    public static final String MENU_GESTION_PUESTOS = "gestionPuestos";
    public static final String MENU_GESTION_EMPLEADOS = "gestionEmpleados";
    public static final String MENU_GESTION_USUARIOS = "gestionUsuarios";
    public static final String MENU_ABM_MARCAS = "gestionDeMarcas";
    public static final String MENU_ABM_OBRAS_SOCIALES = "gestionDeObrasSociales";
    public static final String MENU_ABM_RUBROS = "gestionDeRubros";
    public static final String MENU_ABM_MOTIVOS_PERDIDA = "motivoDePerdida";
    public static final String MENU_SALIR = "salir";

    public static final String MENU_REPORTE_DE_PRUEBA = "reporteDePrueba";
    public static final String MENU_REPORTE_VENTAS_TOTALES = "reporteVentasTotales";
    public static final String MENU_REPORTE_LISTA_ARTICULOS_GRAL = "reporteDeArticulosGral";
    public static final String MENU_REPORTE_BAJO_STOCK_GRAL = "reporteDeBajoStockGral";
    public static final String MENU_REPORTE_LISTA_DE_PROVEEDORES = "reporteListaDeProveedores";
    public static final String MENU_REPORTE_LISTA_DE_CLIENTES = "reporteDeClientes";

    public MenuPrincipal() {

        initComponents();
        fecha();
        panelPrincipal.setBorder(new ImagenMDI());
        /*botones*/
        btnArticulos.setActionCommand(BTN_ARTICULOS);
        btnCompras.setActionCommand(BTN_COMPRAS);
        btnVentas.setActionCommand(BTN_VENTAS);
        btnConsultarVentas.setActionCommand(BTN_CONSULTAR_VENTAS);
        btnLiquidacion.setActionCommand(BTN_LIQUIDACION);
        btnEmpleados.setActionCommand(BTN_EMPLEADOS);
        /*menues*/
        mAgregarModificarArticulo.setActionCommand(MENU_ABM_ARTICULO);
//        mActualizarPrecios.setActionCommand(MENU_ACTUALIZAR_PRECIO_ARTICULO);
        mAgregarModificarCliente.setActionCommand(MENU_ABM_CLIENTE);
        mAgregarModificarProveedor.setActionCommand(MENU_ABM_PROVEEDORES);
        mIngresoDeMercaderia.setActionCommand(MENU_INGRESO_DE_MERCADERIA);
        mConsultarCompras.setActionCommand(MENU_CONSULTAR_COMPRAS);
        mFactura.setActionCommand(MENU_FACTURA);
        mConsultarVentas.setActionCommand(MENU_CONSULTAR_VENTAS);
        mSalidas.setActionCommand(MENU_SALIDAS);
        mConsultarSalidas.setActionCommand(MENU_CONSULTAR_SALIDAS);
        mLiquidacion.setActionCommand(MENU_LIQUIDACION);
        mGestionConceptos.setActionCommand(MENU_GESTION_CONCEPTOS);
        mSeleccionDeConcepto.setActionCommand(MENU_SELECCION_CONCEPTOS);
        mGestionPuestos.setActionCommand(MENU_GESTION_PUESTOS);
        mGestionEmpleados.setActionCommand(MENU_GESTION_EMPLEADOS);
        mGestionUsuarios.setActionCommand(MENU_GESTION_USUARIOS);
        mMarcas.setActionCommand(MENU_ABM_MARCAS);
        mRubros.setActionCommand(MENU_ABM_RUBROS);
        mObraSocial.setActionCommand(MENU_ABM_OBRAS_SOCIALES);
        mMotivoDeSalida.setActionCommand(MENU_ABM_MOTIVOS_PERDIDA);
        mSalir.setActionCommand(MENU_SALIR);

//        mPrueba.setActionCommand(MENU_REPORTE_DE_PRUEBA);
        mVentasTotales.setActionCommand(MENU_REPORTE_VENTAS_TOTALES);
        mListaArticulosGral.setActionCommand(MENU_REPORTE_LISTA_ARTICULOS_GRAL);
        mBajoStockPorDes.setActionCommand(MENU_REPORTE_BAJO_STOCK_GRAL);
        mListaDeProveedores.setActionCommand(MENU_REPORTE_LISTA_DE_PROVEEDORES);
        mListaDeClientes.setActionCommand(MENU_REPORTE_LISTA_DE_CLIENTES);
    }

    public void ejecutar() {
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

    public void setControlador(ControlMenuPrincipal control) {
        /*botones*/
        btnArticulos.addActionListener(control);
        btnCompras.addActionListener(control);
        btnVentas.addActionListener(control);
        btnConsultarVentas.addActionListener(control);
        btnLiquidacion.addActionListener(control);
        btnEmpleados.addActionListener(control);
        /*menues*/
        mAgregarModificarArticulo.addActionListener(control);
//        mActualizarPrecios.addActionListener(control);
        mAgregarModificarCliente.addActionListener(control);
        mAgregarModificarProveedor.addActionListener(control);
        mIngresoDeMercaderia.addActionListener(control);
        mConsultarCompras.addActionListener(control);
        mFactura.addActionListener(control);
        mConsultarVentas.addActionListener(control);
        mSalidas.addActionListener(control);
        mConsultarSalidas.addActionListener(control);
        mLiquidacion.addActionListener(control);
        mGestionConceptos.addActionListener(control);
        mSeleccionDeConcepto.addActionListener(control);
        mGestionEmpleados.addActionListener(control);
        mGestionPuestos.addActionListener(control);
        mGestionUsuarios.addActionListener(control);
        mMarcas.addActionListener(control);
        mObraSocial.addActionListener(control);
        mRubros.addActionListener(control);
        mMotivoDeSalida.addActionListener(control);
        mSalir.addActionListener(control);

//        mPrueba.addActionListener(control);
        mVentasTotales.addActionListener(control);
        mListaArticulosGral.addActionListener(control);
        mBajoStockPorDes.addActionListener(control);
        mListaDeProveedores.addActionListener(control);
        mListaDeClientes.addActionListener(control);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelPrincipal = new javax.swing.JPanel();
        btnArticulos = new javax.swing.JButton();
        btnVentas = new javax.swing.JButton();
        btnEmpleados = new javax.swing.JButton();
        btnCompras = new javax.swing.JButton();
        btnLiquidacion = new javax.swing.JButton();
        btnConsultarVentas = new javax.swing.JButton();
        lblUsuario = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mAgregarModificarArticulo = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mAgregarModificarCliente = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        mAgregarModificarProveedor = new javax.swing.JMenuItem();
        jMenu9 = new javax.swing.JMenu();
        mIngresoDeMercaderia = new javax.swing.JMenuItem();
        mConsultarCompras = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        mFactura = new javax.swing.JMenuItem();
        mConsultarVentas = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        mSalidas = new javax.swing.JMenuItem();
        mConsultarSalidas = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenu10 = new javax.swing.JMenu();
        mVentasTotales = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        mListaArticulosGral = new javax.swing.JMenuItem();
        mBajoStockPorDes = new javax.swing.JMenuItem();
        jMenu12 = new javax.swing.JMenu();
        mListaDeProveedores = new javax.swing.JMenuItem();
        jMenu13 = new javax.swing.JMenu();
        mListaDeClientes = new javax.swing.JMenuItem();
        personal = new javax.swing.JMenu();
        mLiquidacion = new javax.swing.JMenuItem();
        mGestionConceptos = new javax.swing.JMenuItem();
        mSeleccionDeConcepto = new javax.swing.JMenuItem();
        mGestionPuestos = new javax.swing.JMenuItem();
        mGestionEmpleados = new javax.swing.JMenuItem();
        mAdmistracion = new javax.swing.JMenu();
        mGestionUsuarios = new javax.swing.JMenuItem();
        jMenu11 = new javax.swing.JMenu();
        mMarcas = new javax.swing.JMenuItem();
        mMotivoDeSalida = new javax.swing.JMenuItem();
        mObraSocial = new javax.swing.JMenuItem();
        mRubros = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        mSalir = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("MENÚ PRINCIPAL");
        setModalExclusionType(null);
        setResizable(false);

        btnArticulos.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnArticulos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/package.png"))); // NOI18N
        btnArticulos.setText("Artículos");

        btnVentas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnVentas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/venta1.png"))); // NOI18N
        btnVentas.setText("Ventas");

        btnEmpleados.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnEmpleados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/empleado.png"))); // NOI18N
        btnEmpleados.setText("Empleados");

        btnCompras.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnCompras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/compra.png"))); // NOI18N
        btnCompras.setText("Compras");

        btnLiquidacion.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnLiquidacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/receipt.png"))); // NOI18N
        btnLiquidacion.setText("Liquidación");
        btnLiquidacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLiquidacionActionPerformed(evt);
            }
        });

        btnConsultarVentas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnConsultarVentas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/search_find.png"))); // NOI18N
        btnConsultarVentas.setText("Consulta \nde ventas");

        lblUsuario.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lblUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/user.png"))); // NOI18N
        lblUsuario.setText("Usuario:");

        lblFecha.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N
        lblFecha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/date.png"))); // NOI18N
        lblFecha.setText("Fecha:");

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addComponent(lblUsuario)
                        .addGap(41, 41, 41)
                        .addComponent(lblFecha))
                    .addGroup(panelPrincipalLayout.createSequentialGroup()
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnArticulos, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
                            .addComponent(btnCompras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(42, 42, 42)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnConsultarVentas, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnVentas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(42, 42, 42)
                        .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnEmpleados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnLiquidacion, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE))))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnArticulos, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCompras, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConsultarVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLiquidacion, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUsuario)
                    .addComponent(lblFecha))
                .addGap(17, 17, 17))
        );

        jMenuBar1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMenu1.setText("Artículos");

        mAgregarModificarArticulo.setText("Agregar/Modificar");
        jMenu1.add(mAgregarModificarArticulo);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Clientes");

        mAgregarModificarCliente.setText("Agregar/Modificar");
        jMenu2.add(mAgregarModificarCliente);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Proveedores");

        mAgregarModificarProveedor.setText("Agregar/Modificar");
        jMenu3.add(mAgregarModificarProveedor);

        jMenuBar1.add(jMenu3);

        jMenu9.setText("Compras");

        mIngresoDeMercaderia.setText("Ingreso de mercaderia");
        jMenu9.add(mIngresoDeMercaderia);

        mConsultarCompras.setText("Consultar compras");
        jMenu9.add(mConsultarCompras);

        jMenuBar1.add(jMenu9);

        jMenu4.setText("Ventas");

        mFactura.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, 0));
        mFactura.setText("Venta");
        jMenu4.add(mFactura);

        mConsultarVentas.setText("Consultar ventas");
        jMenu4.add(mConsultarVentas);

        jMenuBar1.add(jMenu4);

        jMenu6.setText("Salidas de mercaderia");

        mSalidas.setText("Perdidas de mercaderia");
        jMenu6.add(mSalidas);

        mConsultarSalidas.setText("Consulta de perdidas");
        jMenu6.add(mConsultarSalidas);

        jMenuBar1.add(jMenu6);

        jMenu7.setText("Reportes");

        jMenu10.setText("Ventas");

        mVentasTotales.setText("Ventas totales ");
        jMenu10.add(mVentasTotales);

        jMenu7.add(jMenu10);

        jMenu5.setText("Artículos");

        mListaArticulosGral.setText("Listado Gral por descripción");
        jMenu5.add(mListaArticulosGral);

        mBajoStockPorDes.setText("Bajo stock por descripción");
        jMenu5.add(mBajoStockPorDes);

        jMenu7.add(jMenu5);

        jMenu12.setText("Proveedores");

        mListaDeProveedores.setText("Lista de proveedores");
        jMenu12.add(mListaDeProveedores);

        jMenu7.add(jMenu12);

        jMenu13.setText("Clientes");

        mListaDeClientes.setText("Lista de clientes");
        jMenu13.add(mListaDeClientes);

        jMenu7.add(jMenu13);

        jMenuBar1.add(jMenu7);

        personal.setText("Personal");

        mLiquidacion.setText("Liquidación");
        personal.add(mLiquidacion);

        mGestionConceptos.setText("Gestión de conceptos");
        personal.add(mGestionConceptos);

        mSeleccionDeConcepto.setText("Selección de conceptos");
        personal.add(mSeleccionDeConcepto);

        mGestionPuestos.setText("Gestión de puestos");
        personal.add(mGestionPuestos);

        mGestionEmpleados.setText("Gestión de empleados");
        personal.add(mGestionEmpleados);

        jMenuBar1.add(personal);

        mAdmistracion.setText("Administración");

        mGestionUsuarios.setText("Usuarios");
        mGestionUsuarios.setInheritsPopupMenu(true);
        mAdmistracion.add(mGestionUsuarios);

        jMenu11.setText("Tablas auxiliares");

        mMarcas.setText("Marcas");
        jMenu11.add(mMarcas);

        mMotivoDeSalida.setText("Motivos de perdida");
        jMenu11.add(mMotivoDeSalida);

        mObraSocial.setText("Obras sociales");
        jMenu11.add(mObraSocial);

        mRubros.setText("Rubros");
        jMenu11.add(mRubros);

        mAdmistracion.add(jMenu11);

        jMenuBar1.add(mAdmistracion);

        jMenu8.setText("Salida");

        mSalir.setText("Salir");
        jMenu8.add(mSalir);

        jMenuBar1.add(jMenu8);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLiquidacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLiquidacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnLiquidacionActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnArticulos;
    private javax.swing.JButton btnCompras;
    private javax.swing.JButton btnConsultarVentas;
    private javax.swing.JButton btnEmpleados;
    private javax.swing.JButton btnLiquidacion;
    private javax.swing.JButton btnVentas;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu10;
    private javax.swing.JMenu jMenu11;
    private javax.swing.JMenu jMenu12;
    private javax.swing.JMenu jMenu13;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JMenu mAdmistracion;
    private javax.swing.JMenuItem mAgregarModificarArticulo;
    private javax.swing.JMenuItem mAgregarModificarCliente;
    private javax.swing.JMenuItem mAgregarModificarProveedor;
    private javax.swing.JMenuItem mBajoStockPorDes;
    private javax.swing.JMenuItem mConsultarCompras;
    private javax.swing.JMenuItem mConsultarSalidas;
    private javax.swing.JMenuItem mConsultarVentas;
    private javax.swing.JMenuItem mFactura;
    private javax.swing.JMenuItem mGestionConceptos;
    private javax.swing.JMenuItem mGestionEmpleados;
    private javax.swing.JMenuItem mGestionPuestos;
    private javax.swing.JMenuItem mGestionUsuarios;
    private javax.swing.JMenuItem mIngresoDeMercaderia;
    private javax.swing.JMenuItem mLiquidacion;
    private javax.swing.JMenuItem mListaArticulosGral;
    private javax.swing.JMenuItem mListaDeClientes;
    private javax.swing.JMenuItem mListaDeProveedores;
    private javax.swing.JMenuItem mMarcas;
    private javax.swing.JMenuItem mMotivoDeSalida;
    private javax.swing.JMenuItem mObraSocial;
    private javax.swing.JMenuItem mRubros;
    private javax.swing.JMenuItem mSalidas;
    private javax.swing.JMenuItem mSalir;
    private javax.swing.JMenuItem mSeleccionDeConcepto;
    private javax.swing.JMenuItem mVentasTotales;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JMenu personal;
    // End of variables declaration//GEN-END:variables

    public void fecha() {
        Calendar hoy = Calendar.getInstance();

        int dia = hoy.get(Calendar.DAY_OF_MONTH);
        int mes = hoy.get(Calendar.MONTH) + 1;
        int año = hoy.get(Calendar.YEAR);

        lblFecha.setText("Fecha: " + dia + "/" + mes + "/" + año);
    }

    public void setLabelUsuario(String usuario) {
        lblUsuario.setText("Usuario: " + usuario);
    }

    public void desactivarOpcionesDeAdministrador() {
        personal.setEnabled(false);
        btnEmpleados.setEnabled(false);
        btnLiquidacion.setEnabled(false);
        mAdmistracion.setEnabled(false);
    }

}
