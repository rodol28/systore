package vistas;

import controladores.ControlFrameConsultarPerdidas;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.JTextField;
import utilidadesParaFrames.UtilFrameConsultarMotivoDePerdida;

public class FrameConsultarPerdidas extends javax.swing.JDialog {

    public static final String BTN_BUSCAR_POR_NRO_PERDIDA = "buscarProMotivo";
    public static final String BTN_SALIR = "salir";

    public FrameConsultarPerdidas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnBuscarPorMotivo.setActionCommand(BTN_BUSCAR_POR_NRO_PERDIDA);
        btnSalir.setActionCommand(BTN_SALIR);

        tablaPerdidas.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 1) {
                    try {
                        int fila = tablaPerdidas.getSelectedRow();
                        int idPerdida = Integer.parseInt((String) tablaPerdidas.getModel().getValueAt(fila, 0));
                        txtNroPerdida.setText("" + idPerdida);
                        UtilFrameConsultarMotivoDePerdida.mostrarFecha(idPerdida, txtFecha);
                        UtilFrameConsultarMotivoDePerdida.mostrarLineasDePerdida(tablaLineasDePerdida, txtFecha, idPerdida);
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            }
        });

        this.setLocationRelativeTo(null);
    }

    public void ejecutar() {
        this.setVisible(true);
    }

    public void setContralador(ControlFrameConsultarPerdidas control) {
        btnBuscarPorMotivo.addActionListener(control);
        btnSalir.addActionListener(control);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tablaPerdidas = new javax.swing.JTable();
        btnSalir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtNroPerdida = new javax.swing.JTextField();
        txtFecha = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaLineasDePerdida = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        btnBuscarPorMotivo = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("CONSULTAR PERDIDAS");
        setResizable(false);

        tablaPerdidas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Nro. Perdida", "Fecha", "Nro. Empleado", "Costo total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tablaPerdidas);

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/door_out.png"))); // NOI18N
        btnSalir.setText("Salir");

        jLabel1.setText("Nro. Perdida:");

        txtNroPerdida.setEditable(false);
        txtNroPerdida.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtNroPerdida.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        txtFecha.setEditable(false);
        txtFecha.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel3.setText("Fecha:");

        tablaLineasDePerdida.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Motivo", "Cant.", "Codigo", "Descripcion", "Precio"
            }
        ));
        jScrollPane2.setViewportView(tablaLineasDePerdida);

        jMenu1.setText("Búsqueda");

        btnBuscarPorMotivo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        btnBuscarPorMotivo.setText("por Nro. Perdida");
        jMenu1.add(btnBuscarPorMotivo);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSalir, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtNroPerdida, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNroPerdida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem btnBuscarPorMotivo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tablaLineasDePerdida;
    private javax.swing.JTable tablaPerdidas;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtNroPerdida;
    // End of variables declaration//GEN-END:variables

    public JTable getTablaLineasDePerdida() {
        return tablaLineasDePerdida;
    }

    public void setTablaLineasDePerdida(JTable tablaLineasDePerdida) {
        this.tablaLineasDePerdida = tablaLineasDePerdida;
    }

    public JTable getTablaPerdidas() {
        return tablaPerdidas;
    }

    public void setTablaPerdidas(JTable tablaPerdidas) {
        this.tablaPerdidas = tablaPerdidas;
    }

    public String getTxtFecha() {
        return txtFecha.getText();
    }

    public JTextField getJTextFieldFecha() {
        return txtFecha;
    }

    public void setTxtFecha(String txtFecha) {
        this.txtFecha.setText(txtFecha);
    }

    public String getTxtNroPerdida() {
        return txtNroPerdida.getText();
    }
    
    public JTextField getJTextFieldNroPerdida() {
        return txtNroPerdida;
    }

    public void setTxtNroPerdida(String txtNroPerdida) {
        this.txtNroPerdida.setText(txtNroPerdida);
    }
}
