package vistas;

import controladores.ControlFrameFactura;
import java.sql.SQLException;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import utilidadesParaFrames.UtilFrameFactura;

public class FrameFactura extends java.awt.Dialog {

    public static final String BTN_ACEPTAR = "aceptar";
    public static final String BTN_BUSCAR = "buscar";
    public static final String BTN_MODIFICAR_CANTIDAD = "modificar_cantidad";
    public static final String BTN_ELIMINAR_RENGLON = "eliminar_renglon";
    public static final String BTN_SALIR = "salir";

    public FrameFactura(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnAceptar.setActionCommand(BTN_ACEPTAR);
        btnBuscar.setActionCommand(BTN_BUSCAR);
        btnModificarCantidad.setActionCommand(BTN_MODIFICAR_CANTIDAD);
        btnEliminarRenglon.setActionCommand(BTN_ELIMINAR_RENGLON);
        btnSalir.setActionCommand(BTN_SALIR);
        fecha();
        setTitle("VENTA DE MERCADERIA");

        txtCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                try {
                    keyEnterReleased(evt);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        });
        this.setLocationRelativeTo(null);
    }

    private void keyEnterReleased(java.awt.event.KeyEvent evt) throws SQLException, ClassNotFoundException {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            boolean existe = verificarSiExiste(getTablaLineasDeVenta(), getTxtCodigo());
            if (existe) {
                JOptionPane.showMessageDialog(null, "Este articulo ya fue seleccionado.");
            } else {
                UtilFrameFactura.agregarArticuloEnTabla(getTxtCodigo(), tablaLineasDeVenta, this);
                setTxtCodigo("");
            }
        }
    }

    public boolean verificarSiExiste(JTable tablaArticulos, int idArticulo) {
        int cantidadDeFilas = UtilFrameFactura.obtenerLaCantidadDeFilasDeLaTabla(tablaArticulos);
        boolean sonIguales = false;
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            int codigo = Integer.parseInt("" + tablaArticulos.getModel().getValueAt(fila, 1));
            if (codigo == idArticulo) {
                sonIguales = true;
            }
        }
        return sonIguales;
    }

    public void ejecutar() {
        this.setVisible(true);
    }

    public void setContralador(ControlFrameFactura control) {
        btnAceptar.addActionListener(control);
        btnBuscar.addActionListener(control);
        btnModificarCantidad.addActionListener(control);
        btnEliminarRenglon.addActionListener(control);
        btnSalir.addActionListener(control);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtNroFactura = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtVendedor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaLineasDeVenta = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        btnModificarCantidad = new javax.swing.JButton();
        btnEliminarRenglon = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        txtFecha = new javax.swing.JTextField();

        setResizable(false);
        setTitle("Venta de mercaderia");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jLabel1.setText("Nro. factura:");

        txtNroFactura.setEditable(false);
        txtNroFactura.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtNroFactura.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel2.setText("Vendedor:");

        txtVendedor.setEditable(false);
        txtVendedor.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtVendedor.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel3.setText("Fecha:");

        jLabel4.setText("Código:");

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/find.png"))); // NOI18N
        btnBuscar.setText("Buscar");

        tablaLineasDeVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cantidad", "Codigo", "Descripción", "$ Unit.", "$ Subtotal"
            }
        ));
        jScrollPane1.setViewportView(tablaLineasDeVenta);

        jLabel5.setText("Total:");

        txtTotal.setEditable(false);
        txtTotal.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        btnModificarCantidad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/table_edit.png"))); // NOI18N

        btnEliminarRenglon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/delete.png"))); // NOI18N
        btnEliminarRenglon.setToolTipText("");

        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/table_save.png"))); // NOI18N
        btnAceptar.setText("Registrar");

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnSalir.setText("Cancelar");

        txtFecha.setEditable(false);
        txtFecha.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 590, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnBuscar))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNroFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnModificarCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminarRenglon)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAceptar)
                        .addGap(24, 24, 24)
                        .addComponent(btnSalir)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNroFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAceptar)
                        .addComponent(btnSalir))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnModificarCantidad)
                        .addComponent(btnEliminarRenglon)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEliminarRenglon;
    private javax.swing.JButton btnModificarCantidad;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaLineasDeVenta;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtNroFactura;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtVendedor;
    // End of variables declaration//GEN-END:variables

    public JTable getTablaLineasDeVenta() {
        return tablaLineasDeVenta;
    }

    public void setTablaLineasDeVenta(JTable tablaLineasDeVenta) {
        this.tablaLineasDeVenta = tablaLineasDeVenta;
    }

    public int getTxtCodigo() {
        return Integer.parseInt(txtCodigo.getText());
    }

    public void setTxtCodigo(String txtCodigo) {
        this.txtCodigo.setText(txtCodigo);
    }

    public String getTxtFecha() {
        return txtFecha.getText();
    }

    public void setTxtFecha(String txtFecha) {
        this.txtFecha.setText(txtFecha);
    }

    public int getTxtNroFactura() {
        return Integer.parseInt(txtNroFactura.getText());
    }

    public void setTxtNroFactura(String txtNroFactura) {
        this.txtNroFactura.setText(txtNroFactura);
    }

    public float getTxtTotal() {
        return Float.parseFloat(txtTotal.getText());
    }

    public void setTxtTotal(String txtTotal) {
        this.txtTotal.setText(txtTotal);
    }

    public int getTxtVendedor() {
        return Integer.parseInt(txtVendedor.getText());
    }

    public JTextField getJTextFieldVendedor() {
        return txtVendedor;
    }

    public void setTxtVendedor(String txtVendedor) {
        this.txtVendedor.setText(txtVendedor);
    }

    public void fecha() {
        Calendar hoy = Calendar.getInstance();

        int dia = hoy.get(Calendar.DAY_OF_MONTH);
        int mes = hoy.get(Calendar.MONTH) + 1;
        int año = hoy.get(Calendar.YEAR);

        txtFecha.setText(dia + "/" + mes + "/" + año);
    }
}
