package vistas;

import controladores.ControlFrameConsultarCompras;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.JTextField;
import utilidadesParaFrames.UtilFrameConsultarCompras;

public class FrameConsultarCompras extends javax.swing.JDialog {

    public static final String BTN_SALIR = "salir";
    public static final String MENU_BTN_BUSCAR_POR_FAC = "buscarPorNroFac";

    public FrameConsultarCompras(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnSalir.setActionCommand(BTN_SALIR);
        menuBtnBuscarPorNroFac.setActionCommand(MENU_BTN_BUSCAR_POR_FAC);

        tablaCompras.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 1) {
                    try {
                        int fila = tablaCompras.getSelectedRow();
                        int idCompra = Integer.parseInt((String) tablaCompras.getModel().getValueAt(fila, 0));
                        txtNroFactura.setText("" + idCompra);
                        UtilFrameConsultarCompras.mostrarFecha(idCompra, txtFecha);
                        UtilFrameConsultarCompras.mostrarLineasDeCompra(tablaLineasDeCompras, idCompra);
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            }
        }
        );

        this.setLocationRelativeTo(null);
    }

    public void ejecutar() {
        this.setVisible(true);
    }

    public void setControlador(ControlFrameConsultarCompras control) {
        btnSalir.addActionListener(control);
        menuBtnBuscarPorNroFac.addActionListener(control);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tablaCompras = new javax.swing.JTable();
        btnSalir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtNroFactura = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaLineasDeCompras = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuBtnBuscarPorNroFac = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("CONSULTAR COMPRAS");

        tablaCompras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Nro. Comp.", "Fecha", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tablaCompras);

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/door_out.png"))); // NOI18N
        btnSalir.setText("Salir");

        jLabel1.setText("Nro. factura:");

        txtNroFactura.setEditable(false);
        txtNroFactura.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtNroFactura.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel3.setText("Fecha:");

        txtFecha.setEditable(false);
        txtFecha.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        tablaLineasDeCompras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cantidad", "Descripción", "Subtotal"
            }
        ));
        jScrollPane2.setViewportView(tablaLineasDeCompras);

        jMenu1.setText("Búsqueda");

        menuBtnBuscarPorNroFac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        menuBtnBuscarPorNroFac.setText("nro de Factura");
        jMenu1.add(menuBtnBuscarPorNroFac);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 530, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSalir, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtNroFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNroFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenuItem menuBtnBuscarPorNroFac;
    private javax.swing.JTable tablaCompras;
    private javax.swing.JTable tablaLineasDeCompras;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtNroFactura;
    // End of variables declaration//GEN-END:variables

    public JTable getTablaCompras() {
        return tablaCompras;
    }

    public void setTablaCompras(JTable tablaCompras) {
        this.tablaCompras = tablaCompras;
    }

    public JTable getTablaLineasDeCompras() {
        return tablaLineasDeCompras;
    }

    public void setTablaLineasDeCompras(JTable tablaLineasDeCompras) {
        this.tablaLineasDeCompras = tablaLineasDeCompras;
    }

    public int getTxtFactura() {
        return Integer.parseInt(txtNroFactura.getText());
    }

    public JTextField getJTextFieldFactura() {
        return txtNroFactura;
    }

    public void setTxtFactura(String txtFactura) {
        this.txtNroFactura.setText(txtFactura);
    }

    public String getTxtFecha() {
        return txtFecha.getText();
    }
    
    public JTextField getJTextFieldFecha() {
        return txtFecha;
    }

    public void setTxtFecha(String txtFecha) {
        this.txtFecha.setText(txtFecha);
    }
}
