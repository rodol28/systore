package vistas;

import controladores.ControlFrameIngresoDeMercaderia;
import java.sql.SQLException;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import utilidadesParaFrames.UtilFrameIngresoDeMercaderia;

public class FrameIngresoDeMercaderia extends java.awt.Dialog {

    public static final String BTN_BUSCAR = "buscarArticulo";
    public static final String BTN_MODIFICAR_CANTIDAD = "modificarCantidad";
    public static final String BTN_MODIFICAR_PRECIO = "modificarPrecio";
    public static final String BTN_NUEVO_ARTICULO = "nuevoArticulo";
    public static final String BTN_SUPRIMIR_FILA = "suprimirFila";
    public static final String BTN_ACEPTAR = "aceptar";
    public static final String BTN_SALIR = "salir";

    public FrameIngresoDeMercaderia(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnBuscar.setActionCommand(BTN_BUSCAR);
        btnCambiarCantidad.setActionCommand(BTN_MODIFICAR_CANTIDAD);
        btnCambiarPrecio.setActionCommand(BTN_MODIFICAR_PRECIO);
        btnNuevoArticulo.setActionCommand(BTN_NUEVO_ARTICULO);
        btnSuprimirFila.setActionCommand(BTN_SUPRIMIR_FILA);
        btnAceptar.setActionCommand(BTN_ACEPTAR);
        btnSalir.setActionCommand(BTN_SALIR);
        setTitle("INGRESO DE MERCADERIA");

        txtCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                try {
                    keyEnterReleased(evt);
                } catch (SQLException ex) {
                } catch (ClassNotFoundException ex) {
                }
            }
        });
        this.setLocationRelativeTo(null);
    }

    private void keyEnterReleased(java.awt.event.KeyEvent evt) throws SQLException, ClassNotFoundException {
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            boolean existe = verificarSiExiste(getTablaArticulos(), getTxtCodigo());
            if (existe) {
                JOptionPane.showMessageDialog(null, "Este articulo ya fue seleccionado.");
            } else {
                UtilFrameIngresoDeMercaderia.agregarArticuloEnTabla(getTxtCodigo(), tablaArticulos, true);
                setTxtCodigo("");
            }
        }
    }

    public boolean verificarSiExiste(JTable tablaArticulos, int idArticulo) {
        int cantidadDeFilas = UtilFrameIngresoDeMercaderia.obtenerLaCantidadDeFilasDeLaTabla(tablaArticulos);
        boolean sonIguales = false;
        for (int fila = 0; fila < cantidadDeFilas; fila++) {
            int codigo = Integer.parseInt("" + tablaArticulos.getModel().getValueAt(fila, 1));
            if (codigo == idArticulo) {
                sonIguales = true;
            }
        }
        return sonIguales;
    }

    public void ejecutar() {
        this.setVisible(true);
    }

    public void setControlador(ControlFrameIngresoDeMercaderia control) {
        btnBuscar.addActionListener(control);
        btnCambiarCantidad.addActionListener(control);
        btnCambiarPrecio.addActionListener(control);
        btnNuevoArticulo.addActionListener(control);
        btnSuprimirFila.addActionListener(control);
        btnAceptar.addActionListener(control);
        btnSalir.addActionListener(control);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jLabel1 = new javax.swing.JLabel();
        txtNroCompra = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaArticulos = new javax.swing.JTable();
        btnCambiarCantidad = new javax.swing.JButton();
        btnCambiarPrecio = new javax.swing.JButton();
        btnNuevoArticulo = new javax.swing.JButton();
        btnSuprimirFila = new javax.swing.JButton();
        btnAceptar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        comboProvedores = new javax.swing.JComboBox<>();
        jdcFechaDeCompra = new com.toedter.calendar.JDateChooser();

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setResizable(false);
        setTitle("Ingreso de mercaderia");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jLabel1.setText("Nro. Compra:");
        jLabel1.setToolTipText("");

        txtNroCompra.setEditable(false);
        txtNroCompra.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtNroCompra.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtNroCompra.setSelectedTextColor(new java.awt.Color(0, 0, 0));

        jLabel2.setText("Fecha:");

        jLabel3.setText("Código:");

        txtCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodigoKeyPressed(evt);
            }
        });

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/find.png"))); // NOI18N
        btnBuscar.setText("Buscar");

        tablaArticulos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cant.", "Cod.", "Descripción", "Marca", "Costo", "Precio de venta", "Stock Ant.", "Actual"
            }
        ));
        jScrollPane1.setViewportView(tablaArticulos);

        btnCambiarCantidad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/table_edit.png"))); // NOI18N

        btnCambiarPrecio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/edit.png"))); // NOI18N

        btnNuevoArticulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/page_add.png"))); // NOI18N

        btnSuprimirFila.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/delete.png"))); // NOI18N

        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/table_save.png"))); // NOI18N
        btnAceptar.setText("Registrar");

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnSalir.setText("Cancelar");

        jLabel4.setText("Proveedor:");

        comboProvedores.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnBuscar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel4))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNroCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(182, 182, 182)
                                .addComponent(jLabel2)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboProvedores, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jdcFechaDeCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnCambiarCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnCambiarPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnNuevoArticulo, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnSuprimirFila)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnAceptar)
                            .addGap(21, 21, 21)
                            .addComponent(btnSalir))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 738, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtNroCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addComponent(jdcFechaDeCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscar)
                    .addComponent(jLabel4)
                    .addComponent(comboProvedores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCambiarCantidad)
                    .addComponent(btnCambiarPrecio)
                    .addComponent(btnNuevoArticulo)
                    .addComponent(btnSuprimirFila)
                    .addComponent(btnAceptar)
                    .addComponent(btnSalir))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        dispose();
    }//GEN-LAST:event_closeDialog

    private void txtCodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyPressed

    }//GEN-LAST:event_txtCodigoKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCambiarCantidad;
    private javax.swing.JButton btnCambiarPrecio;
    private javax.swing.JButton btnNuevoArticulo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnSuprimirFila;
    private javax.swing.JComboBox<String> comboProvedores;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private com.toedter.calendar.JDateChooser jdcFechaDeCompra;
    private javax.swing.JTable tablaArticulos;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtNroCompra;
    // End of variables declaration//GEN-END:variables

    public JTable getTablaArticulos() {
        return tablaArticulos;
    }

    public void setTablaArticulos(JTable tablaArticulos) {
        this.tablaArticulos = tablaArticulos;
    }

    public int getTxtCodigo() {
        return Integer.parseInt(txtCodigo.getText());
    }

    public void setTxtCodigo(String txtCodigo) {
        this.txtCodigo.setText(txtCodigo);
    }

    public int getTxtNroCompra() {
        return Integer.parseInt(txtNroCompra.getText());
    }

    public void setTxtNroCompra(String txtNroCompra) {
        this.txtNroCompra.setText(txtNroCompra);
    }

    public JComboBox<String> getComboProvedores() {
        return comboProvedores;
    }

    public void setComboProvedores(JComboBox<String> comboProvedores) {
        this.comboProvedores = comboProvedores;
    }

    public java.sql.Date getJdcFechaDeCompra() {

        Date date = jdcFechaDeCompra.getDate();

        long d = date.getTime();

        java.sql.Date fechaDeCompra = new java.sql.Date(d);

        return fechaDeCompra;
    }
}
