package vistas;

import controladores.ControlFrameLiquidacion;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import javax.swing.JTable;
import utilidadesParaFrames.UtilFrameLiquidacion;

public class FrameLiquidacion extends javax.swing.JDialog {

    public static final String BTN_ACEPTAR = "aceptar";
    public static final String BTN_CARGAR_HORAS_EXTRA = "cargarHorasExtra";
    public static final String BTN_CANCELAR = "cancelar";

    public FrameLiquidacion(java.awt.Frame parent, boolean modal) throws ParseException {
        super(parent, modal);
        initComponents();
        radioMensual.setEnabled(true);
        btnAceptar.setActionCommand(BTN_ACEPTAR);
        btnCargar.setActionCommand(BTN_CARGAR_HORAS_EXTRA);
        btnCancelar.setActionCommand(BTN_CANCELAR);
        this.setLocationRelativeTo(null);
        radioMensual.setSelected(true);
        radioEspecial.setEnabled(false);
//        txtEspecial.setEnabled(false);
        setCombos();
        setJdcFechaDePago();

        tablaLegajos.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 1) {
                    int fila = tablaLegajos.getSelectedRow();
                    int legajo = (int) tablaLegajos.getModel().getValueAt(fila, 1);
                    try {
                        if ((comboMes.getSelectedIndex() == 11
                                || comboMes.getSelectedIndex() == 5) && radioEspecial.isSelected()) {
                            UtilFrameLiquidacion.mostrarLosConceptosDelSACDelEmpleadoEnTabla(tablaConceptos, legajo, getComboAño(), getComboMes());
                        } else {
                            UtilFrameLiquidacion.mostrarLosConceptosDelEmpleadoEnTabla(tablaConceptos, legajo);
                        }

                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    } catch (ParseException ex) {
                        Logger.getLogger(FrameLiquidacion.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        );
        radioMensual.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (radioMensual.isSelected()) {
                    radioEspecial.setEnabled(false);
//                    txtEspecial.setEnabled(false);
                } else if (radioMensual.isSelected() == false) {
                    radioEspecial.setEnabled(true);
//                    txtEspecial.setEnabled(false);
                }
            }//fin del mouseClicked
        }//din del addMouseListiner
        );
        radioEspecial.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (radioEspecial.isSelected()) {
                    radioMensual.setEnabled(false);
//                    txtEspecial.setEnabled(true);
                } else if (radioEspecial.isSelected() == false) {
                    radioMensual.setEnabled(true);
//                    txtEspecial.setEnabled(false);
                }
            }//fin del mouseClicked
        }//din del addMouseListiner
        );
    }

    public void ejecutar() {
        this.setVisible(true);
    }

    public void setControlador(ControlFrameLiquidacion control) {
        btnAceptar.addActionListener(control);
        btnCargar.addActionListener(control);
        btnCancelar.addActionListener(control);
        comboPuestos.addItemListener(control);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        radioMensual = new javax.swing.JRadioButton();
        radioEspecial = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNroLiq = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        comboAño = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        comboMes = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jdcFechaDePago = new com.toedter.calendar.JDateChooser();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaLegajos = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaConceptos = new javax.swing.JTable();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        comboPuestos = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtBasico = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtHrs50 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtHrs100 = new javax.swing.JTextField();
        btnCargar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("NUEVA LIQUIDACIÓN ");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel1.setText("Nueva liquidación");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de liquidación"));

        radioMensual.setText("Mensual");

        radioEspecial.setText("Especial");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(radioMensual)
                .addGap(18, 18, 18)
                .addComponent(radioEspecial)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioMensual)
                    .addComponent(radioEspecial))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));

        jLabel2.setText("Liq. nro.:");

        txtNroLiq.setEditable(false);
        txtNroLiq.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtNroLiq.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel3.setText("Año:");

        comboAño.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", " ", " ", " ", " " }));

        jLabel4.setText("Mes:");

        comboMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));

        jLabel5.setText("Fecha de pago:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(txtNroLiq, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(comboAño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(comboMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jdcFechaDePago, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(txtNroLiq, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)
                        .addComponent(comboAño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)
                        .addComponent(comboMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jdcFechaDePago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 19, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Legajos"));

        tablaLegajos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "", "Legajo", "Ape. y nombre", "C.U.I.L."
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tablaLegajos);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 788, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(223, 223, 223))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Conceptos a liquidar"));

        tablaConceptos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Descripcion", "Total Hs. Ds.", "Haberes", "Deducciones", "Haberes S/Descuento"
            }
        ));
        jScrollPane3.setViewportView(tablaConceptos);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 949, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/calculator.png"))); // NOI18N
        btnAceptar.setText("Calcular");

        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar.setText("Cancelar");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Puesto/Categoría"));

        comboPuestos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel6.setText("Basico:");

        txtBasico.setEditable(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboPuestos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(txtBasico, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 90, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(comboPuestos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtBasico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(44, Short.MAX_VALUE))
        );

        jLabel7.setText("Hrs. 50%");

        txtHrs50.setText("0");
        txtHrs50.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtHrs50KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHrs50KeyReleased(evt);
            }
        });

        jLabel8.setText("Hrs. 100%");

        txtHrs100.setText("0");
        txtHrs100.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtHrs100KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHrs100KeyReleased(evt);
            }
        });

        btnCargar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/table.png"))); // NOI18N
        btnCargar.setText("Cargar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel7)
                                    .addComponent(txtHrs100)
                                    .addComponent(txtHrs50)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnAceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnCargar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 22, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 239, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtHrs50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtHrs100, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCargar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAceptar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancelar))
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtHrs50KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHrs50KeyPressed

    }//GEN-LAST:event_txtHrs50KeyPressed

    private void txtHrs100KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHrs100KeyPressed

    }//GEN-LAST:event_txtHrs100KeyPressed

    private void txtHrs50KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHrs50KeyReleased
        if (!txtHrs50.getText().matches("[0-9--]*")) {
            txtHrs50.setText("");
            txtHrs50.requestFocus();
        }
    }//GEN-LAST:event_txtHrs50KeyReleased

    private void txtHrs100KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHrs100KeyReleased
        if (!txtHrs100.getText().matches("[0-9--]*")) {
            txtHrs100.setText("");
            txtHrs100.requestFocus();
        }
    }//GEN-LAST:event_txtHrs100KeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCargar;
    private javax.swing.JComboBox<String> comboAño;
    private javax.swing.JComboBox<String> comboMes;
    private javax.swing.JComboBox<String> comboPuestos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private com.toedter.calendar.JDateChooser jdcFechaDePago;
    private javax.swing.JRadioButton radioEspecial;
    private javax.swing.JRadioButton radioMensual;
    private javax.swing.JTable tablaConceptos;
    private javax.swing.JTable tablaLegajos;
    private javax.swing.JTextField txtBasico;
    private javax.swing.JTextField txtHrs100;
    private javax.swing.JTextField txtHrs50;
    private javax.swing.JTextField txtNroLiq;
    // End of variables declaration//GEN-END:variables

    public java.sql.Date getJdcFechaDePago() {

        Date date = jdcFechaDePago.getDate();

        long d = date.getTime();

        java.sql.Date fechaDePago = new java.sql.Date(d);

        return fechaDePago;
    }

    public void setJdcFechaDePago() throws ParseException {

        Calendar hoy = Calendar.getInstance();

        int dia = hoy.get(Calendar.DAY_OF_MONTH);
        int mes = hoy.get(Calendar.MONTH) + 1;
        int año = hoy.get(Calendar.YEAR);
        String fechaSQL = año + "-" + mes + "-" + dia;

        SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaTxt;
        fechaTxt = formatoFecha.parse(fechaSQL);
        jdcFechaDePago.setDate(fechaTxt);

    }

    public JTable getTablaConceptos() {
        return tablaConceptos;
    }

    public void setTablaConceptos(JTable tablaConceptos) {
        this.tablaConceptos = tablaConceptos;
    }

    public JTable getTablaLegajos() {
        return tablaLegajos;
    }

    public void setTablaLegajos(JTable tablaLegajos) {
        this.tablaLegajos = tablaLegajos;
    }

    public int getTxtNroLiq() {
        return Integer.parseInt(txtNroLiq.getText());
    }

    public void setTxtNroLiq(String txtNroLiq) {
        this.txtNroLiq.setText(txtNroLiq);
    }

    public int getComboAño() {
        return Integer.parseInt("" + comboAño.getSelectedItem());
    }

    public void setComboAño(JComboBox<String> comboAño) {
        this.comboAño = comboAño;
    }

    public String getComboMes() {
        return comboMes.getSelectedItem() + "";
    }

    public void setComboMes(JComboBox<String> comboMes) {
        this.comboMes = comboMes;
    }

    public void setCombos() {
        DefaultComboBoxModel modeloComboMes = new DefaultComboBoxModel();
        DefaultComboBoxModel modeloComboAño = new DefaultComboBoxModel();
        comboMes.setModel(modeloComboMes);
        comboAño.setModel(modeloComboAño);

        modeloComboMes.addElement("Enero");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Febrero");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Marzo");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Abril");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Mayo");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Junio");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Julio");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Agosto");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Septiembre");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Octubre");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Noviembre");
        comboMes.setModel(modeloComboMes);
        modeloComboMes.addElement("Diciembre");
        comboMes.setModel(modeloComboMes);

        for (int año = 2017; año <= 2030; año++) {
            modeloComboAño.addElement("" + año);
            comboAño.setModel(modeloComboAño);
        }

        Calendar hoy = Calendar.getInstance();
        int año = hoy.get(Calendar.YEAR);
        int mes = hoy.get(Calendar.MONTH);
        comboMes.setSelectedIndex(mes);
        int indexAño = 0;
        for (int i = 0; i < comboAño.getItemCount(); i++) {
            if (Integer.parseInt(comboAño.getItemAt(i)) == año) {
                indexAño = i;
            }
        }
        comboAño.setSelectedIndex(indexAño);
    }

    public int getTxtHrs100() {
        if (txtHrs100.getText().equals("")) {
            return 0;
        } else {
            return Integer.parseInt(txtHrs100.getText());
        }
    }

    public void setTxtHrs100(String txtHrs100) {
        this.txtHrs100.setText(txtHrs100);
    }

    public int getTxtHrs50() {
        if (txtHrs50.getText().equals("")) {
            return 0;
        } else {
            return Integer.parseInt(txtHrs50.getText());
        }
    }

    public void setTxtHrs50(String txtHrs50) {
        this.txtHrs50.setText(txtHrs50);
    }

    public JComboBox<String> getComboPuestos() {
        return comboPuestos;
    }

    public String getPuestoSeleccionado() {
        return String.valueOf(comboPuestos.getSelectedItem());
    }

    public void setComboPuestos(JComboBox<String> comboPuestos) {
        this.comboPuestos = comboPuestos;
    }

    public float getTxtBasico() {
        return Float.parseFloat(txtBasico.getText());
    }

    public void setTxtBasico(String txtBasico) {
        this.txtBasico.setText(txtBasico);
    }

    public void limpiar() {
        setTxtHrs50("0");
        setTxtHrs100("0");
    }

}
