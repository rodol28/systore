package vistas;

import controladores.ControlFrameConsultarVentas;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.JTextField;
import utilidadesParaFrames.UtilFrameConsultarVentas;

public class FrameConsultarVentas extends javax.swing.JDialog {

    public static final String BTN_BUSCAR_NRO = "buscarPorNro";
    public static final String BTN_SALIR = "salir";

    public FrameConsultarVentas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        btnBuscarNroFac.setActionCommand(BTN_BUSCAR_NRO);
        btnSalir.setActionCommand(BTN_SALIR);

        tablaVentas.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 1) {

                    int fila = tablaVentas.getSelectedRow();
                    int idVenta = Integer.parseInt((String) tablaVentas.getModel().getValueAt(fila, 0));
                    txtNroFactura.setText("" + idVenta);
                    try {
                        UtilFrameConsultarVentas.mostrarFecha(idVenta, txtFechaVenta);
                        UtilFrameConsultarVentas.mostrarVendedor(idVenta, txtVendedor);
                        UtilFrameConsultarVentas.mostrarLineasDeVenta(txtFechaVenta, tablaLineasDeVenta, idVenta);
                    } catch (SQLException ex) {
                    } catch (ClassNotFoundException ex) {
                    }
                }
            }
        }
        );
        this.setLocationRelativeTo(null);
    }

    public void ejecutar() {
        this.setVisible(true);
    }

    public void setControlador(ControlFrameConsultarVentas control) {
        btnBuscarNroFac.addActionListener(control);
        btnSalir.addActionListener(control);
    }

    private void closeDialog(java.awt.event.WindowEvent evt) {
        setVisible(false);
        dispose();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tablaVentas = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtNroFactura = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtVendedor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtFechaVenta = new javax.swing.JTextField();
        btnSalir = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaLineasDeVenta = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        btnBuscarNroFac = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("CONSULTAR VENTAS");

        tablaVentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nro. Factura", "Fecha", "Total"
            }
        ));
        jScrollPane1.setViewportView(tablaVentas);

        jLabel1.setText("Factura");

        txtNroFactura.setEditable(false);
        txtNroFactura.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtNroFactura.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel2.setText("Vendedor");

        txtVendedor.setEditable(false);
        txtVendedor.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtVendedor.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel3.setText("Fecha");

        txtFechaVenta.setEditable(false);
        txtFechaVenta.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/door_out.png"))); // NOI18N
        btnSalir.setText("Salir");

        tablaLineasDeVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cantidad", "Descripción", "Precio", "Subtotal"
            }
        ));
        jScrollPane2.setViewportView(tablaLineasDeVenta);

        jMenu1.setText("Búsqueda");

        btnBuscarNroFac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        btnBuscarNroFac.setText("nro de Factura");
        jMenu1.add(btnBuscarNroFac);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(txtNroFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(txtFechaVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(btnSalir, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNroFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(txtFechaVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem btnBuscarNroFac;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tablaLineasDeVenta;
    private javax.swing.JTable tablaVentas;
    private javax.swing.JTextField txtFechaVenta;
    private javax.swing.JTextField txtNroFactura;
    private javax.swing.JTextField txtVendedor;
    // End of variables declaration//GEN-END:variables

    public JTable getTablaLineasDeVenta() {
        return tablaLineasDeVenta;
    }

    public JTable getTablaVentas() {
        return tablaVentas;
    }

    public String getTxtFechaVenta() {
        return txtFechaVenta.getText();
    }
    
    public JTextField getJTextFieldFechaVenta() {
        return txtFechaVenta;
    }

    public void setTxtFechaVenta(String txtFechaVenta) {
        this.txtFechaVenta.setText(txtFechaVenta);
    }

    public int getTxtNroFactura() {
        return Integer.parseInt(txtNroFactura.getText());
    }
    
    public JTextField getJTextFieldNroFactura() {
        return txtNroFactura;
    }

    public void setTxtNroFactura(String txtNroFactura) {
        this.txtNroFactura.setText(txtNroFactura);
    }

    public int getTxtVendedor() {
        return Integer.parseInt(txtVendedor.getText());
    }
    
    public JTextField getJTextFieldVendedor() {
        return txtVendedor;
    }

    public void setTxtVendedor(String txtVendedor) {
        this.txtVendedor.setText(txtVendedor);
    }
}
